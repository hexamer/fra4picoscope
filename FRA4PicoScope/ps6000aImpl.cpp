//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014-2020 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ps6000aImpl.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "utility.h"
#include "ps6000aApi.h"
#include "ps6000aApiExperimental.h"
#include "picoStatus.h"
#include "ps6000aImpl.h"
#include "StatusLog.h"

#define PS6000A_IMPL
#include "psCommonImpl.cpp"

const std::vector<std::wstring> ps6000aImpl::baseAvailableCouplings = { TEXT("AC"), TEXT("DC 1M"), TEXT("DC 50R") };
const std::vector<std::wstring> ps6000aImpl::restrictedAvailableCouplings = { TEXT("AC"), TEXT("DC 1M") };

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps6000aImpl::GetTimebase
//
// Purpose: Get a timebase from a desired frequency, rounding such that the frequency is at least
//          as high as requested, if possible.
//
// Parameters: [in] desiredFrequency: caller's requested frequency in Hz
//             [in] resolution: the vertical resolution to consider; can use RESOLUTION_CURRENT
//             [out] actualFrequency: the frequency corresponding to the returned timebase.
//             [out] timebase: the timebase that will achieve the requested freqency or greater
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps6000aImpl::GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase )
{
    bool retVal = false;
    wstringstream fraStatusText;
    PICO_CHANNEL_FLAGS channelFlags;
    double timeIntervalAvailable;
    uint32_t _minTimebase;
    PICO_STATUS status;

    channelFlags = (PICO_CHANNEL_FLAGS)((1<<mInputChannel) | (1<<mOutputChannel));

    if (RESOLUTION_CURRENT == resolution)
    {
        resolution = (RESOLUTION_T)currentResolution;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aGetMinimumTimebaseStateless), handle, channelFlags, &_minTimebase, &timeIntervalAvailable, (PICO_DEVICE_RESOLUTION)resolution );
    if (PICO_OK == (status = ps6000aGetMinimumTimebaseStateless( handle, channelFlags, &_minTimebase, &timeIntervalAvailable, (PICO_DEVICE_RESOLUTION)resolution )))
    {
        LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aNearestSampleIntervalStateless), handle, channelFlags, 1.0 / desiredFrequency, (PICO_DEVICE_RESOLUTION)resolution, timebase, &timeIntervalAvailable );
        if (PICO_OK == (status = ps6000aNearestSampleIntervalStateless( handle, channelFlags, 1.0 / desiredFrequency, (PICO_DEVICE_RESOLUTION)resolution, timebase, &timeIntervalAvailable )))
        {
            retVal = true;
        }
        else
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Unable to determine nearest timebase in GetTimebase: " << status;
            LogMessage( fraStatusText.str() );
        }
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Unable to determine minimum timebase in GetTimebase: " << status;
        LogMessage( fraStatusText.str() );
    }

    // Ensure the sampling frequency is greater than or equal to requested if possible.
    if (retVal && desiredFrequency > 1.0 / timeIntervalAvailable && *timebase > _minTimebase)
    {
        (*timebase)--;
    }

    if (retVal)
    {
        retVal = GetFrequencyFromTimebase( *timebase, *actualFrequency );
    }

    return retVal;
}

bool ps6000aImpl::GetFrequencyFromTimebase( uint32_t timebase, double &frequency )
{
    bool retVal = false;

    if (timebase >= minTimebase && timebase <= maxTimebase)
    {
        if (timebase <= 4)
        {
            frequency = 5.0e9 / (double)(1<<(timebase));
        }
        else
        {
            frequency = 156250000.0 / ((double)(timebase - 4)); // ps6000pg.en r9 p19
        }
        retVal = true;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps6000aImpl::AdjustResolutionForChannels
//
// Purpose: Adjust a resolution parameter based on channels set
//
// Parameters: [in] resolution - the resolution to be adjusted
//             [out] return - the adjusted resolution
//
// Notes: The PS6000E FlexRes scopes do not support 12 bits when more than one channel in a channel
//        block is being used.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

RESOLUTION_T ps6000aImpl::AdjustResolutionForChannels(RESOLUTION_T resolution)
{
    if (RESOLUTION_12BIT == resolution && !ChannelDesignationsAreOptimal())
    {
        resolution = RESOLUTION_10BIT;
    }
    return resolution;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps6000aImpl::InitializeScope
//
// Purpose: Initialize scope/family-specific implementation details.
//
// Parameters: N/A
//
// Notes: Assumes scope was opened with max resolution
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps6000aImpl::InitializeScope(void)
{
    bool retVal = false;
    PS_RANGE minRange, maxRange;

    // Encode for the best case, and we'll adjust in the GetNoiseRejectMode[SampleRate|Timebase]
    // functions when both channels are from same block.
    minTimebase = (model == PS6403E) ? 1 : 0;
    maxTimebase = (std::numeric_limits<uint32_t>::max)();
    // A bandwidth limiter is available on these scope and will be used.
    timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 6;

    // Map is in decending order of min timebase
    flexResMinTbMap = {{PICO_DR_12BIT, 2},
                       {PICO_DR_10BIT, 1},
                       {PICO_DR_8BIT, 0}};

    currentResolution = maxResolution = (model == PS6424E || model == PS6824E ||
                                         model == PS6425E || model == PS6426E) ? PICO_DR_12BIT : PICO_DR_8BIT;

    // PS6000E scopes have 40 kS AWG buffer.  Since this isn�t a power of two it doesn�t fill the 64k DDS address space.
    // Thus, we lose a factor of 1.6 precision (64/40).  Refer to SUPPORT-30775
    signalGeneratorPrecision = (64.0 / 40.0) * 200.0e6 / (double)UINT32_MAX;

    minRange = (PS_RANGE)PICO_X1_PROBE_10MV;
    maxRange = (PS_RANGE)PICO_X1_PROBE_20V;

    channelCurrentAvailableCouplings.resize(PS_MAX_CHANNELS);

    for (uint8_t chan = PS_CHANNEL_A; chan < numAvailableChannels; chan++)
    {
        channelNonSmartMinRanges[chan] = channelMinRanges[chan] = minRange;
        channelNonSmartMaxRanges[chan] = channelMaxRanges[chan] = maxRange;
        channelCurrentAvailableCouplings[chan] = baseAvailableCouplings;
    }

    if (retVal = AllocateBuffers())
    {
        retVal = InitProbeEventHandling();
    }

    return retVal;
}