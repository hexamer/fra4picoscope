//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014-2020 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ps6000aImpl.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PicoScopeInterface.h"
#include "PicoDeviceStructs.h"

#define PS6000A_IMPL

class ps6000aImpl : public PicoScope
{
    public:
        ps6000aImpl( int16_t _handle, PICO_STATUS _initialPowerState );
        ~ps6000aImpl();
    private:
        static const std::vector<std::wstring> baseAvailableCouplings;
        static const std::vector<std::wstring> restrictedAvailableCouplings;
        std::vector<std::vector<std::wstring>> channelCurrentAvailableCouplings;
        RESOLUTION_T AdjustResolutionForChannels( RESOLUTION_T resolution );

#define NEW_PS_DRIVER_MODEL
#define IMPLEMENTS_SMART_PROBES
#include "psCommonImpl.h"

};