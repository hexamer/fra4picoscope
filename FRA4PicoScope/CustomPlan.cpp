//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014-2019 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: CustomPlan.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <Windowsx.h>
#include <CommCtrl.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <limits>
#include "CustomPlan.h"
#include "PicoScopeFraApp.h"
#include "utility.h"
#include "Resource.h"

uint8_t numRows = 0;
uint8_t highWaterMark = 0;
RECT rowSpacing = {0};
RECT basePlusButtonPosition = {0};
RECT baseMinusButtonPosition = {0};
RECT baseOkButtonPosition = {0};
RECT baseCancelButtonPosition = {0};

int prevx = 1;
int prevy = 1;
void SD_OnHVScroll(HWND hwnd, int bar, UINT code);
void SD_ScrollClient(HWND hwnd, int bar, int pos);
int SD_GetScrollPos(HWND hwnd, int bar, UINT code);

std::vector<HFONT> fontHandles;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: CustomPlanDialog_Cleanup
//
// Purpose: Releases resources used by the Custom Plan Dialog
//
// Parameters: None
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void CustomPlanDialog_Cleanup(void)
{
    for (const auto& hFont : fontHandles)
    {
        if (hFont)
        {
            DeleteObject(hFont);
        }
    }
    fontHandles.clear();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ChangeHeaderTextFont
//
// Purpose: Bolds and underlines a static text field used for a column header
//
// Parameters: [in] hCtrl - handle to the static text field control
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void ChangeHeaderTextFont( HWND hCtrl )
{
    HFONT hNewFont;
    HFONT hOldFont = (HFONT)SendMessage( hCtrl, WM_GETFONT, 0, 0 );

    LOGFONT lf;
    GetObject(hOldFont, sizeof(LOGFONT), &lf );

    lf.lfUnderline = TRUE;
    lf.lfWeight |= FW_BOLD;

    hNewFont = CreateFontIndirect(&lf);

    if (hNewFont)
    {
        // Store these for later deletion so we're not leaking objects
        fontHandles.push_back(hNewFont);
        SendMessage( hCtrl, WM_SETFONT, (WPARAM)hNewFont, 0 );
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetupCustomPlanFields
//
// Purpose: Manages displaying/hiding enabling/disabling fields on the Custom Plan dialog based
//          on the number of plan rows defined
//
// Parameters: [in] hDlg - handle to the Custom Plan dialog window
//             [out] return - amount to scroll after row updates were made
//
// Notes: Also helps manage scrolling so that newest fields are in-view by computing and returning
//        a scrolling paramter.  Also copies some field values down as new rows are created.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

int SetupCustomPlanFields(HWND hDlg)
{
    int retVal = 0;
    HWND hndCtrl;
    int32_t ctrlIdBase;

    // Get button position baselines from dummy controls
    hndCtrl = GetDlgItem(hDlg, IDC_CUSTOM_PLAN_PLUS_BUTTON_DUMMY);
    GetWindowRect(hndCtrl, &basePlusButtonPosition);
    ScreenToClient(hDlg, (LPPOINT)&(basePlusButtonPosition.left));
    ScreenToClient(hDlg, (LPPOINT)&(basePlusButtonPosition.right));
    hndCtrl = GetDlgItem(hDlg, IDC_CUSTOM_PLAN_MINUS_BUTTON_DUMMY);
    GetWindowRect(hndCtrl, &baseMinusButtonPosition);
    ScreenToClient(hDlg, (LPPOINT)&(baseMinusButtonPosition.left));
    ScreenToClient(hDlg, (LPPOINT)&(baseMinusButtonPosition.right));
    hndCtrl = GetDlgItem(hDlg, IDOK_DUMMY);
    GetWindowRect(hndCtrl, &baseOkButtonPosition);
    ScreenToClient(hDlg, (LPPOINT)&(baseOkButtonPosition.left));
    ScreenToClient(hDlg, (LPPOINT)&(baseOkButtonPosition.right));
    hndCtrl = GetDlgItem(hDlg, IDCANCEL_DUMMY);
    GetWindowRect(hndCtrl, &baseCancelButtonPosition);
    ScreenToClient(hDlg, (LPPOINT)&(baseCancelButtonPosition.left));
    ScreenToClient(hDlg, (LPPOINT)&(baseCancelButtonPosition.right));

    // Set the Plus and Minus button positions
    hndCtrl = GetDlgItem(hDlg, IDC_CUSTOM_PLAN_PLUS_BUTTON);
    SetWindowPos(hndCtrl, NULL, basePlusButtonPosition.left, basePlusButtonPosition.top+(numRows-1)*rowSpacing.bottom, 0, 0, SWP_NOSIZE);
    hndCtrl = GetDlgItem(hDlg, IDC_CUSTOM_PLAN_MINUS_BUTTON);
    SetWindowPos(hndCtrl, NULL, baseMinusButtonPosition.left, baseMinusButtonPosition.top+(numRows-1)*rowSpacing.bottom, 0, 0, SWP_NOSIZE);

    // Set the Ok/Cancel button positions
    hndCtrl = GetDlgItem(hDlg, IDOK);
    SetWindowPos(hndCtrl, NULL, baseOkButtonPosition.left, baseOkButtonPosition.top+(numRows-1)*rowSpacing.bottom, 0, 0, SWP_NOSIZE);
    hndCtrl = GetDlgItem(hDlg, IDCANCEL);
    SetWindowPos(hndCtrl, NULL, baseCancelButtonPosition.left, baseCancelButtonPosition.top+(numRows-1)*rowSpacing.bottom, 0, 0, SWP_NOSIZE);

    RECT dlgClientRect;
    GetClientRect(hDlg, &dlgClientRect);

    // Check to see if fields/buttons have gone outside the window client area and compute a scrolling distance to get them back in view
    // Check the OK/Cancel buttons on add
    if (baseOkButtonPosition.bottom + (numRows - 1) * rowSpacing.bottom > (dlgClientRect.bottom - dlgClientRect.top))
    {
        retVal = (baseOkButtonPosition.bottom + (numRows - 1) * rowSpacing.bottom) - 
                 (dlgClientRect.bottom - dlgClientRect.top) + (rowSpacing.bottom / 4); // Go another 10 DLUs
    }
    // Check the whole segment on subtract, but if only one row left, scroll to top
    if (1 == numRows)
    {
        retVal = MINLONG;
    }
    else if (baseMinusButtonPosition.top + (numRows-2) * rowSpacing.bottom < dlgClientRect.top)
    {
        retVal = (baseMinusButtonPosition.top + (numRows-2) * rowSpacing.bottom) - 
                 dlgClientRect.top - (rowSpacing.bottom / 4); // Go another 10 DLUs
    }

    // Show/hide fields
    ctrlIdBase = IDC_CP_EDIT_FREQUENCY_1;
    for (uint8_t i = 0; i < numRows+1; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_SHOW);
    }
    for (uint8_t i = numRows+1; i < 11; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    ctrlIdBase = IDC_CP_EDIT_STEPS_1;
    for (uint8_t i = 0; i < numRows; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_SHOW);
    }
    for (uint8_t i = numRows; i < 10; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    ctrlIdBase = IDC_CP_SEGMENT_1;
    for (uint8_t i = 0; i < numRows; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_SHOW);
    }
    for (uint8_t i = numRows; i < 10; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    ctrlIdBase = IDC_CP_EDIT_STIMULUS_VPP_1;
    for (uint8_t i = 0; i < numRows; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_SHOW);
    }
    for (uint8_t i = numRows; i < 10; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    ctrlIdBase = IDC_CP_EDIT_STIMULUS_OFFSET_V_1;
    for (uint8_t i = 0; i < numRows; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_SHOW);
        if (psFRA->SigGenSupportsDcOffset())
        {
            EnableWindow(hndCtrl, TRUE);
        }
        else
        {
            EnableWindow(hndCtrl, FALSE);
        }
    }
    for (uint8_t i = numRows; i < 10; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    ctrlIdBase = IDC_BRACKET_1;
    for (uint8_t i = 0; i < numRows; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_SHOW);
    }
    for (uint8_t i = numRows; i < 10; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    ctrlIdBase = IDC_CONNECTOR_11;
    for (uint8_t i = 0; i < numRows; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_SHOW);
    }
    for (uint8_t i = numRows; i < 10; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    ctrlIdBase = IDC_CONNECTOR_12;
    for (uint8_t i = 0; i < numRows; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        if (psFRA->SigGenSupportsDcOffset())
        {
            ShowWindow(hndCtrl, SW_SHOW);
        }
        else
        {
            ShowWindow(hndCtrl, SW_HIDE);
        }
    }
    for (uint8_t i = numRows; i < 10; i++)
    {
        hndCtrl = GetDlgItem(hDlg, ctrlIdBase+i);
        ShowWindow(hndCtrl, SW_HIDE);
    }

    // If we're at a new high water mark, copy fields down
    if (numRows > highWaterMark)
    {
        TCHAR editText[64];
        highWaterMark = numRows;
        hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STEPS_1+numRows-2);
        Edit_GetText( hndCtrl, editText, 16 );
        hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STEPS_1+numRows-1);
        Edit_SetText( hndCtrl, editText );
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PopulateCustomPlanFields
//
// Purpose: Populates Custom Plan dialog fields from data stored in the application settings.
//
// Parameters: [in] hDlg - handle to the Custom Plan dialog window
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PopulateCustomPlanFields(HWND hDlg)
{
    HWND hndCtrl;
    StepType_T stepType;
    std::vector<FRASegmentStrings_T> fraSegments;
    pSettings->GetCustomPlanAsString(fraSegments, stepType);

    // Until octave and linear are available, select decade and disable
    hndCtrl = GetDlgItem(hDlg, IDC_DECADE_RADIO);
    Button_SetCheck(hndCtrl, BST_CHECKED);
    EnableWindow(hndCtrl, FALSE);

    hndCtrl = GetDlgItem(hDlg, IDC_OCTAVE_RADIO);
    EnableWindow(hndCtrl, FALSE);

    hndCtrl = GetDlgItem(hDlg, IDC_LINEAR_RADIO);
    EnableWindow(hndCtrl, FALSE);

    // Populate data
    if (fraSegments.size())
    {
        for (uint8_t i = 0; i < numRows; i++)
        {
            hndCtrl = GetDlgItem(hDlg, IDC_CP_EDIT_FREQUENCY_1+i);
            Edit_SetText( hndCtrl, fraSegments[i].startFreqHz.c_str() );

            hndCtrl = GetDlgItem(hDlg, IDC_CP_EDIT_FREQUENCY_1+i+1);
            Edit_SetText( hndCtrl, fraSegments[i].stopFreqHz.c_str() );

            hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STEPS_1+i);
            Edit_SetText( hndCtrl, fraSegments[i].steps.c_str() );

            hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STIMULUS_VPP_1+i );
            Edit_SetText( hndCtrl, fraSegments[i].stimulusVpp.c_str() );

            hndCtrl = GetDlgItem(hDlg, IDC_CP_EDIT_STIMULUS_OFFSET_V_1+i);
            if (psFRA->SigGenSupportsDcOffset())
            {
                Edit_SetText(hndCtrl, fraSegments[i].stimulusOffset.c_str());
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: CustomPlanDialog_ValidateAndStoreSettings
//
// Purpose: Checks settings for validity and stores them if they are valid
//
// Parameters: [in] hDlg: handle to the Custom Plan dialog window
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CustomPlanDialog_ValidateAndStoreSettings( HWND hDlg )
{
    auto retVal = true;

    HWND hndCtrl;
    wstring errorConditions[64];
    uint8_t numErrors = 0;
    TCHAR editText[64];
    wstringstream wss;
    double stimulusVpp;
    double stimulusOffset;
    double startFreq;
    double stopFreq;
    std::wstring strStartFreq;
    std::wstring strStopFreq;
    std::wstring strSteps;
    std::wstring strStimulusVpp;
    std::wstring strStimulusOffset;
    tuple <bool, double, double> freqScale;
    int32_t steps;
    bool validStartFreq = false;
    bool validStopFreq = false;
    bool validStimulusVpp = false;
    bool validStimulusOffset = false;
    FRASegmentStrings_T fraSegment;
    std::vector<FRASegmentStrings_T> fraSegments;

    try
    {
        for (uint8_t i = 0; i < numRows; i++)
        {
            validStartFreq = false;
            validStopFreq = false;
            validStimulusVpp = false;
            validStimulusOffset = false;

            hndCtrl = GetDlgItem(hDlg, IDC_CP_EDIT_FREQUENCY_1+i);
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToDouble(editText, startFreq))
            {
                wstringstream wss;
                wss << L"Segment " << i+1 << L" start frequency is not a valid number.";
                errorConditions[numErrors++] = wss.str();
                retVal = false;
            }
            else
            {
                double minFreq = psFRA->GetMinFrequency( pSettings->GetDeviceResolution() );
                if (startFreq < minFreq)
                {
                    wstringstream wss;
                    wss << L"Segment " << i+1 << L" start frequency must be >= " << minFreq << L" Hz";
                    errorConditions[numErrors++] = wss.str();
                    retVal = false;
                }
                else
                {
                    validStartFreq = true;
                    strStartFreq = editText;
                }
            }

            hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_FREQUENCY_1+i+1 );
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToDouble(editText, stopFreq))
            {
                wstringstream wss;
                wss << L"Segment " << i+1 << L" stop frequency is not a valid number.";
                errorConditions[numErrors++] = wss.str();
                retVal = false;
            }
            else
            {
                double maxFreq = psFRA->GetMaxFrequency();
                if (stopFreq > maxFreq)
                {
                    wstringstream wss;
                    wss << L"Segment " << i+1 << L" stop frequency must be <= " << fixed << setprecision(1) << maxFreq;
                    errorConditions[numErrors++] = wss.str();
                    retVal = false;
                }
                else
                {
                    validStopFreq = true;
                    strStopFreq = editText;
                }
            }

            if (validStartFreq && validStopFreq && startFreq >= stopFreq)
            {
                wstringstream wss;
                wss << L"Segment " << i+1 << L" stop frequency must be > start frequency";
                errorConditions[numErrors++] = wss.str();
                validStartFreq = validStopFreq = false;
                retVal = false;
            }

            hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STEPS_1+i);
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToInt32(editText, steps))
            {
                wstringstream wss;
                wss << L"Segment " << i+1 << L" steps is not a valid number.";
                errorConditions[numErrors++] = wss.str();
                retVal = false;
            }
            else if (steps < 1)
            {
                wstringstream wss;
                wss << L"Segment " << i+1 << L" steps must be >= 1.";
                errorConditions[numErrors++] = wss.str();
                retVal = false;
            }
            else
            {
                strSteps = editText;
            }

            hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STIMULUS_VPP_1+i );
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToDouble(editText, stimulusVpp))
            {
                wstringstream wss;
                wss << L"Segment " << i+1 << L" stimulus amplitude is not a valid number.";
                errorConditions[numErrors++] = wss.str();
                retVal = false;
            }
            else if (stimulusVpp <= 0.0)
            {
                wstringstream wss;
                wss << L"Segment " << i+1 << L" stimulus amplitude must be > 0.0";
                errorConditions[numErrors++] = wss.str();
                retVal = false;
            }
            else
            {
                validStimulusVpp = true;
                strStimulusVpp = editText;
            }

            if (psFRA->SigGenSupportsDcOffset())
            {
                hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STIMULUS_OFFSET_V_1+i );
                Edit_GetText( hndCtrl, editText, 16 );
                if (!WStringToDouble(editText, stimulusOffset))
                {
                    wstringstream wss;
                    wss << L"Segment " << i+1 << L" stimulus offset is not a valid number.";
                    errorConditions[numErrors++] = wss.str();
                    retVal = false;
                }
                else
                {
                    validStimulusOffset = true;
                    strStimulusOffset = editText;
                }
            }
            else
            {
                stimulusOffset = 0.0;
                strStimulusOffset = L"0";
                validStimulusOffset = true;
            }

            if (validStimulusVpp && validStimulusOffset && validStartFreq && validStopFreq)
            {
                // This logic assumes that the maximum sine amplitude is also the maximum range for the signal generator.
                // This is definitely true for PicoScopes, and for many other standalone signal generators, but it's possible
                // that an external signal generator someome wants to add support for in the future may have independently
                // adjustable amplitude and offset.  If that happens, the external signal generator API will need to change
                // to publish whether this is the case and this logic will need to adapt accordingly.

                double maxSignalAbs = psFRA->GetMaxFuncGenVpp(startFreq, stopFreq) / 2.0;
                if ((fabs(stimulusOffset) + stimulusVpp / 2.0) > maxSignalAbs)
                {
                    wstringstream wss;
                    wss << L"Segment " << i+1 << L" stimulus exceeds signal generator limit of +/-" << fixed << setprecision(1) << maxSignalAbs << L" V";
                    errorConditions[numErrors++] = wss.str();
                    retVal = false;
                }

                if (stimulusVpp < psFRA->GetMinFuncGenVpp(startFreq, stopFreq))
                {
                    double minSignalAbs = psFRA->GetMinFuncGenVpp(startFreq, stopFreq) / 2.0;
                    wstringstream wss;
                    wss << L"Segment " << i+1 << L" stimulus smaller than signal generator limit of +/-" << fixed << setprecision(1) << minSignalAbs << L" V";
                    errorConditions[numErrors++] = wss.str();
                    retVal = false;
                }
            }

            // Push segment
            fraSegment.startFreqHz = strStartFreq;
            fraSegment.stopFreqHz = strStopFreq;
            fraSegment.steps = strSteps;
            fraSegment.stimulusVpp = strStimulusVpp;
            fraSegment.stimulusOffset = strStimulusOffset;

            fraSegments.push_back(fraSegment);
        }
    }
    catch (std::exception e)
    {
        UNREFERENCED_PARAMETER(e);
        MessageBox( hDlg, L"Could not validate settings.", L"Error", MB_OK );
        retVal = false;
    }

    if (numErrors)
    {
        uint8_t i;
        wstring errorMessage = L"The following are invalid:\n";
        for (i = 0; i < numErrors-1; i++)
        {
            errorMessage += L"- " + errorConditions[i] + L",\n";
        }
        errorMessage += L"- " + errorConditions[i];

        MessageBox( hDlg, errorMessage.c_str(), L"Error", MB_OK );
    }

    else
    {
        pSettings->SetCustomPlanEnabled(true);
        pSettings->SetCustomPlan(fraSegments, STEP_LOG_DECADE);
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: CustomPlanDialogHandler
//
// Purpose: Dialog procedure for the Custom Plan Dialog.  Handles initialization and user actions.
//
// Parameters: See Windows API documentation
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK CustomPlanDialogHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_INITDIALOG:
        {
            // Disable parent window to make this modal.  This is necessary because this dialog was
            // created with a custom message loop.
            EnableWindow(GetParent(hDlg), FALSE);

            // Initialize scrolling parameters
            prevx = 1;
            prevy = 1;
            RECT rc = {};
            GetClientRect(hDlg, &rc);

            const SIZE sz = { rc.right - rc.left, rc.bottom - rc.top };

            SCROLLINFO si = {};
            si.cbSize = sizeof(SCROLLINFO);
            si.fMask = SIF_PAGE | SIF_POS | SIF_RANGE;
            si.nPos = si.nMin = 1;

            si.nMax = sz.cx;
            si.nPage = sz.cx;
            SetScrollInfo(hDlg, SB_HORZ, &si, FALSE);

            si.nMax = sz.cy;
            si.nPage = sz.cy;
            SetScrollInfo(hDlg, SB_VERT, &si, FALSE);

            // Shorten the window height to 300 DLUs
            RECT newDlgDim = {0, 0, 430, 300};
            MapDialogRect(hDlg, &newDlgDim);
            AdjustWindowRect(&newDlgDim, GetWindowLong(hDlg, GWL_STYLE), FALSE);
            SetWindowPos(hDlg, NULL, 0, 0, newDlgDim.right - newDlgDim.left, newDlgDim.bottom - newDlgDim.top, SWP_NOMOVE);

            // Set fonts
            ChangeHeaderTextFont(GetDlgItem(hDlg, IDC_FREQUENCY_HZ_TEXT));
            ChangeHeaderTextFont(GetDlgItem(hDlg, IDC_STEPS_TEXT));
            ChangeHeaderTextFont(GetDlgItem(hDlg, IDC_STIMULUS_VPP_TEXT));
            ChangeHeaderTextFont(GetDlgItem(hDlg, IDC_STIMULUS_OFFSET_V_TEXT));

            if (!(psFRA->SigGenSupportsDcOffset()))
            {
                EnableWindow(GetDlgItem(hDlg, IDC_STIMULUS_OFFSET_V_TEXT), FALSE);
            }

            // Map dialog row spacing into pixels
            rowSpacing.bottom = 40;
            MapDialogRect(hDlg, &rowSpacing);

            // Initialize row count
            StepType_T stepType;
            std::vector<FRASegmentStrings_T> fraSegments;
            pSettings->GetCustomPlanAsString(fraSegments, stepType);
            if (fraSegments.size() == 0)
            {
                numRows = 1;
            }
            else if (fraSegments.size() > (std::numeric_limits<uint8_t>::max)())
            {
                numRows = (std::numeric_limits<uint8_t>::max)();
            }
            else
            {
                numRows = (uint8_t)fraSegments.size();
            }

            highWaterMark = numRows;

            // Setup fields
            SetupCustomPlanFields(hDlg);

            // Populate data into fields
            PopulateCustomPlanFields(hDlg);

            return (INT_PTR)TRUE;
            break;
        }
        case WM_CLOSE:
        {
            DestroyWindow (hDlg);
            return (INT_PTR)TRUE;
            break;
        }
        case WM_DESTROY:
        {
            CustomPlanDialog_Cleanup();
            // Re-enable parent window.  This is necessary because this dialog was
            // created with a custom message loop.
            EnableWindow(GetParent(hDlg), TRUE);
            SetForegroundWindow(GetParent(hDlg));

            PostQuitMessage(0);
            return (INT_PTR)TRUE;
            break;
        }
        case WM_NOTIFY:
        {
            return (INT_PTR)TRUE;
            break;
        }
        case WM_SIZE:
        {
            UINT state = (UINT)wParam;
            int cx = (int)(short)LOWORD(lParam);
            int cy = (int)(short)HIWORD(lParam);

            if(state != SIZE_RESTORED && state != SIZE_MAXIMIZED)
            {
                return (INT_PTR)TRUE;
            }

            SCROLLINFO si = {};
            si.cbSize = sizeof(SCROLLINFO);

            const int bar[] = { SB_HORZ, SB_VERT };
            const int page[] = { cx, cy };

            for(size_t i = 0; i < ARRAYSIZE(bar); ++i)
            {
                si.fMask = SIF_PAGE;
                si.nPage = page[i];
                SetScrollInfo(hDlg, bar[i], &si, TRUE);

                si.fMask = SIF_RANGE | SIF_POS;
                GetScrollInfo(hDlg, bar[i], &si);

                const int maxScrollPos = si.nMax - (page[i] - 1);

                // Scroll client only if scroll bar is visible and window's
                // content is fully scrolled toward right and/or bottom side.
                // Also, update window's content on maximize.
                const bool needToScroll =
                    (si.nPos != si.nMin && si.nPos == maxScrollPos) ||
                    (state == SIZE_MAXIMIZED);

                if(needToScroll)
                {
                    SD_ScrollClient(hDlg, bar[i], si.nPos);
                }
            }
            return (INT_PTR)TRUE;
            break;
        }
        case WM_VSCROLL:
        {
            SD_OnHVScroll(hDlg, SB_VERT, (UINT)(LOWORD(wParam)));
            return (INT_PTR)TRUE;
            break;
        }
        case WM_HSCROLL:
        {
            SD_OnHVScroll(hDlg, SB_HORZ, (UINT)(LOWORD(wParam)));
            return (INT_PTR)TRUE;
            break;
        }
        case WM_COMMAND:
        {
            switch (LOWORD(wParam))
            {
                case IDOK:
                {
                    if (CustomPlanDialog_ValidateAndStoreSettings(hDlg))
                    {
                        DestroyWindow (hDlg);
                    }
                    return (INT_PTR)TRUE;
                    break;
                }
                case IDCANCEL:
                {
                    DestroyWindow (hDlg);
                    return (INT_PTR)TRUE;
                    break;
                }
                case IDC_CUSTOM_PLAN_MINUS_BUTTON:
                {
                    if (numRows > 1)
                    {
                        numRows--;
                        int scrollAmount;
                        // If the new fields will be outside the window client area, then scroll up so they won't be
                        if (scrollAmount = SetupCustomPlanFields(hDlg))
                        {
                            if (MINLONG == scrollAmount)
                            {
                                SendMessage(hDlg, WM_VSCROLL, SB_TOP, 0);
                            }
                            else
                            {
                                SetScrollPos(hDlg, SB_VERT, GetScrollPos(hDlg,SB_VERT) + scrollAmount, TRUE);
                                SD_ScrollClient(hDlg, SB_VERT, prevy + scrollAmount);
                            }
                        }
                    }
                    return (INT_PTR)TRUE;
                    break;
                }
                case IDC_CUSTOM_PLAN_PLUS_BUTTON:
                {
                    if (numRows < 10)
                    {
                        numRows++;
                        int scrollAmount;
                        // If the new fields will be outside the window client area, then scroll down so they won't be
                        if (scrollAmount = SetupCustomPlanFields(hDlg))
                        {
                            SetScrollPos(hDlg, SB_VERT, GetScrollPos(hDlg,SB_VERT) + scrollAmount, TRUE);
                            SD_ScrollClient(hDlg, SB_VERT, prevy + scrollAmount);
                        }
                    }
                    return (INT_PTR)TRUE;
                    break;
                }
                case IDC_COPY_DOWN:
                {
                    HWND hndCtrl;
                    TCHAR editText[64];
                    hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STEPS_1);
                    Edit_GetText( hndCtrl, editText, 16 );
                    for (int i = 1; i < numRows; i++)
                    {
                        hndCtrl = GetDlgItem( hDlg, IDC_CP_EDIT_STEPS_1+i);
                        Edit_SetText( hndCtrl, editText );
                    }
                    break;
                }
                default:
                    return (INT_PTR)TRUE;
                    break;
            }
        }
        default:
            return (INT_PTR)FALSE;
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SD_OnHVScroll
//
// Purpose: Manages horizontal and vertical scrolling
//
// Parameters: [in] hwnd: handle to the dialog window
//             [in] bar: which scroll bar (SB_HORZ, SB_VERT)
//             [in] code: scroll bar command code
//
// Notes: Based on https://www.codeproject.com/Articles/32925/Scrollable-Dialog-in-Pure-Win32-API
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SD_OnHVScroll(HWND hwnd, int bar, UINT code)
{
    const int scrollPos = SD_GetScrollPos(hwnd, bar, code);

    if(scrollPos == -1)
    {
        return;
    }

    SetScrollPos(hwnd, bar, scrollPos, TRUE);
    SD_ScrollClient(hwnd, bar, scrollPos);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SD_ScrollClient
//
// Purpose: Manages horizontal and vertical scrolling
//
// Parameters: [in] hwnd: handle to the dialog window
//             [in] bar: which scroll bar (SB_HORZ, SB_VERT)
//             [in] pos: new position
//
// Notes: Based on https://www.codeproject.com/Articles/32925/Scrollable-Dialog-in-Pure-Win32-API
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SD_ScrollClient(HWND hwnd, int bar, int pos)
{
    int cx = 0;
    int cy = 0;

    int& delta = (bar == SB_HORZ ? cx : cy);
    int& prev = (bar == SB_HORZ ? prevx : prevy);

    delta = prev - pos;
    prev = pos;

    if(cx || cy)
    {
        ScrollWindow(hwnd, cx, cy, NULL, NULL);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SD_GetScrollPos
//
// Purpose: Manages horizontal and vertical scrolling
//
// Parameters: [in] hwnd: handle to the dialog window
//             [in] bar: which scroll bar (SB_HORZ, SB_VERT)
//             [in] code: scroll bar command code
//
// Notes: Based on https://www.codeproject.com/Articles/32925/Scrollable-Dialog-in-Pure-Win32-API
//
///////////////////////////////////////////////////////////////////////////////////////////////////

int SD_GetScrollPos(HWND hwnd, int bar, UINT code)
{
    SCROLLINFO si = {};
    si.cbSize = sizeof(SCROLLINFO);
    si.fMask = SIF_PAGE | SIF_POS | SIF_RANGE | SIF_TRACKPOS;
    GetScrollInfo(hwnd, bar, &si);

    const int minPos = si.nMin;
    const int maxPos = si.nMax - (si.nPage - 1);

    int result = -1;

    switch(code)
    {
        case SB_LINEUP /*SB_LINELEFT*/:
            result = max(si.nPos - 1, minPos);
            break;

        case SB_LINEDOWN /*SB_LINERIGHT*/:
            result = min(si.nPos + 1, maxPos);
            break;

        case SB_PAGEUP /*SB_PAGELEFT*/:
            result = max(si.nPos - (int)si.nPage, minPos);
            break;

        case SB_PAGEDOWN /*SB_PAGERIGHT*/:
            result = min(si.nPos + (int)si.nPage, maxPos);
            break;

        case SB_THUMBPOSITION:
            // do nothing
            break;

        case SB_THUMBTRACK:
            result = si.nTrackPos;
            break;

        case SB_TOP /*SB_LEFT*/:
            result = minPos;
            break;

        case SB_BOTTOM /*SB_RIGHT*/:
            result = maxPos;
            break;

        case SB_ENDSCROLL:
            // do nothing
            break;
    }

    return result;
}
