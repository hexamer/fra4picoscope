//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014-2022 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module PreRunAlert.cpp: Contains functions handling the Pre-Run Alert dialog, which is
//                         used to display warnings and errors found in the sample planning phase
//                         that a user can adjust settings to remedy.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <Windows.h>
#include <Windowsx.h>
#include <commctrl.h>
#include <shellapi.h>
#include "FRA4PicoScopeInterfaceTypes.h"
#include "PicoScopeFraApp.h"
#include "Resource.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Get[Warning|Error]CategoryText
//
// Purpose: Returns a string explaining the warning/error category
//
// Parameters: [in] category - the warning/error category index
//             [out] return - the explanatory string
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

static const wchar_t preRunAlertWarningStrings[NUMBER_OF_PRE_RUN_WARNING_CATEGORIES+1][512] =
{
    L"WARNING: Can't meet requested resolution for some steps. Loss of dynamic range may cause inaccuracy. Decrease sampling rate or resolution. Or use channels from different sampling blocks.",
    L"WARNING: Can't meet requested sampling rate for some steps. Undersampling may lead to aliasing of higher frequency noise. Decrease maximum sweep frequency.",
    L"WARNING: Sampling rate limited by channel utilization. Undersampling may lead to aliasing of higher frequency noise. Use channels from different sampling blocks.",
    L"WARNING: Cycles adjusted to remain within scope sample buffer. DFT bandwidth may be affected. Increase bandwidth, or decrease sampling rate or resolution.",
    L"WARNING: DFT bandwidth greater than requested for some steps. Desired noise rejection may be not be achieved. Decrease sampling rate or resolution.",
    L"WARNING: AC Coupling is being used with low frequencies. Loss of signal quality may occur. Switch to DC coupling or increase minimum sweep frequency.",
    L"WARNING: Input and output channel's AC/DC coupling don�t match and low frequencies are being used. Loss of FRA accuracy may occur. Match copupling for channels or increase minimum sweep frequency.",
    L"WARNING: Sampling frequency is less than the Nyquist frequency for some steps. FRA results may be inaccurate. Increase sampling rate or decrease maximum sweep frequency.",
    L"UNKNOWN"
};

static const wchar_t preRunAlertErrorStrings[NUMBER_OF_PRE_RUN_ERROR_CATEGORIES + 1][512] =
{
    L"ERROR: Request exceeds scope sample buffer. FRA cannot proceed. Decrease low noise cycles captured or low noise oversampling setting.",
    L"ERROR: Cannot fit a whole cycle into the sample buffer. FRA cannot proceed. Increase bandwith, decrease sampling rate or resolution.",
    L"UNKNOWN"
};

const wchar_t* GetWarningCategoryText( FRA_PRE_RUN_WARNING_T category)
{
    if (category >= 0 && category < NUMBER_OF_PRE_RUN_WARNING_CATEGORIES)
    {
        return preRunAlertWarningStrings[category];
    }
    else
    {
        return preRunAlertWarningStrings[NUMBER_OF_PRE_RUN_WARNING_CATEGORIES];
    }
}

const wchar_t* GetErrorCategoryText( FRA_PRE_RUN_ERROR_T category )
{
    if (category >= 0 && category < NUMBER_OF_PRE_RUN_ERROR_CATEGORIES)
    {
        return preRunAlertErrorStrings[category];
    }
    else
    {
        return preRunAlertErrorStrings[NUMBER_OF_PRE_RUN_ERROR_CATEGORIES];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PreRunAlertHandler
//
// Purpose: Dialog procedure for the Settings Dialog.  Handles initialization and user actions.
//
// Parameters: See Windows API documentation
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK PreRunAlertHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_INITDIALOG:
        {
            HWND hndCtrl;
            RECT smallIconDimensions;
            bool errorsExist = false;
            bool warningsExist = false;
            HICON warningIcon = NULL;
            HICON errorIcon = NULL;
            wstringstream wssMainText;
            FraStatusPreRunAlert* pPreRunAlert;

            HWND hPreRunAlertTree = GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_TREE );

            smallIconDimensions = { 0, 0, 7, 7 };
            MapDialogRect( hDlg, &smallIconDimensions );

            HIMAGELIST hImageList = ImageList_Create( smallIconDimensions.right, smallIconDimensions.bottom, ILC_COLOR32, 2, 2 );

            SHSTOCKICONINFO stockIconInfo;
            stockIconInfo.cbSize = sizeof( SHSTOCKICONINFO );

            if (S_OK == SHGetStockIconInfo( SIID_WARNING, SHGSI_ICON | SHGSI_SMALLICON, &stockIconInfo ))
            {
                ImageList_AddIcon( hImageList, stockIconInfo.hIcon );
            }
            if (S_OK == SHGetStockIconInfo( SIID_ERROR, SHGSI_ICON | SHGSI_SMALLICON, &stockIconInfo ))
            {
                ImageList_AddIcon( hImageList, stockIconInfo.hIcon );
            }

            TreeView_SetImageList( hPreRunAlertTree, hImageList, TVSIL_NORMAL );

            if (NULL != (pPreRunAlert = (FraStatusPreRunAlert*)lParam))
            {
                for (auto errorCategory : pPreRunAlert->errors)
                {
                    for (auto error : errorCategory)
                    {
                        errorsExist = true;
                    }
                }

                for (auto warningCategory : pPreRunAlert->warnings)
                {
                    for (auto warning : warningCategory)
                    {
                        warningsExist = true;
                    }
                }

                if (errorsExist)
                {
                    hndCtrl = GetDlgItem( hDlg, IDYES );
                    ShowWindow( hndCtrl, FALSE );
                    hndCtrl = GetDlgItem( hDlg, IDNO );
                    ShowWindow( hndCtrl, FALSE );
                }
                else
                {
                    hndCtrl = GetDlgItem( hDlg, IDOK );
                    ShowWindow( hndCtrl, FALSE );
                }

                TVINSERTSTRUCT tvInsertStruct;
                HTREEITEM hParentCategory;

                for (uint8_t warningCategoryIdx = 0; warningCategoryIdx < NUMBER_OF_PRE_RUN_WARNING_CATEGORIES; warningCategoryIdx++)
                {
                    if (pPreRunAlert->warnings[warningCategoryIdx].size())
                    {
                        tvInsertStruct = { 0 };
                        tvInsertStruct.hParent = TVI_ROOT;
                        tvInsertStruct.hInsertAfter = TVI_LAST;
                        tvInsertStruct.itemex.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN;
                        tvInsertStruct.itemex.pszText = (LPWSTR)GetWarningCategoryText( (FRA_PRE_RUN_WARNING_T)warningCategoryIdx );
                        tvInsertStruct.itemex.cchTextMax = 0;
                        tvInsertStruct.itemex.iImage = 0;
                        tvInsertStruct.itemex.iSelectedImage = 0;
                        tvInsertStruct.itemex.cChildren = 1;

                        hParentCategory = TreeView_InsertItem( hPreRunAlertTree, &tvInsertStruct );

                        for (auto warning : pPreRunAlert->warnings[warningCategoryIdx])
                        {
                            tvInsertStruct = { 0 };
                            tvInsertStruct.hParent = hParentCategory;
                            tvInsertStruct.hInsertAfter = TVI_LAST;
                            tvInsertStruct.itemex.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
                            tvInsertStruct.itemex.pszText = (LPWSTR)warning.c_str();
                            tvInsertStruct.itemex.cchTextMax = 0;
                            tvInsertStruct.itemex.iImage = 0;
                            tvInsertStruct.itemex.iSelectedImage = 0;

                            TreeView_InsertItem( hPreRunAlertTree, &tvInsertStruct );
                        }
                    }
                }
                for (uint8_t errorCategoryIdx = 0; errorCategoryIdx < NUMBER_OF_PRE_RUN_ERROR_CATEGORIES; errorCategoryIdx++)
                {
                    if (pPreRunAlert->errors[errorCategoryIdx].size())
                    {
                        tvInsertStruct = { 0 };
                        tvInsertStruct.hParent = TVI_ROOT;
                        tvInsertStruct.hInsertAfter = TVI_LAST;
                        tvInsertStruct.itemex.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN;
                        tvInsertStruct.itemex.pszText = (LPWSTR)GetErrorCategoryText( (FRA_PRE_RUN_ERROR_T)errorCategoryIdx );
                        tvInsertStruct.itemex.cchTextMax = 0;
                        tvInsertStruct.itemex.iImage = 1;
                        tvInsertStruct.itemex.iSelectedImage = 1;
                        tvInsertStruct.itemex.cChildren = 1;

                        hParentCategory = TreeView_InsertItem( hPreRunAlertTree, &tvInsertStruct );

                        for (auto error : pPreRunAlert->errors[errorCategoryIdx])
                        {
                            tvInsertStruct = { 0 };
                            tvInsertStruct.hParent = hParentCategory;
                            tvInsertStruct.hInsertAfter = TVI_LAST;
                            tvInsertStruct.itemex.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
                            tvInsertStruct.itemex.pszText = (LPWSTR)error.c_str();
                            tvInsertStruct.itemex.cchTextMax = 0;
                            tvInsertStruct.itemex.iImage = 1;
                            tvInsertStruct.itemex.iSelectedImage = 1;

                            TreeView_InsertItem( hPreRunAlertTree, &tvInsertStruct );
                        }
                    }
                }
            }

            // Initially set focus away from treeview so that selection/highlighting does not initially occur
            if (errorsExist)
            {
                SetFocus( GetDlgItem( hDlg, IDOK ) );
            }
            else
            {
                SetFocus( GetDlgItem( hDlg, IDYES ) );
            }

            // Load the large icon for warning or error
            if (S_OK == SHGetStockIconInfo( SIID_WARNING, SHGSI_ICON | SHGSI_LARGEICON, &stockIconInfo ))
            {
                warningIcon = stockIconInfo.hIcon;
            }
            if (S_OK == SHGetStockIconInfo( SIID_ERROR, SHGSI_ICON | SHGSI_LARGEICON, &stockIconInfo ))
            {
                errorIcon = stockIconInfo.hIcon;
            }

            wstring problemTypes;

            // Load a larger font for main and instruction text
            HFONT hOldFont = (HFONT)SendMessage( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_MAIN_MESSAGE ), WM_GETFONT, NULL, NULL );
            LOGFONT logFont;
            GetObject( hOldFont, sizeof( LOGFONT ), &logFont );
            logFont.lfHeight = (LONG)(1.5 * logFont.lfHeight);
            HFONT hFont = CreateFontIndirect( &logFont );

            if (warningsExist && errorsExist)
            {
                problemTypes = L"warnings and errors";
                ShowWindow( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_CONTINUE ), FALSE );
                SendMessage( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_CANT_CONTINUE ), WM_SETFONT, (WPARAM)hFont, TRUE );
                Static_SetIcon( GetDlgItem( hDlg, IDI_PRE_RUN_ALERT_ICON ), errorIcon );
            }
            else if (warningsExist)
            {
                problemTypes = L"warnings";
                ShowWindow( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_CANT_CONTINUE ), FALSE );
                SendMessage( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_CONTINUE ), WM_SETFONT, (WPARAM)hFont, TRUE );
                Static_SetIcon( GetDlgItem( hDlg, IDI_PRE_RUN_ALERT_ICON ), warningIcon );
            }
            else
            {
                problemTypes = L"errors";
                ShowWindow( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_CONTINUE ), FALSE );
                SendMessage( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_CANT_CONTINUE ), WM_SETFONT, (WPARAM)hFont, TRUE );
                Static_SetIcon( GetDlgItem( hDlg, IDI_PRE_RUN_ALERT_ICON ), errorIcon );
            }

            wssMainText << L"The following " << problemTypes << L" were found while planning samples:";

            SendMessage( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_MAIN_MESSAGE ), WM_SETFONT, (WPARAM)hFont, TRUE );

            Static_SetText( GetDlgItem( hDlg, IDC_PRE_RUN_ALERT_MAIN_MESSAGE ), wssMainText.str().c_str() );

            return (INT_PTR)FALSE; // Return FALSE so that SetFocus will work
        }
        case WM_COMMAND:
        {
            if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDYES || LOWORD(wParam) == IDNO)
            {
                EndDialog(hDlg, LOWORD(wParam));
                return (INT_PTR)TRUE;
            }
            return (INT_PTR)FALSE;
        }
        default:
            return (INT_PTR)FALSE;
    }
}