//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: psCommonImpl.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include <algorithm>
#include <bitset>
#include <boost/math/special_functions/round.hpp>
using namespace boost::math;

#define WORKAROUND_SUPPORT_31730

// Define lower case and upper case tokens
#if defined(PS2000_IMPL)
#define SCOPE_FAMILY_LT 2000
#define SCOPE_FAMILY_UT 2000
#elif defined(PS3000_IMPL)
#define SCOPE_FAMILY_LT 3000
#define SCOPE_FAMILY_UT 3000
#elif defined(PS2000A_IMPL)
#define SCOPE_FAMILY_LT 2000a
#define SCOPE_FAMILY_UT 2000A
#define NEW_PS_DRIVER_MODEL
#elif defined(PS3000A_IMPL)
#define SCOPE_FAMILY_LT 3000a
#define SCOPE_FAMILY_UT 3000A
#define NEW_PS_DRIVER_MODEL
#elif defined(PS4000_IMPL)
#define SCOPE_FAMILY_LT 4000
#define SCOPE_FAMILY_UT 4000
#define NEW_PS_DRIVER_MODEL
#elif defined(PS4000A_IMPL)
#define SCOPE_FAMILY_LT 4000a
#define SCOPE_FAMILY_UT 4000A
#define NEW_PS_DRIVER_MODEL
#elif defined(PS5000_IMPL)
#define SCOPE_FAMILY_LT 5000
#define SCOPE_FAMILY_UT 5000
#define NEW_PS_DRIVER_MODEL
#elif defined(PS5000A_IMPL)
#define SCOPE_FAMILY_LT 5000a
#define SCOPE_FAMILY_UT 5000A
#define NEW_PS_DRIVER_MODEL
#elif defined(PS6000_IMPL)
#define SCOPE_FAMILY_LT 6000
#define SCOPE_FAMILY_UT 6000
#define NEW_PS_DRIVER_MODEL
#elif defined(PS6000A_IMPL)
#define SCOPE_FAMILY_LT 6000a
#define SCOPE_FAMILY_UT 6000A
#define NEW_PS_DRIVER_MODEL
#elif defined(PSOSPA_IMPL)
#define SCOPE_FAMILY_LT ospa
#define SCOPE_FAMILY_UT OSPA
#define NEW_PS_DRIVER_MODEL
#endif

#define LOG_PICO_API_CALL(...) \
fraStatusText.clear(); \
fraStatusText.str(L""); \
LogPicoApiCall(fraStatusText, __VA_ARGS__);

// NEW_PS_DRIVER_MODEL means that the driver API:
// 1) Supports capture completion callbacks
// 2) Returns status with PICO_OK indicating success
// 3) Normally uses CamelCase identifiers
#if defined(NEW_PS_DRIVER_MODEL)
#define PICO_ERROR(x) (status = x) == PICO_OK ? 0 : (PICO_POWER_SUPPLY_CONNECTED == status || PICO_POWER_SUPPLY_NOT_CONNECTED == status) ? throw PicoPowerChange(status) : 1
#define PICO_CONNECTION_ERROR(x) (status = x) == PICO_OK ? 0 : (PICO_BUSY == status) ? 0 : 1
#else
#define PICO_ERROR(x) 0 == (status = x)
#define PICO_CONNECTION_ERROR(x) 0 == (status = x)
#define GetUnitInfo _get_unit_info
#define SetChannel _set_channel
#define SetSigGenBuiltIn _set_sig_gen_built_in
#define GetTimebase _get_timebase
#define SetTriggerChannelConditions _set_trigger
#define Stop _stop
#define CloseUnit _close_unit
#endif

#if defined(PSOSPA_IMPL)
#define psospaSetChannel psospaSetChannelOn
#define psospaGetAnalogueOffset psospaGetAnalogueOffsetLimits
typedef PICO_CHANNEL PSOSPA_CHANNEL;
typedef PICO_COUPLING PSOSPA_COUPLING;
typedef PICO_CONNECT_PROBE_RANGE PSOSPA_RANGE;
#define PSOSPA_RATIO_MODE_NONE PICO_RATIO_MODE_RAW
#define PSOSPA_RATIO_MODE_AGGREGATE PICO_RATIO_MODE_AGGREGATE
#define CommonGenericEnum(FM, ENUM) xCommonGenericEnum(FM, ENUM)
#define xCommonGenericEnum(FM, ENUM) PICO_##ENUM
#elif defined(PS6000A_IMPL)
#define ps6000aSetChannel ps6000aSetChannelOn
#define ps6000aGetAnalogueOffset ps6000aGetAnalogueOffsetLimits
typedef PICO_CHANNEL PS6000A_CHANNEL;
typedef PICO_COUPLING PS6000A_COUPLING;
typedef PICO_CONNECT_PROBE_RANGE PS6000A_RANGE;
#define PS6000A_RATIO_MODE_NONE PICO_RATIO_MODE_RAW
#define PS6000A_RATIO_MODE_AGGREGATE PICO_RATIO_MODE_AGGREGATE
#define CommonGenericEnum(FM, ENUM) xCommonGenericEnum(FM, ENUM)
#define xCommonGenericEnum(FM, ENUM) PICO_##ENUM
#elif defined(PS4000A_IMPL)
#define PS4000A_RANGE PICO_CONNECT_PROBE_RANGE
// This family is not using generic (i.e. PICO_*) enums (yet)
#define CommonGenericEnum(FM, ENUM) xCommonGenericEnum(FM, ENUM)
#define xCommonGenericEnum(FM, ENUM) PS##FM##_##ENUM
#elif defined(PS5000A_IMPL)
// This family is not using generic (i.e. PICO_*) enums (yet)
#define CommonGenericEnum(FM, ENUM) xCommonGenericEnum(FM, ENUM)
#define xCommonGenericEnum(FM, ENUM) PS##FM##_##ENUM
#endif

#define CommonClass(FM) xCommonClass(FM)
#define xCommonClass(FM) ps##FM##Impl

#define CommonCtor(FM) xCommonCtor(FM)
#define xCommonCtor(FM) ps##FM##Impl::ps##FM##Impl

#define CommonDtor(FM) xCommonDtor(FM)
#define xCommonDtor(FM) ps##FM##Impl::~ps##FM##Impl

#define CommonMethod(FM, METHOD) xCommonMethod(FM, METHOD)
#define CommonApi(FM, API) xCommonApi(FM, API)
#define xCommonMethod(FM, METHOD) ps##FM##Impl::##METHOD
#define xCommonApi(FM, API) ps##FM##API

#define CommonEnum(FM, ENUM) xCommonEnum(FM, ENUM)
#define xCommonEnum(FM, ENUM) PS##FM##_##ENUM

#define CommonSine(FM) xCommonSine(FM)
#define xCommonSine(FM) PS##FM##_##SINE

#define CommonDC(FM) xCommonDC(FM)
#define xCommonDC(FM) PS##FM##_##DC_VOLTAGE

#define CommonSweepUp(FM) xCommonSweepUp(FM)
#define xCommonSweepUp(FM) PS##FM##_##UP

#define CommonEsOff(FM) xCommonEsOff(FM)
#define xCommonEsOff(FM) PS##FM##_##ES_OFF

#define CommonSigGenNone(FM) xCommonSigGenNone(FM)
#define xCommonSigGenNone(FM) PS##FM##_##SIGGEN_NONE

#define CommonReadyCB(FM) xCommonReadyCB(FM)
#define xCommonReadyCB(FM) ps##FM##BlockReady

#define CommonErrorCode(FM) xCommonErrorCode(FM)
#define xCommonErrorCode(FM) PS##FM##_ERROR_CODE

// This table covers the a superset of the ranges available on any PicoScope with a 1X probe attached
#if defined(PSOSPA_IMPL)
const std::vector<RANGE_INFO_T> CommonClass(SCOPE_FAMILY_LT)::base1xRangeInfo =
{
    {0.005, 0.5, 0.0, L"� 5 mV"},
    {0.010, 0.5, 2.0, L"� 10 mV"},
    {0.020, 0.4, 2.0, L"� 20 mV"},
    {0.050, 0.5, 2.5, L"� 50 mV"},
    {0.100, 0.5, 2.0, L"� 100 mV"},
    {0.200, 0.4, 2.0, L"� 200 mV"},
    {0.500, 0.5, 2.5, L"� 500 mV"},
    {1.0, 0.5, 2.0, L"� 1 V"},
    {2.0, 0.4, 2.0, L"� 2 V"},
    {5.0, 0.5, 2.5, L"� 5 V"},
    {10.0, 0.5, 2.0, L"� 10 V"},
    {20.0, 0.4, 2.0, L"� 20 V"},
    {50.0, 0.0, 2.5, L"� 50 V"}
};
#else
const std::vector<RANGE_INFO_T> CommonClass( SCOPE_FAMILY_LT )::base1xRangeInfo =
{
    {0.010, 0.5, 0.0, L"� 10 mV"},
    {0.020, 0.4, 2.0, L"� 20 mV"},
    {0.050, 0.5, 2.5, L"� 50 mV"},
    {0.100, 0.5, 2.0, L"� 100 mV"},
    {0.200, 0.4, 2.0, L"� 200 mV"},
    {0.500, 0.5, 2.5, L"� 500 mV"},
    {1.0, 0.5, 2.0, L"� 1 V"},
    {2.0, 0.4, 2.0, L"� 2 V"},
    {5.0, 0.5, 2.5, L"� 5 V"},
    {10.0, 0.5, 2.0, L"� 10 V"},
    {20.0, 0.4, 2.0, L"� 20 V"},
    {50.0, 0.0, 2.5, L"� 50 V"}
};
#endif

const std::vector<double> CommonClass(SCOPE_FAMILY_LT)::attenScalingFactors =
{
    1.0,
    10.0,
    20.0,
    25.0,
    50.0,
    100.0,
    200.0,
    250.0,
    500.0,
    1000.0
};

// It is imperative that this value be greater than (half) the largest buffer in the scope family for
// scopes not implementing the new driver model.  Currently this is 1MB (PS3206)
const uint32_t CommonClass(SCOPE_FAMILY_LT)::maxDataRequestSize = 16 * 1024 * 1024; // 16 MSamp (32 MB)

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common Constructor
//
// Purpose: Handles some of the object initialization
//
// Parameters: [in] _handle - Handle to the scope as defined by the PicoScope driver
//             [in] _initialPowerState - the power state returned by open function
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(PS3000A_IMPL) || defined(PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
CommonCtor(SCOPE_FAMILY_LT)( int16_t _handle, PICO_STATUS _initialPowerState ) : PicoScope()
#else
CommonCtor(SCOPE_FAMILY_LT)( int16_t _handle ) : PicoScope()
#endif
{
    handle = _handle;
    initialized = false;
#if !defined(NEW_PS_DRIVER_MODEL)
    hCheckStatusEvent = NULL;
    hCheckStatusThread = NULL;
    capturing = false;
    readyCB = NULL;
    currentTimeIndisposedMs = 0;
    cbParam = NULL;
#endif
#if defined(IMPLEMENTS_SMART_PROBES)
    smartProbeCallback = NULL;
#endif
    minTimebase = 0;
    maxTimebase = 0;
    timebaseNoiseRejectMode = 0;
    defaultTimebaseNoiseRejectMode = 0;
    signalGeneratorPrecision = 0.0;
    mInputChannel = PS_CHANNEL_INVALID;
    mOutputChannel = PS_CHANNEL_INVALID;
    mNumSamples = 0;
    buffersDirty = true;
    channelRangeInfo.resize(PS_MAX_CHANNELS);
    std::fill(channelRangeInfo.begin(), channelRangeInfo.end(), base1xRangeInfo);
    channelAttens.resize(PS_MAX_CHANNELS);
    std::fill(channelAttens.begin(), channelAttens.end(), ATTEN_1X);
    channelManualAttens = channelAttens;
    channelMinRanges.resize(PS_MAX_CHANNELS);
    std::fill(channelMinRanges.begin(), channelMinRanges.end(), 0);
    channelNonSmartMinRanges = channelMinRanges;
    channelMaxRanges.resize(PS_MAX_CHANNELS);
    std::fill(channelMaxRanges.begin(), channelMaxRanges.end(), 0);
    channelNonSmartMaxRanges = channelMaxRanges;
    channelRangeOffsets.resize(PS_MAX_CHANNELS);
    std::fill(channelRangeOffsets.begin(), channelRangeOffsets.end(), 0);
    channelSmartProbeConnected.resize(PS_MAX_CHANNELS);
    std::fill(channelSmartProbeConnected.begin(), channelSmartProbeConnected.end(), false);
    channelAutoAttens = channelSmartProbeConnected;
#if defined(PS3000A_IMPL) || defined(PS4000A_IMPL) || defined(PS5000A_IMPL)
    numActualChannels = 0;
    initialPowerState = _initialPowerState;
#endif;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common Destructor
//
// Purpose: Handles object cleanup
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

CommonDtor(SCOPE_FAMILY_LT)()
{
    if (handle != -1)
    { 
        Close();
    }
#if !defined(NEW_PS_DRIVER_MODEL)
    if (hCheckStatusEvent)
    {
        CloseHandle(hCheckStatusEvent);
    }
    if (hCheckStatusThread)
    {
        CloseHandle(hCheckStatusThread);
    }
#endif
#if defined(IMPLEMENTS_SMART_PROBES)
#if defined(PS4000A_IMPL)
    if (PS4444 == model)
    {
#endif
        if (hProcessProbeUpdateThread)
        {
            TerminateThread(hProcessProbeUpdateThread, 0);
            CloseHandle(hProcessProbeUpdateThread);
        }
        if (hProbeDataEvent)
        {
            CloseHandle(hProbeDataEvent);
        }
        if (hProbeDataMutex)
        {
            CloseHandle(hProbeDataMutex);
        }
#if defined(PS4000A_IMPL)
    }
#endif
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method Initialized
//
// Purpose: Indicate if the scope has been initialized
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT,Initialized)( void )
{
    return (handle != -1 && initialized);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method Connected
//
// Purpose: Detect if the scope is connected
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT,Connected)( void )
{
    PICO_STATUS status;
    wstringstream fraStatusText;
    wstringstream getUnitInfoCallReturn;

    status = CommonApi(SCOPE_FAMILY_LT, PingUnit)( handle );
    getUnitInfoCallReturn << status << L" <== " << BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, PingUnit));
    LOG_PICO_API_CALL( getUnitInfoCallReturn.str(), handle );
#if defined(NEW_PS_DRIVER_MODEL)
    return !(PICO_CONNECTION_ERROR(status));
#else
    if (PICO_CONNECTION_ERROR(status))
    {
        int8_t lastError[16];
        CommonApi(SCOPE_FAMILY_LT, GetUnitInfo)( handle, lastError, sizeof(lastError), CommonErrorCode(SCOPE_FAMILY_UT) );
        getUnitInfoCallReturn.clear();
        getUnitInfoCallReturn.str(L"");
        getUnitInfoCallReturn << L"string = " << (char*)lastError << L" <== " << BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetUnitInfo));
        LOG_PICO_API_CALL( getUnitInfoCallReturn.str(), handle, lastError, sizeof(lastError), CommonErrorCode(SCOPE_FAMILY_UT) );
        return false;
    }
    else
    {
        return true;
    }
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method USB3ScopeNotOnUSB3Port
//
// Purpose: Detect if a USB 3 scope is connected to a USB 2 (or earlier) port
//
// Parameters: N/A
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, USB3ScopeNotOnUSB3Port)(void)
{
    return (usbVersion == USB30 && !IsUSB3_0Connection());
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common accessors
//
// Purpose: Get scope parameters
//
// Parameters: Various
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

uint8_t CommonMethod(SCOPE_FAMILY_LT,GetNumChannels)( void )
{
#if defined(PS3000A_IMPL) || defined(PS4000A_IMPL) || defined(PS5000A_IMPL)
    if (numActualChannels > 2)
    {
        PICO_STATUS currentPowerState;
        wstringstream fraStatusText;
        LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, CurrentPowerSource)), handle );
        currentPowerState = CommonApi(SCOPE_FAMILY_LT, CurrentPowerSource)(handle);
#if defined(PS5000A_IMPL) // PS5000A/B/D require DC power for >2 channels, regardless of USB power
        if (PICO_POWER_SUPPLY_CONNECTED == currentPowerState)
#else
        if (IsUSB3_0Connection() || PICO_POWER_SUPPLY_CONNECTED == currentPowerState)
#endif
        {
            numAvailableChannels = numActualChannels;
        }
        else if (PICO_POWER_SUPPLY_NOT_CONNECTED == currentPowerState)
        {
            numAvailableChannels = 2;
        }
        else if (PICO_USB3_0_DEVICE_NON_USB3_0_PORT == currentPowerState) // Can only occur on 2 channel devices
        {
            numAvailableChannels = numActualChannels;
        }
        else // The handle is invalid, so just set a default presuming an error will get caught later.
        {
            numAvailableChannels = numActualChannels;
        }
    }
#endif
    return numAvailableChannels;
}

void CommonMethod(SCOPE_FAMILY_LT, GetAvailableCouplings)(PS_CHANNEL channel, vector<wstring>& couplingText)
{
#if defined (PS6000_IMPL)
    UNREFERENCED_PARAMETER(channel);
    if (model == PS6407)
    {
        couplingText.clear();
        couplingText.resize(1);
        couplingText[0] = TEXT("DC 50R");
    }
    else
    {
        couplingText.clear();
        couplingText.resize(3);
        couplingText[0] = TEXT("AC");
        couplingText[1] = TEXT("DC 1M");
        couplingText[2] = TEXT("DC 50R");
    }
#elif defined (PS6000A_IMPL) || defined (PSOSPA_IMPL)
    wstringstream fraStatusText;
    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        couplingText = channelCurrentAvailableCouplings[channel];
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to GetAvailableCouplings: " << channel;
        LogMessage(fraStatusText.str());
        couplingText.clear();
        couplingText.resize(1);
        couplingText[0] = TEXT("INVALID");
    }
#else
    UNREFERENCED_PARAMETER(channel);
    couplingText.clear();
    couplingText.resize(2);
    couplingText[0] = TEXT("AC");
    couplingText[1] = TEXT("DC");
#endif
}

uint32_t CommonMethod(SCOPE_FAMILY_LT,GetMinTimebase)( void )
{
    return minTimebase;
}

uint32_t CommonMethod(SCOPE_FAMILY_LT,GetMaxTimebase)( void )
{
    return maxTimebase;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetNextTimebase
//
// Purpose: Gets the next higher or lower timebase.
//
// Parameters: [in] timebase - timebase from which the next timebase will be derived
//             [in] resolution - resolution for which to get the next timebase
//             [in] roundFaster - if 0, then get next higher timebase; if 1, get next lower timebase
//             [out] newTimebase - the next higher or lower timebase
//             [out] return - whether the call succeeded
//
// Notes: Before PS3000E/PSOSPA driver existed, scope's timebases were compact.  Now that PSOSPA
//        timebase is a number of picoseconds, timebases are sparse.  This function exists to
//        support the settings dialog increment/decrement for noise reject timebase.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod( SCOPE_FAMILY_LT, GetNextTimebase )(uint32_t timebase, RESOLUTION_T resolution, uint8_t roundFaster, uint32_t* newTimebase)
{
    bool retVal = false;
    uint32_t originalTimebase = timebase;

    if (newTimebase)
    {
#if defined(PSOSPA_IMPL)
        for (uint32_t i = 0; i < minTimebase; i++)
        {
#endif
            if (timebase == minTimebase)
            {
                if (!roundFaster)
                {
                    timebase++;
                }
            }
            else if (timebase == maxTimebase)
            {
                if (roundFaster)
                {
                    timebase--;
                }
            }
            else
            {
                roundFaster ? timebase-- : timebase++;
            }
#if defined (PSOSPA_IMPL)
            if (model == PS3417E || model == PS3418E || model == PS3417EMSO || model == PS3418EMSO)
            {
                wstringstream fraStatusText;
                PICO_CHANNEL_FLAGS channelFlags;
                double timeIntervalAvailable;
                PICO_STATUS status;
                double newInterval;

                channelFlags = (PICO_CHANNEL_FLAGS)((1 << mInputChannel) | (1 << mOutputChannel));

                newInterval = (double)timebase / 1e12;

                if (resolution == RESOLUTION_AUTO)
                {
                    resolution = RESOLUTION_8BIT;
                }

                LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( psospaNearestSampleIntervalStateless ), handle, channelFlags, newInterval, roundFaster, (PICO_DEVICE_RESOLUTION)resolution, newTimebase, &timeIntervalAvailable );
                if (PICO_OK == (status = psospaNearestSampleIntervalStateless( handle, channelFlags, newInterval, roundFaster, (PICO_DEVICE_RESOLUTION)resolution, newTimebase, &timeIntervalAvailable )))
                {
                    if (*newTimebase != originalTimebase)
                    {
                        retVal = true;
                        break;
                    }
                }
                else
                {
                    fraStatusText.clear();
                    fraStatusText.str( L"" );
                    fraStatusText << L"Fatal error: Unable to determine nearest timebase in GetNextTimebase: " << status;
                    LogMessage( fraStatusText.str() );
                }
            }
        }
#else
     *newTimebase = timebase;
#endif
     retVal = true;
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method TimebaseIsSparse
//
// Purpose: Tells whether the scope's timebase is sparse.
//
// Parameters: [out] return - whether the scope's timebase is sparse
//
// Notes: Before PS3000E/PSOSPA driver existed, scope's timebases were compact.  Now that PSOSPA
//        timebase is a number of picoseconds, timebases are sparse.  This function exists to
//        support the settings dialog increment/decrement for noise reject timebase.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod( SCOPE_FAMILY_LT, TimebaseIsSparse )( void )
{
#if defined(PSOSPA_IMPL)
    return true;
#else
    return false;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method TimebaseIsValidForResolution
//
// Purpose: Indicates whether the given timebase is valid for the given resolution
//
// Parameters: [in] timebase - timebase to check
//             [in] resolution - resolution at which to check
//             [out] return - whether the given timebase is valid for the given resolution
//
// Notes: The available sample intervals for the PicoScope PSOSPA/3000E oscilloscopes are multiples
//        of the minimum sampling interval for the chosen resolution and number of enabled channels.
//        Must pass a specific resolution (i.e. not RESOLUTION_AUTO, etc.)
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod( SCOPE_FAMILY_LT, TimebaseIsValidForResolution )(uint32_t timebase, RESOLUTION_T resolution)
{
    bool retVal = false;

    if (timebase >= GetMinTimebaseForResolution( resolution ) && timebase <= maxTimebase)
    {
#if defined(PSOSPA_IMPL)
        if (RESOLUTION_10BIT == resolution && 0 == (timebase % 800))
        {
            retVal = true;
        }
        if (RESOLUTION_8BIT == resolution && 0 == (timebase % 400))
        {
            retVal = true;
        }
#else
        retVal = true;
#endif
    }
    return retVal;
}

void CommonMethod(SCOPE_FAMILY_LT,SetDesiredNoiseRejectModeTimebase)( uint32_t timebase )
{
    timebaseNoiseRejectMode = timebase;
}

uint32_t CommonMethod(SCOPE_FAMILY_LT,GetDefaultNoiseRejectModeTimebase)( void )
{
    return defaultTimebaseNoiseRejectMode;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetNoiseRejectModeTimebase
//
// Purpose: Gets the noise reject mode timebase set for the scope.
//
// Parameters: [out] timebase - the noise reject mode timebase
//             [out] adjustedByChannelSettings - whether the timebase needed to be adjusted due to
//                                               channel utilization
//             [out] return - whether the call succeeded
//
// Notes: This function adjusts for channel dependencies, but does not adjust for resolution.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT,GetNoiseRejectModeTimebase)( uint32_t& timebase, bool* adjustedByChannelSettings )
{
#if defined (PS6000_IMPL)
    if (((mInputChannel == PS_CHANNEL_A || mInputChannel == PS_CHANNEL_B) &&
         (mOutputChannel == PS_CHANNEL_A || mOutputChannel == PS_CHANNEL_B)) ||
        ((mInputChannel == PS_CHANNEL_C || mInputChannel == PS_CHANNEL_D) &&
         (mOutputChannel == PS_CHANNEL_C || mOutputChannel == PS_CHANNEL_D)))
    {
        timebase = max( 2, timebaseNoiseRejectMode );
        if (adjustedByChannelSettings)
        {
            *adjustedByChannelSettings = (timebase > timebaseNoiseRejectMode);
        }
        return true;
    }
#elif defined(PS6000A_IMPL)
    // Like PS6000, PS6000A has channel group sampling rate dependencies.  However, the PS6000A API
    // has a function to determine the minimum based on enabled channels.  We'll use that here for easier
    // code maintenanace.
    PICO_STATUS status;
    bool retVal = false;
    wstringstream fraStatusText;
    CommonGenericEnum(SCOPE_FAMILY_UT,CHANNEL_FLAGS) channelFlags = (CommonGenericEnum(SCOPE_FAMILY_UT,CHANNEL_FLAGS))((1<<mInputChannel) | (1<<mOutputChannel));
    uint32_t _minTimebase;
    double timeIntervalAvailable;

    // Use minimum resolution to avoid timebase adjustments due to resolution
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetMinimumTimebaseStateless)), handle, channelFlags, &_minTimebase, &timeIntervalAvailable, PICO_DR_8BIT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT,GetMinimumTimebaseStateless)( handle, channelFlags, &_minTimebase, &timeIntervalAvailable, PICO_DR_8BIT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to get noise reject mode timebase.  Failed to get stateless minimum timebase: " << status;
        LogMessage( fraStatusText.str() );
    }
    else
    {
        timebase = max(_minTimebase, timebaseNoiseRejectMode);
        if (adjustedByChannelSettings)
        {
            *adjustedByChannelSettings = (timebase > timebaseNoiseRejectMode);
        }
        retVal = true;
    }
    return retVal;
#endif
    timebase = timebaseNoiseRejectMode;
    if (adjustedByChannelSettings)
    {
        *adjustedByChannelSettings = false;
    }
    return true;
}

bool CommonMethod(SCOPE_FAMILY_LT,GetNoiseRejectModeSampleRate)( double& sampleRate )
{
    bool retVal = false;
    wstringstream fraStatusText;
    uint32_t timebase;
    if (GetNoiseRejectModeTimebase(timebase))
    {
        retVal = GetFrequencyFromTimebase( timebase, sampleRate );
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to get noise reject mode sample rate.  Failed to get noise reject mode timebase.";
        LogMessage( fraStatusText.str() );
    }
    return retVal;
}

double CommonMethod(SCOPE_FAMILY_LT,GetSignalGeneratorPrecision)( void )
{
    return signalGeneratorPrecision;
}

uint32_t CommonMethod(SCOPE_FAMILY_LT,GetMaxDataRequestSize)(void)
{
    return (uint32_t)mInputBuffer.size();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method Get[Min|Max]Range
//
// Purpose: Gets the minimum or maximum range for the channel based on the coupling and DC offset.
//
// Parameters: [in] channel - the channel for which the min/max range is determined
//             [in] coupling - coupling type to be used
//             [in] dcOffset - the DC offset to be used
//             [out] return - the min/max range determined; if there is no valid range for the
//                            coupling/dcOffset returns -1; if there is a failure returns -2
//
// Notes: This function takes the full set of ranges for the scope and narrows it if necessary based
//        on the dCOffset.  For this to work, the min/max DC offset vs range either has to be
//        monotonic or downard concave, otherwise the range would need to get split into multiple
//        segments.  This property is currently true for all scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

PS_RANGE CommonMethod(SCOPE_FAMILY_LT,GetMinRange)( PS_CHANNEL channel, PS_COUPLING coupling, double dcOffset )
{
    PS_RANGE retVal = -1;
    wstringstream fraStatusText;

    mMinDcOffset = mMaxDcOffset = 0.0;

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        if (!supportsChannelDcOffset || dcOffset == 0.0)
        {
            retVal = channelMinRanges[channel];
        }
        else
        {
#if defined (PS2000_IMPL) || defined (PS3000_IMPL) || defined (PS4000_IMPL) || defined (PS5000_IMPL)
            UNREFERENCED_PARAMETER(dcOffset); // These scope families have no DC offset capability
            UNREFERENCED_PARAMETER(coupling);
            retVal = -1; // Really shouldn't get here, but just in case
#else
            PICO_STATUS status;
            PS_RANGE couplingAdjustedMaxRange = coupling == PS_DC_50R ? channelMaxRanges[channel] - 2 : channelMaxRanges[channel];
#if defined(PS6000A_IMPL) || defined (PSOSPA_IMPL)
            double rangeMinDcOffset, rangeMaxDcOffset;
            coupling = coupling == PS_DC_50R ? (PS_COUPLING)PICO_DC_50OHM : coupling; // PS6000A and PSOSPA API sets this to 50 instead of 2
#else
            float rangeMinDcOffset, rangeMaxDcOffset;
#endif
            double attenFactor = channelAutoAttens[channel] ? 1.0 : attenScalingFactors[channelAttens[channel]]; // If the channel is in auto-attenuation mode, GetAnalogOffset already returns an attenuation adjusted value;
            for (PS_RANGE range = couplingAdjustedMaxRange; range >= channelMinRanges[channel]; range--)
            {
#if defined(PSOSPA_IMPL)
                LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( CommonApi( SCOPE_FAMILY_LT, GetAnalogueOffset ) ), handle, -(int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), (int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), PICO_PROBE_NONE_NV, (CommonEnum( SCOPE_FAMILY_UT, COUPLING ))coupling, &rangeMaxDcOffset, &rangeMinDcOffset );
                if (PICO_ERROR( CommonApi( SCOPE_FAMILY_LT, GetAnalogueOffset )(handle, -(int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), (int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), PICO_PROBE_NONE_NV, (CommonEnum( SCOPE_FAMILY_UT, COUPLING ))coupling, &rangeMaxDcOffset, &rangeMinDcOffset) ))
#else
                LOG_PICO_API_CALL(BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetAnalogueOffset)), handle, (CommonEnum(SCOPE_FAMILY_UT, RANGE))(range + channelRangeOffsets[channel]), (CommonEnum(SCOPE_FAMILY_UT, COUPLING))coupling, &rangeMaxDcOffset, &rangeMinDcOffset);
                if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetAnalogueOffset)(handle, (CommonEnum(SCOPE_FAMILY_UT, RANGE))(range + channelRangeOffsets[channel]), (CommonEnum(SCOPE_FAMILY_UT, COUPLING))coupling, &rangeMaxDcOffset, &rangeMinDcOffset)))
#endif
                {
                    fraStatusText.clear();
                    fraStatusText.str(L"");
                    fraStatusText << L"Fatal error: Failed to get min/max analog offset in GetMinRange: " << status;
                    LogMessage(fraStatusText.str());
                    retVal = -2;
                    break;
                }
                else if (dcOffset <= rangeMaxDcOffset * attenFactor &&
                         dcOffset >= rangeMinDcOffset * attenFactor)
                {
                    retVal = range;
                }
                mMinDcOffset = rangeMinDcOffset < mMinDcOffset ? rangeMinDcOffset : mMinDcOffset;
                mMaxDcOffset = rangeMaxDcOffset > mMaxDcOffset ? rangeMaxDcOffset : mMaxDcOffset;
            }
            mMinDcOffset *= attenFactor;
            mMaxDcOffset *= attenFactor;
#endif
        }
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to GetMinRange: " << channel;
        LogMessage(fraStatusText.str());
    }
    return retVal;
}

PS_RANGE CommonMethod(SCOPE_FAMILY_LT,GetMaxRange)( PS_CHANNEL channel, PS_COUPLING coupling, double dcOffset )
{
    PS_RANGE retVal = -1;
    wstringstream fraStatusText;

    mMinDcOffset = mMaxDcOffset = 0.0;

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        if (!supportsChannelDcOffset || dcOffset == 0.0)
        {
            retVal = coupling == PS_DC_50R ? channelMaxRanges[channel] - 2 : channelMaxRanges[channel];
        }
        else
        {
#if defined (PS2000_IMPL) || defined (PS3000_IMPL) || defined (PS4000_IMPL) || defined (PS5000_IMPL)
            UNREFERENCED_PARAMETER(dcOffset); // These scope families have no DC offset capability
            UNREFERENCED_PARAMETER(coupling);
            retVal = -1; // Really shouldn't get here, but just in case
#else
            PICO_STATUS status;
            PS_RANGE couplingAdjustedMaxRange = coupling == PS_DC_50R ? channelMaxRanges[channel] - 2 : channelMaxRanges[channel];
#if defined(PS6000A_IMPL) || defined (PSOSPA_IMPL)
            double rangeMinDcOffset, rangeMaxDcOffset;
            coupling = coupling == PS_DC_50R ? (PS_COUPLING)PICO_DC_50OHM : coupling; // PS6000A and PSOSPA API sets this to 50 instead of 2
#else
            float rangeMinDcOffset, rangeMaxDcOffset;
#endif
            double attenFactor = channelAutoAttens[channel] ? 1.0 : attenScalingFactors[channelAttens[channel]]; // If the channel is in auto-attenuation mode, GetAnalogOffset already returns an attenuation adjusted value;
            for (PS_RANGE range = channelMinRanges[channel]; range <= couplingAdjustedMaxRange; range++)
            {
#if defined(PSOSPA_IMPL)
                LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( CommonApi( SCOPE_FAMILY_LT, GetAnalogueOffset ) ), handle, -(int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), (int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), PICO_PROBE_NONE_NV, (CommonEnum( SCOPE_FAMILY_UT, COUPLING ))coupling, &rangeMaxDcOffset, &rangeMinDcOffset );
                if (PICO_ERROR( CommonApi( SCOPE_FAMILY_LT, GetAnalogueOffset )(handle, -(int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), (int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL), PICO_PROBE_NONE_NV, (CommonEnum( SCOPE_FAMILY_UT, COUPLING ))coupling, &rangeMaxDcOffset, &rangeMinDcOffset) ))
#else
                LOG_PICO_API_CALL(BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetAnalogueOffset)), handle, (CommonEnum(SCOPE_FAMILY_UT, RANGE))(range + channelRangeOffsets[channel]), (CommonEnum(SCOPE_FAMILY_UT, COUPLING))coupling, &rangeMaxDcOffset, &rangeMinDcOffset);
                if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetAnalogueOffset)(handle, (CommonEnum(SCOPE_FAMILY_UT, RANGE))(range + channelRangeOffsets[channel]), (CommonEnum(SCOPE_FAMILY_UT, COUPLING))coupling, &rangeMaxDcOffset, &rangeMinDcOffset)))
#endif
                {
                    fraStatusText.clear();
                    fraStatusText.str(L"");
                    fraStatusText << L"Fatal error: Failed to get min/max analog offset in GetMaxRange: " << status;
                    LogMessage(fraStatusText.str());
                    retVal = -2;
                    break;
                }
                else if (dcOffset <= rangeMaxDcOffset * attenFactor &&
                         dcOffset >= rangeMinDcOffset * attenFactor)
                {
                    retVal = range;
                }
                mMinDcOffset = rangeMinDcOffset < mMinDcOffset ? rangeMinDcOffset : mMinDcOffset;
                mMaxDcOffset = rangeMaxDcOffset > mMaxDcOffset ? rangeMaxDcOffset : mMaxDcOffset;
            }
            mMinDcOffset *= attenFactor;
            mMaxDcOffset *= attenFactor;
#endif
        }
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to GetMaxRange: " << channel;
        LogMessage(fraStatusText.str());
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetMinMaxChannelDcOffsets
//
// Purpose: Gets the minimum and maximum DC offset allowed across all ranges.
//
// Parameters: [out] minDcOffset - the minimum allowed DC Offset
//             [out] maxDcOffset - the maximum allowed DC Offset
//
// Notes: This function must be called after Get[Min|Max]Range to function properly.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void CommonMethod(SCOPE_FAMILY_LT, GetMinMaxChannelDcOffsets)(double& minDcOffset, double& maxDcOffset)
{
    minDcOffset = mMinDcOffset;
    maxDcOffset = mMaxDcOffset;
}

bool CommonMethod(SCOPE_FAMILY_LT,GetMaxValue)(int16_t& maxValue)
{
    bool retVal = true;
    int16_t minValue = 0;

#if defined(PS4000_IMPL)
    if (model == PS4262)
    {
        maxValue = PS4262_MAX_VALUE;
    }
    else
    {
        maxValue = PS4000_MAX_VALUE;
    }
#elif defined(PS2000_IMPL)
    maxValue = PS2000_MAX_VALUE;
#elif defined(PS3000_IMPL)
    maxValue = PS3000_MAX_VALUE;
#elif defined(PS5000_IMPL)
    maxValue = PS5000_MAX_VALUE;
#elif defined(PS6000_IMPL)
    maxValue = PS6000_MAX_VALUE;
#elif defined(PS6000A_IMPL) || defined (PSOSPA_IMPL)
    PICO_STATUS status;
    wstringstream fraStatusText;
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT,GetAdcLimits)), handle, currentResolution, &minValue, &maxValue );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetAdcLimits)(handle, (PICO_DEVICE_RESOLUTION)currentResolution, &minValue, &maxValue)))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to get maximum value: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
    else
    {
        maxValue = max(abs(minValue), abs(maxValue));
    }
#else
    PICO_STATUS status;
    wstringstream fraStatusText;
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT,MaximumValue)), handle, &maxValue );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT,MaximumValue)( handle, &maxValue )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to get maximum value: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#endif
    return retVal;
}

double CommonMethod(SCOPE_FAMILY_LT,GetMinFuncGenFreq)( void )
{
    return minFuncGenFreq;
}

double CommonMethod(SCOPE_FAMILY_LT,GetMaxFuncGenFreq)( void )
{
    return maxFuncGenFreq;
}

double CommonMethod(SCOPE_FAMILY_LT,GetMinFuncGenVpp)( void )
{
    return minFuncGenVpp;
}

double CommonMethod(SCOPE_FAMILY_LT,GetMinNonZeroFuncGenVpp)( void )
{
    // Current API for setting the signal generator has a resolution of 1 uV.
    // So, to avoid rounding the value to 0, add epsilon
    return max( minFuncGenVpp, 1e-6+std::numeric_limits<double>::epsilon() );
}

double CommonMethod(SCOPE_FAMILY_LT,GetMaxFuncGenVpp)( void )
{
    return maxFuncGenVpp;
}

bool CommonMethod(SCOPE_FAMILY_LT, SupportsChannelDcOffset)(void)
{
    return supportsChannelDcOffset;
}

bool CommonMethod(SCOPE_FAMILY_LT,IsCompatible)( void )
{
    return compatible;
}

bool CommonMethod(SCOPE_FAMILY_LT,RequiresExternalSignalGenerator)( void )
{
    return requiresExternalSignalGenerator;
}

const RANGE_INFO_T* CommonMethod(SCOPE_FAMILY_LT,GetRangeCaps)( PS_CHANNEL channel )
{
    RANGE_INFO_T* retVal = nullptr;
    wstringstream fraStatusText;

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        retVal = channelRangeInfo[channel].data();
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to GetRangeCaps: " << channel;
        LogMessage(fraStatusText.str());
    }

    return retVal;
}

const std::vector<RESOLUTION_T> CommonMethod(SCOPE_FAMILY_LT,GetResolutionCaps)( void )
{
    std::vector<RESOLUTION_T> res;
#if defined (PS3000_IMPL)
    if (model == PS3425 || model == PS3223 || model == PS3224 || model == PS3423 ||
        model == PS3424)
    {
        res.push_back(RESOLUTION_12BIT);
    }
    else if (model == PS3204 || model == PS3205 || model == PS3206)
    {
        res.push_back(RESOLUTION_8BIT);
    }
#elif defined (PS4000_IMPL)
    if (model == PS4223 || model == PS4423 || model == PS4224 || model == PS4424 ||
        model == PS4224IEPE || model == PS4226 || model == PS4227)
    {
        res.push_back(RESOLUTION_12BIT);
    }
    else if (model == PS4262)
    {
        res.push_back(RESOLUTION_16BIT);
    }
#elif defined(PS4000A_IMPL)
    if (model == PS4444)
    {
        res.push_back(RESOLUTION_14BIT);
    }
    res.push_back(RESOLUTION_12BIT);
#elif defined (PS5000A_IMPL)
    res.push_back(RESOLUTION_15BIT);
    res.push_back(RESOLUTION_12BIT);
    res.push_back(RESOLUTION_8BIT);
#elif defined (PS6000A_IMPL)
    if (model == PS6424E || model == PS6824E || model == PS6425E || model == PS6426E)
    {
        res.push_back(RESOLUTION_12BIT);
        res.push_back(RESOLUTION_10BIT);
    }
    res.push_back(RESOLUTION_8BIT);
#elif defined(PSOSPA_IMPL)
    if (model == PS3417E || model == PS3418E || model == PS3417EMSO || model == PS3418EMSO)
    {
        res.push_back( RESOLUTION_10BIT );
        res.push_back( RESOLUTION_8BIT );
    }
#else
    res.push_back(RESOLUTION_8BIT);
#endif
    return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetResolution
//
// Purpose: Sets vertical resolution for FlexRes scopes.
//
// Parameters: [in] resolution - vertical resolution
//             [out] actualResolution - the actual resolution set
//             [out] return - whether the call succeeded
//
// Notes: This is a null operation for non FlexRes scopes.  This will automatically adjust the
//        resolution lower based on channels for 6000A family scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT,SetResolution)(RESOLUTION_T resolution, RESOLUTION_T& actualResolution)
{
#if defined(PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000A_IMPL)  || defined (PSOSPA_IMPL)
#if defined(PS4000A_IMPL)
    if (model != PS4444)
    {
        actualResolution = resolution;
        return true;
    }
#elif defined(PS6000A_IMPL)
    if (model != PS6424E && model != PS6824E && model != PS6425E && model != PS6426E)
    {
        actualResolution = resolution;
        return true;
    }
#endif
    PICO_STATUS status;
    wstringstream fraStatusText;

    if (RESOLUTION_MAX == resolution)
    {
        resolution = (RESOLUTION_T)maxResolution;
    }
    else if (RESOLUTION_MIN == resolution && model == PS4444)
    {
        resolution = (RESOLUTION_T)RESOLUTION_12BIT;
    }
    // No need for handling RESOLUTION_MIN for PS6000E and current PSOSPA scopes since RESOLUTION_MIN = RESOLUTION_8BIT

#if defined (PS6000A_IMPL)
    resolution = AdjustResolutionForChannels(resolution);
#endif

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDeviceResolution)), handle, (CommonGenericEnum(SCOPE_FAMILY_UT,DEVICE_RESOLUTION))resolution );
    if( PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDeviceResolution)( handle, (CommonGenericEnum(SCOPE_FAMILY_UT,DEVICE_RESOLUTION))resolution )) )
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set device resolution: " << status;
        LogMessage( fraStatusText.str() );
        return false;
    }
    else
    {
        currentResolution = actualResolution = resolution;
        return true;
    }
#else
    actualResolution = resolution;
    return true;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetMaxResolutionForFrequency
//
// Purpose: Gets the maximum vertical resolution for FlexRes scopes compatible with the passed
//          sampling frequency.
//
// Parameters: [in] frequency - sampling frequency
//             [out] resolution - max resolution for frequency
//             [out] return - whether the call succeeded
//
// Notes: This is a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT,GetMaxResolutionForFrequency)(double sampleFrequency, RESOLUTION_T& resolution)
{
#if defined(PS4000A_IMPL) || defined (PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#if defined(PS4000A_IMPL)
    if (model != PS4444)
    {
        resolution = GetResolutionCaps()[0];
        return true;
    }
#elif defined(PS6000A_IMPL)
    if (model != PS6424E && model != PS6824E && model != PS6425E && model != PS6426E)
    {
        resolution = GetResolutionCaps()[0];
        return true;
    }
#endif
    wstringstream fraStatusText;
    bool retVal = false;
    double actualFrequency;
    RESOLUTION_T lowestResolution;
    uint32_t timebase;
    // Set resolution member variable to lowest for the scope so that GetTimebase is not limited by resolution
#if defined(PS4000A_IMPL)
    lowestResolution = RESOLUTION_12BIT;
#else
    lowestResolution = RESOLUTION_8BIT;
#endif
    if (GetTimebase(sampleFrequency, lowestResolution, &actualFrequency, &timebase))
    {
        if (GetMaxResolutionForTimebase(timebase, resolution))
        {
            retVal = true;
        }
        else
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to get max resolution for frequency.  Unable to get max resolution for timebase.";
            LogMessage( fraStatusText.str() );
        }
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to get max resolution for frequency.  Unable to get timebase for frequency.";
        LogMessage( fraStatusText.str() );
    }

    return retVal;
#else
    resolution = GetResolutionCaps()[0];
    return true;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetMaxResolutionForTimebase
//
// Purpose: Sets the maximum vertical resolution for FlexRes scopes given a fixed timebase.
//
// Parameters: [in] timebase - fixed timebase
//             [out] resolution - max resolution for timebase
//             [out] return - whether the call succeeded
//
// Notes: This is a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, GetMaxResolutionForTimebase)(uint32_t timebase, RESOLUTION_T& resolution)
{
#if defined(PS4000A_IMPL) || defined (PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#if defined(PS4000A_IMPL)
    if (model != PS4444)
    {
        resolution = GetResolutionCaps()[0];
        return true;
    }
#elif defined(PS6000A_IMPL)
    if (model != PS6424E && model != PS6824E && model != PS6425E && model != PS6426E)
    {
        resolution = GetResolutionCaps()[0];
        return true;
    }
#endif

#if defined(PSOSPA_IMPL)
    if (0 == (timebase % 800))
    {
        resolution = RESOLUTION_10BIT;
    }
    else
    {
        resolution = RESOLUTION_8BIT;
    }
#else
    uint8_t i = 0;
    for (i = 0; i < flexResMinTbMap.size(); i++)
    {
        if (timebase >= flexResMinTbMap[i].minTimebase)
        {
            break;
        }
    }
    resolution = (RESOLUTION_T)(flexResMinTbMap[min(i, flexResMinTbMap.size() - 1)].flexRes);
#endif

#if defined (PS6000A_IMPL)
    resolution = AdjustResolutionForChannels( resolution );
#endif

    return true;
#else
    resolution = GetResolutionCaps()[0];
    return true;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetChannelConfigurationForMinimumTimebase
//
// Purpose: Sets a channel configuration that enables the lowest timebase possible for FRA.
//
// Parameters: [out] return - whether the call succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT,SetChannelConfigurationForMinimumTimebase)( void )
{
    bool retVal = false;
    bool channelsDisabled = false;
    wstringstream fraStatusText;

    // Start by disabling all channels
    if (DisableAllDigitalChannels())
    {
        uint8_t chan;
        for (chan = PS_CHANNEL_A; chan < numAvailableChannels; chan++)
        {
            if (!DisableChannel((PS_CHANNEL)chan))
            {
                break;
            }
        }
        channelsDisabled = (chan == numAvailableChannels);
    }

    // Then enable only the channels required for optimal timebase
    if (channelsDisabled)
    {
#if defined(PS6000_IMPL) || defined(PS6000A_IMPL)
        // All scopes in this series have at least 4 channels with the simplest optimal configuration
        // being only channels A and C enabled.for 4 channel scopes and only channels A and E enabled
        // for 8 channel scopes.
        if (8 == numAvailableChannels)
        {
            if (SetupChannel(PS_CHANNEL_A, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_A], 0.0, 0.0) && SetupChannel(PS_CHANNEL_E, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_E], 0.0, 0.0))
            {
                retVal = true;
            }
            else
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to setup optimal channels for timebase.  Unable to enable channels A and E.";
                LogMessage( fraStatusText.str() );
            }
        }
        if (4 == numAvailableChannels)
        {
            if (SetupChannel(PS_CHANNEL_A, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_A], 0.0, 0.0) && SetupChannel(PS_CHANNEL_C, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_C], 0.0, 0.0))
            {
                retVal = true;
            }
            else
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to setup optimal channels for timebase.  Unable to enable channels A and C.";
                LogMessage( fraStatusText.str() );
            }
        }
#else
        if (SetupChannel(PS_CHANNEL_A, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_A], 0.0, 0.0) && SetupChannel(PS_CHANNEL_B, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_B], 0.0, 0.0))
        {
            retVal = true;
        }
        else
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to setup optimal channels for timebase.  Unable to enable channels A and B.";
            LogMessage(fraStatusText.str());
        }
#endif
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to setup optimal channels for timebase.  Unable to disable channels.";
        LogMessage( fraStatusText.str() );
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetMaxResolutionForTimebase
//
// Purpose: Gets the maximum vertical resolution for FlexRes scopes given a fixed timebase.
//
// Parameters: [in] timebase - fixed timebase
//             [out] return - the maximum resolution
//
// Notes: This is a effectively a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

RESOLUTION_T CommonMethod(SCOPE_FAMILY_LT,GetMaxResolutionForTimebase)(uint32_t timebase)
{
#if defined(PS4000A_IMPL) || defined (PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#if defined(PS4000A_IMPL)
    if (model != PS4444)
    {
        return RESOLUTION_MAX;
    }
#elif defined(PS6000A_IMPL)
    if (model != PS6424E && model != PS6824E && model != PS6425E && model != PS6426E)
    {
        return RESOLUTION_MAX;
    }
#endif

#if defined(PSOSPA_IMPL)
    RESOLUTION_T resolution;
    if (0 == (timebase % 800))
    {
        resolution = RESOLUTION_10BIT;
    }
    else
    {
        resolution = RESOLUTION_8BIT;
    }
    return resolution;
#else
    uint8_t i = 0;
    for( i = 0; i < flexResMinTbMap.size(); i++)
    {
        if(timebase >= flexResMinTbMap[i].minTimebase)
        {
            break;
        }
    }
    return (RESOLUTION_T)(flexResMinTbMap[min(i, flexResMinTbMap.size()-1)].flexRes);
#endif

#else
    return RESOLUTION_MAX;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetMinTimebaseForResolution
//
// Purpose: Gets the minimum timebase for a given vertical resolution for FlexRes scopes.
//
// Parameters: [in] resolution - vertical resolution
//             [out] return - the minimum timebase
//
// Notes: This is a effectively a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

uint32_t CommonMethod(SCOPE_FAMILY_LT,GetMinTimebaseForResolution)( RESOLUTION_T resolution )
{
#if defined(PS4000A_IMPL) || defined (PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#if defined(PS4000A_IMPL)
    if (model != PS4444)
    {
        return minTimebase;
    }
#elif defined(PS6000A_IMPL)
    if (model != PS6424E && model != PS6824E && model != PS6425E && model != PS6426E)
    {
        return minTimebase;
    }
#endif
    uint8_t i = 0;
    for( i = 0; i < flexResMinTbMap.size(); i++)
    {
        if (resolution == flexResMinTbMap[i].flexRes)
        {
            break;
        }
    }
    return (flexResMinTbMap[min(i, flexResMinTbMap.size()-1)].minTimebase);
#else
    return minTimebase;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetResolutionString
//
// Purpose: Gets a string representing the vertical resolution parameter
//
// Parameters: [in] resolution - the resolutiion setting for the scope
//             [out] return - string representing the current resolution setting
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

std::wstring CommonMethod(SCOPE_FAMILY_LT, GetResolutionString)(RESOLUTION_T resolution)
{
    std::wstring res;
#if defined (PS3000_IMPL)
    if (model == PS3425 || model == PS3223 || model == PS3224 || model == PS3423 ||
        model == PS3424)
    {
        res = L"12 bits";
    }
    else if (model == PS3204 || model == PS3205 || model == PS3206)
    {
        res = L"8 bits";
    }
    else
    {
        res = L"INVALID";
    }
#elif defined (PS4000_IMPL)
    if (model == PS4223 || model == PS4423 || model == PS4224 || model == PS4424 ||
        model == PS4224IEPE || model == PS4226 || model == PS4227)
    {
        res = L"12 bits";
    }
    else if (model == PS4262)
    {
        res = L"16 bits";
    }
    else
    {
        res = L"INVALID";
    }
#elif defined(PS4000A_IMPL)
    if (model == PS4824 || model == PS4224A || model == PS4424A || model == PS4824A)
    {
        res = L"12 bits";
    }
    else if (model == PS4444)
    {
        if (RESOLUTION_CURRENT == resolution)
        {
            resolution = (RESOLUTION_T)currentResolution;
        }
        res = resolution == PS4000A_DR_12BIT ? L"12 bits" : L"14 bits";
    }
    else
    {
        res = L"INVALID";
    }
#elif defined (PS5000A_IMPL)
    if (RESOLUTION_CURRENT == resolution)
    {
        resolution = (RESOLUTION_T)currentResolution;
    }
    switch (resolution)
    {
        case PS5000A_DR_15BIT:
            res = L"15 bits";
            break;
        case PS5000A_DR_12BIT:
            res = L"12 bits";
            break;
        case PS5000A_DR_8BIT:
            res = L"8 bits";
            break;
        default:
            res = L"INVALID";
            break;
    }
#elif defined (PS6000A_IMPL)
    if (model == PS6424E || model == PS6824E || model == PS6425E || model == PS6426E)
    {
        if (RESOLUTION_CURRENT == resolution)
        {
            resolution = (RESOLUTION_T)currentResolution;
        }
        switch (resolution)
        {
            case PICO_DR_12BIT:
                res = L"12 bits";
                break;
            case PICO_DR_10BIT:
                res = L"10 bits";
                break;
            case PICO_DR_8BIT:
                res = L"8 bits";
                break;
            default:
                res = L"INVALID";
                break;
        }
    }
    else
    {
        res = L"8 bits";
    }
#elif defined(PSOSPA_IMPL)
    if (RESOLUTION_CURRENT == resolution)
    {
        resolution = (RESOLUTION_T)currentResolution;
    }
    switch (resolution)
    {
        case PICO_DR_10BIT:
            res = L"10 bits";
            break;
        case PICO_DR_8BIT:
            res = L"8 bits";
            break;
        default:
            res = L"INVALID";
            break;
    }
#else
    res = L"8 bits";
#endif
    return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetClosestSignalGeneratorFrequency
//
// Purpose: Calculates the frequency closest to the requested frequency that the signal generator
//          is capable of generating.
//
// Parameters: [in] requestedFreq - desired frequency
//             [out] return - closest frequency the scope is capable of generating.
//
// Notes: Frequency generator capabilities are governed by the scope's DDS implementation.  The
//        frequency precision is encoded in the PicoScope object's implementation (signalGeneratorPrecision).
//
//        For PS3000, we need to implement special logic because the API takes an integer, but
//        the scope rounds that to the closest DDS-producable frequency (which is not an integer).
//        The frequency precision is normally far smaller than 1 (true for PS3000).  So, we find
//        the two integers adjacent to the requested frequency, then find the actual frequencies to
//        which those integers will get rounded by the API/scope.  Then we find which of those
//        actual frequencies is closest to the requested frequency.  When the program uses the
//        returned value to setup the generator, which is a double, the value will get truncated
//        back to an integer before passing to the API, but the API/scope will implement the
//        actual frequency desired.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

double CommonMethod(SCOPE_FAMILY_LT,GetClosestSignalGeneratorFrequency)( double requestedFreq )
{
#if defined(PS3000_IMPL) // Unique because the ps3000_set_siggen function takes integers for frequency
    double actualFreq;
    uint32_t N, M; // integers below and above the requested frequency
    double fClosestToN, fClosestToM; // frequencies closest to N and M that generator is capable of producing
    N = (uint32_t)requestedFreq;
    M = N+1;
    // Find closest signalGeneratorPrecision to each of N and M
    fClosestToN = round((double)N / signalGeneratorPrecision)*signalGeneratorPrecision;
    fClosestToM = round((double)M / signalGeneratorPrecision)*signalGeneratorPrecision;
    actualFreq = fabs(requestedFreq-fClosestToN) < fabs(requestedFreq-fClosestToM) ? fClosestToN : fClosestToM;
#else
    double actualFreq = llround(requestedFreq / signalGeneratorPrecision) * signalGeneratorPrecision;
#endif
    // Bound in case any rounding caused us to go outside the capabilities of the generator
    actualFreq = min(actualFreq, maxFuncGenFreq);
    actualFreq = max(actualFreq, minFuncGenFreq);
    return actualFreq;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetModel
//
// Purpose: Gets the model number of the scope as an enumeration
//
// Parameters: [out] model_ - scope model as enumeration type
//             [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, GetModel)(ScopeModel_T& model_)
{
    model_ = model;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetModel
//
// Purpose: Gets the model number of the scope as a string
//
// Parameters: [out] model - wide string to return the scope model
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#if defined NEW_PS_DRIVER_MODEL
#define INFO_STRING_LENGTH_ARG , &infoStringLength
#else
#define INFO_STRING_LENGTH_ARG
#endif

bool CommonMethod(SCOPE_FAMILY_LT,GetModel)( wstring &model )
{
    bool retVal;
    PICO_STATUS status;
    wstringstream fraStatusText;
#if defined(NEW_PS_DRIVER_MODEL)
    int16_t infoStringLength;
#endif
    int8_t scopeModel[32];
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetUnitInfo)), handle, scopeModel, sizeof(scopeModel) INFO_STRING_LENGTH_ARG, PICO_VARIANT_INFO );
    if( PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetUnitInfo)( handle, scopeModel, sizeof(scopeModel) INFO_STRING_LENGTH_ARG, PICO_VARIANT_INFO )) )
    {
        model = L"???";
        retVal = false;
    }
    else
    {
        wstringstream ssScopeModel;
        ssScopeModel << (char*)scopeModel;
        model.assign(ssScopeModel.str());
        retVal = true;
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetFamily
//
// Purpose: Gets the driver family of the scope as an enumeration
//
// Parameters: [out] family_ - scope driver family as enumeration type
//             [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod( SCOPE_FAMILY_LT, GetFamily )(ScopeDriverFamily_T& family_)
{
    family_ = family;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetSerialNumber
//
// Purpose: Gets the serial number of the scope
//
// Parameters: [out] sn - wide string to return the scope serial number
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT,GetSerialNumber)( wstring &sn )
{
    bool retVal;
    PICO_STATUS status;
    wstringstream fraStatusText;
#if defined(NEW_PS_DRIVER_MODEL)
    int16_t infoStringLength;
#endif
    int8_t scopeSN[32];
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetUnitInfo)), handle, scopeSN, sizeof(scopeSN) INFO_STRING_LENGTH_ARG, PICO_BATCH_AND_SERIAL );
    if( PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetUnitInfo)( handle, scopeSN, sizeof(scopeSN) INFO_STRING_LENGTH_ARG, PICO_BATCH_AND_SERIAL )) )
    {
        sn = L"???";
        retVal = false;
    }
    else
    {
        wstringstream ssScopeSN;
        ssScopeSN << (char*)scopeSN;
        sn.assign(ssScopeSN.str());
        retVal = true;
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetupChannel
//
// Purpose: Sets a scope's sampling channel settings
//
// Parameters: [in] channel - channel number
//             [in] coupling - AC or DC coupling
//             [in] range - voltage range
//             [in] offset - DC offset
//             [in] currentFrequency - the frequency of the stimulus, used to determine how to
//                                     configure bandwidth limiters
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(PS2000A_IMPL) || defined(PS3000A_IMPL) || defined (PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000_IMPL)
#define ANALOG_OFFSET_ARG , (float)offset
#elif defined(PS6000A_IMPL)
#define ANALOG_OFFSET_ARG , offset
#elif defined(PS4000_IMPL) || defined(PS2000_IMPL) || defined(PS3000_IMPL) || defined(PS5000_IMPL)
#define ANALOG_OFFSET_ARG
#endif

#if defined(PS6000_IMPL) || defined(PS6000A_IMPL)
#define BANDWIDTH_LIMITER_ARG , bwLimiter
#else
#define BANDWIDTH_LIMITER_ARG
#endif

#if defined(PS6000A_IMPL)
#define ENABLE_ARG
#else
#define ENABLE_ARG , TRUE
#endif

bool CommonMethod(SCOPE_FAMILY_LT, SetupChannel)( PS_CHANNEL channel, PS_COUPLING coupling, PS_RANGE range, double offset, double currentFrequency )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;

#if defined(PS6000_IMPL)
    PS6000_BANDWIDTH_LIMITER bwLimiter;
    if (model == PS6407)
    {
        bwLimiter = PS6000_BW_FULL; // PS6407 has no bandwidth limiter
    }
    else if (model == PS6404 || model == PS6404A || model == PS6404B || model == PS6404C || model == PS6404D)
    {
        bwLimiter = currentFrequency > 25e6 ? PS6000_BW_FULL : PS6000_BW_25MHZ;
    }
    else
    {
        bwLimiter = currentFrequency > 20e6 ? PS6000_BW_FULL : PS6000_BW_20MHZ;
    }
#elif defined(PS6000A_IMPL)
    PICO_BANDWIDTH_LIMITER bwLimiter;
    bwLimiter = currentFrequency > 20e6 ? PICO_BW_FULL : PICO_BW_20MHZ;

    if (coupling == PS_DC_50R)
    {
        coupling = (PS_COUPLING)PICO_DC_50OHM; // PS6000A API sets this to 50 instead of 2
    }
#elif defined(PSOSPA_IMPL)
    PICO_BANDWIDTH_LIMITER bwLimiter;
    if (model == PS3417E || model == PS3418E || model == PS3417EMSO || model == PS3418EMSO)
    {
        if (currentFrequency > 350e6)
        {
            bwLimiter = PICO_BW_FULL;
        }
        else if (currentFrequency > 200e6)
        {
            bwLimiter = PICO_BW_350MHZ;
        }
        else if (currentFrequency > 100e6)
        {
            bwLimiter = PICO_BW_200MHZ;
        }
        else if (currentFrequency > 50e6)
        {
            bwLimiter = PICO_BW_100MHZ;
        }
        else if (currentFrequency > 20e6)
        {
            bwLimiter = PICO_BW_50MHZ;
        }
        else
        {
            bwLimiter = PICO_BW_20MHZ;
        }

        // According to the datasheet, you cannot select 350MHz in 10 bit mode
        if (currentResolution == PICO_DR_10BIT && bwLimiter == PICO_BW_350MHZ)
        {
            bwLimiter = PICO_BW_200MHZ;
        }

        if (coupling == PS_DC_50R)
        {
            coupling = (PS_COUPLING)PICO_DC_50OHM; // PSOSPA API sets this to 50 instead of 2
        }
    }
    else
    {
        retVal = false;
    }
#endif

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        range += channelRangeOffsets[channel];
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to SetupChannel: " << channel;
        LogMessage(fraStatusText.str());
        retVal = false;
    }

    // When a smart probe scope is automatically managing attenuation, it will compensate in calls to SetChannel
    // Otherwise if we're manually managing the attenuation, we have to do it.
    if (!channelAutoAttens[channel])
    {
        offset /= attenScalingFactors[channelAttens[channel]];
    }

#if !defined(PSOSPA_IMPL)
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( CommonApi( SCOPE_FAMILY_LT, SetChannel ) ), handle, (CommonEnum( SCOPE_FAMILY_UT, CHANNEL ))channel ENABLE_ARG,
                                                                                       (CommonEnum( SCOPE_FAMILY_UT, COUPLING ))coupling,
                                                                                       (CommonEnum( SCOPE_FAMILY_UT, RANGE ))range ANALOG_OFFSET_ARG
                                                                                       BANDWIDTH_LIMITER_ARG );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetChannel)( handle, (CommonEnum( SCOPE_FAMILY_UT, CHANNEL ))channel ENABLE_ARG,
                                                          (CommonEnum( SCOPE_FAMILY_UT, COUPLING ))coupling,
                                                          (CommonEnum( SCOPE_FAMILY_UT, RANGE ))range ANALOG_OFFSET_ARG
                                                          BANDWIDTH_LIMITER_ARG) ))
#else
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetChannel)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))channel,
                                                                                   (CommonEnum(SCOPE_FAMILY_UT,COUPLING))coupling,
                                                                                   -(int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL),
                                                                                   (int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL),
                                                                                   PICO_PROBE_NONE_NV, offset, bwLimiter );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetChannel)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))channel,
                                                           (CommonEnum(SCOPE_FAMILY_UT,COUPLING))coupling,
                                                           -(int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL),
                                                           (int64_t)(base1xRangeInfo[range].rangeVolts * 1000000000LL),
                                                           PICO_PROBE_NONE_NV, offset, bwLimiter)))
#endif
    {
        if (status != PICO_WARNING_PROBE_CHANNEL_OUT_OF_SYNC) // There are cases where this function is called and we don't care about the ranges
        {
            fraStatusText << L"Fatal error: Failed to setup input channel: " << status;
            LogMessage(fraStatusText.str());
            retVal = false;
        }
    }

#if defined(PS5000A_IMPL)
    if (retVal)
    {
        PS5000A_BANDWIDTH_LIMITER bwLimiter = currentFrequency > 20e6 ? PS5000A_BW_FULL : PS5000A_BW_20MHZ;
        LOG_PICO_API_CALL( L"ps5000aSetBandwidthFilter", handle, (PS5000A_CHANNEL)channel, bwLimiter );
        if (0 != (status = ps5000aSetBandwidthFilter( handle, (PS5000A_CHANNEL)channel, bwLimiter )))
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to setup input channel bandwidth limiter: " << status;
            LogMessage( fraStatusText.str() );
            retVal = false;
        }
    }
#elif defined(PS4000_IMPL)
    if (model == PS4262)
    {
        if (retVal)
        {
            int16_t enable = currentFrequency > 200e3 ? FALSE : TRUE;
            LOG_PICO_API_CALL( L"ps4000SetBwFilter", handle, (PS4000_CHANNEL)channel, enable );
            if (0 != (status = ps4000SetBwFilter( handle, (PS4000_CHANNEL)channel, enable )))
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to setup input channel bandwidth limiter: " << status;
                LogMessage( fraStatusText.str() );
                retVal = false;
            }
        }
    }
#elif defined(PS4000A_IMPL)
    if (model == PS4444)
    {
        PS4000A_BANDWIDTH_LIMITER bwLimiter;
        if (currentFrequency < 1.0e6)
        {
            if (currentFrequency < 100.0e3)
            {
                bwLimiter = PS4000A_BW_100KHZ;
            }
            else
            {
                bwLimiter = PS4000A_BW_1MHZ;
            }
        }
        else
        {
            bwLimiter = PS4000A_BW_FULL;
        }

        LOG_PICO_API_CALL( L"ps4000aSetBandwidthFilter", handle, (PS4000A_CHANNEL)channel, bwLimiter );
        if (0 != (status = ps4000aSetBandwidthFilter( handle, (PS4000A_CHANNEL)channel, bwLimiter )))
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to setup input channel bandwidth limiter: " << status;
            LogMessage( fraStatusText.str() );
            retVal = false;
        }
    }
#elif defined(PS3000A_IMPL)
    if (model == PS3404A || model == PS3404B || model == PS3405A || model == PS3405B || model == PS3406A || model == PS3406B ||
        model == PS3203D || model == PS3203DMSO || model == PS3204D || model == PS3204DMSO || model == PS3205D || model == PS3205DMSO || model == PS3206D || model == PS3206DMSO ||
        model == PS3403D || model == PS3403DMSO || model == PS3404D || model == PS3404DMSO || model == PS3405D || model == PS3405DMSO || model == PS3406D || model == PS3406DMSO)
    {
        if (retVal)
        {
            PS3000A_BANDWIDTH_LIMITER bwLimiter = currentFrequency > 20e6 ? PS3000A_BW_FULL : PS3000A_BW_20MHZ;
            LOG_PICO_API_CALL( L"ps3000aSetBandwidthFilter", handle, (PS3000A_CHANNEL)channel, bwLimiter );
            if(0 != (status = ps3000aSetBandwidthFilter( handle, (PS3000A_CHANNEL)channel, bwLimiter )))
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to setup input channel bandwidth limiter: " << status;
                LogMessage( fraStatusText.str() );
                retVal = false;
            }
        }
    }
#endif

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method DisableAllDigitalChannels
//
// Purpose: Disable all digital channels
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: When digital channels are on, they can reduce available sampling frequency
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, DisableAllDigitalChannels)(void)
{
    bool retVal = true;
#if defined(PS2000A_IMPL) || defined(PS3000A_IMPL) || defined (PS5000A_IMPL)
    PICO_STATUS status;
    wstringstream fraStatusText;

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDigitalPort)), handle, CommonEnum(SCOPE_FAMILY_UT,DIGITAL_PORT0), 0, 0 );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDigitalPort)( handle, CommonEnum(SCOPE_FAMILY_UT,DIGITAL_PORT0), 0, 0 )))
    {
        if (PICO_NOT_USED != status) // PICO_NOT_USED is returned when the scope has no digital channels.
        {
            fraStatusText << L"WARNING: Failed to disable digital channels 0-7: " << status;
            LogMessage(fraStatusText.str(), FRA_WARNING);
            retVal = false;
        }
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDigitalPort)), handle, CommonEnum(SCOPE_FAMILY_UT,DIGITAL_PORT1), 0, 0 );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDigitalPort)( handle, CommonEnum(SCOPE_FAMILY_UT,DIGITAL_PORT1), 0, 0 )))
    {
        if (PICO_NOT_USED != status) // PICO_NOT_USED is returned when the scope has no digital channels.
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"WARNING: Failed to disable digital channels 8-15: " << status;
            LogMessage(fraStatusText.str(), FRA_WARNING);
            retVal = false;
        }
    }
#elif defined (PS6000A_IMPL)
    PICO_STATUS status;
    wstringstream fraStatusText;

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aSetDigitalPortOff), handle, PICO_PORT0 );
    if (PICO_ERROR(ps6000aSetDigitalPortOff( handle, PICO_PORT0 )))
    {
        if (PICO_NOT_USED != status) // PICO_NOT_USED is returned when the scope has no digital channels.
        {
            fraStatusText << L"WARNING: Failed to disable digital channels 0-7: " << status;
            LogMessage(fraStatusText.str(), FRA_WARNING);
            retVal = false;
        }
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aSetDigitalPortOff), handle, PICO_PORT1 );
    if (PICO_ERROR(ps6000aSetDigitalPortOff( handle, PICO_PORT1 )))
    {
        if (PICO_NOT_USED != status) // PICO_NOT_USED is returned when the scope has no digital channels.
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"WARNING: Failed to disable digital channels 8-15: " << status;
            LogMessage(fraStatusText.str(), FRA_WARNING);
            retVal = false;
        }
    }
#endif
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method DisableChannel
//
// Purpose: Disable a channel
//
// Parameters: [in] channel - channel to be disabled 
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#undef BANDWIDTH_LIMITER_ARG
#if defined(PS6000_IMPL)
#define BANDWIDTH_LIMITER_ARG , PS6000_BW_FULL
#else
#define BANDWIDTH_LIMITER_ARG
#endif

bool CommonMethod(SCOPE_FAMILY_LT, DisableChannel)( PS_CHANNEL channel )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;
    float offset = 0.0;

#if defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetChannelOff)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))channel);
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetChannelOff)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))channel)))
#else
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetChannel)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))channel,
                                                                                   FALSE, (CommonEnum(SCOPE_FAMILY_UT,COUPLING))0,
                                                                                   (CommonEnum(SCOPE_FAMILY_UT,RANGE))0 ANALOG_OFFSET_ARG
                                                                                   BANDWIDTH_LIMITER_ARG );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetChannel)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))channel,
                                                           FALSE, (CommonEnum(SCOPE_FAMILY_UT,COUPLING))0,
                                                           (CommonEnum(SCOPE_FAMILY_UT,RANGE))0 ANALOG_OFFSET_ARG
                                                           BANDWIDTH_LIMITER_ARG )))
#endif
    {
        fraStatusText << L"Fatal error: Failed to disable channel: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetSignalGenerator
//
// Purpose: Setup the signal generator to generate a signal with the given amplitude and frequency
//
// Parameters: [in] vPP - voltage peak-to-peak in volts
//             [in] offset
//             [in] frequency - frequency to generate in Hz
//             [out] return - whether the function succeeded
//
// Notes: vPP and offset are ignored for PS3000 series scopes
//        If frequency or vPP is 0.0, sets the generator to 0V DC
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#define PS4000_WAVE_TYPE WAVE_TYPE
#define PS5000_WAVE_TYPE WAVE_TYPE

bool CommonMethod(SCOPE_FAMILY_LT, SetSignalGenerator)( double vPP, double offset, double frequency )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;
#if !defined(PS3000_IMPL) && !defined(PS6000A_IMPL) && !defined(PSOSPA_IMPL)
    CommonEnum(SCOPE_FAMILY_UT, WAVE_TYPE) waveType;
    if (vPP == 0.0 || frequency == 0.0) // To disable the frequency generator, fake it by setting the signal to DC 0V
    {
        waveType = CommonDC(SCOPE_FAMILY_UT);
        offset = 0.0;
    }
    else
    {
        waveType = CommonSine(SCOPE_FAMILY_UT);
    }
#endif
#if defined(PS2000_IMPL)
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltIn)), handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, (float)frequency, (float)maxFuncGenFreq,
                                                                                         0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT), 0 );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltIn)( handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, (float)frequency, (float)maxFuncGenFreq,
                                                                 0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT), 0 )))
#elif defined(PS3000_IMPL)
    UNREFERENCED_PARAMETER(vPP);
    UNREFERENCED_PARAMETER(offset);
    // Truncation of frequency to 0 will disable the generator.  However, for PS3000 this function should never be called with values less than 100 Hz, unless
    // actually attempting to disable.
    int32_t intFreq = saturation_cast<int32_t,double>(frequency);
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _set_siggen)), handle, CommonSine(SCOPE_FAMILY_UT), intFreq, intFreq, 1.0, 1, 0, 0 );
    if (0 == (status = CommonApi(SCOPE_FAMILY_LT, _set_siggen)( handle, CommonSine(SCOPE_FAMILY_UT), intFreq, intFreq, 1.0, 1, 0, 0 )))
#elif defined(PS4000_IMPL)
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltIn)), handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, (float)frequency, (float)maxFuncGenFreq,
                                                                                         0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT),
                                                                                         CommonEsOff(SCOPE_FAMILY_UT), 
                                                                                         0, 0, (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_TYPE))0,
                                                                                         (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_SOURCE))CommonSigGenNone(SCOPE_FAMILY_UT), 0 );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltIn)( handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, (float)frequency, (float)maxFuncGenFreq,
                                                                 0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT),
                                                                 CommonEsOff(SCOPE_FAMILY_UT), 
                                                                 0, 0, (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_TYPE))0,
                                                                 (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_SOURCE))CommonSigGenNone(SCOPE_FAMILY_UT), 0 )))
#elif defined(PS4000A_IMPL)
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltIn)), handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, frequency, maxFuncGenFreq,
                                                                                         0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT),
                                                                                         CommonEsOff(SCOPE_FAMILY_UT),
                                                                                         0, 0, (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_TYPE))0,
                                                                                         (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_SOURCE))CommonSigGenNone(SCOPE_FAMILY_UT), 0 );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltIn)( handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, frequency, maxFuncGenFreq,
                                                                 0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT),
                                                                 CommonEsOff(SCOPE_FAMILY_UT),
                                                                 0, 0, (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_TYPE))0,
                                                                (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_SOURCE))CommonSigGenNone(SCOPE_FAMILY_UT), 0 )))
#elif defined(PS6000A_IMPL)
    bool ps6000aSigGenFail = true;
    double actualFrequency = frequency;
    double stopFrequency = frequency;
    double frequencyIncrement = 1.0;
    double dwellTime = 1.0;
    int16_t enable;

    if (vPP == 0.0 || frequency == 0.0)
    {
        enable = 0;
        frequency = 1.0; // This driver sometimes reports PICO_SIGGEN_FREQUENCY_OUT_OF_RANGE if frequency is set to 0
    }
    else
    {
        enable = 1;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aSigGenWaveform), handle, PICO_SINE, NULL, 0 );
    if (!PICO_ERROR(ps6000aSigGenWaveform( handle, PICO_SINE, NULL, 0 )))
    {
        LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aSigGenRange), handle, vPP, offset );
        if (!PICO_ERROR(ps6000aSigGenRange( handle, vPP, offset )))
        {
            LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aSigGenFrequency), handle, frequency );
            if (!PICO_ERROR(ps6000aSigGenFrequency( handle, frequency )))
            {
                LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(ps6000aSigGenApply), handle, enable, 0, 0, 0, 0, &actualFrequency, &stopFrequency, &frequencyIncrement, &dwellTime );
                if (!PICO_ERROR(ps6000aSigGenApply(handle, enable, 0, 0, 0, 0, &actualFrequency, &stopFrequency, &frequencyIncrement, &dwellTime)))
                {
                    ps6000aSigGenFail = false;
                }
            }
        }
    }
    if (ps6000aSigGenFail)
#elif defined(PSOSPA_IMPL)
    bool psospaSigGenFail = true;
    double actualFrequency = frequency;
    double stopFrequency = frequency;
    double frequencyIncrement = 1.0;
    double dwellTime = 1.0;
    int16_t enable;

    if (vPP == 0.0 || frequency == 0.0)
    {
        enable = 0;
        frequency = 1.0; // This driver sometimes reports PICO_SIGGEN_FREQUENCY_OUT_OF_RANGE if frequency is set to 0
    }
    else
    {
        enable = 1;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( psospaSigGenWaveform ), handle, PICO_SINE, NULL, 0 );
    if (!PICO_ERROR( psospaSigGenWaveform( handle, PICO_SINE, NULL, 0 ) ))
    {
        LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( psospaSigGenRange ), handle, vPP, offset );
        if (!PICO_ERROR( psospaSigGenRange( handle, vPP, offset ) ))
        {
            LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( psospaSigGenFrequency ), handle, frequency );
            if (!PICO_ERROR( psospaSigGenFrequency( handle, frequency ) ))
            {
                LOG_PICO_API_CALL( BOOST_PP_STRINGIZE( psospaSigGenApply ), handle, enable, 0, 0, &actualFrequency, &stopFrequency, &frequencyIncrement, &dwellTime );
                if (!PICO_ERROR( psospaSigGenApply( handle, enable, 0, 0, &actualFrequency, &stopFrequency, &frequencyIncrement, &dwellTime ) ))
                {
                    psospaSigGenFail = false;
                }
            }
        }
    }
    if (psospaSigGenFail)
#else
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltInV2)), handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, frequency, maxFuncGenFreq,
                                                                                           0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT),
                                                                                           CommonEsOff(SCOPE_FAMILY_UT),
                                                                                           0, 0, (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_TYPE))0,
                                                                                           (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_SOURCE))CommonSigGenNone(SCOPE_FAMILY_UT), 0 );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetSigGenBuiltInV2)( handle, (int32_t)(offset*1.0e6), (uint32_t)(vPP*1.0e6), waveType, frequency, maxFuncGenFreq,
                                                                   0.0, 1.0, CommonSweepUp(SCOPE_FAMILY_UT),
                                                                   CommonEsOff(SCOPE_FAMILY_UT),
                                                                   0, 0, (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_TYPE))0,
                                                                   (CommonEnum(SCOPE_FAMILY_UT,SIGGEN_TRIG_SOURCE))CommonSigGenNone(SCOPE_FAMILY_UT), 0 )))
#endif
    {
        fraStatusText << L"Fatal error: Failed to setup stimulus signal: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#if defined(PS3000_IMPL)
    if (status != intFreq)
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Intended and actual stimulus signal frequency don't match: Intended = " << intFreq << ", Actual = " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#endif

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method DisableSignalGenerator
//
// Purpose: Disable the signal generator
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, DisableSignalGenerator)( void )
{
    bool retVal = true;
    wstringstream fraStatusText;
    if (!SetSignalGenerator( 0.0, 0.0, 0.0 ))
    {
        fraStatusText << L"Error: Failed to disable signal generator.";
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetMaxSamples
//
// Purpose: Gets the maximum possible number of samples per channel per the scope's reported
//          buffer capabilities
//
// Parameters: [out] pMaxSamples - the maximum samples per channel
//             [in] timebase - the timebase that will get used in the capture
//             [in] resolution - the resolution that will get used in the capture
//                               do not pass RESOLUTION_AUTO
//             [out] return - whether the function succeeded
//
// Notes: - Do not pass RESOLUTION_AUTO
//        - Must pass a timebase compatible with the current (not passed) resolution setting
//          of the device.
//        - Some manuals indicate that timebase affects sample memory due to some memory being
//          used for some overhead.  Therefore we'll provide a timebase.
//        - Even though the PS6000A interface returns a uint64_t for maxSamples, there are
//          currently no scopes with buffers > 4GS, so there is no need to switch to a uint64_t
//          for pMaxSamples.
//        - For future consideration: There are other APIs for PS6000A that may be more useful
//          here (require passing less unnecessary parameters); e.g. ps6000aMemorySegments or
//          ps6000aGetMaximumAvailableMemory
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#if defined (PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#define TIME_UNITS_ARG
#define OVERSAMPLE_ARG
#define SEGMENT_INDEX_ARG , 0
#elif defined(PS2000A_IMPL) || defined(PS3000A_IMPL) || defined(PS4000_IMPL) || defined(PS5000_IMPL) || defined(PS6000_IMPL)
#define TIME_UNITS_ARG
#define OVERSAMPLE_ARG 1,
#define SEGMENT_INDEX_ARG , 0
#elif defined(PS2000_IMPL) || defined(PS3000_IMPL)
#define TIME_UNITS_ARG NULL,
#define OVERSAMPLE_ARG 1,
#define SEGMENT_INDEX_ARG
#endif

bool CommonMethod(SCOPE_FAMILY_LT, GetMaxSamples)( uint32_t* pMaxSamples, RESOLUTION_T resolution, uint32_t timebase )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;

#if defined (PS6000A_IMPL) || defined(PSOSPA_IMPL)
    uint64_t maxSamples;
#elif defined(PS6000_IMPL)
    uint32_t maxSamples;
#else
    int32_t maxSamples;
#endif

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetTimebase)), handle, timebase, 2, NULL, TIME_UNITS_ARG OVERSAMPLE_ARG &maxSamples SEGMENT_INDEX_ARG );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetTimebase)( handle, timebase, 2, NULL, TIME_UNITS_ARG OVERSAMPLE_ARG &maxSamples SEGMENT_INDEX_ARG )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to determine sample buffer size: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
    else if (maxSamples == 0)
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to determine sample buffer size.  GetTimebase returned 0";
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
    else
    {
        // Adjust for mismatch in requested resolution and device's actual set resolution
        // Currently the PS4444 is the only FlexRes scope in the PS4000A family and it does
        // not have an 8 bit mode, but we'll code for this anyway in case another PS4000A
        // FlexRes scope is addded that does have both 8 bit and higher resolutions.
#if defined(PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#if defined(PS4000A_IMPL)
        if (model == PS4444)
        {
#elif defined(PS6000A_IMPL)
        if (model == PS6424E || model == PS6824E || model == PS6425E || model == PS6426E)
        {
#endif
            if (RESOLUTION_CURRENT != resolution)
            {
                if (RESOLUTION_MAX == resolution)
                {
                    resolution = (RESOLUTION_T)maxResolution;
                }

                CommonGenericEnum(SCOPE_FAMILY_UT,DEVICE_RESOLUTION) deviceResolution;
                LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetDeviceResolution)), handle, &deviceResolution );
                if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetDeviceResolution)( handle, &deviceResolution )))
                {
                    fraStatusText.clear();
                    fraStatusText.str(L"");
                    fraStatusText << L"Fatal error: Failed to determine sample buffer size.  Failed to determine device resolution: " << status;
                    LogMessage( fraStatusText.str() );
                    retVal = false;
                }
                else
                {
                    if (resolution == CommonGenericEnum(SCOPE_FAMILY_UT,DR_8BIT) && deviceResolution > CommonGenericEnum(SCOPE_FAMILY_UT,DR_8BIT))
                    {
                        maxSamples*=2;
                    }
                    else if (deviceResolution == CommonGenericEnum(SCOPE_FAMILY_UT,DR_8BIT) && resolution > CommonGenericEnum(SCOPE_FAMILY_UT,DR_8BIT))
                    {
                        maxSamples/=2;
                    }
                }
            }
#if defined(PS4000A_IMPL) || defined (PS6000A_IMPL)
        }
#endif
#endif
        *pMaxSamples = (uint32_t)maxSamples;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method AllocateBuffers
//
// Purpose: Allocates buffers to hold samples transferred from the scope
//
// Parameters: N/A
//
// Notes: Where [Device Family]MemorySegments is available it will get used, otherwise we'll use
//        GetTimebase.
//
///////////////////////////////////////////////////////////////////////////////////////////////////
bool CommonMethod(SCOPE_FAMILY_LT,AllocateBuffers)(void)
{
    PICO_STATUS status;
    wstringstream fraStatusText;
    bool retVal = false;
    bool channelsSetup = false;
    bool sizeDetermined = false;
    uint32_t timebase = 0;
    uint32_t timebaseLimit = 16;
    uint32_t highestMaxSamples = 0;
    uint32_t bufferSize;

#if defined (PS6000A_IMPL) || defined(PSOSPA_IMPL)
    uint64_t maxSamples;
#elif defined(PS6000_IMPL)
    uint32_t maxSamples;
#else
    int32_t maxSamples;
#endif

#if defined (PS2000_IMPL) || defined (PS3000_IMPL)
    // Enable two channels guaranteed to be available, parameters aren't critical; Disable all other channels
    if (SetupChannel(PS_CHANNEL_A, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_A], 0.0, 0.0) && SetupChannel(PS_CHANNEL_B, PS_AC, (PS_RANGE)channelMinRanges[PS_CHANNEL_B], 0.0, 0.0))
    {
        uint8_t numChannels = GetNumChannels();
        uint8_t chan;
        for (chan = PS_CHANNEL_B+1; chan < numChannels; chan++)
        {
            if (!DisableChannel((PS_CHANNEL)chan))
            {
                break;
            }
        }
        channelsSetup = (chan == numChannels);
    }
    if (channelsSetup)
    {
        for (timebase = 0; timebase <= timebaseLimit; timebase++ )
        {
            LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetTimebase)), handle, timebase, 2, NULL, TIME_UNITS_ARG OVERSAMPLE_ARG &maxSamples SEGMENT_INDEX_ARG );
            if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetTimebase)(handle, timebase, 2, NULL, TIME_UNITS_ARG OVERSAMPLE_ARG &maxSamples SEGMENT_INDEX_ARG)))
            {
                if (PICO_INVALID_TIMEBASE != status) // Invalid timebase is OK since we're guessing
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
            else
            {
                highestMaxSamples = maxSamples;
                break;
            }
        }

        if (highestMaxSamples != 0)
        {
            LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetTimebase)), handle, maxTimebase, 2, NULL, TIME_UNITS_ARG OVERSAMPLE_ARG &maxSamples SEGMENT_INDEX_ARG );
            if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetTimebase)(handle, maxTimebase, 2, NULL, TIME_UNITS_ARG OVERSAMPLE_ARG &maxSamples SEGMENT_INDEX_ARG)) || maxTimebase == 0)
            {
                // This timebase should always be valid
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to determine sample buffer size for max timebase: " << status << L", " << maxTimebase;
                LogMessage( fraStatusText.str() );
            }
            else
            {
                if ((uint32_t)maxSamples > highestMaxSamples)
                {
                    highestMaxSamples = (uint32_t)maxSamples;
                }
                sizeDetermined = true;
            }
        }
        else
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to determine sample buffer size for small timebase: " << status;
            LogMessage( fraStatusText.str() );
        }
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to determine sample buffer size.  Unable to setup channels.";
        LogMessage( fraStatusText.str() );
    }
#else
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, MemorySegments)), handle, 1, &maxSamples );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, MemorySegments)(handle, 1, &maxSamples)))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to determine sample buffer size during buffer allocation.  Failed to set memory segments: " << status;
        LogMessage( fraStatusText.str() );
    }
    else
    {
        highestMaxSamples = (uint32_t)maxSamples / 2; // MemorySegments reports samples available across all channels

        // Adjust for possibility that the device resolution is not at minimum
        // Currently the PS4444 is the only FlexRes scope in the PS4000A family and it does
        // not have an 8 bit mode, but we'll code for this anyway in case another PS4000A
        // FlexRes scope is added that does have both 8 bit and higher resolutions.
#if defined(PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA) // A family with FlexRes
#if defined(PS4000A_IMPL) // A family with mixed FlexRes
        if (model == PS4444)
        {
#elif defined(PS6000A_IMPL) // A family with mixed FlexRes
        if (model == PS6424E || model == PS6824E || model == PS6425E || model == PS6426E)
        {
#endif
            CommonGenericEnum(SCOPE_FAMILY_UT,DEVICE_RESOLUTION) deviceResolution;
            LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetDeviceResolution)), handle, &deviceResolution );
            if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetDeviceResolution)( handle, &deviceResolution )))
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to determine sample buffer size during buffer allocation.  Failed to determine device resolution: " << status;
                LogMessage( fraStatusText.str() );
            }
            else
            {
                if (deviceResolution > CommonGenericEnum(SCOPE_FAMILY_UT, DR_8BIT))
                {
                    highestMaxSamples *= 2;
                }
                sizeDetermined = true;
            }
#if defined(PS4000A_IMPL) || defined (PS6000A_IMPL) // A family with mixed FlexRes
        }
        else
        {
            sizeDetermined = true;
        }
#endif
#else // Not a family with FlexRes
        sizeDetermined = true;
#endif
    }
#endif
    if (sizeDetermined)
    {
        bufferSize = min(maxDataRequestSize, highestMaxSamples);
        try
        {
            mInputBuffer.resize(bufferSize);
            mOutputBuffer.resize(bufferSize);
            retVal = true;
        }
        catch (std::exception ex)
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to allocate sample buffers: " << ex.what();
            LogMessage(fraStatusText.str());
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method DisableChannelTriggers
//
// Purpose: Disables any triggers for the scope's sampling channels
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(PS4000A_IMPL)
#define CONDITIONS_INFO , PS4000A_CLEAR
#define AUTO_TRIGGER_MILLISECONDS , 0
#define AUX_OUTPUT_ENABLE , 0
#elif defined(PS5000A_IMPL)
#define CONDITIONS_INFO , PS5000A_CLEAR
#define SetTriggerChannelConditions SetTriggerChannelConditionsV2
#define SetTriggerChannelProperties SetTriggerChannelPropertiesV2
#define AUTO_TRIGGER_MILLISECONDS
#define AUX_OUTPUT_ENABLE , 0
#elif defined(PS6000A_IMPL)
#define CONDITIONS_INFO , PICO_CLEAR_ALL
#define AUTO_TRIGGER_MILLISECONDS , 0
#define AUX_OUTPUT_ENABLE , 0
#elif defined(PSOSPA_IMPL)
#define CONDITIONS_INFO , PICO_CLEAR_ALL
#define AUTO_TRIGGER_MILLISECONDS , 0
#define AUX_OUTPUT_ENABLE
#else
#define CONDITIONS_INFO
#define AUTO_TRIGGER_MILLISECONDS , 0
#define AUX_OUTPUT_ENABLE , 0
#endif

bool CommonMethod(SCOPE_FAMILY_LT, DisableChannelTriggers)( void )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;

#if defined(NEW_PS_DRIVER_MODEL)
    // It's possible that only one of these is necessary.  But it's probably good practice to take some action to disable
    // triggers rather than rely on a scopes default state.
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetTriggerChannelConditions)), handle, NULL, 0 CONDITIONS_INFO );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetTriggerChannelConditions)( handle, NULL, 0 CONDITIONS_INFO)))
    {
        fraStatusText << L"Fatal error: Failed to setup channel trigger conditions: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetTriggerChannelProperties)), handle, NULL, 0 AUX_OUTPUT_ENABLE AUTO_TRIGGER_MILLISECONDS );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetTriggerChannelProperties)( handle, NULL, 0 AUX_OUTPUT_ENABLE AUTO_TRIGGER_MILLISECONDS )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to setup channel trigger properties: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#else
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _set_trigger)), handle, CommonEnum(SCOPE_FAMILY_UT, NONE), 0, CommonEnum(SCOPE_FAMILY_UT, RISING), 0, 0 );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, _set_trigger)( handle, CommonEnum(SCOPE_FAMILY_UT, NONE), 0, CommonEnum(SCOPE_FAMILY_UT, RISING), 0, 0 )))
    {
        fraStatusText << L"Fatal error: Failed to setup channel trigger properties: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#endif

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method RunBlock
//
// Purpose: Starts a sample capture on the scope using block mode
//
// Parameters: [in] numSamples - number of samples to capture
//             [in] timebase - sampling timebase for the scope
//             [out] pfTimeIndisposedMs - estimated time it will take to capture the block of data 
//                                        in milliseconds
//             [in] lpReady - pointer to the callback function to be called when the block
//                            has been captured
//             [in] pParameter - parameter to be based to the callback function
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#define TIME_INDISPOSED_ARG pfTimeIndisposedMs
#else
#define TIME_INDISPOSED_ARG &iTimeIndisposedMs
#endif

bool CommonMethod(SCOPE_FAMILY_LT, RunBlock)( int32_t numSamples, uint32_t timebase, double *pfTimeIndisposedMs, psBlockReady lpReady, void *pParameter )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;
#if !defined(PS6000A_IMPL) && !defined(PSOSPA_IMPL)
    int32_t iTimeIndisposedMs;
#endif

    // Setup block mode
#if defined(NEW_PS_DRIVER_MODEL)
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, RunBlock)), handle, 0, numSamples, timebase, OVERSAMPLE_ARG TIME_INDISPOSED_ARG, 0,
                                                                                 (CommonReadyCB(SCOPE_FAMILY_LT))lpReady, pParameter );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, RunBlock)( handle, 0, numSamples, timebase, OVERSAMPLE_ARG TIME_INDISPOSED_ARG, 0,
                                                         (CommonReadyCB(SCOPE_FAMILY_LT))lpReady, pParameter)))
#else
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _run_block)), handle, numSamples, timebase, 1, TIME_INDISPOSED_ARG );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, _run_block)( handle, numSamples, timebase, 1, TIME_INDISPOSED_ARG )))
#endif
    {
        fraStatusText << L"Fatal error: Failed to start channel data capture: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#if !defined(NEW_PS_DRIVER_MODEL)
    else
    {
        currentTimeIndisposedMs = iTimeIndisposedMs;
        capturing = true;
        readyCB = lpReady;
        cbParam = pParameter;
        if (!SetEvent( hCheckStatusEvent ))
        {
            fraStatusText << L"Fatal error: Failed to start channel data capture: Failed to start ready status checking.";
            LogMessage( fraStatusText.str() );
            retVal = false;
        }
    }
#endif

#if !defined(PS6000A_IMPL) && !defined(PSOSPA_IMPL)
    *pfTimeIndisposedMs = iTimeIndisposedMs;
#endif

    // Remember number of samples for later operations
    mNumSamples = numSamples;
    buffersDirty = true;

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method CheckStatus
//
// Purpose: Used for scopes with driver families preceding the new driver model (e.g. PS2000 and
//          PS3000) which don't support block mode completion callbacks.  This is a thread function 
//          that supports an emulation of the callback feature present in the new driver model.
//
// Parameters: [in] lpThreadParameter - parameter passed to the thread function.  Here it's a
//                                      pointer to the object instance.
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(NEW_PS_DRIVER_MODEL)
DWORD WINAPI CommonMethod(SCOPE_FAMILY_LT, CheckStatus)(LPVOID lpThreadParameter)
{
    int16_t readyStatus;
    PICO_STATUS status;
    wstringstream fraStatusText;
    wstringstream readyStatusText;
    int32_t delayCounter;
    CommonClass(SCOPE_FAMILY_LT)* inst = (CommonClass(SCOPE_FAMILY_LT)*)lpThreadParameter;
    do
    {
        if (ResetEvent(inst->hCheckStatusEvent))
        {
            if (WAIT_OBJECT_0 == WaitForSingleObject( inst->hCheckStatusEvent, INFINITE ))
            {
                // Check that the handle is valid and implement a delay limit here, because unfortunately the ready function return
                // does not distinguish between an invalid handle and not ready
                if (inst->handle > 0)
                {
                    delayCounter = 0;
                    readyStatus = 0;
                    // delay with a safety factor of 3x+10 seconds
                    while (inst->capturing && delayCounter < ((inst->currentTimeIndisposedMs)*6)/2 + 10000 &&
                           0 == (readyStatus = CommonApi(SCOPE_FAMILY_LT, _ready)(inst->handle)))
                    {
                        //// diagnostic logging
                        readyStatusText.clear();
                        readyStatusText.str(L"");
                        readyStatusText << readyStatus << L" <== " << BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _ready)) << L"( " << inst->handle << L" );";
                        LogMessage( readyStatusText.str(), PICO_API_CALL );
                        //// diagnostic logging

                        Sleep(100);
                        delayCounter += 100;
                    }
                    if (readyStatus == 0) // timed out or canceled
                    {
                        continue; // Don't call the callback to help avoid races between this expiry and the one in PicoScopeFRA
                    }
                    if (readyStatus < 0)
                    {
                        //// diagnostic logging
                        readyStatusText.clear();
                        readyStatusText.str(L"");
                        readyStatusText << readyStatus << L" <== " << BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _ready)) << L"( " << inst->handle << L" );";
                        LogMessage( readyStatusText.str(), PICO_API_CALL );
                        //// diagnostic logging

                        status = PICO_DATA_NOT_AVAILABLE;
                    }
                    else
                    {
                        //// diagnostic logging
                        readyStatusText.clear();
                        readyStatusText.str(L"");
                        readyStatusText << readyStatus << L" <== " << BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _ready)) << L"( " << inst->handle << L" );";
                        LogMessage( readyStatusText.str(), PICO_API_CALL );
                        //// diagnostic logging

                        status = PICO_OK;
                    }
                }
                else
                {
                    status = PICO_DATA_NOT_AVAILABLE;
                }
                // Call the callback
                inst->readyCB(inst->handle, status, inst->cbParam);
            }
            else
            {
                // Something has gone catastrophically wrong, so just exit the thread.  At this point no callbacks will work
                // and the upper level code should detect the failure by timing out.
                fraStatusText << L"Fatal error: Invalid result from waiting on status checking start event";
                LogMessage( fraStatusText.str() );
                return -1;
            }
        }
        else
        {
            // Something has gone catastrophically wrong, so just exit the thread.  At this point no callbacks will work
            // and the upper level code should detect the failure by timing out.
            fraStatusText << L"Fatal error: Invalid result from resetting status checking start event";
            LogMessage( fraStatusText.str() );
            return -1;
        }

    } while (1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method InitStatusChecking
//
// Purpose: Initializes the event and thread supporting status checking and block mode callbacks
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, InitStatusChecking)(void)
{
    bool retVal = true;
    if (NULL == (hCheckStatusEvent = CreateEvent( NULL, true, false, NULL )))
    {
        retVal = false;
    }
    else if (NULL == (hCheckStatusThread = CreateThread( NULL, 0, CheckStatus, this, 0, NULL )))
    {
        retVal = false;
    }
    return retVal;
}
#endif

#if defined(IMPLEMENTS_SMART_PROBES)
HANDLE CommonClass(SCOPE_FAMILY_LT)::hProbeDataMutex;
HANDLE CommonClass(SCOPE_FAMILY_LT)::hProbeDataEvent;
HANDLE CommonClass(SCOPE_FAMILY_LT)::hProcessProbeUpdateThread;
std::vector<PICO_USER_PROBE_INTERACTIONS> CommonClass(SCOPE_FAMILY_LT)::newProbeInteractionData;
std::vector<PICO_USER_PROBE_INTERACTIONS> CommonClass(SCOPE_FAMILY_LT)::currentProbeInteractionData;
PICO_STATUS CommonClass(SCOPE_FAMILY_LT)::probeInteractionStatus;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method ProbeInteractionsCallback
//
// Purpose: Capture probe interaction events from scope and record them for later use.
//
// Parameters: [in] handle - identifier for the scope device
//             [in] status - indicates success or failure. If multiple errors have occurred, the
//                           most general error is returned here. Probe-specific errors are returned
//                           in the status field of the relevant elements of the probes array
//             [in] probes - pointer to an array of PICO_USER_PROBE_INTERACTIONS structures
//             [in] nProbes - the number of elements in the probes array
//
// Notes: Defined by Pico API
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void __stdcall CommonMethod(SCOPE_FAMILY_LT, ProbeInteractionsCallback)(int16_t handle, PICO_STATUS status, PICO_USER_PROBE_INTERACTIONS* probes, uint32_t nProbes)
{
    if (WAIT_OBJECT_0 == WaitForSingleObject(hProbeDataMutex, INFINITE))
    {
        probeInteractionStatus = status;

        for (uint32_t i = 0; i < nProbes; i++)
        {
            uint32_t channelNumber = probes[i].channel_;
            if (channelNumber < newProbeInteractionData.size())
            {
                std::memcpy(&newProbeInteractionData.data()[channelNumber], &probes[i], sizeof(PICO_USER_PROBE_INTERACTIONS));
            }
        }

        ReleaseMutex(hProbeDataMutex);

        // Signal that new data is available
        SetEvent(hProbeDataEvent);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method InitProbeEventHandling
//
// Purpose: Initialize the probe event handling event, thread, mutex, and data structures
//
// Parameters: [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

// Currently the PS4000A probe interactions callback signature and structures are fully compatible with
// the generic ones used in PS6000A.  We can't easily make a common callback member with the PS4000A specific
// signature because that would require including the ps4000aApi.h header in ps4000aImpl.h which would
// lead to lots of naming clashes in things like ScopeSelector.  Maybe some day these will be unified,
// or we can try using namespaces to avoid clashes.  But for now forcibly casting the version that uses
// the generic signature should work fine.

#if defined(PS4000A_IMPL)
#define PROBE_INTERACTIONS_CALLBACK_CAST ps4000aProbeInteractions
#elif defined(PS6000A_IMPL)
#define PROBE_INTERACTIONS_CALLBACK_CAST PicoProbeInteractions
#else
#define PROBE_INTERACTIONS_CALLBACK_CAST
#endif

bool CommonMethod(SCOPE_FAMILY_LT, InitProbeEventHandling)(void)
{
    bool retVal = false;

#if defined(PS4000A_IMPL)
    if (PS4444 == model)
    {
#endif
        wstringstream fraStatusText;
        PICO_STATUS status;

        if (NULL != (hProbeDataMutex = CreateMutex(NULL, FALSE, NULL)))
        {
            if (NULL != (hProbeDataEvent = CreateEvent(NULL, true, false, NULL)))
            {
                if (NULL != (hProcessProbeUpdateThread = CreateThread(NULL, 0, ProcessProbeUpdate, this, 0, NULL)))
                {
                    newProbeInteractionData.resize(PS_MAX_CHANNELS);
                    currentProbeInteractionData.resize(PS_MAX_CHANNELS);
                    probeInteractionStatus = PICO_PROBE_COLLECTION_NOT_STARTED;
                    for (uint32_t i = 0; i < PS_MAX_CHANNELS; i++)
                    {
                        currentProbeInteractionData[i].probeName_ = PICO_CONNECT_PROBE_UNKNOWN_PROBE;
                        currentProbeInteractionData[i].connected_ = 0;
                        currentProbeInteractionData[i].enabled_ = 0;
                        currentProbeInteractionData[i].status_ = PICO_PROBE_COLLECTION_NOT_STARTED;
                        currentProbeInteractionData[i].requiresPower_ = 0;
                        currentProbeInteractionData[i].isPowered_ = 0;
                    }
                    newProbeInteractionData = currentProbeInteractionData;
                    LOG_PICO_API_CALL(BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetProbeInteractionCallback)), handle, ProbeInteractionsCallback);
                    if (PICO_OK == (status = CommonApi(SCOPE_FAMILY_LT, SetProbeInteractionCallback)(handle, (PROBE_INTERACTIONS_CALLBACK_CAST)ProbeInteractionsCallback)))
                    {
                        retVal = true;
                    }
                }
            }
        }
#if defined(PS4000A_IMPL)
    }
    else
    {
        retVal = true;
    }
#endif
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method ProcessProbeUpdate
//
// Purpose: Process probe update events coming from the smart probe callback
//
// Parameters: See Win32 API documentation for a thread function
//
// Notes: This is a thread function signaled into action by the Probe Interactions Callback
//
///////////////////////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI CommonMethod(SCOPE_FAMILY_LT, ProcessProbeUpdate)(LPVOID lpThreadParameter)
{
    wstringstream fraStatusText;
    std::vector<SmartProbeEvent_T> smartProbeEvents;

    CommonClass(SCOPE_FAMILY_LT)* inst = (CommonClass(SCOPE_FAMILY_LT)*)lpThreadParameter;
    do
    {
        if (ResetEvent(inst->hProbeDataEvent))
        {
            if (WAIT_OBJECT_0 == WaitForSingleObject(inst->hProbeDataEvent, INFINITE))
            {
                if (WAIT_OBJECT_0 == WaitForSingleObject(hProbeDataMutex, INFINITE))
                {
                    // Interpret updates and store things like current channel attenuation, and range info
                    // Call callback to signal to the application that something changed.
                    smartProbeEvents.clear();

                    for (uint32_t i = 0; i < PS_MAX_CHANNELS; i++)
                    {
                        if (newProbeInteractionData[i].probeName_ != currentProbeInteractionData[i].probeName_ ||
                            newProbeInteractionData[i].connected_ != currentProbeInteractionData[i].connected_ ||
                            newProbeInteractionData[i].enabled_ != currentProbeInteractionData[i].enabled_ ||
                            newProbeInteractionData[i].status_ != currentProbeInteractionData[i].status_ ||
                            newProbeInteractionData[i].requiresPower_ != currentProbeInteractionData[i].requiresPower_ ||
                            newProbeInteractionData[i].isPowered_ != currentProbeInteractionData[i].isPowered_)
                        {
                            SmartProbeEvent_T smartProbeEvent;
                            smartProbeEvent.channel = (PS_CHANNEL)i;
                            smartProbeEvent.availableCouplings.clear();
                            smartProbeEvent.smartProbeConnected = newProbeInteractionData[i].status_ == PICO_OK &&
                                                                  newProbeInteractionData[i].connected_ &&
                                                                  (newProbeInteractionData[i].requiresPower_ == 0 ||
                                                                   (newProbeInteractionData[i].requiresPower_ && newProbeInteractionData[i].isPowered_));
                            if (smartProbeEvent.smartProbeConnected)
                            {
                                // In the case of a smart probe, ideally we'd use functions like ps6000aGetScalingValues to build the range table,
                                // but it appears broken at the moment
                                switch (newProbeInteractionData[i].probeName_)
                                {
                                    case PICO_PASSIVE_PROBE_X10: // P2036 or P2056 passive oscilloscope probe 2.5 mm 10:1
                                    {
                                        smartProbeEvent.autoAtten = true;
                                        smartProbeEvent.atten = ATTEN_10X;
                                        smartProbeEvent.attenStr = L"10x";
#if defined (PS6000A_IMPL)
                                        smartProbeEvent.availableCouplings = restrictedAvailableCouplings;
                                        inst->channelCurrentAvailableCouplings[i] = restrictedAvailableCouplings;
#endif
                                        inst->channelRangeOffsets[i] = PICO_X10_PROBE_100MV;
                                        inst->channelAttens[i] = ATTEN_10X;
                                        inst->channelAutoAttens[i] = true;
#if defined (WORKAROUND_SUPPORT_31730)
#else
                                        inst->channelMinRanges[i] = 0;
                                        inst->channelMaxRanges[i] = newProbeInteractionData[i].rangeLast_ - newProbeInteractionData[i].rangeFirst_;
#endif
                                        break;
                                    }
                                    case PICO_CONNECT_PROBE_DIFFERENTIAL: // PicoConnect 441 1:1 differential probe PQ098
                                    {
                                        smartProbeEvent.autoAtten = true;
                                        smartProbeEvent.atten = ATTEN_1X;
                                        smartProbeEvent.attenStr = L"1x";
                                        inst->channelRangeOffsets[i] = PICO_DIFFERENTIAL_10MV;
                                        inst->channelAttens[i] = ATTEN_1X;
                                        inst->channelAutoAttens[i] = true;
                                        inst->channelMinRanges[i] = 0;
                                        inst->channelMaxRanges[i] = newProbeInteractionData[i].rangeLast_ - newProbeInteractionData[i].rangeFirst_;
                                        break;
                                    }
                                    case PICO_CONNECT_PROBE_CAT3_HV_1KV: // PicoConnect 442 25:1 differential probe (1000 V CAT III) PQ087
                                    {
                                        smartProbeEvent.autoAtten = true;
                                        smartProbeEvent.atten = ATTEN_25X;
                                        smartProbeEvent.attenStr = L"25x";
                                        // If using a 25x attenuation, the upper range of +/- 50V would compute to +/- 1250V.  The spec list the probe/scope having an
                                        // upper limit of +/- 1000V.  It is assumed this is just a probe rating limitation and not a special 20x attenuation on
                                        // the highest scale.  If that assumption is incorrect, the range table might need to be constructed with ps4000aGetScalingValues
                                        // If so, we'd need to move calls to inst->SetChannelAttenuation_ into this table
                                        inst->channelRangeOffsets[i] = PICO_1KV_2_5V;
                                        inst->channelAttens[i] = ATTEN_25X;
                                        inst->channelAutoAttens[i] = true;
                                        inst->channelMinRanges[i] = 3; // +/- 100 mV * 25 = 2.5V
                                        inst->channelMaxRanges[i] = newProbeInteractionData[i].rangeLast_ - newProbeInteractionData[i].rangeFirst_;
                                        break;
                                    }
                                    case PICO_CONNECT_PROBE_D9_BNC: // D9 to single BNC adaptor TA271
                                    {
                                        smartProbeEvent.autoAtten = false;
                                        smartProbeEvent.atten = inst->channelManualAttens[i];
                                        smartProbeEvent.attenStr = L"";
                                        inst->channelRangeOffsets[i] = PICO_D9_BNC_10MV;
                                        inst->channelAttens[i] = inst->channelManualAttens[i];
                                        inst->channelAutoAttens[i] = false;
                                        inst->channelMinRanges[i] = 0;
                                        inst->channelMaxRanges[i] = newProbeInteractionData[i].rangeLast_ - newProbeInteractionData[i].rangeFirst_;
                                        break;
                                    }
                                    case PICO_CONNECT_PROBE_D9_2X_BNC: // D9 to dual BNC adaptor TA299
                                    {
                                        smartProbeEvent.autoAtten = false;
                                        smartProbeEvent.atten = inst->channelManualAttens[i];
                                        smartProbeEvent.attenStr = L"";
                                        inst->channelRangeOffsets[i] = PICO_D9_2X_BNC_10MV;
                                        inst->channelAttens[i] = inst->channelManualAttens[i];
                                        inst->channelAutoAttens[i] = false;
                                        inst->channelMinRanges[i] = 0;
                                        inst->channelMaxRanges[i] = newProbeInteractionData[i].rangeLast_ - newProbeInteractionData[i].rangeFirst_;
                                        break;
                                    }
                                    default:
                                    {
                                        smartProbeEvent.autoAtten = false;
                                        smartProbeEvent.atten = inst->channelManualAttens[i];
                                        smartProbeEvent.attenStr = L"???";
                                        inst->channelRangeOffsets[i] = PICO_X1_PROBE_10MV;
                                        inst->channelAttens[i] = inst->channelManualAttens[i];
                                        inst->channelAutoAttens[i] = false;
                                        inst->channelMinRanges[i] = 0;
                                        inst->channelMaxRanges[i] = newProbeInteractionData[i].rangeLast_ - newProbeInteractionData[i].rangeFirst_;
                                        break;
                                    }
                                }
                                inst->channelSmartProbeConnected[i] = true;
                                // There are currently no known smart probes for PS6000A that also allow 50 Ohm coupling.
                                // But we'll code for that case anyway in case there ever are such smart probes that allow it.
                                // This code is compensating for the fact that GetMaxRange is passed a coupling and is making
                                // an adjustment assuming the max range does not take 50 ohn coupling restrictions into account.
                                if (newProbeInteractionData[i].couplingCurrent_ == PICO_DC_50OHM)
                                {
                                    inst->channelMaxRanges[i] += 2;
                                }
                            }
                            else
                            {
                                smartProbeEvent.autoAtten = false;
                                smartProbeEvent.atten = inst->channelManualAttens[i];
#if defined (PS6000A_IMPL)
                                smartProbeEvent.availableCouplings = baseAvailableCouplings;
                                inst->channelCurrentAvailableCouplings[i] = baseAvailableCouplings;
#endif
                                inst->channelSmartProbeConnected[i] = false;
                                inst->channelRangeOffsets[i] = PICO_X1_PROBE_10MV;
                                inst->channelAttens[i] = inst->channelManualAttens[i];
                                inst->channelAutoAttens[i] = false;
                                // Set the min/max range back to non-smart probe values because it seems that the smart probe
                                // interactions don't want to report these correctly when a smart probe has been removed
                                inst->channelMinRanges[i] = inst->channelNonSmartMinRanges[i];
                                inst->channelMaxRanges[i] = inst->channelNonSmartMaxRanges[i];
                            }

                            smartProbeEvents.push_back(smartProbeEvent);

                            inst->SetChannelAttenuation_((PS_CHANNEL)i, inst->channelAttens[i]);
                        }
                    }

                    // Call callback, passing the smartProbeEvents
                    if (inst->smartProbeCallback)
                    {
                        inst->smartProbeCallback(smartProbeEvents);
                    }

                    currentProbeInteractionData = newProbeInteractionData;

                    ReleaseMutex(hProbeDataMutex);
                }
            }
        }
        else
        {
            // Something has gone catastrophically wrong, so just exit the thread.
            fraStatusText << L"Fatal error: Invalid result from waiting on probe data event";
            LogMessage( fraStatusText.str() );
            return -1;
        }
    } while (1);

    return -1;
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetChannelAttenuation
//
// Purpose: For cases where a smart probe is not being used, tells the PicoScope object the current
//          attentuation setting for the channel so it can re-build the range table
//
// Parameters: [in] channel - channel number to set attenuation
//             [in] atten - attenuation to set
//             [out] return - whether the call succeeded
//
// Notes: Call will return false if the channel has a currently connected smart probe with an
//        automatic attenuation setting
//        Calls a private version that does the work or reconstructing the range table
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, SetChannelAttenuation)( PS_CHANNEL channel, ATTEN_T atten )
{
    bool retVal = false;
    wstringstream fraStatusText;

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
#if defined(IMPLEMENTS_SMART_PROBES)
#if defined(PS4000A_IMPL)
        if (PS4444 == model)
        {
#endif
            if (WAIT_OBJECT_0 == WaitForSingleObject(hProbeDataMutex, INFINITE))
            {
#endif
                if (!channelAutoAttens[channel])
                {
                    channelManualAttens[channel] = atten;
                    SetChannelAttenuation_(channel, atten);
                    retVal = true;
                }
#if defined(IMPLEMENTS_SMART_PROBES)
                ReleaseMutex(hProbeDataMutex);
            }
            else
            {
                // Something has gone catastrophically wrong
                fraStatusText << L"Fatal error: Invalid result from waiting on probe data mutex";
                LogMessage(fraStatusText.str());
            }
#if defined(PS4000A_IMPL)
        }
        else
        {
            if (!channelSmartProbeConnected[channel])
            {
                channelManualAttens[channel] = atten;
                SetChannelAttenuation_(channel, atten);
                retVal = true;
            }
        }
#endif
#endif
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to SetChannelAttenuation: " << channel;
        LogMessage(fraStatusText.str());
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetChannelAttenuation_
//
// Purpose: For cases where a smart probe is being used, tells the PicoScope object the current
//          attentuation setting for the channel so it can re-build the range table
//
// Parameters: [in] channel - channel number to set attenuation
//             [in] atten - attenuation to set
//
// Notes: Caller must have the smart probe data mutex before calling this.
//
////////////////////////////////////////////////////////////////////////////////////////////////////

void CommonMethod(SCOPE_FAMILY_LT, SetChannelAttenuation_)( PS_CHANNEL channel, ATTEN_T atten )
{
    wstringstream fraStatusText;

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        channelAttens[channel] = atten;
        if (ATTEN_1X == atten)
        {
            channelRangeInfo[channel] = base1xRangeInfo;
        }
        else
        {
            channelRangeInfo[channel].clear();
            for (auto rangeInfo : base1xRangeInfo)
            {
                rangeInfo.rangeVolts *= attenScalingFactors[atten];
                CreateRangeName(rangeInfo.name, rangeInfo.rangeVolts);
                channelRangeInfo[channel].push_back(rangeInfo);
            }
        }
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to SetChannelAttenuation_: " << channel;
        LogMessage(fraStatusText.str());
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetChannelAttenuation
//
// Purpose: Gets the current attenuation being used for the channel specified
//
// Parameters: [in] channel - channel number to get attenuation
//             [out] atten - attenuation to get
//
// Notes: Not bothering with getting the probe data mutex since this is a simple atomic access.
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, GetChannelAttenuation)(PS_CHANNEL channel, ATTEN_T& atten)
{
    wstringstream fraStatusText;
    bool retVal = false;

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        atten = channelAttens[channel];
        retVal = true;
    }
    else
    {
        fraStatusText << L"Fatal error: Invalid channel in call to GetChannelAttenuation: " << channel;
        LogMessage( fraStatusText.str() );
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method IsAutoAttenuation
//
// Purpose: Indicates whether the channel is a smart probe with an automatic attenuation setting
//          and therefore should not be manually set by the application
//
// Parameters: [in] channel - channel number for query
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, IsAutoAttenuation)(PS_CHANNEL channel)
{
    wstringstream fraStatusText;
    bool retVal = false;

    if (channel != PS_CHANNEL_INVALID && channel < numAvailableChannels)
    {
        retVal = channelAutoAttens[channel];
    }
    else
    {
        fraStatusText << L"Error: Invalid channel in call to IsAutoAttenuation: " << channel;
        LogMessage(fraStatusText.str());
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetSmartProbeCallback
//
// Purpose: Sets a function that will be called back whenever any relevant change in smart probe
//          status occurs
//
// Parameters: [in] smartProbeCB - pointer to callback function
//
// Notes: This is a null function for scopes that don't implement smart probes
//
////////////////////////////////////////////////////////////////////////////////////////////////////

void CommonMethod(SCOPE_FAMILY_LT, SetSmartProbeCallback)(SMART_PROBE_CALLBACK smartProbeCB)
{
#if defined(IMPLEMENTS_SMART_PROBES)
    smartProbeCallback = smartProbeCB;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetChannelDesignations
//
// Purpose: Tells the PicoScope object which channel is input and which is output
//
// Parameters: [in] inputChannel - input channel number
//             [in] outputChannel - output channel number
//
// Notes: Also re-sync's resolution for the upcoming FRA run in case it got out of sync
//
////////////////////////////////////////////////////////////////////////////////////////////////////
void CommonMethod(SCOPE_FAMILY_LT, SetChannelDesignations)( PS_CHANNEL inputChannel, PS_CHANNEL outputChannel )
{
    mInputChannel = inputChannel;
    mOutputChannel = outputChannel;
#if defined(PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
    RESOLUTION_T actualResolution;
    SetResolution((RESOLUTION_T)currentResolution, actualResolution);
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method ChannelDesignationsAreOptimal
//
// Purpose: Indicates whether the channels chosen for the scope are optimal for timebase and
//          resolution
//
// Parameters: [out] return - whether the channels are optimal
//
// Notes: Channels are always optimal for non-PS6000 and non-PS6000A scopes
//
////////////////////////////////////////////////////////////////////////////////////////////////////
bool CommonMethod(SCOPE_FAMILY_LT, ChannelDesignationsAreOptimal)(void)
{
#if defined (PS6000_IMPL) || defined (PS6000A_IMPL)
    uint32_t channelFlags = ((1 << mInputChannel) | (1 << mOutputChannel));
    uint32_t upperMask = numAvailableChannels == 4 ? 0x0C : 0xF0;
    uint32_t lowerMask = numAvailableChannels == 4 ? 0x03 : 0x0F;
    std::bitset<32> upperChannels(channelFlags & upperMask);
    std::bitset<32> lowerChannels(channelFlags & lowerMask);

    return (upperChannels.count() != 2 && lowerChannels.count() != 2);
#else
    return true;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Methods associated with getting data
// Notes: PS2000 and PS3000 drivers support an aggregation mode that works with "fast streaming"
//        which may be able to mimic block mode (using auto_stop).  None of the PS3000 scopes that
//        support fast streaming also have a signal generator, so are irrelevant.  All of the 
//        compatible PS2000 scopes do support fast streaming.  However, given the relatively 
//        smaller size of the buffers these scopes use, I decided to favor code simplicity over
//        speed and just use block mode which has no aggregation.  The aggregation will be done
//        in software on the PC-side.
//
////////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(PS2000A_IMPL) || defined(PS3000A_IMPL) || defined(PS4000A_IMPL) || defined(PS5000A_IMPL)
#define RATIO_MODE_NONE_ARG , CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_NONE)
#define RATIO_MODE_AGGREGATE_ARG , CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE)
#define SEGMENT_ARG , 0
#define DATA_TYPE_ARG
#define ACTION_ARG_INPUT
#define ACTION_ARG_OUTPUT
#elif defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
#define RATIO_MODE_NONE_ARG , PICO_RATIO_MODE_RAW
#define RATIO_MODE_AGGREGATE_ARG , PICO_RATIO_MODE_AGGREGATE
#define DATA_TYPE_ARG , PICO_INT16_T
#define SEGMENT_ARG , 0
#define ACTION_ARG_INPUT , (PICO_ACTION)(PICO_CLEAR_ALL | PICO_ADD)
#define ACTION_ARG_OUTPUT , (PICO_ACTION)(PICO_ADD)
#elif defined(PS6000_IMPL)
#define RATIO_MODE_NONE_ARG , CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_NONE)
#define RATIO_MODE_AGGREGATE_ARG , CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE)
#define SEGMENT_ARG
#define DATA_TYPE_ARG
#define ACTION_ARG_INPUT
#define ACTION_ARG_OUTPUT
#elif defined(PS4000_IMPL) || defined(PS5000_IMPL)
#define RATIO_MODE_NONE_ARG
#define RATIO_MODE_AGGREGATE_ARG
#define SEGMENT_ARG
#define DATA_TYPE_ARG
#define ACTION_ARG_INPUT
#define ACTION_ARG_OUTPUT
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetData
//
// Purpose: Gets data from the scope that was captured in block mode.
//
// Parameters: [in] numSamples - number of samples to retrieve
//             [in] startIndex - at what index to start retrieving
//             [in] inputBuffer - buffer to store input samples
//             [in] outputBuffer - buffer to store output samples
//             [out] return - whether the function succeeded
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, GetData)( uint32_t numSamples, uint32_t startIndex,
                                             vector<int16_t>** inputBuffer, vector<int16_t>** outputBuffer )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;
    int16_t overflow;

    // First check for the programming error of not setting the channel designations
    if ( PS_CHANNEL_INVALID == mInputChannel ||
         PS_CHANNEL_INVALID == mOutputChannel )
    {
        fraStatusText << L"Fatal error: invalid internal state: input and/or output channel number invalid.";
        LogMessage( fraStatusText.str() );
        return false;
    }
#if defined(NEW_PS_DRIVER_MODEL)
#if defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
    uint64_t numSamplesInOut;
#else
    uint32_t numSamplesInOut;
#endif

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT,SetDataBuffer)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel, mInputBuffer.data(),
                                                                                     numSamples DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_NONE_ARG ACTION_ARG_INPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT,SetDataBuffer)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel, mInputBuffer.data(),
                                                             numSamples DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_NONE_ARG ACTION_ARG_INPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set input data capture buffer: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT,SetDataBuffer)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel, mOutputBuffer.data(),
                                                                                     numSamples DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_NONE_ARG ACTION_ARG_OUTPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT,SetDataBuffer)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel, mOutputBuffer.data(),
                                                                numSamples DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_NONE_ARG ACTION_ARG_OUTPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set output data capture buffer: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    numSamplesInOut = numSamples;
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetValues)), handle, startIndex, &numSamplesInOut, 1, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_NONE), 0, &overflow );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetValues)( handle, startIndex, &numSamplesInOut, 1, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_NONE), 0, &overflow )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to retrieve data capture buffer(s): " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#else // !defined(NEW_PS_DRIVER_MODEL)
    if (buffersDirty)
    {
        // Just use block mode without aggregation to get all the data.

        int16_t* buffer[PS_CHANNEL_D+1] = {NULL};

        buffer[mInputChannel] = mInputBuffer.data();
        buffer[mOutputChannel] = mOutputBuffer.data();

        LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _get_values)), handle, buffer[PS_CHANNEL_A], buffer[PS_CHANNEL_B], buffer[PS_CHANNEL_C], buffer[PS_CHANNEL_D], &overflow, numSamples );
        if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, _get_values)( handle, buffer[PS_CHANNEL_A], buffer[PS_CHANNEL_B], buffer[PS_CHANNEL_C], buffer[PS_CHANNEL_D], &overflow, numSamples )))
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to retrieve data capture buffer: " << status;
            LogMessage( fraStatusText.str() );
            retVal = false;
        }
    }
#endif
    *inputBuffer = &mInputBuffer;
    *outputBuffer = &mOutputBuffer;

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetCompressedData
//
// Purpose: Gets a compressed form of the data from the scope that was captured in block mode.
//          The compressed form is an aggregation where a range of samples is provided as the min
//          and max value in that range.
//
// Parameters: [in] downsampleTo - number of samples to store in a compressed aggregate buffer
//                                 if 0, it means don't compress, just copy
//             [in] inputCompressedMinBuffer - buffer to store aggregated min input samples
//             [in] outputCompressedMinBuffer - buffer to store aggregated min ouput samples
//             [in] inputCompressedMaxBuffer - buffer to store aggregated max input samples
//             [in] outputCompressedMaxBuffer - buffer to store aggregated max ouput samples
//             [out] return - whether the function succeeded
//
// Notes: For old driver model, need to call GetData first for this to function correctly
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, GetCompressedData)( uint32_t downsampleTo,
                                                       vector<int16_t>& inputCompressedMinBuffer, vector<int16_t>& outputCompressedMinBuffer,
                                                       vector<int16_t>& inputCompressedMaxBuffer, vector<int16_t>& outputCompressedMaxBuffer )
{
    bool retVal = true;
    wstringstream fraStatusText;
    uint32_t compressedBufferSize;
    uint32_t downsampleRatio;

    // First check for the programming error of not setting the channel designations
    if ( PS_CHANNEL_INVALID == mInputChannel ||
         PS_CHANNEL_INVALID == mOutputChannel )
    {
        fraStatusText << L"Fatal error: invalid internal state: input and/or output channel number invalid.";
        LogMessage( fraStatusText.str() );
        return false;
    }

    // For NEW_PS_DRIVER_MODEL: Get the downsampled (compressed) buffer in one step by using the scope's aggregation function
    // For non-NEW_PS_DRIVER_MODEL: We're calculating an aggregation parameter for our own manual aggregation 

    // Determine the aggregation parameter, rounding up to ensure that the actual size of the compressed buffer is no more than requested
    compressedBufferSize = (downsampleTo == 0 || mNumSamples <= downsampleTo) ? mNumSamples : downsampleTo;
    downsampleRatio = mNumSamples / compressedBufferSize;
    if (mNumSamples % compressedBufferSize)
    {
        downsampleRatio++;
    }
    // Now compute actual size of aggregate buffer
    compressedBufferSize = mNumSamples / downsampleRatio;
    compressedBufferSize += (mNumSamples % downsampleRatio) ? 1 : 0;

    // And allocate the data for the compressed buffers
    inputCompressedMinBuffer.resize(compressedBufferSize);
    outputCompressedMinBuffer.resize(compressedBufferSize);
    inputCompressedMaxBuffer.resize(compressedBufferSize);
    outputCompressedMaxBuffer.resize(compressedBufferSize);

#if defined(NEW_PS_DRIVER_MODEL)
    PICO_STATUS status;
    int16_t overflow;
#if defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
    uint64_t numSamplesInOut;
#else
    uint32_t numSamplesInOut;
#endif

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel,
                                                                                       inputCompressedMaxBuffer.data(), inputCompressedMinBuffer.data(), 
                                                                                       compressedBufferSize DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_INPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel,
                                                               inputCompressedMaxBuffer.data(), inputCompressedMinBuffer.data(), 
                                                               compressedBufferSize DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_INPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set input data capture aggregation buffers: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel,
                                                                                       outputCompressedMaxBuffer.data(), outputCompressedMinBuffer.data(),
                                                                                       compressedBufferSize DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_OUTPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel,
                                                               outputCompressedMaxBuffer.data(), outputCompressedMinBuffer.data(), 
                                                               compressedBufferSize DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_OUTPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set output data capture aggregation buffers: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

#if defined (PS6000A_IMPL) || defined(PSOSPA_IMPL)
    numSamplesInOut = mNumSamples;
#else
    numSamplesInOut = compressedBufferSize;
#endif
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetValues)), handle, 0, &numSamplesInOut, downsampleRatio, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE), 0, &overflow );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetValues)( handle, 0, &numSamplesInOut, downsampleRatio, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE), 0, &overflow )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to retrieve compressed data: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
#else // !defined(NEW_PS_DRIVER_MODEL)
    // Manually aggregate, if necessary
    if (compressedBufferSize == mNumSamples) // No need to aggregate, just copy
    {
        // Using explicit copy instead of assignment to avoid unwanted resize
        copy(mInputBuffer.begin(), mInputBuffer.begin() + compressedBufferSize, inputCompressedMinBuffer.begin());
        copy(mInputBuffer.begin(), mInputBuffer.begin() + compressedBufferSize, inputCompressedMaxBuffer.begin());
        copy(mOutputBuffer.begin(), mOutputBuffer.begin() + compressedBufferSize, outputCompressedMinBuffer.begin());
        copy(mOutputBuffer.begin(), mOutputBuffer.begin() + compressedBufferSize, outputCompressedMaxBuffer.begin());
    }
    else
    {
        uint32_t i;
        for (i = 0; i < compressedBufferSize-1; i++)
        {
            auto minMaxIn = minmax_element( &mInputBuffer[i*downsampleRatio], &mInputBuffer[(i+1)*downsampleRatio] );
            inputCompressedMinBuffer[i] = *minMaxIn.first;
            inputCompressedMaxBuffer[i] = *minMaxIn.second;
            auto minMaxOut = minmax_element( &mOutputBuffer[i*downsampleRatio], &mOutputBuffer[(i+1)*downsampleRatio] );
            outputCompressedMinBuffer[i] = *minMaxOut.first;
            outputCompressedMaxBuffer[i] = *minMaxOut.second;
        }
        // Handle the last few samples
        auto minMaxIn = minmax_element( &mInputBuffer[i*downsampleRatio], &mInputBuffer[mNumSamples] );
        inputCompressedMinBuffer[i] = *minMaxIn.first;
        inputCompressedMaxBuffer[i] = *minMaxIn.second;
        auto minMaxOut = minmax_element( &mOutputBuffer[i*downsampleRatio], &mOutputBuffer[mNumSamples] );
        outputCompressedMinBuffer[i] = *minMaxOut.first;
        outputCompressedMaxBuffer[i] = *minMaxOut.second;
    }
#endif
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method GetPeakValues
//
// Purpose: Gets channel peak data values as well as flags indicating channels in overvoltage 
//          condition (ADC railed)
//
// Parameters: [in] inputPeak - Maximum absolute value of input data captured
//             [in] outputPeak - Maximum absolute value of input data captured
//             [in] inputOv - whether the input channel is over-voltage
//             [in] outputOv - whether the output channel is over-voltage 
//             [out] return - whether the function succeeded
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, GetPeakValues)( uint16_t& inputPeak, uint16_t& outputPeak, bool& inputOv, bool& outputOv )
{
    PICO_STATUS status;
    bool retVal = true;
    wstringstream fraStatusText;
    int16_t overflow = 0;
    uint16_t commonModeOverflow = 0;

    // First check for the programming error of not setting the channel designations
    if ( PS_CHANNEL_INVALID == mInputChannel ||
         PS_CHANNEL_INVALID == mOutputChannel )
    {
        fraStatusText << L"Fatal error: invalid internal state: input and/or output channel number invalid.";
        LogMessage( fraStatusText.str() );
        return false;
    }

#define WORKAROUND_AGGREGATION_BUG
#if defined(NEW_PS_DRIVER_MODEL)
    // This is a workaround for the issue described in Picotech support ID SUPPORT-7588
    // Aggregating down to a single sample causes issues for some drivers.  Instead, this aggregates down to no
    // more than 512 samples.  Once it has the samples, it searches for the min/max among them
#if defined(WORKAROUND_AGGREGATION_BUG)
#if defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
    uint64_t numSamplesInOut;
#else
    uint32_t numSamplesInOut;
#endif

    uint32_t downsampleRatio;

    int16_t inputDataMin[512];
    int16_t inputDataMax[512];
    int16_t outputDataMin[512];
    int16_t outputDataMax[512];

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel,
                                                                                       inputDataMax, inputDataMin, 512 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_INPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel,
                                                               inputDataMax, inputDataMin, 512 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_INPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set input data capture aggregation buffers for peak/overvoltage detection: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel,
                                                                                       outputDataMax, outputDataMin, 512 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_OUTPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel,
                                                               outputDataMax, outputDataMin, 512 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_OUTPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set output data capture aggregation buffers for peak/overvoltage detection: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    downsampleRatio = (mNumSamples/512)+1;
#if defined (PS6000A_IMPL) || defined(PSOSPA_IMPL)
    numSamplesInOut = mNumSamples;
#else
    numSamplesInOut = mNumSamples / downsampleRatio;
    numSamplesInOut += (mNumSamples % downsampleRatio) ? 1 : 0;
#endif
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetValues)), handle, 0, &numSamplesInOut, downsampleRatio, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE), 0, &overflow );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetValues)( handle, 0, &numSamplesInOut, downsampleRatio, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE), 0, &overflow )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to retrieve aggregated data for peak/overvoltage detection: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
    else
    {
        inputPeak = max(abs(*std::max_element(&inputDataMax[0], &inputDataMax[numSamplesInOut])), abs(*std::min_element(&inputDataMin[0], &inputDataMin[numSamplesInOut])));
        outputPeak = max(abs(*std::max_element(&outputDataMax[0], &outputDataMax[numSamplesInOut])), abs(*std::min_element(&outputDataMin[0], &outputDataMin[numSamplesInOut])));
    }
#else // Don't workaround aggregation bug
#if defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
    uint64_t numSamplesInOut;
#else
    uint32_t numSamplesInOut;
#endif

    int16_t inputDataMin;
    int16_t inputDataMax;
    int16_t outputDataMin;
    int16_t outputDataMax;

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel,
                                                                                       &inputDataMax, &inputDataMin, 1 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_INPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mInputChannel,
                                                               &inputDataMax, &inputDataMin, 1 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_INPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set input data capture aggregation buffers for peak/overvoltage detection: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)), handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel,
                                                                                       &outputDataMax, &outputDataMin, 1 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_OUTPUT );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, SetDataBuffers)( handle, (CommonEnum(SCOPE_FAMILY_UT,CHANNEL))mOutputChannel,
                                                               &outputDataMax, &outputDataMin, 1 DATA_TYPE_ARG SEGMENT_ARG RATIO_MODE_AGGREGATE_ARG ACTION_ARG_OUTPUT )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to set output data capture aggregation buffers for peak/overvoltage detection: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    numSamplesInOut = 1;
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetValues)), handle, 0, &numSamplesInOut, mNumSamples, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE), 0, &overflow );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetValues)( handle, 0, &numSamplesInOut, mNumSamples, CommonEnum(SCOPE_FAMILY_UT, RATIO_MODE_AGGREGATE), 0, &overflow )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to retrieve aggregated data for peak/overvoltage detection: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
    else
    {
        inputPeak = max(abs(inputDataMax), abs(inputDataMin));
        outputPeak = max(abs(outputDataMax), abs(outputDataMin));
    }
#endif
#if defined (PS4000A_IMPL)
    if (PS4444 == model)
    {
        LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetCommonModeOverflow)), handle, &commonModeOverflow );
        if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetCommonModeOverflow)(handle, &commonModeOverflow)))
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to retrieve common mode overflows: " << status;
            LogMessage( fraStatusText.str() );
            retVal = false;
        }
        else
        {
            if (commonModeOverflow)
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"WARNING: Common mode overflow on channel(s): ";
                bool bFirst = false;
                for (int chan = PS_CHANNEL_A; chan <= PS_CHANNEL_D; chan++)
                {
                    if (commonModeOverflow & 1 << (chan + 8))
                    {
                        wchar_t chanString[2];
                        wsprintf(chanString, L"%c", 'A' + chan);
                        chanString[1] = 0;
                        if (bFirst)
                        {
                            fraStatusText << L", ";
                        }
                        fraStatusText << chanString;
                        bFirst = true;
                    }
                }
                LogMessage(fraStatusText.str(), FRA_WARNING);
            }
        }
    }
#endif
#else
    // Just use block mode without aggregation to get all the data.

    int16_t* buffer[PS_CHANNEL_D+1] = {NULL};

    buffer[mInputChannel] = mInputBuffer.data();
    buffer[mOutputChannel] = mOutputBuffer.data();

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, _get_values)), handle, buffer[PS_CHANNEL_A], buffer[PS_CHANNEL_B], buffer[PS_CHANNEL_C], buffer[PS_CHANNEL_D], &overflow, mNumSamples );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, _get_values)( handle, buffer[PS_CHANNEL_A], buffer[PS_CHANNEL_B], buffer[PS_CHANNEL_C], buffer[PS_CHANNEL_D], &overflow, mNumSamples )))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to retrieve data capture buffer in peak/overvoltage detection: " << status;
        LogMessage( fraStatusText.str() );
        retVal = false;
    }
    else
    {
        buffersDirty = false;
    }

    // Get peak values
    auto inputMinMax = std::minmax_element(&mInputBuffer[0], &mInputBuffer[mNumSamples]);
    inputPeak = max(abs(*inputMinMax.first), abs(*inputMinMax.second));
    auto outputMinMax = std::minmax_element(&mOutputBuffer[0], &mOutputBuffer[mNumSamples]);
    outputPeak = max(abs(*outputMinMax.first), abs(*outputMinMax.second));

#endif

    // Decode overflow
    inputOv = (((overflow & 1<<mInputChannel) | (commonModeOverflow & 1<<(mInputChannel+8))) != 0);
    outputOv = (((overflow & 1<<mOutputChannel) | (commonModeOverflow & 1<<(mOutputChannel+8))) != 0);

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method ChangePower
//
// Purpose: Change flexible power mode for the scope
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: Only relevant for scopes with flexible power implementations (driver families PS3000A,
//        PS4000A and PS5000A)
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, ChangePower)( PICO_STATUS powerState )
{
#if defined(PS3000A_IMPL) || defined(PS5000A_IMPL)
    PICO_STATUS status;
    wstringstream fraStatusText;
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, ChangePowerSource)), handle, powerState );
    status = CommonApi(SCOPE_FAMILY_LT, ChangePowerSource)( handle, powerState );
    return (status==PICO_OK);
#else
    return false;
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method CancelCapture
//
// Purpose: Cancel the current capture
//
// Parameters: [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, CancelCapture)(void)
{
    PICO_STATUS status;
    wstringstream fraStatusText;
#if !defined(NEW_PS_DRIVER_MODEL)
    capturing = false;
    // Wait until the CheckStatus loop is reset before calling stop.
    // This is to avoid any possible simultaneous API calls between stop and ready.
    while (WAIT_OBJECT_0 == WaitForSingleObject(hCheckStatusEvent, 0))
    {
        Sleep(100);
    }
#endif;
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, Stop)), handle );
    status = CommonApi(SCOPE_FAMILY_LT, Stop)( handle );
    return !(PICO_ERROR(status));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method Close
//
// Purpose: Close the scope
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, Close)(void)
{
    PICO_STATUS status;
    wstringstream fraStatusText;
    try
    {
        LOG_PICO_API_CALL(BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, CloseUnit)), handle);
    }
    catch (const std::runtime_error& ex)
    {
        UNREFERENCED_PARAMETER(ex);
        // Coverity says std::runtime_error and std::ios_base::failure (derived from std::runtime_error)
        // can be thrown in from wstringstream::clear() and wstringstream ctor; we'll just ignore them, since
        // it's just logging.
    }
    status = CommonApi(SCOPE_FAMILY_LT, CloseUnit)( handle );
    handle = -1;
    return (PICO_OK == status);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method IsUSB3_0Connection
//
// Purpose: Determine if a USB 3.0 scope is on a USB 3.0 port
//
// Parameters: [out] return - true if the connection is USB 3.0
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool CommonMethod(SCOPE_FAMILY_LT, IsUSB3_0Connection)(void)
{
    bool retVal;
    PICO_STATUS status;
    wstringstream fraStatusText;
#if defined(NEW_PS_DRIVER_MODEL)
    int16_t infoStringLength;
#endif
    int8_t usbVersion[16];
    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(CommonApi(SCOPE_FAMILY_LT, GetUnitInfo)), handle, usbVersion, sizeof(usbVersion) INFO_STRING_LENGTH_ARG, PICO_USB_VERSION );
    if (PICO_ERROR(CommonApi(SCOPE_FAMILY_LT, GetUnitInfo)(handle, usbVersion, sizeof(usbVersion) INFO_STRING_LENGTH_ARG, PICO_USB_VERSION)))
    {
        retVal = false;
    }
    else
    {
        if (!strcmp((char*)usbVersion, "3.0"))
        {
            retVal = true;
        }
        else
        {
            retVal = false;
        }
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method CreateRangeName
//
// Purpose: Utility function to write a range name on the fly
//
// Parameters: [in] string - the string to write to
//             [in] scale - the scale for the range
//
// Notes: E.g. a scale of 1.0 will result in a string of "� 1 V"
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void CommonMethod(SCOPE_FAMILY_LT, CreateRangeName)(wchar_t* string, double scale)
{
    if (scale < 1.0)
    {
        std::swprintf(string, sizeof(RANGE_INFO_T::name)/sizeof(wchar_t), L"� %d mV", (int)std::round(1000.0 * scale));
    }
    else if (scale < 1000.0)
    {
        std::swprintf( string, sizeof(RANGE_INFO_T::name)/sizeof(wchar_t), L"� %d V", (int)std::round(scale) );
    }
    else
    {
        std::swprintf( string, sizeof(RANGE_INFO_T::name)/sizeof(wchar_t), L"� %d kV", (int)std::round(scale/1000.0) );
    }
}

void CommonMethod(SCOPE_FAMILY_LT, LogPicoApiCall)(wstringstream& fraStatusText)
{
    // If there is a trailing ", " remove it
    size_t length = fraStatusText.str().length();
    if (!fraStatusText.str().compare(length-2, 2, L", "))
    {
        fraStatusText.seekp( -2, ios_base::end );
    }

    fraStatusText << L" );";
    LogMessage( fraStatusText.str(), PICO_API_CALL );
}

template <typename First, typename... Rest> void CommonMethod(SCOPE_FAMILY_LT, LogPicoApiCall)( wstringstream& fraStatusText, First first, Rest... rest)
{
    if (fraStatusText.str().empty()) // first is the function name
    {
        fraStatusText << first << L"( ";
    }
    else // first is a parameter
    {
        fraStatusText << first << L", ";
    }
    LogPicoApiCall( fraStatusText, rest... );
}

#undef GetUnitInfo
#undef SetChannel
#undef SetSigGenBuiltIn
#undef GetTimebase
#undef SetTriggerChannelConditions
#undef CloseUnit
