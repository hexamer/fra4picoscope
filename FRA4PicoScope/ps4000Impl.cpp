//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ps4000Impl.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "utility.h"
#include "ps4000Api.h"
#include "picoStatus.h"
#include "ps4000Impl.h"
#include "StatusLog.h"

typedef enum enPS4000Coupling
{
    PS4000_AC,
    PS4000_DC
} PS4000_COUPLING;

typedef SWEEP_TYPE PS4000_SWEEP_TYPE;
#define PS4000_UP UP

typedef SIGGEN_TRIG_TYPE PS4000_SIGGEN_TRIG_TYPE;
typedef SIGGEN_TRIG_SOURCE PS4000_SIGGEN_TRIG_SOURCE;

const int PS4000_SIGGEN_NONE = SIGGEN_NONE;
const int PS4000_ES_OFF = PS4000_OP_NONE;
const int PS4000_RATIO_MODE_NONE = RATIO_MODE_NONE;
const int PS4000_RATIO_MODE_AGGREGATE = RATIO_MODE_AGGREGATE;

#define PS4000_IMPL
#include "psCommonImpl.cpp"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps4000Impl::GetTimebase
//
// Purpose: Get a timebase from a desired frequency, rounding such that the frequency is at least
//          as high as requested, if possible.
//
// Parameters: [in] desiredFrequency: caller's requested frequency in Hz
//             [in] resolution: the vertical resolution to consider; can use RESOLUTION_CURRENT
//             [out] actualFrequency: the frequency corresponding to the returned timebase.
//             [out] timebase: the timebase that will achieve the requested freqency or greater
//
// Notes: resolution is unused by this scope family
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps4000Impl::GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase )
{
    bool retVal = false;
    double fTimebase;

    if (desiredFrequency != 0.0 && actualFrequency && timebase)
    {
        if (model == PS4262)
        {
            fTimebase = max(0.0, ((10.0e6/(desiredFrequency)) - 1.0)); // ps4000pg.en r9 p7
            fTimebase = min(fTimebase, (double)((uint32_t)1<<30) - 1.0); // limit is 2^30-1
            *timebase = (uint32_t)fTimebase;
            retVal = GetFrequencyFromTimebase(*timebase, *actualFrequency);
        }
        else if (model == PS4226 || model == PS4227)
        {
            if (desiredFrequency > 15625000.0)
            {
                *timebase = saturation_cast<uint32_t,double>(log(250.0e6/desiredFrequency) / M_LN2); // ps4000pg.en r9 p7; log2(n) implemented as log(n)/log(2)
                if (*timebase == 0)
                {
                    *timebase = 1; // Can't use timebase 0 with two channels enabled
                }
            }
            else
            {
                fTimebase = ((31250000.0/(desiredFrequency)) + 2.0); // ps4000pg.en r9 p7
                fTimebase = min(fTimebase, (double)((uint32_t)1<<30) - 1.0); // limit is 2^30-1
                *timebase = (uint32_t)fTimebase;
                *timebase = max( *timebase, 4 ); // make sure it's at least 4
            }
            retVal = GetFrequencyFromTimebase(*timebase, *actualFrequency);
        }
        else if (model == PS4223 || model == PS4224 || model == PS4423 || model == PS4424 || model == PS4224IEPE)
        {
            if (desiredFrequency > 10.0e6)
            {
                *timebase = saturation_cast<uint32_t,double>(log(80.0e6/desiredFrequency) / M_LN2); // ps4000pg.en r9 p7; log2(n) implemented as log(n)/log(2)

                if (model == PS4423 || model == PS4424)
                {
                    if (((mInputChannel == PS_CHANNEL_A || mInputChannel == PS_CHANNEL_B) &&
                         (mOutputChannel == PS_CHANNEL_A || mOutputChannel == PS_CHANNEL_B)) ||
                        ((mInputChannel == PS_CHANNEL_C || mInputChannel == PS_CHANNEL_D) &&
                         (mOutputChannel == PS_CHANNEL_C || mOutputChannel == PS_CHANNEL_D)))
                        {
                            *timebase = max(*timebase, 2); // If both channels are from same block, cannot use timebase less than 2 (20 MS/s)
                        }
                }
            }
            else
            {
                fTimebase = ((20.0e6/(desiredFrequency)) + 1.0); // ps4000pg.en r9 p7
                fTimebase = min(fTimebase, (double)((uint32_t)1<<30) - 1.0); // limit is 2^30-1
                *timebase = (uint32_t)fTimebase;
                *timebase = max( *timebase, 3 ); // make sure it's at least 3
            }
            retVal = GetFrequencyFromTimebase(*timebase, *actualFrequency);
        }
    }

    return retVal;
}

bool ps4000Impl::GetFrequencyFromTimebase(uint32_t timebase, double &frequency)
{
    bool retVal = false;
    
    if (timebase >= minTimebase && timebase <= maxTimebase)
    {
        if (model == PS4262)
        {
            frequency = 10.0e6 / ((double)(timebase + 1)); // ps4000pg.en r8 p17
            retVal = true;
        }
        else if (model == PS4226 || model == PS4227)
        {
            if (timebase <= 3)
            {
                frequency = 250.0e6 / (double)(1<<(timebase));
            }
            else
            {
                frequency = 31250000.0 / ((double)(timebase - 2)); // ps4000pg.en r8 p17
            }
            retVal = true;
        }
        else if (model == PS4223 || model == PS4224 || model == PS4423 || model == PS4424 || model == PS4224IEPE)
        {
            if (timebase <= 2)
            {
                frequency = 80.0e6 / (double)(1<<(timebase));
            }
            else
            {
                frequency = 20.0e6 / ((double)(timebase - 1)); // ps4000pg.en r8 p17
            }
            retVal = true;
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps4000Impl::InitializeScope
//
// Purpose: Initialize scope/family-specific implementation details.
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps4000Impl::InitializeScope(void)
{
    bool retVal = false;
    PS_RANGE minRange, maxRange;

    if (model == PS4262)
    {
        minRange = (PS_RANGE)PS4000_10MV;
        maxRange = (PS_RANGE)PS4000_20V;
        timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 15; // for PS4262 => 625 kHz, approximately 3x HW BW limiter
        minTimebase = 0;
        signalGeneratorPrecision = 192.0e3 / (double)UINT32_MAX;
    }
    else if (model == PS4226 || model == PS4227)
    {
        minRange = (PS_RANGE)PS4000_50MV; // +/- 50mV
        maxRange = (PS_RANGE)PS4000_20V;
        timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 1; // 125 MHz
        minTimebase = 1;
        signalGeneratorPrecision = 20.0e6 / (double)UINT32_MAX;
    }
    else if (model == PS4223 || model == PS4224 || model == PS4423 || model == PS4424)
    {
        minRange = (PS_RANGE)PS4000_50MV; // +/- 50mV
        maxRange = (PS_RANGE)PS4000_100V;
        timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 0; // 80 Mhz
        minTimebase = 0;
    }
    else if (model == PS4224IEPE)
    {
        minRange = (PS_RANGE)PS4000_50MV; // +/- 50mV
        maxRange = (PS_RANGE)PS4000_20V;
        timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 0; // 80 Mhz
        minTimebase = 0;
    }

    for (uint8_t chan = PS_CHANNEL_A; chan < numAvailableChannels; chan++)
    {
        channelNonSmartMinRanges[chan] = channelMinRanges[chan] = minRange;
        channelNonSmartMaxRanges[chan] = channelMaxRanges[chan] = maxRange;
    }

    maxTimebase = ((uint32_t)1<<30) - 1;

    retVal = AllocateBuffers();

    return retVal;
}