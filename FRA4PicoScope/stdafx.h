// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <vector>
#include <stack>
#include <string>
#include <iostream>
#include <locale>
#include <codecvt>
#include <iomanip>
#include <complex>
#include <unordered_map>
#include <algorithm>
#include <variant>

using std::exception;
using std::runtime_error;
using std::stringstream;
using std::istringstream;
using std::wstringstream;
using std::wstring_convert;
using std::wifstream;
using std::wofstream;
using std::ofstream;
using std::ios;
using std::ios_base;
using std::locale;
using std::fixed;
using std::setprecision;
using std::codecvt_utf8;
using std::get;
using std::make_tuple;
using std::pair;
using std::tuple;
using std::vector;
using std::array;
using std::stack;
using std::unordered_map;
using std::variant;
using std::string;
using std::wstring;
using std::noskipws;
using std::unique_ptr;
using std::numeric_limits;
using std::complex;
using std::minmax_element;