//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: PicoScopeInterface.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "StdAfx.h"
#include "picoStatus.h"
#include "FRA4PicoScopeInterfaceTypes.h"
#include <string>
#include <vector>

// If either of these enumerations are updated in a way that invalidates existing tags,
// don't forget to change the application version.

typedef enum
{
    PSDEMO_FAMILY,
    PS2000,
    PS2000A,
    PS3000,
    PS3000A,
    PS4000,
    PS4000A,
    PS5000,
    PS5000A,
    PS6000,
    PS6000A,
#if defined(_M_X64)
    PSOSPA,
#endif
    PS_NO_FAMILY
} ScopeDriverFamily_T;

typedef enum
{
    PSDEMO_SCOPE,
    PS2204A,
    PS2205A,
    PS2206A,
    PS2207A,
    PS2208A,
    PS2206B,
    PS2207B,
    PS2208B,
    PS2405A,
    PS2406B,
    PS2407B,
    PS2408B,
    PS2205MSO,
    PS2205AMSO,
    PS2206BMSO,
    PS2207BMSO,
    PS2208BMSO,
    PS3203D,
    PS3203DMSO,
    PS3204A,
    PS3204B,
    PS3204MSO,
    PS3204D,
    PS3204DMSO,
    PS3205A,
    PS3205B,
    PS3205MSO,
    PS3205D,
    PS3205DMSO,
    PS3206A,
    PS3206B,
    PS3206MSO,
    PS3206D,
    PS3206DMSO,
    PS3207A,
    PS3207B,
    PS3403D,
    PS3403DMSO,
    PS3404A,
    PS3404B,
    PS3404D,
    PS3404DMSO,
    PS3405A,
    PS3405B,
    PS3405D,
    PS3405DMSO,
    PS3406A,
    PS3406B,
    PS3406D,
    PS3406DMSO,
    PS3425,
    PS4224,
    PS4224IEPE,
    PS4424,
    PS4262,
    PS4444,
    PS4824,
    PS4224A,
    PS4424A,
    PS4824A,
    PS5242A,
    PS5242B,
    PS5442A,
    PS5442B,
    PS5243A,
    PS5243B,
    PS5443A,
    PS5443B,
    PS5244A,
    PS5244B,
    PS5444A,
    PS5444B,
    PS5242D,
    PS5442D,
    PS5243D,
    PS5443D,
    PS5244D,
    PS5444D,
    PS5242DMSO,
    PS5442DMSO,
    PS5243DMSO,
    PS5443DMSO,
    PS5244DMSO,
    PS5444DMSO,
    PS6402C,
    PS6402D,
    PS6403C,
    PS6403D,
    PS6404C,
    PS6404D,
    PS6407,
    PS6403E,
    PS6404E,
    PS6424E,
    PS6804E,
    PS6824E,
    PS6405E,
    PS6425E,
    PS6406E,
    PS6426E,
    PS2202,
    PS2203,
    PS2204,
    PS2205,
    PS2206,
    PS2207,
    PS2208,
    PS3204,
    PS3205,
    PS3206,
    PS3223,
    PS3224,
    PS3423,
    PS3424,
    PS4223,
    PS4423,
    PS4226,
    PS4227,
    PS5203,
    PS5204,
    PS6402,
    PS6402A,
    PS6402B,
    PS6403,
    PS6403A,
    PS6403B,
    PS6404,
    PS6404A,
    PS6404B,
#if defined(_M_X64) // PSOSPA driver is only available in 64-bit
    PS3417E,
    PS3418E,
    PS3417EMSO,
    PS3418EMSO,
#endif
    PS_NUM_SCOPE_MODELS,
    PS_NO_MODEL = -1
} ScopeModel_T;

typedef void (__stdcall *psBlockReady)
(
    int16_t      handle,
    PICO_STATUS  status,
    void         *pParameter
);

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: class PicoScope
//
// Purpose: This is a pure virtual interface class, defining a common interface to PicoScope objects
//          for the purposes of supporting FRA.
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

class PicoScope
{
        friend class ScopeSelector;

    public:
        PicoScope() : initialized(false), model(PS_NO_MODEL), family(PS_NO_FAMILY), numAvailableChannels(2), minFuncGenFreq(0.0), maxFuncGenFreq(0.0), minFuncGenVpp(0.0), maxFuncGenVpp(0.0), signalGeneratorPrecision(0.0), compatible(false) {};
        virtual ~PicoScope() {};

        class PicoPowerChange : public exception
        {
            public:
                PicoPowerChange(PICO_STATUS _state) { state = _state; }
                PICO_STATUS GetState(void) const { return state; }
            private:
                PICO_STATUS state;
        };

        virtual bool Initialized(void) = 0;
        virtual bool Connected(void) = 0;
        virtual bool USB3ScopeNotOnUSB3Port(void) = 0;
        virtual uint8_t GetNumChannels( void ) = 0;
        virtual void GetAvailableCouplings( PS_CHANNEL channel, vector<wstring>& couplingText ) = 0;
        virtual uint32_t GetMinTimebase( void ) = 0;
        virtual uint32_t GetMaxTimebase( void ) = 0;
        virtual bool GetNextTimebase( uint32_t timebase, RESOLUTION_T resolution, uint8_t roundFaster, uint32_t* newTimebase ) = 0;
        virtual bool TimebaseIsSparse( void ) = 0;
        virtual bool TimebaseIsValidForResolution( uint32_t timebase, RESOLUTION_T resolution ) = 0;
        virtual void SetDesiredNoiseRejectModeTimebase( uint32_t timebase ) = 0;
        virtual bool GetNoiseRejectModeTimebase( uint32_t& timebase, bool* adjustedByChannelSettings = NULL ) = 0;
        virtual uint32_t GetDefaultNoiseRejectModeTimebase( void ) = 0;
        virtual bool GetNoiseRejectModeSampleRate( double& sampleRate ) = 0;
        virtual double GetSignalGeneratorPrecision( void ) = 0;
        virtual double GetClosestSignalGeneratorFrequency( double requestedFreq ) = 0;
        virtual uint32_t GetMaxDataRequestSize( void ) = 0;
        virtual PS_RANGE GetMinRange( PS_CHANNEL channel, PS_COUPLING coupling, double dcoffset = 0.0 ) = 0;
        virtual PS_RANGE GetMaxRange( PS_CHANNEL channel, PS_COUPLING coupling, double dcoffset = 0.0 ) = 0;
        virtual void GetMinMaxChannelDcOffsets(double& minDcOffset, double& maxDcOffset) = 0;
        virtual bool GetMaxValue(int16_t& maxValue) = 0;
        virtual double GetMinFuncGenFreq( void ) = 0;
        virtual double GetMaxFuncGenFreq( void ) = 0;
        virtual double GetMinFuncGenVpp( void ) = 0;
        virtual double GetMaxFuncGenVpp( void ) = 0;
        virtual bool SupportsChannelDcOffset(void) = 0;
        virtual double GetMinNonZeroFuncGenVpp( void ) = 0;
        virtual bool SetResolution( RESOLUTION_T resolution, RESOLUTION_T& actualResolution ) = 0;
        virtual bool GetMaxResolutionForFrequency( double frequency, RESOLUTION_T& resolution ) = 0;
        virtual bool GetMaxResolutionForTimebase( uint32_t timebase, RESOLUTION_T& resolution ) = 0;
        virtual bool SetChannelConfigurationForMinimumTimebase( void ) = 0;
        virtual RESOLUTION_T GetMaxResolutionForTimebase( uint32_t timebase ) = 0;
        virtual uint32_t GetMinTimebaseForResolution( RESOLUTION_T resolution ) = 0;
        virtual std::wstring GetResolutionString(RESOLUTION_T resolution) = 0;
        virtual bool IsCompatible( void ) = 0;
        virtual bool RequiresExternalSignalGenerator( void ) = 0;
        virtual bool GetModel( ScopeModel_T &model ) = 0;
        virtual bool GetModel( wstring &model ) = 0;
        virtual bool GetFamily( ScopeDriverFamily_T& family ) = 0;
        virtual bool GetSerialNumber( wstring &sn ) = 0;
        virtual bool SetupChannel( PS_CHANNEL channel, PS_COUPLING coupling, PS_RANGE range, double offset, double currentFrequency = 0.0 ) = 0;
        virtual bool DisableAllDigitalChannels( void ) = 0;
        virtual bool DisableChannel( PS_CHANNEL channel ) = 0;
        virtual bool SetSignalGenerator( double vPP, double offset, double frequency ) = 0;
        virtual bool DisableSignalGenerator( void ) = 0;
        virtual bool DisableChannelTriggers( void ) = 0;
        virtual bool GetMaxSamples( uint32_t* maxSamples, RESOLUTION_T resolution, uint32_t timebase ) = 0;
        virtual bool GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase ) = 0;
        virtual bool GetFrequencyFromTimebase( uint32_t timebase, double &frequency ) = 0;
        virtual bool RunBlock( int32_t numSamples, uint32_t timebase, double *timeIndisposedMs, psBlockReady lpReady, void *pParameter ) = 0;
        virtual void SetChannelDesignations( PS_CHANNEL inputChannel, PS_CHANNEL outputChannel ) = 0;
        virtual bool ChannelDesignationsAreOptimal( void ) = 0;
        virtual bool SetChannelAttenuation( PS_CHANNEL channel, ATTEN_T atten ) = 0;
        virtual bool GetChannelAttenuation( PS_CHANNEL channel, ATTEN_T& atten ) = 0;
        virtual bool IsAutoAttenuation( PS_CHANNEL channel ) = 0;
        virtual void SetSmartProbeCallback(SMART_PROBE_CALLBACK smartProbeCB) = 0;
        virtual bool GetData( uint32_t numSamples, uint32_t startIndex, vector<int16_t>** inputBuffer, vector<int16_t>** outputBuffer ) = 0;
        virtual bool GetCompressedData( uint32_t numSamples, 
                                        vector<int16_t>& inputCompressedMinBuffer, vector<int16_t>& outputCompressedMinBuffer,
                                        vector<int16_t>& inputCompressedMaxBuffer, vector<int16_t>& outputCompressedMaxBuffer ) = 0;
        virtual bool GetPeakValues( uint16_t& inputPeak, uint16_t& outputPeak, bool& inputOv, bool& outputOv ) = 0;
        virtual bool ChangePower( PICO_STATUS powerState ) = 0;
        virtual bool CancelCapture( void ) = 0;
        virtual bool Close( void ) = 0;

        virtual const RANGE_INFO_T* GetRangeCaps( PS_CHANNEL channel ) = 0;
        virtual const std::vector<RESOLUTION_T> GetResolutionCaps( void ) = 0;

    private:
        virtual bool InitializeScope( void ) = 0;
        virtual bool IsUSB3_0Connection(void) = 0;

    protected:
        bool initialized;
        ScopeModel_T model;
        ScopeDriverFamily_T family;
        uint8_t numAvailableChannels;
        double minFuncGenFreq;
        double maxFuncGenFreq;
        double minFuncGenVpp;
        double maxFuncGenVpp;
        double signalGeneratorPrecision;
        bool supportsChannelDcOffset;
        bool compatible;
        bool requiresExternalSignalGenerator;
        UsbVersion_T usbVersion;
};