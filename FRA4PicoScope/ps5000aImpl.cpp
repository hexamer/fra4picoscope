//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ps5000aImpl.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "utility.h"
#include "ps5000aApi.h"
#include "picoStatus.h"
#include "ps5000aImpl.h"
#include "StatusLog.h"

#define PS5000A_IMPL
#include "psCommonImpl.cpp"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps5000aImpl::GetTimebase
//
// Purpose: Get a timebase from a desired frequency, rounding such that the frequency is at least
//          as high as requested, if possible.
//
// Parameters: [in] desiredFrequency: caller's requested frequency in Hz
//             [in] resolution: the vertical resolution to consider; can use RESOLUTION_CURRENT
//             [out] actualFrequency: the frequency corresponding to the returned timebase.
//             [out] timebase: the timebase that will achieve the requested freqency or greater
//
// Notes: Assumes we are using 8, 12, or 15 bits.  15 bits is always better than 14 bits for FRA
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps5000aImpl::GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase )
{
    bool retVal = false;

    if (RESOLUTION_CURRENT == resolution)
    {
        resolution = (RESOLUTION_T)currentResolution;
    }

    if (desiredFrequency != 0.0 && actualFrequency && timebase)
    {
        if (desiredFrequency > 125.0e6)
        {
            *timebase = saturation_cast<uint32_t,double>(log(1.0e9/desiredFrequency) / M_LN2); // ps5000apg.en r4 p22; log2(n) implemented as log(n)/log(2)
        }
        else
        {
            if (resolution == PS5000A_DR_12BIT) // Should never be executed since 15 bit resolution is always favored in this range
            {
                *timebase = saturation_cast<uint32_t, double>((62.5e6 / desiredFrequency) + 3.0); // ps5000apg.en r4: p22
            }
            else // 8 and 15 bit are the same
            {
                *timebase = saturation_cast<uint32_t, double>((125.0e6 / desiredFrequency) + 2.0); // ps5000apg.en r4: p22
            }
        }

        // Clip any timebases that are too low for vertical resolution
        switch (resolution)
        {
            case PS5000A_DR_15BIT:
                *timebase = max(*timebase,3);
                break;
            case PS5000A_DR_12BIT:
                *timebase = max(*timebase,2);
                break;
            case PS5000A_DR_8BIT:
                *timebase = max(*timebase,1);
                break;
            default:
                break;
        }

        retVal = GetFrequencyFromTimebase(*timebase, *actualFrequency);
    }

    return retVal;
}

bool ps5000aImpl::GetFrequencyFromTimebase(uint32_t timebase, double &frequency)
{
    bool retVal = false;

    if (timebase >= minTimebase && timebase <= maxTimebase)
    {
        if (timebase <= 3)
        {
            frequency = 1.0e9 / (double)(1<<(timebase)); // ps5000apg.en r4: p22
        }
        else
        {
            if (currentResolution == PS5000A_DR_12BIT) // Should never be executed since 15 bit resolution is always favored in this range
            {
                frequency = 62.5e6 / ((double)(timebase - 3)); // ps5000apg.en r4: p22
            }
            else
            {
                frequency = 125.0e6 / ((double)(timebase - 2)); // ps5000apg.en r4: p22
            }
        }
        retVal = true;

    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps5000aImpl::InitializeScope
//
// Purpose: Initialize scope/family-specific implementation details.
//
// Parameters: N/A
//
// Notes: Assumes scope was opened with max resolution
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps5000aImpl::InitializeScope(void)
{
    bool retVal = false;
    PS_RANGE minRange, maxRange;

    wstringstream scopeStatusText;
    timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 4; // for PS5000A => 62.5 MHz approximately 3x HW BW limiter

    minTimebase = 1;
    maxTimebase = (std::numeric_limits<uint32_t>::max)();

    // Map is in decending order of min timebase
    flexResMinTbMap = {{PS5000A_DR_15BIT, 3},
                       {PS5000A_DR_12BIT, 2},
                       {PS5000A_DR_8BIT, 1}};

    currentResolution = maxResolution = PS5000A_DR_15BIT;

    if (model == PS5242D || model == PS5442D || model == PS5243D || model == PS5443D || model == PS5244D || model == PS5444D ||
        model == PS5242DMSO || model == PS5442DMSO || model == PS5243DMSO || model == PS5443DMSO || model == PS5244DMSO || model == PS5444DMSO)
    {
        signalGeneratorPrecision = 100.0e6 / (double)UINT32_MAX;
    }
    else
    {
        signalGeneratorPrecision = 200.0e6 / (double)UINT32_MAX;
    }

    minRange = (PS_RANGE)PS5000A_10MV;
    maxRange = (PS_RANGE)PS5000A_20V;

    // Save the value set by the scope factory (ScopeSelector)
    numActualChannels = numAvailableChannels;

    if (PICO_POWER_SUPPLY_NOT_CONNECTED == initialPowerState ||
        PICO_USB3_0_DEVICE_NON_USB3_0_PORT == initialPowerState)
    {
        scopeStatusText << L"ps5000aChangePowerSource( " << handle << ", " << PICO_POWER_SUPPLY_NOT_CONNECTED << L" )";
        LogMessage( scopeStatusText.str(), PICO_API_CALL );
        ps5000aChangePowerSource(handle, initialPowerState);
        numAvailableChannels = 2;
    }
    else
    {
        numAvailableChannels = numActualChannels;
    }

    for (uint8_t chan = PS_CHANNEL_A; chan < numActualChannels; chan++)
    {
        channelNonSmartMinRanges[chan] = channelMinRanges[chan] = minRange;
        channelNonSmartMaxRanges[chan] = channelMaxRanges[chan] = maxRange;
    }

    retVal = AllocateBuffers();

    return retVal;
}