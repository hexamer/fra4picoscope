//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014-2024 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: psospaImpl.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "utility.h"
#include "psospaApi.h"
#include "picoStatus.h"
#include "psospaImpl.h"
#include "StatusLog.h"

#define PSOSPA_IMPL
#include "psCommonImpl.cpp"

const std::vector<std::wstring> psospaImpl::baseAvailableCouplings = { TEXT("AC"), TEXT("DC 1M"), TEXT("DC 50R") };
const std::vector<std::wstring> psospaImpl::restrictedAvailableCouplings = { TEXT("AC"), TEXT("DC 1M") };

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: psospaImpl::GetTimebase
//
// Purpose: Get a timebase from a desired frequency, rounding such that the frequency is at least
//          as high as requested, if possible.
//
// Parameters: [in] desiredFrequency: caller's requested frequency in Hz
//             [in] resolution: the vertical resolution to consider; can use RESOLUTION_CURRENT
//             [out] actualFrequency: the frequency corresponding to the returned timebase.
//             [out] timebase: the timebase that will achieve the requested freqency or greater
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psospaImpl::GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase )
{
    bool retVal = false;
    wstringstream fraStatusText;
    PICO_CHANNEL_FLAGS channelFlags;
    double timeIntervalAvailable;
    PICO_STATUS status;

    channelFlags = (PICO_CHANNEL_FLAGS)((1<<mInputChannel) | (1<<mOutputChannel));

    if (RESOLUTION_CURRENT == resolution)
    {
        resolution = (RESOLUTION_T)currentResolution;
    }

    LOG_PICO_API_CALL( BOOST_PP_STRINGIZE(psospaNearestSampleIntervalStateless), handle, channelFlags, 1.0 / desiredFrequency, 1, (PICO_DEVICE_RESOLUTION)resolution, timebase, &timeIntervalAvailable );
    if (PICO_OK == (status = psospaNearestSampleIntervalStateless( handle, channelFlags, 1.0 / desiredFrequency, 1, (PICO_DEVICE_RESOLUTION)resolution, timebase, &timeIntervalAvailable )))
    {
        retVal = true;
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Unable to determine nearest timebase in GetTimebase: " << status;
        LogMessage( fraStatusText.str() );
    }

    if (retVal)
    {
        *actualFrequency = 1.0 / timeIntervalAvailable;
    }

    return retVal;
}

bool psospaImpl::GetFrequencyFromTimebase( uint32_t timebase, double &frequency )
{
    // Timebase on PSPOSPA is simply the number of picoseconds
    frequency = 1e12 / (double)timebase;

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: psospaImpl::AdjustResolutionForChannels
//
// Purpose: Adjust a resolution parameter based on channels set
//
// Parameters: [in] resolution - the resolution to be adjusted
//             [out] return - the adjusted resolution
//
// Notes: This is a null function for current PSOSPA scopes which have no channel-block based limits
//
///////////////////////////////////////////////////////////////////////////////////////////////////

RESOLUTION_T psospaImpl::AdjustResolutionForChannels(RESOLUTION_T resolution)
{
    return resolution;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: psospaImpl::InitializeScope
//
// Purpose: Initialize scope/family-specific implementation details.
//
// Parameters: N/A
//
// Notes: Assumes scope was opened with max resolution
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psospaImpl::InitializeScope(void)
{
    bool retVal = true;
    PS_RANGE minRange, maxRange;
    double noiseRejectSamplingFrequency;
    uint32_t noiseRejectSamplingTimebase;
    uint64_t numSamples;
    double minFrequencyOut;
    double maxFrequencyOut;
    double minFrequencyStepOut;
    double maxFrequencyStepOut;
    double minDwellTimeOut;
    double maxDwellTimeOut;

    minTimebase = 400; // 2.5GHz => 400 ps
    maxTimebase = (std::numeric_limits<uint32_t>::max)();

    // A bandwidth limiter is available on these scopes and will be used.  Lowest bandwidth limiter for 10 bit mode is 20 MHz.
    // So, like PS5000A, triple that.
    // Need to set channels first for GetTimebase to work
    mInputChannel = PS_CHANNEL_A;
    mOutputChannel = PS_CHANNEL_B;
    if (GetTimebase( 60e6, (RESOLUTION_T)PICO_DR_10BIT, &noiseRejectSamplingFrequency, &noiseRejectSamplingTimebase ))
    {
        timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = noiseRejectSamplingTimebase;
    }
    else
    {
        retVal = false;
    }

    if (retVal)
    {
        // Map is in decending order of min timebase
        flexResMinTbMap = { {PICO_DR_10BIT, 800},
                           {PICO_DR_8BIT, 400} };

        currentResolution = maxResolution = PICO_DR_10BIT;

        signalGeneratorPrecision = 200.0e6 / (double)(1ULL << 48);

        if (PICO_OK != psospaSigGenFrequencyLimits( handle, PICO_SINE, &numSamples,
                                                    &minFrequencyOut, &maxFrequencyOut,
                                                    &minFrequencyStepOut, &maxFrequencyStepOut,
                                                    &minDwellTimeOut, &maxDwellTimeOut ))
        {
            retVal = false;
        }
        else
        {
            minFuncGenFreq = minFrequencyOut;
            maxFuncGenFreq = maxFrequencyOut;
        }

        if (retVal)
        {
            minRange = 0;
            maxRange = 11;

            channelCurrentAvailableCouplings.resize( PS_MAX_CHANNELS );

            for (uint8_t chan = PS_CHANNEL_A; chan < numAvailableChannels; chan++)
            {
                channelNonSmartMinRanges[chan] = channelMinRanges[chan] = minRange;
                channelNonSmartMaxRanges[chan] = channelMaxRanges[chan] = maxRange;
                channelCurrentAvailableCouplings[chan] = baseAvailableCouplings;
            }

            retVal = AllocateBuffers();
        }
    }

    return retVal;
}