//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2020 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: psDemoImpl.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include <immintrin.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <boost/math/special_functions/round.hpp>
using namespace boost::math;
#include "utility.h"
#include "picoStatus.h"
#include "psDemoImpl.h"
#include "StatusLog.h"

#define SIMULATE_CAPTURE_TIME

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: psDemoImpl::GetTimebase
//
// Purpose: Get a timebase from a desired frequency, rounding such that the frequency is at least
//          as high as requested, if possible.
//
// Parameters: [in] desiredFrequency: caller's requested frequency in Hz
//             [in] resolution: the vertical resolution to consider; can use RESOLUTION_CURRENT
//             [out] actualFrequency: the frequency corresponding to the returned timebase.
//             [out] timebase: the timebase that will achieve the requested freqency or greater
//
// Notes: Mimics PS5000A, but without resolution restrictions
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase )
{
    bool retVal = false;

    if (desiredFrequency > 125.0e6)
    {
        *timebase = saturation_cast<uint32_t,double>(log(1.0e9/desiredFrequency) / M_LN2); // ps5000apg.en r4 p22; log2(n) implemented as log(n)/log(2)
    }
    else
    {
        *timebase = saturation_cast<uint32_t, double>((125.0e6 / desiredFrequency) + 2.0); // ps5000apg.en r4: p22
    }

    *timebase = max(*timebase,1);

    retVal = GetFrequencyFromTimebase(*timebase, *actualFrequency);

    return retVal;
}

bool psDemoImpl::GetFrequencyFromTimebase( uint32_t timebase, double &frequency )
{
    bool retVal = false;

    if (timebase >= minTimebase && timebase <= maxTimebase)
    {
        if (timebase <= 3)
        {
            frequency = 1.0e9 / (double)(1<<(timebase)); // ps5000apg.en r4: p22
        }
        else
        {
            frequency = 125.0e6 / ((double)(timebase - 2)); // ps5000apg.en r4: p22
        }
        retVal = true;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: psDemoImpl::InitializeScope
//
// Purpose: Initialize scope/family-specific implementation details.
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::InitializeScope(void)
{
    wstringstream fraStatusText;
    bool retVal = false;
    PS_RANGE minRange, maxRange;

    timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 1;

    minTimebase = 1;
    maxTimebase = (std::numeric_limits<uint32_t>::max)();
    signalGeneratorPrecision = 200.0e6 / (double)(std::numeric_limits<uint32_t>::max)();
    minFuncGenFreq = signalGeneratorPrecision;

    maxRange = (PS_RANGE)PSDEMO_20V;
    minRange = (PS_RANGE)PSDEMO_10MV;

    for (uint8_t chan = PS_CHANNEL_A; chan < numAvailableChannels; chan++)
    {
        channelNonSmartMinRanges[chan] = channelMinRanges[chan] = minRange;
        channelNonSmartMaxRanges[chan] = channelMaxRanges[chan] = maxRange;
    }

    if (AllocateBuffers())
    {
        GenerateDDSTable();

        // Create an unnamed waitable timer.
        hCaptureTimer = CreateWaitableTimer(NULL, TRUE, NULL);
        if (NULL == hCaptureTimer)
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to create simulated time timer.";
            LogMessage( fraStatusText.str() );
        }
        else
        {
            // Create the start capture event
            hStartCaptureEvent = CreateEventW( NULL, true, false, L"Start Simulated Capture" );
            if (NULL == hStartCaptureEvent)
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to create start Start Simulated Capture event.";
                LogMessage( fraStatusText.str() );
            }
            else
            {
                // Start the simulated capture thread
                hSimulatedCaptureThread = CreateThread( NULL, 0, SimulateCapture, this, 0, NULL );
                if (NULL == hSimulatedCaptureThread)
                {
                    fraStatusText.clear();
                    fraStatusText.str(L"");
                    fraStatusText << L"Fatal error: Failed to create simulated capture thread.";
                    LogMessage( fraStatusText.str() );
                }
                else
                {
                    retVal = true;
                    open = true;
                }
            }
        }
    }

    return retVal;
}

const uint32_t psDemoImpl::maxDataRequestSize = 16 * 1024 * 1024; // 16 MSamp (32 MB); for this simulation scope, this needs to be divisible by simdLanes.
const uint8_t psDemoImpl::simdLanesSine = 4;
const uint8_t psDemoImpl::simdLanesVolts = 8;
const uint8_t psDemoImpl::simdLanesSamples = 8;
const uint32_t psDemoImpl::ddsTableSize = (std::numeric_limits<uint16_t>::max)()+1;
bool psDemoImpl::open = false;

// This table covers the a superset of the ranges available on any PicoScope with a 1X probe attached
const std::vector<RANGE_INFO_T> psDemoImpl::base1xRangeInfo =
{
    {0.010, 0.5, 0.0, L"� 10 mV"},
    {0.020, 0.4, 2.0, L"� 20 mV"},
    {0.050, 0.5, 2.5, L"� 50 mV"},
    {0.100, 0.5, 2.0, L"� 100 mV"},
    {0.200, 0.4, 2.0, L"� 200 mV"},
    {0.500, 0.5, 2.5, L"� 500 mV"},
    {1.0, 0.5, 2.0, L"� 1 V"},
    {2.0, 0.4, 2.0, L"� 2 V"},
    {5.0, 0.5, 2.5, L"� 5 V"},
    {10.0, 0.5, 2.0, L"� 10 V"},
    {20.0, 0.0, 2.0, L"� 20 V"}
};

const std::vector<double> psDemoImpl::attenScalingFactors =
{
    1.0,
    10.0,
    20.0,
    25.0,
    50.0,
    100.0,
    200.0,
    250.0,
    500.0,
    1000.0
};

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Constructor
//
// Purpose: Handles some of object initialization
//
// Parameters: N/A
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

psDemoImpl::psDemoImpl( int16_t _handle ) : PicoScope()
{
    handle = _handle;
    initialized = false;
    minTimebase = 0;
    maxTimebase = 0;
    timebaseNoiseRejectMode = 0;
    defaultTimebaseNoiseRejectMode = 0;
    signalGeneratorPrecision = 0.0;
    mInputChannel = PS_CHANNEL_INVALID;
    mOutputChannel = PS_CHANNEL_INVALID;

    mNumSamples = 0;
    buffersDirty = true;

    channelRangeInfo.resize(PS_MAX_CHANNELS);
    std::fill(channelRangeInfo.begin(), channelRangeInfo.end(), base1xRangeInfo);
    channelAttens.resize(PS_MAX_CHANNELS);
    std::fill(channelAttens.begin(), channelAttens.end(), ATTEN_1X);
    channelManualAttens = channelAttens;
    channelMinRanges.resize(PS_MAX_CHANNELS);
    std::fill(channelMinRanges.begin(), channelMinRanges.end(), 0);
    channelNonSmartMinRanges = channelMinRanges;
    channelMaxRanges.resize(PS_MAX_CHANNELS);
    std::fill(channelMaxRanges.begin(), channelMaxRanges.end(), 0);
    channelNonSmartMaxRanges = channelMaxRanges;
    channelRangeOffsets.resize(PS_MAX_CHANNELS);
    std::fill(channelRangeOffsets.begin(), channelRangeOffsets.end(), 0);
    channelSmartProbeConnected.resize(PS_MAX_CHANNELS);
    std::fill(channelSmartProbeConnected.begin(), channelSmartProbeConnected.end(), false);
    channelAutoAttens = channelSmartProbeConnected;

    currentSigGenVpp = 0.0;
    currentSigGenOffset = 0.0;
    currentSigGenFrequency = 0.0;
    simDataTimebaseBasis = 0;
    simDataSigGenVppBasis = 0.0;
    simDataSigGenOffsetBasis = 0.0;
    simDataSigGenFrequencyBasis = 0.0;

    currentChannelCoupling.resize(numAvailableChannels);
    currentChannelRange.resize(numAvailableChannels);
    currentChannelOffset.resize(numAvailableChannels);
    simDataChannelCouplingBasis.resize(numAvailableChannels);
    simDataChannelRangeBasis.resize(numAvailableChannels);
    simDataChannelOffsetBasis.resize(numAvailableChannels);

    simulatedLpfCornerFrequency = 1000.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Destructor
//
// Purpose: Handles object cleanup
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

psDemoImpl::~psDemoImpl()
{
    if (handle != -1)
    { 
        Close();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Initialized
//
// Purpose: Indicate if the scope has been initialized
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::Initialized( void )
{
    return (handle != -1 && initialized);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Connected
//
// Purpose: Detect if the scope is connected
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::Connected( void )
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: USB3ScopeNotOnUSB3Port
//
// Purpose: Detect if a USB 3 scope is connected to a USB 2 (or earlier) port
//
// Parameters: N/A
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::USB3ScopeNotOnUSB3Port(void)
{
    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common accessors
//
// Purpose: Get scope parameters
//
// Parameters: Various
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

uint8_t psDemoImpl::GetNumChannels( void )
{
    return numAvailableChannels;
}

void psDemoImpl::GetAvailableCouplings( PS_CHANNEL channel, vector<wstring>& couplingText )
{
    UNREFERENCED_PARAMETER(channel);

    couplingText.clear();
    couplingText.resize(2);
    couplingText[0] = TEXT("AC");
    couplingText[1] = TEXT("DC");
}

uint32_t psDemoImpl::GetMinTimebase( void )
{
    return minTimebase;
}

uint32_t psDemoImpl::GetMaxTimebase( void )
{
    return maxTimebase;
}

bool psDemoImpl::GetNextTimebase( uint32_t timebase, RESOLUTION_T resolution, uint8_t roundFaster, uint32_t* newTimebase )
{
    bool retVal = false;
    if (newTimebase)
    {
        if (timebase == minTimebase)
        {
            if (!roundFaster)
            {
                timebase++;
            }
        }
        else if (timebase == maxTimebase)
        {
            if (roundFaster)
            {
                timebase--;
            }
        }
        else
        {
            roundFaster ? timebase-- : timebase++;
        }
        retVal = true;
        *newTimebase = timebase;
    }
    return retVal;
}

bool psDemoImpl::TimebaseIsSparse( void )
{
    return false;
}

bool psDemoImpl::TimebaseIsValidForResolution( uint32_t timebase, RESOLUTION_T resolution )
{
    return (timebase >= GetMinTimebaseForResolution( resolution ) && timebase <= maxTimebase);
}

void psDemoImpl::SetDesiredNoiseRejectModeTimebase( uint32_t timebase )
{
    timebaseNoiseRejectMode = timebase;
}

uint32_t psDemoImpl::GetDefaultNoiseRejectModeTimebase( void )
{
    return defaultTimebaseNoiseRejectMode;
}

bool psDemoImpl::GetNoiseRejectModeTimebase( uint32_t& timebase, bool* adjustedByChannelSettings )
{
    timebase = timebaseNoiseRejectMode;
    if (adjustedByChannelSettings)
    {
        *adjustedByChannelSettings = false;
    }
    return true;
}

bool psDemoImpl::GetNoiseRejectModeSampleRate( double& sampleRate )
{
    bool retVal = false;
    wstringstream fraStatusText;
    uint32_t timebase;
    if (GetNoiseRejectModeTimebase(timebase))
    {
        retVal = GetFrequencyFromTimebase( timebase, sampleRate );
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to get noise reject mode sample rate.  Failed to get noise reject mode timebase.";
        LogMessage( fraStatusText.str() );
    }
    return retVal;
}

double psDemoImpl::GetSignalGeneratorPrecision( void )
{
    return signalGeneratorPrecision;
}

uint32_t psDemoImpl::GetMaxDataRequestSize(void)
{
    return maxDataRequestSize;
}

PS_RANGE psDemoImpl::GetMinRange( PS_CHANNEL channel, PS_COUPLING coupling, double dcOffset )
{
    UNREFERENCED_PARAMETER(coupling);
    if (dcOffset >= -20.0 && dcOffset <= 20.0)
    {
        return channelMinRanges[channel];
    }
    else
    {
        return -1;
    }
}

PS_RANGE psDemoImpl::GetMaxRange( PS_CHANNEL channel, PS_COUPLING coupling, double dcOffset )
{
    UNREFERENCED_PARAMETER(coupling);
    if (dcOffset >= -20.0 && dcOffset <= 20.0)
    {
        return channelMaxRanges[channel];
    }
    else
    {
        return -1;
    }
}

void psDemoImpl::GetMinMaxChannelDcOffsets(double& minDcOffset, double& maxDcOffset)
{
    minDcOffset = -20.0;
    maxDcOffset = 20.0;
}

bool psDemoImpl::GetMaxValue(int16_t& maxValue)
{
    maxValue = (int16_t)PSDEMO_MAX_VALUE;
    return true;
}

double psDemoImpl::GetMinFuncGenFreq( void )
{
    return minFuncGenFreq;
}

double psDemoImpl::GetMaxFuncGenFreq( void )
{
    return maxFuncGenFreq;
}

bool psDemoImpl::SupportsChannelDcOffset(void)
{
    return supportsChannelDcOffset;
}

double psDemoImpl::GetMinFuncGenVpp( void )
{
    return minFuncGenVpp;
}

double psDemoImpl::GetMinNonZeroFuncGenVpp( void )
{
    // Current API for setting the signal generator has a resolution of 1 uV.
    // So, to avoid rounding the value to 0, add epsilon
    return max( minFuncGenVpp, 1e-6+std::numeric_limits<double>::epsilon() );
}

double psDemoImpl::GetMaxFuncGenVpp( void )
{
    return maxFuncGenVpp;
}

bool psDemoImpl::IsCompatible( void )
{
    return compatible;
}

bool psDemoImpl::RequiresExternalSignalGenerator( void )
{
    return requiresExternalSignalGenerator;
}

const RANGE_INFO_T* psDemoImpl::GetRangeCaps( PS_CHANNEL channel )
{
    return channelRangeInfo[channel].data();
}

const std::vector<RESOLUTION_T> psDemoImpl::GetResolutionCaps( void )
{
    std::vector<RESOLUTION_T> res;
    res.push_back(RESOLUTION_16BIT);
    return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetResolution
//
// Purpose: Sets vertical resolution for FlexRes scopes.
//
// Parameters: [in] resolution - vertical resolution
//             [out] actualResolution - the actual resolution set
//             [out] return - whether the call succeeded
//
// Notes: This is a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::SetResolution(RESOLUTION_T resolution, RESOLUTION_T& actualResolution)
{
    actualResolution = resolution;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetMaxResolutionForFrequency
//
// Purpose: Sets the maximum vertical resolution for FlexRes scopes compatible with the passed
//          sampling frequency.
//
// Parameters: [in] frequency - sampling frequency
//             [out] actualResolution - the actual resolution set
//             [out] return - whether the call succeeded
//
// Notes: According to SDK documentation, you must call SetChannel after calling this function.
//        This is a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetMaxResolutionForFrequency(double sampleFrequency, RESOLUTION_T& actualResolution )
{
    actualResolution = GetResolutionCaps()[0];
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetMaxResolutionForTimebase
//
// Purpose: Sets the maximum vertical resolution for FlexRes scopes given a fixed timebase.
//
// Parameters: [in] timebase - fixed timebase
//             [out] actualResolution - the actual resolution set
//             [out] return - whether the call succeeded
//
// Notes: According to SDK documentation, you must call SetChannel after calling this function.
//        This is a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetMaxResolutionForTimebase(uint32_t timebase, RESOLUTION_T& actualResolution )
{
    actualResolution = GetResolutionCaps()[0];
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetChannelConfigurationForMinimumTimebase
//
// Purpose: Sets a channel configuration that enables the lowest timebase possible for FRA.
//
// Parameters: [out] return - whether the call succeeded
//
// Notes: This is only relevant for scopes that have timebase dependencies on channel
// configuration (e.g. PS6000 family scopes).  This is a null operation for other scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::SetChannelConfigurationForMinimumTimebase(void)
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetMaxResolutionForTimebase
//
// Purpose: Gets the maximum vertical resolution for FlexRes scopes given a fixed timebase.
//
// Parameters: [in] timebase - fixed timebase
//             [out] return - the maximum resolution
//
// Notes: This is a effectively a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

RESOLUTION_T psDemoImpl::GetMaxResolutionForTimebase(uint32_t timebase)
{
    return RESOLUTION_16BIT;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetMinTimebaseForResolution
//
// Purpose: Gets the minimum timebase for a given vertical resolution for FlexRes scopes.
//
// Parameters: [in] resolution - vertical resolution
//             [out] return - the minimum timebase
//
// Notes: This is a effectively a null operation for non FlexRes scopes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

uint32_t psDemoImpl::GetMinTimebaseForResolution( RESOLUTION_T resolution )
{
    return minTimebase;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetResolutionString
//
// Purpose: Gets a string representing the vertical resolution parameter
//
// Parameters: [in] resolution - the resolutiion setting for the scope
//             [out] return - string representing the current resolution setting
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

std::wstring psDemoImpl::GetResolutionString(RESOLUTION_T resolution)
{
    std::wstring res = L"16 bits";
    return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetClosestSignalGeneratorFrequency
//
// Purpose: Calculates the frequency closest to the requested frequency that the signal generator
//          is capable of generating.
//
// Parameters: [in] requestedFreq - desired frequency
//             [out] return - closest frequency the scope is capable of generating.
//
// Notes: Frequency generator capabilities are governed by the scope's DDS implementation.  The
//        frequency precision is encoded in the PicoScope object's implementation (signalGeneratorPrecision).
//
///////////////////////////////////////////////////////////////////////////////////////////////////

double psDemoImpl::GetClosestSignalGeneratorFrequency( double requestedFreq )
{
    double actualFreq = lround(requestedFreq / signalGeneratorPrecision) * signalGeneratorPrecision;
    // Bound in case any rounding caused us to go outside the capabilities of the generator
    actualFreq = min(actualFreq, maxFuncGenFreq);
    actualFreq = max(actualFreq, minFuncGenFreq);
    return actualFreq;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetModel
//
// Purpose: Gets the model number of the scope as an enumeration
//
// Parameters: [out] model_ - scope model as enumeration type
//             [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetModel(ScopeModel_T& model_)
{
    model_ = model;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetModel
//
// Purpose: Gets the model number of the scope
//
// Parameters: [out] model - wide string to return the scope model
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

#if defined NEW_PS_DRIVER_MODEL
#define INFO_STRING_LENGTH_ARG , &infoStringLength
#else
#define INFO_STRING_LENGTH_ARG
#endif

bool psDemoImpl::GetModel( wstring &model )
{
    model = L"PSDEMO";
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetFamily
//
// Purpose: Gets the driver family of the scope as an enumeration
//
// Parameters: [out] family_ - scope driver family as enumeration type
//             [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetFamily( ScopeDriverFamily_T& family_ )
{
    family_ = family;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetSerialNumber
//
// Purpose: Gets the serial number of the scope
//
// Parameters: [out] sn - wide string to return the scope serial number
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetSerialNumber( wstring &sn )
{
    sn = L"PS-DEMO-001";
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetupChannel
//
// Purpose: Sets a scope's sampling channel settings
//
// Parameters: [in] channel - channel number
//             [in] coupling - AC or DC coupling
//             [in] range - voltage range
//             [in] offset - DC offset
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::SetupChannel( PS_CHANNEL channel, PS_COUPLING coupling, PS_RANGE range, double offset, double currentFrequency )
{
    bool retVal = true;

    currentChannelCoupling[channel] = coupling;
    currentChannelRange[channel] = range;
    currentChannelOffset[channel] = offset;

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DisableAllDigitalChannels
//
// Purpose: Disable all digital channels
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: When digital channels are on, they can reduce available sampling frequency
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::DisableAllDigitalChannels(void)
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DisableChannel
//
// Purpose: Disable a channel
//
// Parameters: [in] channel - channel to be disabled 
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::DisableChannel( PS_CHANNEL channel )
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetSignalGenerator
//
// Purpose: Setup the signal generator to generate a signal with the given amplitude and frequency
//
// Parameters: [in] vPP - voltage peak-to-peak in volts
//             [in] frequency - frequency to generate in Hz
//             [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::SetSignalGenerator( double vPP, double offset, double frequency )
{
    bool retVal = false;

    if (frequency >= minFuncGenFreq && frequency <= maxFuncGenFreq &&
        vPP + 2.0*fabs(offset) <= maxFuncGenVpp &&
        vPP >= minFuncGenVpp)
    {
        currentSigGenVpp = vPP;
        currentSigGenOffset = offset;
        currentSigGenFrequency = frequency;
        retVal = true;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DisableSignalGenerator
//
// Purpose: Disable the signal generator
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::DisableSignalGenerator( void )
{
    bool retVal = true;
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetMaxSamples
//
// Purpose: Gets the maximum possible number of samples per channel per the scope's reported
//          buffer capabilities
//
// Parameters: [out] pMaxSamples - the maximum samples per channel
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetMaxSamples( uint32_t* pMaxSamples, RESOLUTION_T resolution, uint32_t timebase )
{
    UNREFERENCED_PARAMETER(resolution);
    UNREFERENCED_PARAMETER(timebase);
    *pMaxSamples = 1<<31; // 2GB per channel
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: AllocateBuffers
//
// Purpose: Allocates buffers to hold samples transferred from the scope
//
// Parameters: N/A
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////
bool psDemoImpl::AllocateBuffers(void)
{
    wstringstream fraStatusText;
    bool retVal = false;
    uint32_t simDataBufferSize;
    uint32_t maxSamples;

    if (GetMaxSamples( &maxSamples, RESOLUTION_16BIT, minTimebase ))
    {
        simDataBufferSize = min( maxDataRequestSize, maxSamples );
        try
        {
            ddsTable.resize(ddsTableSize);
            channelASines.resize( simDataBufferSize );
            channelAVolts.resize( simDataBufferSize );
            channelBSines.resize( simDataBufferSize );
            channelBVolts.resize( simDataBufferSize );
            channelASamples.resize( simDataBufferSize );
            channelBSamples.resize( simDataBufferSize );
            retVal = true;
        }
        catch(std::exception ex)
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to allocate sample buffers: " << ex.what();
            LogMessage( fraStatusText.str() );
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DisableChannelTriggers
//
// Purpose: Disables any triggers for the scope's sampling channels
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::DisableChannelTriggers( void )
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: RunBlock
//
// Purpose: Starts a sample capture on the scope using block mode
//
// Parameters: [in] numSamples - number of samples to capture
//             [in] timebase - sampling timebase for the scope
//             [out] timeIndisposedMs - estimated time it will take to capture the block of data 
//                                      in milliseconds
//             [in] lpReady - pointer to the callback function to be called when the block
//                            has been captured
//             [in] pParameter - parameter to be based to the callback function
//             [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::RunBlock( int32_t numSamples, uint32_t timebase, double *timeIndisposedMs, psBlockReady _lpReady, void* _pParameter )
{
    wstringstream fraStatusText;
    bool retVal = true;
    double samplingFrequency;

    LARGE_INTEGER liDueTime;

    if (retVal = GetFrequencyFromTimebase(timebase, samplingFrequency))
    {
        currentSigGenOffsetChannelA = (currentChannelCoupling[PS_CHANNEL_A] == PS_AC) ? 0.0 : currentSigGenOffset;
        currentSigGenOffsetChannelB = (currentChannelCoupling[PS_CHANNEL_B] == PS_AC) ? 0.0 : currentSigGenOffset;

        recomputeSines = (timebase != simDataTimebaseBasis || currentSigGenFrequency != simDataSigGenFrequencyBasis || numSamples != mNumSamples || currentBlockNum != 0);
        recomputeVolts = (recomputeSines || currentSigGenVpp != simDataSigGenVppBasis || currentSigGenOffset != simDataSigGenOffsetBasis ||
                          currentSigGenOffsetChannelA != simDataSigGenOffsetBasisChannelA || currentSigGenOffsetChannelB != simDataSigGenOffsetBasisChannelB);
        recomputeChannelASamples = recomputeVolts || currentChannelCoupling[PS_CHANNEL_A] != simDataChannelCouplingBasis[PS_CHANNEL_A] ||
                                   currentChannelRange[PS_CHANNEL_A] != simDataChannelRangeBasis[PS_CHANNEL_A] ||
                                   currentChannelOffset[PS_CHANNEL_A] != simDataChannelOffsetBasis[PS_CHANNEL_A];
        recomputeChannelBSamples = recomputeVolts || currentChannelCoupling[PS_CHANNEL_B] != simDataChannelCouplingBasis[PS_CHANNEL_B] ||
                                   currentChannelRange[PS_CHANNEL_B] != simDataChannelRangeBasis[PS_CHANNEL_B] ||
                                   currentChannelOffset[PS_CHANNEL_B] != simDataChannelOffsetBasis[PS_CHANNEL_B];

        mNumSamples = numSamples;
        currentSamplingFrequency = samplingFrequency;
        simDataTimebaseBasis = timebase;
        simDataSigGenFrequencyBasis = currentSigGenFrequency;
        simDataSigGenVppBasis = currentSigGenVpp;
        simDataSigGenOffsetBasis = currentSigGenOffset;
        simDataSigGenOffsetBasisChannelA = currentSigGenOffsetChannelA;
        simDataSigGenOffsetBasisChannelB = currentSigGenOffsetChannelB;
        simDataChannelCouplingBasis[mInputChannel] = currentChannelCoupling[mInputChannel];
        simDataChannelRangeBasis[mInputChannel] = currentChannelRange[mInputChannel];
        simDataChannelOffsetBasis[mInputChannel] = (float)currentChannelOffset[mInputChannel];
        simDataChannelCouplingBasis[mOutputChannel] = currentChannelCoupling[mOutputChannel];
        simDataChannelRangeBasis[mOutputChannel] = currentChannelRange[mOutputChannel];
        simDataChannelOffsetBasis[mOutputChannel] = (float)currentChannelOffset[mOutputChannel];

        *timeIndisposedMs = 1000.0 * (double)numSamples / samplingFrequency;

        liDueTime.QuadPart = -(LONGLONG)(*timeIndisposedMs * 10000); // Relative time in 100 ns intervals

        // Set the timer
        if (retVal && !SetWaitableTimer(hCaptureTimer, &liDueTime, 0, NULL, NULL, 0))
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to set simulated time timer.";
            LogMessage( fraStatusText.str() );
            retVal = false;
        }

        currentChannelADdsPhase = 0.0;
        UpdateTransferFunction();

        lpReady = _lpReady;
        pParameter = _pParameter;
        SetEvent( hStartCaptureEvent );
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SimulateCapture
//
// Purpose: Thread function that simulates capturing data
//
// Parameters: [in] lpdwThreadParam - thread parameter (pointer to the demo scope)
//             [out] return - See Windows API documentation
//
// Notes: This function computes at least enough samples to get a full stimulus cycle so that it
//        will have an accurate representation of min/max/ov.  So, in cases where the full capture
//        is less than a block, all the data is fully ready and the simulated "transfer" is nearly
//        instantaneous.  In cases where the full capture is larger than a block, the GetData
//        function will have to re-compute it.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI psDemoImpl::SimulateCapture(LPVOID lpdwThreadParam)
{
    wstringstream fraStatusText;
    DWORD dwWaitResult;
    psDemoImpl* pDemoScope = (psDemoImpl*)lpdwThreadParam;

    bool recomputeSines, recomputeVolts, recomputeChannelASamples, recomputeChannelBSamples;
    uint32_t mNumSamples;
    uint32_t blockSize = (uint32_t)pDemoScope->channelASines.size();
    double currentSamplingFrequency, currentSigGenFrequency;
    HANDLE hCaptureTimer = pDemoScope->hCaptureTimer;
    psBlockReady lpReady;
    void *pParameter;
    int16_t handle;

    bool overallInOv, overallOutOv;
    int16_t overallInMinSample, overallInMaxSample, overallOutMinSample, overallOutMaxSample;

    while (1)
    {
        if (ResetEvent( pDemoScope->hStartCaptureEvent ))
        {
            dwWaitResult = WaitForSingleObject( pDemoScope->hStartCaptureEvent, INFINITE );

            if (dwWaitResult == WAIT_OBJECT_0)
            {
                // Makes the code a little easier to read
                recomputeSines = pDemoScope->recomputeSines;
                recomputeVolts = pDemoScope->recomputeVolts;
                recomputeChannelASamples = pDemoScope->recomputeChannelASamples;
                recomputeChannelBSamples = pDemoScope->recomputeChannelBSamples;
                mNumSamples = pDemoScope->mNumSamples;
                currentSamplingFrequency = pDemoScope->currentSamplingFrequency;
                currentSigGenFrequency = pDemoScope->currentSigGenFrequency;
                lpReady = pDemoScope->lpReady;
                pParameter = pDemoScope->pParameter;
                handle = pDemoScope->handle;

                overallInOv = overallOutOv = false;
                overallInMinSample = overallOutMinSample = PSDEMO_MAX_VALUE;
                overallInMaxSample = overallOutMaxSample = PSDEMO_MIN_VALUE;

                // Complete at least one cycle to get an accurate representation of min/max/ov,
                // but complete at least one block or all samples even if it contains multiple cycles
                uint32_t initialSamplesToCompute = min( mNumSamples, max((uint32_t)ceil(currentSamplingFrequency / currentSigGenFrequency), blockSize) );
                uint32_t initialBlocksToCompute = initialSamplesToCompute / blockSize;
                initialBlocksToCompute += (initialSamplesToCompute % blockSize) ? 1 : 0;

                for (pDemoScope->currentBlockNum = 0; pDemoScope->currentBlockNum < initialBlocksToCompute; pDemoScope->currentBlockNum++)
                {
                    pDemoScope->numSamplesInCurrentBlock = min( initialSamplesToCompute, blockSize );
                    // Call functions to compute sines and volts
                    if (recomputeSines)
                    {
                        pDemoScope->GenerateSines();
                    }
                    if (recomputeVolts)
                    {
                        pDemoScope->GenerateVolts();
                    }
                    if (recomputeChannelASamples || recomputeChannelBSamples)
                    {
                        pDemoScope->GenerateSamples(recomputeChannelASamples, recomputeChannelBSamples);
                    }
                    initialSamplesToCompute -= pDemoScope->numSamplesInCurrentBlock;

                    // Compute overall min/max/ov
                    if (pDemoScope->channelAOv)
                    {
                        overallInOv = true;
                    }
                    if (pDemoScope->channelBOv)
                    {
                        overallOutOv = true;
                    }
                    overallInMinSample = min( overallInMinSample, pDemoScope->channelAMinSample );
                    overallOutMinSample = min( overallOutMinSample, pDemoScope->channelBMinSample );
                    overallInMaxSample = max( overallInMaxSample, pDemoScope->channelAMaxSample );
                    overallOutMaxSample = max( overallOutMaxSample, pDemoScope->channelBMaxSample );
                }

                pDemoScope->currentBlockNum--; // Set back to index of last block computed

                pDemoScope->channelAOv = overallInOv;
                pDemoScope->channelBOv = overallOutOv;
                pDemoScope->channelAMinSample = overallInMinSample;
                pDemoScope->channelBMinSample = overallOutMinSample;
                pDemoScope->channelAMaxSample = overallInMaxSample;
                pDemoScope->channelBMaxSample = overallOutMaxSample;
#if defined(SIMULATE_CAPTURE_TIME)
                // If we're done earlier than the simulated capture time, wait
                if (WaitForSingleObject(hCaptureTimer, INFINITE) != WAIT_OBJECT_0)
                {
                    fraStatusText.clear();
                    fraStatusText.str(L"");
                    fraStatusText << L"Fatal error: Failed to wait on simulated time timer: " << GetLastError();
                    LogMessage( fraStatusText.str() );
                }
#endif
                lpReady( handle, PICO_OK, pParameter );
            }
            else
            {
                fraStatusText.clear();
                fraStatusText.str(L"");
                fraStatusText << L"Fatal error: Failed to wait on start simulated capture event: " << dwWaitResult;
                LogMessage( fraStatusText.str() );
                return -1;
            }
        }
        else
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to reset start simulated capture event: " << GetLastError();
            LogMessage( fraStatusText.str() );
            return -1;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GenerateSines
//
// Purpose: Generates the sine samples for input and output channels
//
// Parameters: N/A
//
// Notes: This uses a DDS-like approach.  Because the phase increment can be so small, we have to
//        use doubles here to compute phase.  Also, to account for extremely high cycle runs, it
//        repeatedly removes whole cycles from the phase to avoid precision issues as the phase
//        grows.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void psDemoImpl::GenerateSines(void)
{
    double samplingPhaseIncrement;
    __m256d iterationInputPhases;
    __m256d iterationOutputPhases;
    __m256d iterationPhaseOffset;
    __m256d iterationSamplePhaseOffsets;
    __m128i iterationInputIndices;
    __m128i iterationOutputIndices;
    __m256d ddsTableSizeVector;

    sineDataSimdIterations = numSamplesInCurrentBlock / simdLanesSine;
    sineDataSimdIterations += (numSamplesInCurrentBlock % simdLanesSine) ? 1 : 0;

    samplingPhaseIncrement = simDataSigGenFrequencyBasis / currentSamplingFrequency;

    for (uint8_t i = 0; i < simdLanesSine; i++)
    {
        iterationSamplePhaseOffsets.m256d_f64[i] = i * samplingPhaseIncrement;
    }
    iterationPhaseOffset = _mm256_set1_pd(simdLanesSine*samplingPhaseIncrement);
    ddsTableSizeVector = _mm256_set1_pd(ddsTableSize);

    iterationInputPhases = _mm256_set1_pd(currentChannelADdsPhase);
    iterationInputPhases = _mm256_add_pd(iterationInputPhases, iterationSamplePhaseOffsets);
    iterationInputPhases = _mm256_sub_pd(iterationInputPhases, _mm256_round_pd(iterationInputPhases,_MM_FROUND_TRUNC)); // Reset to the fractional part

    iterationOutputPhases = _mm256_set1_pd(currentChannelBDdsPhase);
    iterationOutputPhases = _mm256_add_pd(iterationOutputPhases, iterationSamplePhaseOffsets);
    iterationOutputPhases = _mm256_sub_pd(iterationOutputPhases, _mm256_round_pd(iterationOutputPhases,_MM_FROUND_TRUNC)); // Reset to the fractional part

    uint32_t i;
    for (i = 0; i < sineDataSimdIterations; i++)
    {
        iterationInputIndices = _mm256_cvttpd_epi32(_mm256_mul_pd(iterationInputPhases, ddsTableSizeVector));
        iterationOutputIndices = _mm256_cvttpd_epi32(_mm256_mul_pd(iterationOutputPhases, ddsTableSizeVector));
#if defined BUILD_FOR_AVX2
        // No advantage on a Skylake 6700K and AVX2 would limit availability even further
        _mm_store_ps(&channelASines[i*simdLanesSine], _mm_i32gather_ps(ddsTable.data(), iterationInputIndices, sizeof(float)));
        _mm_store_ps(&channelBSines[i*simdLanesSine], _mm_i32gather_ps(ddsTable.data(), iterationOutputIndices, sizeof(float)));
#else
        for (int j = 0; j < simdLanesSine; j++)
        {
            channelASines[i*simdLanesSine+j] = ddsTable[iterationInputIndices.m128i_i32[j]];
            channelBSines[i*simdLanesSine+j] = ddsTable[iterationOutputIndices.m128i_i32[j]];
        }
#endif
        iterationInputPhases = _mm256_add_pd(iterationInputPhases, iterationPhaseOffset);
        iterationInputPhases = _mm256_sub_pd(iterationInputPhases, _mm256_round_pd(iterationInputPhases,_MM_FROUND_TRUNC)); // Reset to the fractional part

        iterationOutputPhases = _mm256_add_pd(iterationOutputPhases, iterationPhaseOffset);
        iterationOutputPhases = _mm256_sub_pd(iterationOutputPhases, _mm256_round_pd(iterationOutputPhases,_MM_FROUND_TRUNC)); // Reset to the fractional part
    }
    // Inaccurate for mNunSamples < sines.size(), but doesn't matter since it won't get used again in that case
    currentChannelADdsPhase = iterationInputPhases.m256d_f64[0];
    currentChannelBDdsPhase = iterationOutputPhases.m256d_f64[0];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GenerateVolts
//
// Purpose: Generates the voltage samples for input and output channels
//
// Parameters: N/A
//
// Notes: The current coding would not work for a HPF since a HPF would block DC.  More work would
//        need to be done to handle cases like that.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void psDemoImpl::GenerateVolts(void)
{
    __m256 iterationInputVolts;
    __m256 iterationInputSines;
    __m256 iterationOutputVolts;
    __m256 iterationOutputSines;

    voltsDataSimdIterations = numSamplesInCurrentBlock / simdLanesVolts;
    voltsDataSimdIterations += (numSamplesInCurrentBlock % simdLanesVolts) ? 1 : 0;

    for (uint32_t i = 0; i < voltsDataSimdIterations; i++)
    {
        iterationInputSines = _mm256_load_ps(&(channelASines[i*simdLanesVolts]));
        iterationInputVolts = _mm256_mul_ps(iterationInputSines, _mm256_set1_ps((float)(simDataSigGenVppBasis / 2.0)));
        iterationInputVolts = _mm256_add_ps(iterationInputVolts, _mm256_set1_ps((float)simDataSigGenOffsetBasisChannelA));
        _mm256_store_ps( &(channelAVolts[i*simdLanesVolts]), iterationInputVolts );

        iterationOutputSines = _mm256_load_ps(&(channelBSines[i*simdLanesVolts]));
        iterationOutputVolts = _mm256_mul_ps(iterationOutputSines, _mm256_set1_ps((float)(simDataSigGenVppBasis / 2.0)));
        iterationOutputVolts = _mm256_mul_ps(iterationOutputVolts, _mm256_set1_ps(currentChannelBGain));
        iterationOutputVolts = _mm256_add_ps(iterationOutputVolts, _mm256_set1_ps((float)simDataSigGenOffsetBasisChannelB));
        _mm256_store_ps( &(channelBVolts[i*simdLanesVolts]), iterationOutputVolts );
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GenerateSamples
//
// Purpose: Generates the 16 bit samples for input and output channels
//
// Parameters: [in] genChannelA - whether to re-generate channnel A samples
//             [in] genChannelB - whether to re-generate channnel B samples
//
// Notes: Handles saturation, and overflow flags
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void psDemoImpl::GenerateSamples( bool genChannelA, bool genChannelB )
{
    __m256 iterationVolts;

    __m256 sampleMax;
    __m256 sampleMin;

    uint32_t maxIterations;
    uint32_t remainderIterations;

    maxIterations = numSamplesInCurrentBlock / simdLanesSamples;       // Unlike Sines and Volts, this needs to be exact to ensure that the extra
    remainderIterations = numSamplesInCurrentBlock % simdLanesSamples; // samples beyond requested don't affect the min/max/overflow results
    sampleMin = _mm256_set1_ps((float)PSDEMO_MIN_VALUE);
    sampleMax = _mm256_set1_ps((float)PSDEMO_MAX_VALUE);

    if (genChannelA)
    {
        __m256 channelARangeVoltsScale;
        __m256 channelAChannelOffset;
        __m256 channelASatFloats;
        __m256 channelAUnsatFloats;
        __m128i channelASatInts;
        __m128i channelAMinInts;
        __m128i channelAMaxInts;
        uint32_t i;
        uint32_t j;

        channelARangeVoltsScale = _mm256_set1_ps((float)(PSDEMO_MAX_VALUE / channelRangeInfo[PS_CHANNEL_A][currentChannelRange[PS_CHANNEL_A]].rangeVolts));
        channelAChannelOffset = _mm256_set1_ps((float)currentChannelOffset[PS_CHANNEL_A]);
        channelAMinInts = _mm_set1_epi16(PSDEMO_MAX_VALUE);
        channelAMaxInts = _mm_set1_epi16(PSDEMO_MIN_VALUE);
        channelAMinSample = PSDEMO_MAX_VALUE;
        channelAMaxSample = PSDEMO_MIN_VALUE;
        channelAOv = false;

        for (i = 0; i < maxIterations; i++)
        {
            iterationVolts = _mm256_load_ps(&(channelAVolts[i*simdLanesSamples]));
            channelAUnsatFloats = _mm256_add_ps(iterationVolts, channelAChannelOffset);
            channelAUnsatFloats = _mm256_mul_ps(channelAUnsatFloats, channelARangeVoltsScale);
            if (_mm256_movemask_ps(_mm256_cmp_ps(channelAUnsatFloats, sampleMax, _CMP_GT_OS)) ||
                _mm256_movemask_ps(_mm256_cmp_ps(channelAUnsatFloats, sampleMin, _CMP_LT_OS)))
            {
                channelAOv = true;
            }
            channelASatFloats = _mm256_min_ps(_mm256_max_ps(channelAUnsatFloats, sampleMin), sampleMax);
            channelASatInts = _mm_packs_epi32( _mm256_extractf128_si256(_mm256_cvtps_epi32(channelASatFloats), 0), _mm256_extractf128_si256(_mm256_cvtps_epi32(channelASatFloats), 1) );
            _mm_store_si128((__m128i*)&(channelASamples[i*simdLanesSamples]), channelASatInts);
            channelAMinInts = _mm_min_epi16( channelAMinInts, channelASatInts );
            channelAMaxInts = _mm_max_epi16( channelAMaxInts, channelASatInts );
        }

        for (j = 0; j < simdLanesSamples; j++)
        {
            channelAMinSample = min( channelAMinSample, channelAMinInts.m128i_i16[j] );
            channelAMaxSample = max( channelAMaxSample, channelAMaxInts.m128i_i16[j] );
        }

        if (remainderIterations)
        {
            iterationVolts = _mm256_load_ps(&(channelAVolts[i*simdLanesSamples]));
            channelAUnsatFloats = _mm256_add_ps(iterationVolts, channelAChannelOffset);
            channelAUnsatFloats = _mm256_mul_ps(channelAUnsatFloats, channelARangeVoltsScale);
            channelASatFloats = _mm256_min_ps(_mm256_max_ps(channelAUnsatFloats, sampleMin), sampleMax);
            channelASatInts = _mm_packs_epi32( _mm256_extractf128_si256(_mm256_cvtps_epi32(channelASatFloats), 0), _mm256_extractf128_si256(_mm256_cvtps_epi32(channelASatFloats), 1) );
            _mm_store_si128((__m128i*)&(channelASamples[i*simdLanesSamples]), channelASatInts);

            for (i = 0; i < remainderIterations; i++)
            {
                if (channelAUnsatFloats.m256_f32[i] > (float)PSDEMO_MAX_VALUE ||
                    channelAUnsatFloats.m256_f32[i] < (float)PSDEMO_MIN_VALUE)
                {
                    channelAOv = true;
                }
                channelAMinSample = min( channelAMinSample, channelASatInts.m128i_i16[i] );
                channelAMaxSample = max( channelAMaxSample, channelASatInts.m128i_i16[i] );
            }
        }
    }

    if (genChannelB)
    {
        __m256 channelBRangeVoltsScale;
        __m256 channelBChannelOffset;
        __m256 channelBSatFloats;
        __m256 channelBUnsatFloats;
        __m128i channelBSatInts;
        __m128i channelBMinInts;
        __m128i channelBMaxInts;
        uint32_t i;
        uint32_t j;

        channelBRangeVoltsScale = _mm256_set1_ps((float)(PSDEMO_MAX_VALUE / channelRangeInfo[PS_CHANNEL_B][currentChannelRange[PS_CHANNEL_B]].rangeVolts));
        channelBChannelOffset = _mm256_set1_ps((float)currentChannelOffset[PS_CHANNEL_B]);
        channelBMinInts = _mm_set1_epi16(PSDEMO_MAX_VALUE);
        channelBMaxInts = _mm_set1_epi16(PSDEMO_MIN_VALUE);
        channelBMinSample = PSDEMO_MAX_VALUE;
        channelBMaxSample = PSDEMO_MIN_VALUE;
        channelBOv = false;

        for (i = 0; i < maxIterations; i++)
        {
            iterationVolts = _mm256_load_ps(&(channelBVolts[i*simdLanesSamples]));
            channelBUnsatFloats = _mm256_add_ps(iterationVolts, channelBChannelOffset);
            channelBUnsatFloats = _mm256_mul_ps(channelBUnsatFloats, channelBRangeVoltsScale);
            if (_mm256_movemask_ps(_mm256_cmp_ps(channelBUnsatFloats, sampleMax, _CMP_GT_OS)) ||
                _mm256_movemask_ps(_mm256_cmp_ps(channelBUnsatFloats, sampleMin, _CMP_LT_OS)))
            {
                channelBOv = true;
            }
            channelBSatFloats = _mm256_min_ps(_mm256_max_ps( channelBUnsatFloats, sampleMin ), sampleMax);
            channelBSatInts = _mm_packs_epi32( _mm256_extractf128_si256(_mm256_cvtps_epi32(channelBSatFloats), 0), _mm256_extractf128_si256(_mm256_cvtps_epi32(channelBSatFloats), 1) );
            _mm_store_si128((__m128i*)&(channelBSamples[i*simdLanesSamples]), channelBSatInts);
            channelBMinInts = _mm_min_epi16( channelBMinInts, channelBSatInts );
            channelBMaxInts = _mm_max_epi16( channelBMaxInts, channelBSatInts );
        }

        for (j = 0; j < simdLanesSamples; j++)
        {
            channelBMinSample = min( channelBMinSample, channelBMinInts.m128i_i16[j] );
            channelBMaxSample = max( channelBMaxSample, channelBMaxInts.m128i_i16[j] );
        }

        if (remainderIterations)
        {
            iterationVolts = _mm256_load_ps(&(channelBVolts[i*simdLanesSamples]));
            channelBUnsatFloats = _mm256_add_ps(iterationVolts, channelBChannelOffset);
            channelBUnsatFloats = _mm256_mul_ps(channelBUnsatFloats, channelBRangeVoltsScale);
            channelBSatFloats = _mm256_min_ps(_mm256_max_ps(channelBUnsatFloats, sampleMin), sampleMax);
            channelBSatInts = _mm_packs_epi32( _mm256_extractf128_si256(_mm256_cvtps_epi32(channelBSatFloats), 0), _mm256_extractf128_si256(_mm256_cvtps_epi32(channelBSatFloats), 1) );
            _mm_store_si128((__m128i*)&(channelBSamples[i*simdLanesSamples]), channelBSatInts);

            for (i = 0; i < remainderIterations; i++)
            {
                if (channelBUnsatFloats.m256_f32[i] > (float)PSDEMO_MAX_VALUE ||
                    channelBUnsatFloats.m256_f32[i] < (float)PSDEMO_MIN_VALUE)
                {
                    channelBOv = true;
                }
                channelBMinSample = min( channelBMinSample, channelBSatInts.m128i_i16[i] );
                channelBMaxSample = max( channelBMaxSample, channelBSatInts.m128i_i16[i] );
            }
        }
    }
}

void psDemoImpl::SetSimulatedLpFCornerFrequency(double cornerFrequency)
{
    simulatedLpfCornerFrequency = cornerFrequency;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: UpdateTransferFunction
//
// Purpose: Updates transfer function variables currentChannelBGain and currentChannelBDdsPhase
//
// Parameters: N/A
//
// Notes: For now, this just implements a simple first order LPF with programmable corner frequency
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void psDemoImpl::UpdateTransferFunction( void )
{
    currentChannelBGain = (float)(1.0 / sqrt( 1.0 + pow( ( simDataSigGenFrequencyBasis / simulatedLpfCornerFrequency ), 2.0 ) ));
    // 1-x to start phase positive
    currentChannelBDdsPhase = (float)(1.0 - atan( simDataSigGenFrequencyBasis / simulatedLpfCornerFrequency ) / (2.0 * M_PI));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GenerateDDSTable
//
// Purpose: Genrates a sine lookup table for direct digital synthesis
//
// Parameters: N/A
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////

void psDemoImpl::GenerateDDSTable(void)
{
    for (uint32_t i = 0; i < ddsTable.size(); i++)
    {
        ddsTable[i] = (float)sin(2.0*M_PI*((double)i/ddsTable.size()));
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetChannelDesignations
//
// Purpose: Tells the PicoScope object which channel is input and which is output
//
// Parameters: [in] inputChannel - input channel number
//             [in] outputChannel - output channel number
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////
void psDemoImpl::SetChannelDesignations( PS_CHANNEL inputChannel, PS_CHANNEL outputChannel )
{
    mInputChannel = inputChannel;
    mOutputChannel = outputChannel;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method SetChannelAttenuation
//
// Purpose: For cases where a smart probe is not being used, tells the PicoScope object the current
//          attentuation setting for the channel so it can re-build the range table
//
// Parameters: [in] channel - channel number to set attenuation
//             [in] atten - attenuation to set
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::SetChannelAttenuation( PS_CHANNEL channel, ATTEN_T atten )
{
    channelAttens[channel] = atten;
    channelManualAttens[channel] = atten;

    if (ATTEN_1X == atten)
    {
        channelRangeInfo[channel] = base1xRangeInfo;
    }
    else
    {
        channelRangeInfo[channel].clear();
        for (auto rangeInfo : base1xRangeInfo)
        {
            rangeInfo.rangeVolts *= attenScalingFactors[atten];
            CreateRangeName(rangeInfo.name, rangeInfo.rangeVolts);
            channelRangeInfo[channel].push_back(rangeInfo);
        }
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetChannelAttenuation
//
// Purpose: Gets the current attenuation being used for the channel specified
//
// Parameters: [in] channel - channel number to get attenuation
//             [out] atten - attenuation to get
//
// Notes: Not bothering with getting the probe data mutex since this is a simple atomic access.
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetChannelAttenuation(PS_CHANNEL channel, ATTEN_T& atten)
{
    wstringstream fraStatusText;
    bool retVal = false;

    if (channel < numAvailableChannels)
    {
        atten = channelAttens[channel];
        retVal = true;
    }
    else
    {
        fraStatusText << L"Fatal error: Invalid channel in call to GetChannelAttenuation: " << channel;
        LogMessage( fraStatusText.str() );
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: IsAutoAttenuation
//
// Purpose: Indicates whether the channel is a smart probe with an automatic attenuation setting
//          and therefore should not be manually set by the application
//
// Parameters: [in] channel - channel number for query
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::IsAutoAttenuation(PS_CHANNEL channel)
{
    return channelAutoAttens[channel];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetSmartProbeCallback
//
// Purpose: Sets a function that will be called back whenever any relevant change in smart probe
//          status occurs
//
// Parameters: [in] smartProbeCB - pointer to callback function
//
// Notes: This is a null function for scopes that don't implement smart probes
//
////////////////////////////////////////////////////////////////////////////////////////////////////

void psDemoImpl::SetSmartProbeCallback(SMART_PROBE_CALLBACK smartProbeCB)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ChannelDesignationsAreOptimal
//
// Purpose: Indicates whether the channels chosen for the scope are optimal for timebase and
//          resolution
//
// Parameters: [out] return - whether the channels are optimal
//
// Notes: Channels are always optimal for non-PS6000 and non-PS6000A scopes
//
////////////////////////////////////////////////////////////////////////////////////////////////////
bool psDemoImpl::ChannelDesignationsAreOptimal(void)
{
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Methods associated with getting data
//
////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetData
//
// Purpose: Gets data from the scope that was captured in block mode.
//
// Parameters: [in] numSamples - number of samples to retrieve
//             [in] startIndex - at what index to start retrieving
//             [in] inputBuffer - buffer to store input samples
//             [in] outputBuffer - buffer to store output samples
//             [out] return - whether the function succeeded
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetData( uint32_t numSamples, uint32_t startIndex,
                          vector<int16_t>** inputBuffer, vector<int16_t>** outputBuffer )
{
    wstringstream fraStatusText;
    bool retVal = true;

    uint32_t requestedBlockNum = startIndex / maxDataRequestSize;

    if (requestedBlockNum != currentBlockNum)
    {
        currentBlockNum = requestedBlockNum;
        numSamplesInCurrentBlock = min(numSamples, maxDataRequestSize);
        if (requestedBlockNum == 0)
        {
            currentChannelADdsPhase = 0.0;
            UpdateTransferFunction();
        }
        GenerateSines();
        GenerateVolts();
        GenerateSamples(true, true);
    }

    if (inputBuffer && outputBuffer)
    {
        if (PS_CHANNEL_A == mInputChannel)
        {
            *inputBuffer = (vector<int16_t>*)&channelASamples;
        }
        else if (PS_CHANNEL_B == mInputChannel)
        {
            *inputBuffer = (vector<int16_t>*)&channelBSamples;
        }
        else
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Unable to get data from channels.  Invalid channel assignment.";
            LogMessage( fraStatusText.str() );
            retVal = false;
        }

        if (PS_CHANNEL_A == mOutputChannel)
        {
            *outputBuffer = (vector<int16_t>*)&channelASamples;
        }
        else if (PS_CHANNEL_B == mOutputChannel)
        {
            *outputBuffer = (vector<int16_t>*)&channelBSamples;
        }
        else
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Unable to get data from channels.  Invalid channel assignment.";
            LogMessage( fraStatusText.str() );
            retVal = false;
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetCompressedData
//
// Purpose: Gets a compressed form of the data from the scope that was captured in block mode.
//          The compressed form is an aggregation where a range of samples is provided as the min
//          and max value in that range.
//
// Parameters: [in] downsampleTo - number of samples to store in a compressed aggregate buffer
//                                 if 0, it means don't compress, just copy
//             [in] inputCompressedMinBuffer - buffer to store aggregated min input samples
//             [in] outputCompressedMinBuffer - buffer to store aggregated min ouput samples
//             [in] inputCompressedMaxBuffer - buffer to store aggregated max input samples
//             [in] outputCompressedMaxBuffer - buffer to store aggregated max ouput samples
//             [out] return - whether the function succeeded
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetCompressedData( uint32_t downsampleTo,
                                    vector<int16_t>& inputCompressedMinBuffer, vector<int16_t>& outputCompressedMinBuffer,
                                    vector<int16_t>& inputCompressedMaxBuffer, vector<int16_t>& outputCompressedMaxBuffer )
{
    wstringstream fraStatusText;
    bool retVal = true;
    uint32_t compressedBufferSize;
    uint32_t initialAggregation;
    uint32_t blockNum, blockIterations, numWholeBlocks, wholeBlockRemainder;
    uint32_t aggregationRemainder, aggregationSpillover;
    vector<int16_t> *pInputSamples = NULL, *pOutputSamples = NULL;

    compressedBufferSize = (downsampleTo == 0 || mNumSamples <= downsampleTo) ? mNumSamples : downsampleTo;
    initialAggregation = mNumSamples / compressedBufferSize;
    if (mNumSamples % compressedBufferSize)
    {
        initialAggregation++;
    }
    // Now compute actual size of aggregate buffer
    compressedBufferSize = mNumSamples / initialAggregation;
    compressedBufferSize += (mNumSamples % initialAggregation) ? 1 : 0;

    // And allocate the data for the compressed buffers
    inputCompressedMinBuffer.resize(compressedBufferSize);
    outputCompressedMinBuffer.resize(compressedBufferSize);
    inputCompressedMaxBuffer.resize(compressedBufferSize);
    outputCompressedMaxBuffer.resize(compressedBufferSize);

    // Set pointers to the correct channel buffers
    if (PS_CHANNEL_A == mInputChannel)
    {
        pInputSamples = (vector<int16_t>*)&channelASamples;
    }
    else if (PS_CHANNEL_B == mInputChannel)
    {
        pInputSamples = (vector<int16_t>*)&channelBSamples;
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Unable to get compressed data from channels.  Invalid channel assignment.";
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    if (PS_CHANNEL_A == mOutputChannel)
    {
        pOutputSamples = (vector<int16_t>*)&channelASamples;
    }
    else if (PS_CHANNEL_B == mOutputChannel)
    {
        pOutputSamples = (vector<int16_t>*)&channelBSamples;
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Unable to get compressed data from channels.  Invalid channel assignment.";
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    if (retVal)
    {
        numWholeBlocks = mNumSamples / maxDataRequestSize;
        wholeBlockRemainder = mNumSamples % maxDataRequestSize;
        blockIterations = (wholeBlockRemainder) ? numWholeBlocks+1 : numWholeBlocks;
        aggregationSpillover = 0;
        uint32_t j = 0;

        // Handle whole and partial blocks
        for (blockNum = 0; blockNum < blockIterations; blockNum++)
        {
            // If we're not on the last block, the request size is definitely maxDataRequestSize.
            // Else, if we are on the last block, then only if the whole block remainder is 0 should the request size be maxDataRequestSize
            // Else, the next request should be maxDataRequestSize
            uint32_t dataRequestSize = (blockNum == blockIterations-1) ? (wholeBlockRemainder ? wholeBlockRemainder : maxDataRequestSize) : maxDataRequestSize;

            // Refresh data buffers with the right data
            GetData(dataRequestSize, blockNum*maxDataRequestSize, NULL, NULL);

            if (compressedBufferSize == mNumSamples) // No need to aggregate, just copy
            {
                // Using explicit copy instead of assignment to avoid unwanted resize
                copy(pInputSamples->begin(), pInputSamples->begin() + dataRequestSize, inputCompressedMinBuffer.begin() + blockNum*maxDataRequestSize);
                copy(pInputSamples->begin(), pInputSamples->begin() + dataRequestSize, inputCompressedMaxBuffer.begin() + blockNum*maxDataRequestSize);
                copy(pOutputSamples->begin(), pOutputSamples->begin() + dataRequestSize, outputCompressedMinBuffer.begin() + blockNum*maxDataRequestSize);
                copy(pOutputSamples->begin(), pOutputSamples->begin() + dataRequestSize, outputCompressedMaxBuffer.begin() + blockNum*maxDataRequestSize);
            }
            else
            {
                uint32_t compressedBufferIterations;
                aggregationRemainder = (dataRequestSize - aggregationSpillover) % initialAggregation;
                compressedBufferIterations = (dataRequestSize - aggregationSpillover) / initialAggregation;

                uint32_t i = 0;
                for (i = 0; i < compressedBufferIterations; i++, j++)
                {
                    auto minMaxIn = minmax_element( &(*pInputSamples)[i*initialAggregation+aggregationSpillover], &(*pInputSamples)[(i+1)*initialAggregation+aggregationSpillover] );
                    inputCompressedMinBuffer[j] = *minMaxIn.first;
                    inputCompressedMaxBuffer[j] = *minMaxIn.second;
                    auto minMaxOut = minmax_element( &(*pOutputSamples)[i*initialAggregation+aggregationSpillover], &(*pOutputSamples)[(i+1)*initialAggregation+aggregationSpillover] );
                    outputCompressedMinBuffer[j] = *minMaxOut.first;
                    outputCompressedMaxBuffer[j] = *minMaxOut.second;
                }
                // Handle the last few samples
                if (aggregationRemainder)
                {
                    // Splice together the last few samples from the current block and the first few (if any) samples from the next block
                    // If we're on the last block, the the next request should be 0
                    // Else, if we're on the second to last block, only if the whole block remainder is 0 should the next request should be maxDataRequestSize
                    // Else, the next request should be maxDataRequestSize
                    uint32_t nextBlockDataRequestSize = (blockNum == blockIterations-1) ? 0 : (blockNum == blockIterations-2) ? wholeBlockRemainder ? wholeBlockRemainder : maxDataRequestSize : maxDataRequestSize;
                    uint32_t nextAggregationSpillover = min(initialAggregation - aggregationRemainder, nextBlockDataRequestSize);
                    uint32_t spliceBufferSize = aggregationRemainder + nextAggregationSpillover;
                    std::vector<int16_t> inputSpliceBuffer;
                    std::vector<int16_t> outputSpliceBuffer;
                    inputSpliceBuffer.resize(spliceBufferSize);
                    outputSpliceBuffer.resize(spliceBufferSize);
                    copy(&(*pInputSamples)[i*initialAggregation+aggregationSpillover], &(*pInputSamples)[i*initialAggregation+aggregationSpillover+aggregationRemainder], inputSpliceBuffer.begin());
                    copy(&(*pOutputSamples)[i*initialAggregation+aggregationSpillover], &(*pOutputSamples)[i*initialAggregation+aggregationSpillover+aggregationRemainder], outputSpliceBuffer.begin());
                    if (nextBlockDataRequestSize)
                    {
                        GetData(nextBlockDataRequestSize, (blockNum+1)*maxDataRequestSize, NULL, NULL); // Get the next data block
                        copy(pInputSamples->begin(), pInputSamples->begin() + nextAggregationSpillover, inputSpliceBuffer.begin() + aggregationRemainder);
                        copy(pOutputSamples->begin(), pOutputSamples->begin() + nextAggregationSpillover, outputSpliceBuffer.begin() + aggregationRemainder);
                    }
                    aggregationSpillover = nextAggregationSpillover;

                    // Find the min/max in the splices
                    auto minMaxIn = minmax_element( inputSpliceBuffer.begin(), inputSpliceBuffer.end() );
                    inputCompressedMinBuffer[j] = *minMaxIn.first;
                    inputCompressedMaxBuffer[j] = *minMaxIn.second;
                    auto minMaxOut = minmax_element( outputSpliceBuffer.begin(), outputSpliceBuffer.end() );
                    outputCompressedMinBuffer[j] = *minMaxOut.first;
                    outputCompressedMaxBuffer[j] = *minMaxOut.second;
                    j++;
                }
            }
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetPeakValues
//
// Purpose: Gets channel peak data values as well as flags indicating channels in overvoltage 
//          condition (ADC railed)
//
// Parameters: [in] inputPeak - Maximum absolute value of input data captured
//             [in] outputPeak - Maximum absolute value of input data captured
//             [in] inputOv - whether the input channel is over-voltage
//             [in] outputOv - whether the output channel is over-voltage 
//             [out] return - whether the function succeeded
//
// Notes:
//
////////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::GetPeakValues( uint16_t& inputPeak, uint16_t& outputPeak, bool& inputOv, bool& outputOv )
{
    wstringstream fraStatusText;
    bool retVal = true;

    if (PS_CHANNEL_A == mInputChannel)
    {
        inputPeak = max(abs(channelAMinSample), abs(channelAMaxSample));
        inputOv = channelAOv;
    }
    else if (PS_CHANNEL_B == mInputChannel)
    {
        inputPeak = max(abs(channelBMinSample), abs(channelBMaxSample));
        inputOv = channelBOv;
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Unable to get peak values from channels.  Invalid channel assignment.";
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    if (PS_CHANNEL_A == mOutputChannel)
    {
        outputPeak = max(abs(channelAMinSample), abs(channelAMaxSample));
        outputOv = channelAOv;
    }
    else if (PS_CHANNEL_B == mOutputChannel)
    {
        outputPeak = max(abs(channelBMinSample), abs(channelBMaxSample));
        outputOv = channelBOv;
    }
    else
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Unable to get peak samples from channels.  Invalid channel assignment.";
        LogMessage( fraStatusText.str() );
        retVal = false;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ChangePower
//
// Purpose: Change flexible power mode for the scope
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: Only relevant for scopes with flexible power implementations (PS3000A and PS5000A)
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::ChangePower( PICO_STATUS powerState )
{
    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: CancelCapture
//
// Purpose: Cancel the current capture
//
// Parameters: [out] return - whether the function succeeded
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::CancelCapture(void)
{
    wstringstream fraStatusText;
    bool retVal = false;

    // This is OK, because there's no cleanup this thread needs to do
    if (!TerminateThread( hSimulatedCaptureThread, -1 ))
    {
        fraStatusText.clear();
        fraStatusText.str(L"");
        fraStatusText << L"Fatal error: Failed to terminate simulated capture thread.";
        LogMessage( fraStatusText.str() );
    }
    else
    {
        // Restart the thread
        hSimulatedCaptureThread = CreateThread( NULL, 0, SimulateCapture, this, 0, NULL );
        if (NULL == hSimulatedCaptureThread)
        {
            fraStatusText.clear();
            fraStatusText.str(L"");
            fraStatusText << L"Fatal error: Failed to re-create simulated capture thread.";
            LogMessage( fraStatusText.str() );
        }
        else
        {
            retVal = true;
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Close
//
// Purpose: Close the scope
//
// Parameters: [out] return - whether the function succeeded
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::Close(void)
{
    handle = -1;
    open = false;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: IsUSB3_0Connection
//
// Purpose: Determine if a USB 3.0 scope is on a USB 3.0 port
//
// Parameters: [out] return - true if the connection is USB 3.0
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool psDemoImpl::IsUSB3_0Connection(void)
{
    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Common method CreateRangeName
//
// Purpose: Utility function to write a range name on the fly
//
// Parameters: [in] string - the string to write to
//             [in] scale - the scale for the range
//
// Notes: E.g. a scale of 1.0 will result in a string of "� 1 V"
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void psDemoImpl::CreateRangeName(wchar_t* string, double scale)
{
    if (scale < 1.0)
    {
        std::swprintf(string, sizeof(RANGE_INFO_T::name)/sizeof(wchar_t), L"� %d mV", (int)std::round(1000.0 * scale));
    }
    else if (scale < 1000.0)
    {
        std::swprintf( string, sizeof(RANGE_INFO_T::name)/sizeof(wchar_t), L"� %d V", (int)std::round(scale) );
    }
    else
    {
        std::swprintf( string, sizeof(RANGE_INFO_T::name)/sizeof(wchar_t), L"� %d kV", (int)std::round(scale/1000.0) );
    }
}

void psDemoImpl::LogPicoApiCall(wstringstream& fraStatusText)
{
    // If there is a trailing ", " remove it
    size_t length = fraStatusText.str().length();
    if (!fraStatusText.str().compare(length-2, 2, L", "))
    {
        fraStatusText.seekp( -2, ios_base::end );
    }

    fraStatusText << L" );";
    LogMessage( fraStatusText.str(), PICO_API_CALL );
}

template <typename First, typename... Rest> void psDemoImpl::LogPicoApiCall( wstringstream& fraStatusText, First first, Rest... rest)
{
    if (fraStatusText.str().empty()) // first is the function name
    {
        fraStatusText << first << L"( ";
    }
    else // first is a parameter
    {
        fraStatusText << first << L", ";
    }
    LogPicoApiCall( fraStatusText, rest... );
}