//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2022 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: PlotTitleDialog.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <Windowsx.h>
#include <CommCtrl.h>
#include <string>
#include <regex>
#include "PicoScopeFraApp.h"
#include "PlotTitleDialog.h"
#include "Resource.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PlotTitleDialogHandler
//
// Purpose: Message handler for Plot Title dialog box.
//
// Parameters: See Windows programming API information
//
// Notes: This function stores the title with spaces replaced with the PLplot unicode character
//        representation of a space #[32].  This is because retrieval of the title from the
//        application settings property tree strips leading and trailing spaces from the title,
//        because read_xml uses trim_whitespace.  However I don't want to abandon use of
//        trim_whitespace because it makes the XML files more readable.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK PlotTitleDialogHandler( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam )
{
    switch (message)
    {
    case WM_INITDIALOG:
    {
        HWND hndCtrl;
        std::wstring plotTitle = pSettings->GetPlotTitle();

        // Substituting spaces for #[32] so user can edit spaces.
        // See comment in Notes above for why.
        std::wregex trailingSpace( L"(#\\[32\\])" );
        plotTitle = std::regex_replace( plotTitle, trailingSpace, L" " );

        hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_TEXT );
        Edit_SetText( hndCtrl, plotTitle.c_str() );

        bool autoIncrement = pSettings->GetPlotTitleAutoIncrement();
        hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_AUTO_INCREMENT );
        Button_SetCheck( hndCtrl, autoIncrement );

        hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_NEXT_NUMBER_LABEL );
        EnableWindow( hndCtrl, autoIncrement );
        hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_NEXT_NUMBER );
        EnableWindow( hndCtrl, autoIncrement );

        std::wstring nextNumber = pSettings->GetPlotTitleNextNumberAsString();
        Edit_SetText( hndCtrl, nextNumber.c_str() );

        std::wstring fontScale = pSettings->GetPlotTitleFontScaleAsString();
        hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_FONT_SCALE );
        Edit_SetText( hndCtrl, fontScale.c_str() );

        return (INT_PTR)TRUE;
    }
    case WM_COMMAND:
    {
        if (LOWORD( wParam ) == IDOK)
        {
            HWND hndCtrl;
            TCHAR editText[128];
            uint16_t nextNumber;
            double fontScale;
            bool valid = false;

            // Validation
            hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_NEXT_NUMBER );
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToUint16( editText, nextNumber ))
            {
                MessageBox( hDlg, L"Next number is not a valid integer between 0 and 65535", L"Error", MB_OK );
            }
            else
            {
                hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_FONT_SCALE );
                Edit_GetText( hndCtrl, editText, 16 );
                if (!WStringToDouble( editText, fontScale ) || fontScale < 0.5 || fontScale > 2.0)
                {
                    MessageBox( hDlg, L"Font scale is not a valid number between 0.5 and 2.0", L"Error", MB_OK );
                }
                else
                {
                    valid = true;
                }
            }

            // Storing
            if (valid)
            {
                hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_TEXT );
                Edit_GetText( hndCtrl, editText, 128 );

                // substituting #[32] for spaces.  See comment in Notes above for why.
                std::wstring plotTitle = editText;
                std::wregex trailingSpaces( L" " );
                plotTitle = std::regex_replace( plotTitle, trailingSpaces, L"#[32]" );

                pSettings->SetPlotTitle( plotTitle );

                hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_AUTO_INCREMENT );
                bool autoIncrement = Button_GetCheck( hndCtrl ) == BST_CHECKED;
                pSettings->SetPlotTitleAutoIncrement( autoIncrement );

                pSettings->SetPlotTitleNextNumber( nextNumber );

                hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_FONT_SCALE );
                Edit_GetText( hndCtrl, editText, 16 );
                pSettings->SetPlotTitleFontScale( editText );

                EndDialog( hDlg, LOWORD( wParam ) );
                return (INT_PTR)TRUE;
            }
        }
        else if (LOWORD( wParam ) == IDCANCEL)
        {
            EndDialog( hDlg, LOWORD( wParam ) );
            return (INT_PTR)TRUE;
        }
        else if (LOWORD( wParam ) == IDC_PLOT_TITLE_AUTO_INCREMENT)
        {
            HWND hndCtrl;
            hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_AUTO_INCREMENT );
            bool autoIncrement = Button_GetCheck( hndCtrl ) == BST_CHECKED;
            hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_NEXT_NUMBER_LABEL );
            EnableWindow( hndCtrl, autoIncrement );
            hndCtrl = GetDlgItem( hDlg, IDC_PLOT_TITLE_NEXT_NUMBER );
            EnableWindow( hndCtrl, autoIncrement );
        }
    }
    }
    return (INT_PTR)FALSE;
}