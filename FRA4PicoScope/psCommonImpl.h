//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: psCommonImpl.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

public:
bool Initialized( void );
bool Connected(void);
bool USB3ScopeNotOnUSB3Port(void);
uint8_t GetNumChannels( void );
void GetAvailableCouplings( PS_CHANNEL channel, vector<wstring>& couplingText );
uint32_t GetMinTimebase( void );
uint32_t GetMaxTimebase( void );
bool GetNextTimebase( uint32_t timebase, RESOLUTION_T resolution, uint8_t roundFaster, uint32_t* newTimebase );
bool TimebaseIsSparse( void );
bool TimebaseIsValidForResolution( uint32_t timebase, RESOLUTION_T resolution );
bool GetNoiseRejectModeTimebase( uint32_t& timebase, bool* adjustedByChannelSettings = NULL );
void SetDesiredNoiseRejectModeTimebase( uint32_t timebase );
uint32_t GetDefaultNoiseRejectModeTimebase( void );
bool GetNoiseRejectModeSampleRate( double& sampleRate );
double GetSignalGeneratorPrecision( void );
double GetClosestSignalGeneratorFrequency( double requestedFreq );
uint32_t GetMaxDataRequestSize( void );
PS_RANGE GetMinRange( PS_CHANNEL channel, PS_COUPLING coupling, double dcOffset = 0.0 );
PS_RANGE GetMaxRange( PS_CHANNEL channel, PS_COUPLING coupling, double dcOffset = 0.0 );
void GetMinMaxChannelDcOffsets( double& minDcOffset, double& maxDcOffset );
bool GetMaxValue(int16_t& maxValue);
double GetMinFuncGenFreq( void );
double GetMaxFuncGenFreq( void );
double GetMinFuncGenVpp( void );
double GetMaxFuncGenVpp( void );
bool SupportsChannelDcOffset(void);
double GetMinNonZeroFuncGenVpp( void );
bool SetResolution( RESOLUTION_T resolution, RESOLUTION_T& actualResolution );
bool GetMaxResolutionForFrequency( double frequency, RESOLUTION_T& resolution );
bool GetMaxResolutionForTimebase( uint32_t timebase, RESOLUTION_T& resolution );
bool SetChannelConfigurationForMinimumTimebase( void );
RESOLUTION_T GetMaxResolutionForTimebase( uint32_t timebase );
uint32_t GetMinTimebaseForResolution( RESOLUTION_T resolution );
std::wstring GetResolutionString( RESOLUTION_T resolution );
bool IsCompatible( void );
bool RequiresExternalSignalGenerator( void );
bool GetModel( ScopeModel_T &model );
bool GetModel( wstring &model );
bool GetFamily( ScopeDriverFamily_T& family );
bool GetSerialNumber( wstring &sn );
bool SetupChannel( PS_CHANNEL channel, PS_COUPLING coupling, PS_RANGE range, double offset, double currentFrequency = 0 );
bool DisableAllDigitalChannels( void );
bool DisableChannel( PS_CHANNEL channel );
bool SetSignalGenerator( double vPP, double offset, double frequency );
bool DisableSignalGenerator( void );
bool DisableChannelTriggers( void );
bool GetMaxSamples( uint32_t* maxSamples, RESOLUTION_T resolution, uint32_t timebase );
bool GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase );
bool GetFrequencyFromTimebase( uint32_t timebase, double &frequency );
bool RunBlock( int32_t numSamples, uint32_t timebase, double *timeIndisposedMs, psBlockReady lpReady, void *pParameter );
void SetChannelDesignations( PS_CHANNEL inputChannel, PS_CHANNEL outputChannel );
bool ChannelDesignationsAreOptimal( void );
bool GetChannelAttenuation( PS_CHANNEL channel, ATTEN_T& atten );
bool SetChannelAttenuation( PS_CHANNEL channel, ATTEN_T atten );
bool IsAutoAttenuation( PS_CHANNEL channel );
void SetSmartProbeCallback(SMART_PROBE_CALLBACK smartProbeCB);
bool GetData( uint32_t numSamples, uint32_t startIndex, vector<int16_t>** inputBuffer, vector<int16_t>** outputBuffer );
bool GetCompressedData( uint32_t numSamples, 
                        vector<int16_t>& inputCompressedMinBuffer, vector<int16_t>& outputCompressedMinBuffer,
                        vector<int16_t>& inputCompressedMaxBuffer, vector<int16_t>& outputCompressedMaxBuffer );
bool GetPeakValues( uint16_t& inputPeak, uint16_t& outputPeak, bool& inputOv, bool& outputOv );
bool ChangePower(PICO_STATUS powerState);
bool CancelCapture( void );
bool Close( void );
const RANGE_INFO_T* GetRangeCaps( PS_CHANNEL channel );
const std::vector<RESOLUTION_T> GetResolutionCaps( void );
private:
std::vector<std::vector<RANGE_INFO_T>> channelRangeInfo;
static const std::vector<RANGE_INFO_T> base1xRangeInfo;
static const std::vector<double> attenScalingFactors;
std::vector<ATTEN_T> channelAttens;
std::vector<ATTEN_T> channelManualAttens;
std::vector<bool> channelSmartProbeConnected;
std::vector<bool> channelAutoAttens;
std::vector<int32_t> channelRangeOffsets;
int16_t handle;
std::vector<PS_RANGE> channelMinRanges;
std::vector<PS_RANGE> channelMaxRanges;
std::vector<PS_RANGE> channelNonSmartMinRanges;
std::vector<PS_RANGE> channelNonSmartMaxRanges;
double mMinDcOffset;
double mMaxDcOffset;
uint32_t minTimebase;
uint32_t maxTimebase;
double maxFrequency;
uint32_t defaultTimebaseNoiseRejectMode;
uint32_t timebaseNoiseRejectMode;
PS_CHANNEL mInputChannel;
PS_CHANNEL mOutputChannel;
vector<int16_t> mInputBuffer;
vector<int16_t> mOutputBuffer;
bool buffersDirty;
uint32_t mNumSamples;
static const uint32_t maxDataRequestSize;
#if !defined(NEW_PS_DRIVER_MODEL)
static DWORD WINAPI CheckStatus(LPVOID lpThreadParameter);
bool InitStatusChecking(void);
HANDLE hCheckStatusEvent;
HANDLE hCheckStatusThread;
bool capturing;
psBlockReady readyCB;
int32_t currentTimeIndisposedMs;
void* cbParam;
#endif
#if defined(IMPLEMENTS_SMART_PROBES)
static HANDLE hProbeDataMutex;
static HANDLE hProbeDataEvent;
static HANDLE hProcessProbeUpdateThread;
static std::vector<PICO_USER_PROBE_INTERACTIONS> newProbeInteractionData;
static std::vector<PICO_USER_PROBE_INTERACTIONS> currentProbeInteractionData;
static PICO_STATUS probeInteractionStatus;
bool InitProbeEventHandling( void );
static void __stdcall ProbeInteractionsCallback(int16_t handle, PICO_STATUS status, PICO_USER_PROBE_INTERACTIONS* probes, uint32_t nProbes);
static DWORD WINAPI ProcessProbeUpdate( LPVOID lpThreadParameter );
SMART_PROBE_CALLBACK smartProbeCallback;
#endif
void SetChannelAttenuation_( PS_CHANNEL channel, ATTEN_T atten );
#if defined(PS4000A_IMPL) || defined(PS5000A_IMPL) || defined(PS6000A_IMPL) || defined(PSOSPA_IMPL)
typedef struct
{
    int32_t flexRes;
    uint32_t minTimebase;
} FlexRes_MinTB_MapEntry_T;
vector<FlexRes_MinTB_MapEntry_T> flexResMinTbMap;
int32_t currentResolution;
int32_t maxResolution;
#endif
bool InitializeScope( void );
bool IsUSB3_0Connection(void);
bool AllocateBuffers(void);
void CreateRangeName( wchar_t* string, double scale );
void LogPicoApiCall( wstringstream& fraStatusText );
template <typename First, typename... Rest> void LogPicoApiCall( wstringstream& fraStatusText, First first, Rest... rest );