//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: PicoScopeFRA.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "FRA4PicoScopeInterfaceTypes.h"
#include "PicoScopeInterface.h"
#include "ExtSigGen.h"
#include "utility.h"
#include <memory>
#include <vector>
#include <array>
#include <string>
#include <complex>
#include <functional>

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: class PicoScopeFRA
//
// Purpose: This is the class supporting Frequency Response Analysis execution
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

class PicoScopeFRA
{
    public:

        PicoScopeFRA(FRA_STATUS_CALLBACK statusCB, SMART_PROBE_CALLBACK smartProbeCB);
        ~PicoScopeFRA(void);
        void SetInstrument( PicoScope* _ps );
        PicoScope* GetInstrument();
        void SetExtSigGen( ExtSigGen* _pESG );
        double GetMinFrequency( RESOLUTION_T resolution );
        double GetMinFrequency( SamplingMode_T samplingMode, RESOLUTION_T resolution, uint32_t timebase );
        double GetMaxFrequency(void);
        double GetMaxFuncGenVpp(double startFreq, double stopFreq);
        double GetMinFuncGenVpp(double startFreq, double stopFreq);
        bool SigGenSupportsDcOffset(void);
        bool ExecuteFRA( double startFreqHz, double stopFreqHz, int stepsPerDecade );
        bool ExecuteFRA( vector<FRASegment_T> fraSegments );
        bool CancelFRA();
        static void SetCaptureStatus(PICO_STATUS status);
        void SetFraSettings( SamplingMode_T samplingMode, bool adaptiveStimulusMode, double targetSignalAmplitude,
                             bool sweepDescending, double phaseWrappingThreshold );
        void SetFraTuning( double purityLowerLimit, uint16_t extraSettlingTimeMs, uint8_t autorangeTriesPerStep,
                           double autorangeTolerance, double smallSignalResolutionTolerance, double maxAutorangeAmplitude,
                           int32_t inputStartRange, int32_t outputStartRange, uint8_t adaptiveStimulusTriesPerStep,
                           double targetSignalAmplitudeTolerance, uint16_t lowNoiseCyclesCaptured, double maxDftBw,
                           uint16_t lowNoiseOversampling, RESOLUTION_T deviceResolution );
        void SetLowNoiseCyclesCaptured( uint16_t lowNoiseCyclesCaptured );
        bool SetupChannels( int inputChannel, int inputChannelCoupling, int inputChannelAttenuation, double inputDcOffset,
                            int outputChannel, int outputChannelCoupling, int outputChannelAttenuation, double outputDcOffset,
                            double initialSignalVpp, double maxSignalVpp, double stimulusDcOffset );
        void GetResults( int* numSteps, double** freqsLogHz, double** gainsDb, double** phasesDeg, double** unwrappedPhasesDeg );
        void EnableDiagnostics( wstring baseDataPath );
        void DisableDiagnostics( void );

    private:
        // Data about the scope and signal generator
        PicoScope* ps;
        ExtSigGen* pESG;
        uint8_t numAvailableChannels;
        uint32_t maxScopeSamplesPerChannel;

        FRA_STATUS_CALLBACK StatusCallback;
        SMART_PROBE_CALLBACK ClientSmartProbeCallback;

        double currentFreqHz;
        double currentStimulusVpp;
        double currentStimulusOffset;
        double stepStimulusVpp;

        double mStartFreqHz;
        double mStopFreqHz;
        int mStepsPerDecade;
        SamplingMode_T mSamplingMode;
        PS_CHANNEL mInputChannel;
        PS_CHANNEL mOutputChannel;
        PS_COUPLING mInputChannelCoupling;
        PS_COUPLING mOutputChannelCoupling;
        ATTEN_T mInputChannelAttenuation;
        ATTEN_T mOutputChannelAttenuation;
        PS_RANGE currentInputChannelRange;
        PS_RANGE currentOutputChannelRange;
        PS_RANGE adaptiveStimulusInputChannelRange;
        PS_RANGE adaptiveStimulusOutputChannelRange;
        double mInputDcOffset;
        double mOutputDcOffset;
        int numSteps;
        int numTotalSegmentSteps;
        int numCompletedSteps;
        int numCompletedSegmentSteps;
        int currentSegmentNumber;
        bool skipFirstStep;
        vector<double> freqsHz;
        vector<double> freqsLogHz;
        vector<double> phasesDeg;
        vector<double> unwrappedPhasesDeg;
        vector<double> gainsDb;
        int latestCompletedNumSteps;
        vector<double> latestCompletedFreqsLogHz;
        vector<double> latestCompletedPhasesDeg;
        vector<double> latestCompletedUnwrappedPhasesDeg;
        vector<double> latestCompletedGainsDb;
        double actualSampFreqHz; // Scope sampling frequency
        uint32_t numSamples;
        double timeIndisposedMs;
        double currentInputMagnitude;
        double currentOutputMagnitude;
        double currentInputPhase;
        double currentOutputPhase;
        double currentInputPurity;
        double currentOutputPurity;
        bool ovIn;
        bool ovOut;
        bool delayForAcCoupling;
        double currentOutputAmplitudeVolts;
        double currentInputAmplitudeVolts;
        vector<double> idealStimulusVpp; // Recorded and used for predicting next stimulus Vpp

        AUTORANGE_STATUS_T inputChannelAutorangeStatus;
        AUTORANGE_STATUS_T outputChannelAutorangeStatus;

        double mPurityLowerLimit;           // Lowest allowed purity before we warn the user and allow action
        double minAllowedAmplitudeRatio;    // Lowest amplitude we will tolerate for measurement on lowest range.
        double minAmplitudeRatioTolerance;  // Tolerance so that when we step up we're not over maxAmplitudeRatio
        double maxAmplitudeRatio;           // Max we want an amplitude to be before switching ranges
        int maxAutorangeRetries;            // max number of tries to auto-range before failing
        uint16_t mExtraSettlingTimeMs;      // Extra settling time between auto-range tries
        uint16_t mLowNoiseCyclesCaptured;   // Whole stimulus signal cycles to capture in low noise mode
        double mMaxDftBw;                   // Maximum bandwidth for DFT in noise reject mode
        uint16_t mLowNoiseOversampling;     // Amount to oversample the stimulus frequency in low noise mode
        RESOLUTION_T mDeviceResolution;     // Vertical resolution for capturing data
        bool mSweepDescending;              // Whether to sweep frequency from high to low
        bool mAdaptiveStimulus;             // Whether to adjust stimulus Vpp to target an output goal
        double mTargetResponseAmplitude;    // Target amplitude for measured signals, goal is that both signals be at least this large
        double mTargetResponseAmplitudeTolerance; // Amount the smallest signal is allowed to exceed target signal amplitude (percent)
        int32_t mInputStartRange;           // Range to start input channel; -1 means base on stimulus
        int32_t mOutputStartRange;          // Range to start output channel; -1 means base on stimulus
        double mMaxStimulusVpp;             // Maximum allowed stimulus voltage in adaptive stimulus mode
        int maxAdaptiveStimulusRetries;     // Maximum number of tries to adapt stimulus before failing
        double mPhaseWrappingThreshold;     // Phase value to use as wrapping point (in degrees); absolute value should be less than 360

        double rangeCounts; // Maximum ADC value
        double signalGeneratorPrecision;

        wstring previousResolution;

        // This function allocates data used in the FRA, which is a combination
        // of diagnostic data and sample data.
        void AllocateFraData(void);
        // These variables are for keeping diagnostic data and sample data.
        int autorangeRetryCounter;
        int adaptiveStimulusRetryCounter;
        bool stimulusChanged;
        int freqStepCounter;
        int freqStepIndex;
        vector<int16_t>* pInputBuffer;
        vector<int16_t>* pOutputBuffer;
        uint16_t inputAbsMax;
        uint16_t outputAbsMax;

        // Any  more than 4096 would only benefit > 4K monitors, and the plotting of aggregation mode data is compute intensive
        static const uint32_t timeDomainDiagnosticDataLengthLimit = 4096;

        typedef struct
        {
            uint32_t segmentNumber;
            uint32_t stepNumber;
            uint32_t autoRangeTry;
            bool adaptiveStimulusEnabled;
            uint32_t adaptiveStimulusTry;
            double freqHz;
            double stimVpp;
            uint32_t numStimulusCyclesCaptured;
            uint32_t numSamplesCaptured;
            uint32_t numSamplesToPlot;
            double rangeCounts;
            double sampleInterval;
            PS_RANGE inputRange;
            PS_RANGE outputRange;
            bool inputOV;
            bool outputOV;
            double inputAmplitude;
            double outputAmplitude;
            double inputPurity;
            double outputPurity;
            int16_t inputMinData[timeDomainDiagnosticDataLengthLimit];
            int16_t outputMinData[timeDomainDiagnosticDataLengthLimit];
            int16_t inputMaxData[timeDomainDiagnosticDataLengthLimit];
            int16_t outputMaxData[timeDomainDiagnosticDataLengthLimit];
        } TimeDomainDiagnosticData_T;

        TimeDomainDiagnosticData_T timeDomainDiagnosticData;
        MessageProcessor<TimeDomainDiagnosticData_T>* pTimeDomainDiagnosticDataProcessor = NULL;
        void GenerateDiagnosticOutput( TimeDomainDiagnosticData_T& timeDomainData );
        static std::function<void( TimeDomainDiagnosticData_T& )> functionGenerateDiagnosticOutput;
        static void StaticGenerateDiagnosticOutput( TimeDomainDiagnosticData_T& timeDomainData );

        bool mDiagnosticsOn;
        bool mDiagnosticsFailed;
        wstring mBaseDataPath;
        HANDLE hPLplotMutex = NULL;
        static int HandlePLplotError(const char* error);

        typedef struct
        {
            RESOLUTION_T resolution;
            uint32_t timebase;
            double actualSampFreqHz;
            uint32_t numSamples;
            uint32_t numCycles;
            vector<wstring> warnings;
        } SamplePlan_T;

        vector<SamplePlan_T> samplePlan;

        static const double acCouplingSignalDegradationThreshold;
        static const double mixedCouplingSignalMismatchThreshold;

        // Treated as an array where indices here correspond to range enums/indices
        const RANGE_INFO_T* inputRangeInfo;
        const RANGE_INFO_T* outputRangeInfo;
        PS_RANGE inputMinRange;
        PS_RANGE inputMaxRange;
        PS_RANGE outputMinRange;
        PS_RANGE outputMaxRange;

        static const double attenInfo[];
        static const double stimulusBasedInitialRangeEstimateMargin;

        HANDLE hCaptureEvent;
        static PICO_STATUS captureStatus;
        bool cancel;

        class FraFault : public exception {};

        bool PlanSampling( vector<vector<wstring>> &warnings, vector<vector<wstring>>& errors );
        bool StartCapture( double measFreqHz );
        void GenerateFrequencyPoints();
        bool ProcessData();
        bool ResetChannelRangesBasedOnStimulus(void);
        void CalculateStepInitialStimulusVpp(void);
        bool CheckStimulusTarget(bool forceAdjust = false);
        bool CheckSignalRanges(void);
        bool CheckSignalOverflows(void);
        bool CalculateGainAndPhase( double* gain, double* phase );
        void UnwrapPhases(void);
        void InitGoertzel( uint32_t N, double fSamp, double fDetect );
        void FeedGoertzel( int16_t* inputSamples, int16_t* outputSamples, uint32_t n );
        void GetGoertzelResults( double& inputMagnitude, double& inputPhase, double& inputAmplitude, double& inputPurity,
                                 double& outputMagnitude, double& outputPhase, double& outputAmplitude, double& outputPurity );
        void TransferLatestResults(void);

        static std::function<void(vector<SmartProbeEvent_T>)> functionSmartProbeCallback;
        void SmartProbeCallback(std::vector<SmartProbeEvent_T> probeEvents);
        static void StaticSmartProbeCallback(std::vector<SmartProbeEvent_T> probeEvents);

        // Utilities for sending a message via the callback
        // New data point
        inline bool UpdateStatus(FRA_STATUS_MESSAGE_T &msg, FRA_STATUS_T status, double freqLogHz, double gainDb, double phaseDeg)
        {
            msg.status = status;

            FraStatusFraData data = { freqLogHz, gainDb, phaseDeg };
            msg.statusData = data;

            return StatusCallback(msg);
        }
        // Power State
        inline bool UpdateStatus(FRA_STATUS_MESSAGE_T &msg, FRA_STATUS_T status, bool powerState)
        {
            msg.status = status;
            FraStatusPowerState ps = { powerState };
            msg.statusData = ps;
            // If the user selected channels C or D and the power mode is PICO_POWER_SUPPLY_NOT_CONNECTED (a given),
            // the FRA cannot be continued.
            if (mInputChannel >= PS_CHANNEL_C || mOutputChannel >= PS_CHANNEL_C)
            {
                msg.responseData.proceed = false;
            }
            else
            {
                msg.responseData.proceed = true;
            }
            return StatusCallback(msg);
        }
        // Progress Status
        inline bool UpdateStatus( FRA_STATUS_MESSAGE_T &msg, FRA_STATUS_T status )
        {
            int _numSteps, _stepsComplete;

            _numSteps = numTotalSegmentSteps ? numTotalSegmentSteps : numSteps;
            _stepsComplete = numTotalSegmentSteps ? numCompletedSegmentSteps + numCompletedSteps : numCompletedSteps;

            msg.status = status;
            if (status == FRA_STATUS_IN_PROGRESS ||
                status == FRA_STATUS_COMPLETE )
            {
                FraStatusProgress progress;
                progress.numSteps = _numSteps;
                progress.stepsComplete = _stepsComplete;
                msg.statusData = progress;
            }
            else if (status == FRA_STATUS_CANCELED)
            {
                FraStatusCancelPoint cancelPoint;
                cancelPoint.numSteps = _numSteps;
                cancelPoint.stepsComplete = _stepsComplete;
                msg.statusData = cancelPoint;
            }
            return StatusCallback( msg );
        }
        // Retry Limit Reached
        inline bool UpdateStatus( FRA_STATUS_MESSAGE_T &msg, FRA_STATUS_T status, AUTORANGE_STATUS_T inputChannelStatus, AUTORANGE_STATUS_T outputChannelStatus )
        {
            msg.status = status;

            FraStatusRetryLimit retryLimit;

            retryLimit.autorangeLimit.allowedTries = maxAutorangeRetries;
            retryLimit.autorangeLimit.triesAttempted = autorangeRetryCounter;
            // Use the ranges recorded at the beginning of the attempt because they may have been recomputed
            retryLimit.autorangeLimit.inputRange = adaptiveStimulusInputChannelRange;
            retryLimit.autorangeLimit.outputRange = adaptiveStimulusOutputChannelRange;
            retryLimit.autorangeLimit.inputChannelStatus = inputChannelStatus;
            retryLimit.autorangeLimit.outputChannelStatus = outputChannelStatus;
            retryLimit.autorangeLimit.inputRangeInfo = ps->GetRangeCaps(mInputChannel);
            retryLimit.autorangeLimit.outputRangeInfo = ps->GetRangeCaps(mOutputChannel);
            retryLimit.adaptiveStimulusLimit.allowedTries = maxAdaptiveStimulusRetries;
            retryLimit.adaptiveStimulusLimit.triesAttempted = adaptiveStimulusRetryCounter;
            // Use the stimulus recorded at the beginning of the attempt because it may have been recomputed
            retryLimit.adaptiveStimulusLimit.stimulusVpp = stepStimulusVpp;
            retryLimit.adaptiveStimulusLimit.inputResponseAmplitudeV = currentInputAmplitudeVolts;
            retryLimit.adaptiveStimulusLimit.outputResponseAmplitudeV = currentOutputAmplitudeVolts;

            msg.statusData = retryLimit;

            return StatusCallback( msg );
        }
        // Pre-Run Alert
        inline bool UpdateStatus( FRA_STATUS_MESSAGE_T& msg, FRA_STATUS_T status, vector<vector<wstring>> warnings, vector<vector<wstring>> errors )
        {
            msg.status = status;

            FraStatusPreRunAlert preRunAlert;

            preRunAlert.warnings = warnings;
            preRunAlert.errors = errors;

            msg.statusData = preRunAlert;

            return StatusCallback( msg );
        }
        // Status Messages
        inline bool UpdateStatus( FRA_STATUS_MESSAGE_T &msg, FRA_STATUS_T status, const wchar_t* statusMessage, LOG_MESSAGE_FLAGS_T type = FRA_ERROR )
        {
            int _numSteps, _stepsComplete;

            _numSteps = numTotalSegmentSteps ? numTotalSegmentSteps : numSteps;
            _stepsComplete = numTotalSegmentSteps ? numCompletedSegmentSteps + numCompletedSteps : numCompletedSteps;

            msg.status = status;

            FraStatusProgress progress;

            progress.numSteps = _numSteps;
            progress.stepsComplete = _stepsComplete;

            msg.statusData = progress;

            msg.statusText = statusMessage;
            msg.messageType = type;
            return StatusCallback( msg );
        }
};
