//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2016 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module FRA4PicoScopeInterfaceTypes.h: Types supporting both the FRA4PicoScope Application
//                                       and the FRA4PicoScope API DLL.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef __cplusplus
#include <variant>
#include <string>
#include <vector>

using std::variant;
using std::wstring;
using std::vector;
#endif

typedef enum
{
    PS_CHANNEL_A,
    PS_CHANNEL_B,
    PS_CHANNEL_C,
    PS_CHANNEL_D,
    PS_CHANNEL_E,
    PS_CHANNEL_F,
    PS_CHANNEL_G,
    PS_CHANNEL_H,
    PS_MAX_CHANNELS,
    PS_CHANNEL_INVALID = -1
} PS_CHANNEL;

typedef enum
{
    PS_AC,
    PS_DC,
    PS_DC_1M = PS_DC,
    PS_DC_50R
} PS_COUPLING;

typedef enum
{
    ATTEN_1X,
    ATTEN_10X,
    ATTEN_20X,
    ATTEN_25X,
    ATTEN_50X,
    ATTEN_100X,
    ATTEN_200X,
    ATTEN_250X,
    ATTEN_500X,
    ATTEN_1000X,
    NUM_ATTENS
} ATTEN_T;

typedef enum
{
    RESOLUTION_8BIT = 0,
    RESOLUTION_12BIT = 1,
    RESOLUTION_14BIT = 2,
    RESOLUTION_15BIT = 3,
    RESOLUTION_16BIT = 4,
    RESOLUTION_10BIT = 10,

    RESOLUTION_MIN = 0,
    RESOLUTION_AUTO = 126,
    RESOLUTION_MAX = 127,
    RESOLUTION_CURRENT = 128
} RESOLUTION_T;

typedef int32_t PS_RANGE;

typedef enum
{
    USB20,
    USB30
} UsbVersion_T;

typedef struct
{
    double rangeVolts; // expressed as +/- X volts
    double ratioUp; // multiplier for how the signal will scale when increasing the size of the range, 0.0 if NA
    double ratioDown; // multiplier for how the signal will scale when decreasing the size of the range, 0.0 if NA
    wchar_t name[32];
} RANGE_INFO_T;

typedef enum
{
    LOW_NOISE,
    HIGH_NOISE
} SamplingMode_T;

typedef enum
{
    STEP_LINEAR,
    STEP_LOG_DECADE,
    STEP_LOG_OCTAVE
} StepType_T;

typedef struct
{
    double startFreqHz;
    double stopFreqHz;
    int steps;
    StepType_T stepType;
    double stimulusVpp;
    double stimulusOffset;
} FRASegment_T;

typedef enum
{
    OK, // Measurement is acceptable
    HIGHEST_RANGE_LIMIT_REACHED, // Overflow or amplitude too high and already on the highest range
    LOWEST_RANGE_LIMIT_REACHED, // Amplitude too low for good measurement precision and already on lowest range
    CHANNEL_OVERFLOW, // Overflow flag set
    AMPLITUDE_TOO_HIGH, // Amplitude is close enough to full scale that it would be best to increase the range
    AMPLITUDE_TOO_LOW, // Amplitude is low enough that range can be decreased to increase the measurement precision.
    NUM_AUTORANGE_STATUS_VALUES
} AUTORANGE_STATUS_T;

typedef enum
{
    FRA_STATUS_IDLE,
    FRA_STATUS_IN_PROGRESS,
    FRA_STATUS_COMPLETE,
    FRA_STATUS_CANCELED,
    FRA_STATUS_PRE_RUN_ALERT,
    FRA_STATUS_RETRY_LIMIT,
    FRA_STATUS_POWER_CHANGED,
    FRA_STATUS_FATAL_ERROR,
    FRA_STATUS_MESSAGE,
    FRA_STATUS_DATA
} FRA_STATUS_T;

typedef enum
{
    INSTRUMENT_ACCESS_DIAGNOSTICS = 0x0001,
    FRA_PROGRESS = 0x0002,
    STEP_TRIAL_PROGRESS = 0x0004,
    SIGNAL_GENERATOR_DIAGNOSTICS = 0x0008,
    VERTICAL_RESOLUTION_DIAGNOSTICS = 0x0010,
    AUTORANGE_DIAGNOSTICS = 0x0020,
    ADAPTIVE_STIMULUS_DIAGNOSTICS = 0x0040,
    SAMPLE_PROCESSING_DIAGNOSTICS = 0x0080,
    DFT_DIAGNOSTICS = 0x0100,
    SCOPE_POWER_EVENTS = 0x0200,
    SAVE_EXPORT_STATUS = 0x0400,
    FRA_WARNING = 0x0800,
    PICO_API_CALL = 0x1000,
    FRA_ERROR = 0x8000 // Errors are not maskable
} LOG_MESSAGE_FLAGS_T;

#ifdef __cplusplus
typedef struct
{
    PS_CHANNEL channel;
    bool smartProbeConnected;
    bool autoAtten;
    ATTEN_T atten;
    wstring attenStr;
    bool autoCoupling;
    PS_COUPLING coupling;
    wstring couplingStr;
    vector<wstring> availableCouplings;
} SmartProbeEvent_T;

struct FraStatusProgress
{
    int stepsComplete;
    int numSteps;
};

struct FraStatusCancelPoint
{
    int stepsComplete;
    int numSteps;
};

struct FraStatusRetryLimit
{
    struct
    {
        uint8_t triesAttempted;
        uint8_t allowedTries;
        AUTORANGE_STATUS_T inputChannelStatus;
        AUTORANGE_STATUS_T outputChannelStatus;
        PS_RANGE inputRange;
        PS_RANGE outputRange;
        const RANGE_INFO_T* inputRangeInfo;
        const RANGE_INFO_T* outputRangeInfo;
    } autorangeLimit;
    struct
    {
        uint8_t triesAttempted;
        uint8_t allowedTries;
        double stimulusVpp;
        double inputResponseAmplitudeV;
        double outputResponseAmplitudeV;
    } adaptiveStimulusLimit;
    // TODO history array
};

struct FraStatusPreRunAlert
{
    vector<vector<wstring>> warnings; // For each type of warning, a collection of the warning strings
    vector<vector<wstring>> errors; // For each type of error, a collection of the error strings
};

enum FRA_PRE_RUN_WARNING_T
{
    UNABLE_TO_MEET_REQUESTED_RESOLUTION,
    SAMPLING_RATE_LESS_THAN_REQUESTED,
    SAMPLING_RATE_LIMITED_BY_CHANNEL_UTILIZATION,
    CYCLES_ADJUSTED_TO_REMAIN_WITHIN_BUFFER,
    DFT_BANDWIDTH_GREATER_THAN_REQUESTED,
    AC_COUPLING_USED_WITH_LOW_FREQUENCY,
    MISMATCHED_COUPLING_WITH_LOW_FREQUENCY,
    SAMPLING_RATE_LESS_THAN_NYQUIST,
    NUMBER_OF_PRE_RUN_WARNING_CATEGORIES
};

enum FRA_PRE_RUN_ERROR_T
{
    REQUEST_EXCEEDS_SAMPLE_BUFFER,
    CANNOT_FIT_WHOLE_CYCLE_INTO_BUFFER,
    NUMBER_OF_PRE_RUN_ERROR_CATEGORIES
};

struct FraStatusPowerState
{
    bool auxDcPowered;
};

struct FraStatusFraData
{
    double freqLogHz;
    double gainDb;
    double phaseDeg;
};

struct FRA_STATUS_MESSAGE_T
{
    FRA_STATUS_T status;

    variant<FraStatusProgress, FraStatusCancelPoint, FraStatusRetryLimit, FraStatusPreRunAlert, FraStatusPowerState, FraStatusFraData> statusData;

    LOG_MESSAGE_FLAGS_T messageType;
    wstring statusText; // used to encode log or failure messages

    struct
    {
        // Whether to proceed, or cancel the FRA execution.  Should only be
        // used as a response to FRA_STATUS_RETRY_LIMIT, FRA_STATUS_PRE_RUN_ALERT or
        // FRA_STATUS_POWER_CHANGED so that we don't create a race condition.  Cancel
        // from the UI that's asynchronous (not tied to a specific FRA operation
        // like autorange) is handled separately.
        bool proceed; // If true, continue the FRA execution, otherwise cancel
        bool retry; // If true, try the failed step again, otherwise move on to next step

        union
        {
            struct
            {
                ATTEN_T inputAtten;
                ATTEN_T outputAtten;
                double inputAmplify;
                double outputAmplify;
            } autorangeAdjustment;
        } response;

    } responseData;

};

typedef bool (*FRA_STATUS_CALLBACK)(FRA_STATUS_MESSAGE_T& pFraStatus);
typedef void (*SMART_PROBE_CALLBACK)(vector<SmartProbeEvent_T>);
#endif