﻿//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: PicoScopeFRA.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PicoScopeFRA.h"
#include "StatusLog.h"
#include "picoStatus.h"
#include "utility.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <boost/math/special_functions/round.hpp>
#include <complex>
#include <algorithm>
#include <memory>
#include <vector>
#include <intrin.h>
#include <sstream>
#include <iomanip>
#include <functional>
#include "plplot.h" // For creating diagnostic graphs

#define WORKAROUND_PS_TIMEINDISPOSED_BUG

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DataReady
//
// Purpose: Callback used to signal when data capture has been completed by the scope.
//
// Parameters: See PicoScope programming API
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void __stdcall DataReady( short handle, PICO_STATUS status, void * pParameter)
{
    wstringstream fraStatusText;
    fraStatusText << L"BlockReady( " << handle << L", " << status << L", " << pParameter << L" )";
    LogMessage( fraStatusText.str(), PICO_API_CALL );

    PicoScopeFRA::SetCaptureStatus( status );
    SetEvent( *(HANDLE*)pParameter );
}

const double PicoScopeFRA::stimulusBasedInitialRangeEstimateMargin = 0.95;

const double PicoScopeFRA::acCouplingSignalDegradationThreshold = 2.0; // 2 Hz
const double PicoScopeFRA::mixedCouplingSignalMismatchThreshold = 200.0; // 200 Hz

PICO_STATUS PicoScopeFRA::captureStatus;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::PicoScopeFRA
//
// Purpose: Constructor
//
// Parameters: [in] statusCB - status callback
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

PicoScopeFRA::PicoScopeFRA(FRA_STATUS_CALLBACK statusCB, SMART_PROBE_CALLBACK smartProbeCB)
{
    StatusCallback = statusCB;
    ClientSmartProbeCallback = smartProbeCB;
    functionSmartProbeCallback = [=](std::vector<SmartProbeEvent_T> e) { SmartProbeCallback(e); };

    functionGenerateDiagnosticOutput = [=](TimeDomainDiagnosticData_T& e) { GenerateDiagnosticOutput(e); };

    currentInputChannelRange = (PS_RANGE)0;
    currentOutputChannelRange = (PS_RANGE)0;
    adaptiveStimulusInputChannelRange = (PS_RANGE)0;
    adaptiveStimulusOutputChannelRange = (PS_RANGE)0;

    mSamplingMode = LOW_NOISE;

    numSteps = numTotalSegmentSteps = numCompletedSegmentSteps = numCompletedSteps =
               latestCompletedNumSteps = currentSegmentNumber = freqStepCounter = freqStepIndex = 0;
    skipFirstStep = false;

    pInputBuffer = pOutputBuffer = NULL;

    ovIn = ovOut = false;
    delayForAcCoupling = false;
    inputChannelAutorangeStatus = outputChannelAutorangeStatus = OK;

    hCaptureEvent = CreateEventW( NULL, false, false, L"CaptureEvent" );

    if ((HANDLE)NULL == hCaptureEvent)
    {
        throw runtime_error( "Failed to create Capture Event" );
    }

    mInputDcOffset = 0.0;
    mOutputDcOffset = 0.0;
    actualSampFreqHz = 0.0;
    numSamples = 0;
    timeIndisposedMs = 0;
    currentInputMagnitude = 0.0;
    currentOutputMagnitude = 0.0;
    currentInputPhase = 0.0;
    currentOutputPhase = 0.0;
    currentInputPurity = 0.0;
    currentOutputPurity = 0.0;
    mPurityLowerLimit = 0.0;
    minAllowedAmplitudeRatio = 0.0;
    minAmplitudeRatioTolerance = 0.0;
    maxAmplitudeRatio = 0.0;
    maxAutorangeRetries = 0;
    mInputStartRange = 0;
    mOutputStartRange = 0;
    mExtraSettlingTimeMs = 0;
    mLowNoiseCyclesCaptured = 0;
    mMaxDftBw = 0.0;
    mLowNoiseOversampling = 0;
    mSweepDescending = false;
    mAdaptiveStimulus = false;
    mTargetResponseAmplitude = 0.0;
    mTargetResponseAmplitudeTolerance = 0.0;
    maxAdaptiveStimulusRetries = 0;
    mPhaseWrappingThreshold = 180.0;
    rangeCounts = 0.0;
    signalGeneratorPrecision = 0.0;
    previousResolution = L"";
    autorangeRetryCounter = 0;
    adaptiveStimulusRetryCounter = 0;
    stimulusChanged = false;
    mDiagnosticsOn = false;
    inputRangeInfo = outputRangeInfo = NULL;
    inputMinRange = 0;
    inputMaxRange = 0;
    outputMinRange = 0;
    outputMaxRange = 0;
    captureStatus = PICO_OK;
    ps = NULL;
    pESG = NULL;
    numAvailableChannels = 2;
    maxScopeSamplesPerChannel = 0;
    currentFreqHz = 0.0;
    currentStimulusVpp = 0.0;
    currentStimulusOffset = 0.0;
    stepStimulusVpp = 0.0;
    mMaxStimulusVpp = 0.0;
    currentOutputAmplitudeVolts = 0.0;
    currentInputAmplitudeVolts = 0.0;
    mStartFreqHz = 0.0;
    mStopFreqHz = 0.0;
    mStepsPerDecade = 10;
    mInputChannel = PS_CHANNEL_A;
    mOutputChannel = PS_CHANNEL_B;
    mInputChannelCoupling = PS_AC;
    mOutputChannelCoupling = PS_AC;
    mInputChannelAttenuation = ATTEN_1X;
    mOutputChannelAttenuation = ATTEN_1X;

    cancel = false;
}

void PicoScopeFRA::SmartProbeCallback(std::vector<SmartProbeEvent_T> probeEvents)
{
    if (probeEvents.size() > 0)
    {
        CancelFRA();
    }

    if (ClientSmartProbeCallback)
    {
        ClientSmartProbeCallback(probeEvents);
    }
}

std::function<void(vector<SmartProbeEvent_T>)> PicoScopeFRA::functionSmartProbeCallback;

void PicoScopeFRA::StaticSmartProbeCallback(std::vector<SmartProbeEvent_T> probeEvents)
{
    functionSmartProbeCallback(probeEvents);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SetInstrument
//
// Purpose: Tells the FRA object which scope to use.
//
// Parameters: _ps - The scope to use
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::SetInstrument( PicoScope* _ps )
{
    if (NULL != (ps = _ps))
    {
        numAvailableChannels = ps->GetNumChannels();
        signalGeneratorPrecision = ps->GetSignalGeneratorPrecision();
        ps->SetSmartProbeCallback(StaticSmartProbeCallback);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GetInstrument
//
// Purpose: Gets the scope that the FRA object will use
//
// Parameters: [out] pointer to the scope the FRA object will use
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

PicoScope* PicoScopeFRA::GetInstrument()
{
    return ps;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SetExtSigGen
//
// Purpose: Gives the FRA object an external signal generator to use
//
// Parameters: _pESG - The signal generator to use; NULL to signal that the scope's internal signal
//                     generator should be used.
//
// Notes: If this function is not used or passed a NULL, the FRA object assumes it should use the
//        scope's internal signal generator.
//
////////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::SetExtSigGen(ExtSigGen* _pESG)
{
    pESG = _pESG;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SetFraSettings
//
// Purpose: Set basic setting that are optional to set, but may need to be set occassionally
//
// Parameters: [in] samplingMode - Low or high noise sampling mode
//             [in] adaptiveStimulusMode - if true, run in adaptive stimulus mode
//             [in] targetResponseAmplitude - target for amplitude of the response signals; don't
//                                            allow either of input or output be less than this.
//             [in] sweepDescending - if true, sweep from highest frequency to lowest
//             [in] phaseWrappingThreshold - phase value to use as wrapping point (in degrees)
//                                           absolute value should be less than 360
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::SetFraSettings( SamplingMode_T samplingMode, bool adaptiveStimulusMode, double targetResponseAmplitude,
                                   bool sweepDescending, double phaseWrappingThreshold )
{
    mSamplingMode = samplingMode;
    mAdaptiveStimulus = adaptiveStimulusMode;
    mTargetResponseAmplitude = targetResponseAmplitude;
    mSweepDescending = sweepDescending;
    mPhaseWrappingThreshold = phaseWrappingThreshold;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SetFraTuning
//
// Purpose: Set more advanced settings that are optional to set, but may need to be set rarely
//
// Parameters: [in] purityLowerLimit - Lower limit on purity before we take action
//             [in] extraSettlingTimeMs - additional settling time to insert between setting up
//                                         signal generator and sampling
//             [in] autorangeTriesPerStep - Number of range tries allowed
//             [in] autorangeTolerance - Hysterysis used to determine when the switch
//             [in] smallSignalResolutionTolerance - Lower limit on signal amplitide before we
//                                                    take action
//             [in] maxAutorangeAmplitude - Amplitude before we switch to next higher range
//             [in] inputStartRange - Range to start input channel; -1 means base on stimulus
//             [in] outputStartRange - Range to start output channel; -1 means base on stimulus
//             [in] adaptiveStimulusTriesPerStep - Number of adaptive stimulus tries allowed
//             [in] targetResponseAmplitudeTolerance - Percent tolerance above target allowed for
//                                                     the smallest stimulus (input or output)
//             [in] lowNoiseCyclesCaptured - Stimulus signal cycles captured in low noise mode
//             [in] maxDftBw - Maximum bandwidth for DFT in noise reject mode
//             [in] lowNoiseOversampling - Amount to oversample the stimulus frequency in low
//                                         noise mode
//             [in] deviceResolution - Vertical resolution to use for data captures
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::SetFraTuning( double purityLowerLimit, uint16_t extraSettlingTimeMs, uint8_t autorangeTriesPerStep,
                                 double autorangeTolerance, double smallSignalResolutionTolerance, double maxAutorangeAmplitude,
                                 int32_t inputStartRange, int32_t outputStartRange, uint8_t adaptiveStimulusTriesPerStep,
                                 double targetResponseAmplitudeTolerance, uint16_t lowNoiseCyclesCaptured, double maxDftBw,
                                 uint16_t lowNoiseOversampling, RESOLUTION_T deviceResolution )
{
    mPurityLowerLimit = purityLowerLimit;
    mExtraSettlingTimeMs = extraSettlingTimeMs;
    maxAutorangeRetries = autorangeTriesPerStep;
    minAmplitudeRatioTolerance = autorangeTolerance;
    minAllowedAmplitudeRatio = smallSignalResolutionTolerance;
    maxAmplitudeRatio = maxAutorangeAmplitude;
    mInputStartRange = inputStartRange;
    mOutputStartRange = outputStartRange;
    maxAdaptiveStimulusRetries = adaptiveStimulusTriesPerStep;
    mTargetResponseAmplitudeTolerance = targetResponseAmplitudeTolerance;
    mLowNoiseCyclesCaptured = lowNoiseCyclesCaptured;
    mMaxDftBw = maxDftBw;
    mLowNoiseOversampling = lowNoiseOversampling;
    mDeviceResolution = deviceResolution;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SetLowNoiseCyclesCaptured
//
// Purpose: Sets the stimulus signal cycles to capture in low noise mode
//
// Parameters: [in] lowNoiseCyclesCaptured - Stimulus signal cycles captured for low noise mode
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::SetLowNoiseCyclesCaptured(uint16_t lowNoiseCyclesCaptured)
{
    mLowNoiseCyclesCaptured = lowNoiseCyclesCaptured;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::EnableDiagnostics
//
// Purpose: Turn on time domain diagnostic plot output
//
// Parameters: [in] baseDataPath - where to put the "diag" directory, where the plot files will
//                                 be stored
//
// Notes: In the future this could be used to output diagnostic info to the log
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::EnableDiagnostics(wstring baseDataPath)
{
    mBaseDataPath = baseDataPath;
    mDiagnosticsOn = false;
    mDiagnosticsFailed = true;

    if (pTimeDomainDiagnosticDataProcessor == NULL)
    {
        pTimeDomainDiagnosticDataProcessor = new MessageProcessor<TimeDomainDiagnosticData_T>();

        if (NULL == pTimeDomainDiagnosticDataProcessor)
        {
            throw runtime_error("Failed to create time domain diagnostic data processor");
        }

        pTimeDomainDiagnosticDataProcessor->SetProcessor( StaticGenerateDiagnosticOutput );

        hPLplotMutex = CreateMutex(NULL, FALSE, L"PLplot Mutex");

        if ((HANDLE)NULL == hPLplotMutex)
        {
            throw runtime_error("Failed to open PLplot Mutex");
        }
    }

    mDiagnosticsOn = true;
    mDiagnosticsFailed = false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::DisableDiagnostics
//
// Purpose: Turn off time domain diagnostic plot output
//
// Parameters: N/A
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::DisableDiagnostics(void)
{
    mDiagnosticsOn = false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GetMinFrequency
//
// Purpose: Gets the minimum frequency that can be supported with the given resolution, sampling
//          rate and buffer space
//
// Parameters: [in] resolution - the vertical resolution of the scope to consider; may pass
//                               RESOLUTION_AUTO
//             [out] return - Min Frequency supported
//
// Notes: This is a version that uses the current sampling mode and current state of the device
//        to determine a timebase and result.
//        E.g. In PS6000 devices with min timebase dependencies based on channel groupings, the
//        current channel settings are taken into consideration.  Thus, this function is useful
//        for input parameter validation before starting FRA.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

double PicoScopeFRA::GetMinFrequency( RESOLUTION_T resolution )
{
    FRA_STATUS_MESSAGE_T fraStatusMsg;

    if (ps)
    {
        double minSigGenFreq;
        double sigGenPrecision;
        uint32_t timebase;
        RESOLUTION_T actualResolution;
        RESOLUTION_T deviceResolution;

        if (pESG)
        {
            double dacFrequency;
            uint8_t phaseAccumulatorSize;
            if (!(pESG->GetMinFrequency(&minSigGenFreq)))
            {
                throw FraFault();
            }
            if ((pESG->IsDDS(&dacFrequency, &phaseAccumulatorSize)))
            {
                sigGenPrecision = dacFrequency / std::pow(2.0, phaseAccumulatorSize);
            }
            else
            {
                sigGenPrecision = 0.0;
            }
        }
        else
        {
            minSigGenFreq = ps->GetMinFuncGenFreq();
            sigGenPrecision = ps->GetSignalGeneratorPrecision();
        }

        if (mSamplingMode == LOW_NOISE)
        {
            // Since we're bound by minSigGenFreq, highest timebase in sweep is based on minSigGenFreq.
            // Unless mLowNoiseOversampling or minSigGenFreq are extremely high, this won't force a situation
            // where we're influenced by resolution-timebase dependencies.

            if (resolution == RESOLUTION_AUTO)
            {
                if (!(ps->GetMaxResolutionForFrequency( minSigGenFreq * (double)mLowNoiseOversampling, deviceResolution )))
                {
                    UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine minimum frequency.  Unable to determine maximum resolution for frequency" );
                    throw FraFault();
                }
            }
            else
            {
                deviceResolution = resolution;
            }

            if (!(ps->GetTimebase(minSigGenFreq * (double)mLowNoiseOversampling, deviceResolution, &actualSampFreqHz, &timebase)))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine minimum frequency.  Unable to determine low noise mode timebase" );
                throw FraFault();
            }
        }
        else
        {
            if (!ps->GetNoiseRejectModeTimebase(timebase))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine minimum frequency.  Unable to determine noise reject mode timebase." );
                throw FraFault();
            }
            deviceResolution = (resolution == RESOLUTION_AUTO) ? ps->GetMaxResolutionForTimebase( timebase ) : resolution;
        }

        // Set device resolution.  Per issue 175, this ensures GetTimebase functions don't fail since the
        // scope is open by default in max resolution.
        if (!ps->SetResolution( deviceResolution, actualResolution ))
        {
            UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to set device resolution while determining minimum stimulus frequency." );
            throw FraFault();
        }

        return GetMinFrequency( mSamplingMode, deviceResolution, timebase );
    }
    else
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Device not initialized." );
        throw FraFault();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GetMinFrequency
//
// Purpose: Gets the minimum frequency that can be supported with the given sampling mode,
//          resolution, sampling rate and buffer space
//
// Parameters: [in] samplingMode - sampling mode to consider, either LOW_NOISE or HIGH_NOISE
//             [in] resolution - the vertical resolution of the scope to consider; may pass
//                               RESOLUTION_AUTO
//             [in] timebase - the timebase to consider
//             [out] return - Min Frequency supported
//
// Notes: This is a version that does not use the current state of the device to determine the
//        timebase.  Thus, this function is useful for getting the minimum frequency to inform the
//        user during settings updates.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

double PicoScopeFRA::GetMinFrequency( SamplingMode_T samplingMode, RESOLUTION_T resolution, uint32_t timebase )
{
    FRA_STATUS_MESSAGE_T fraStatusMsg;

    if (ps)
    {
        double sigGenPrecision;

        if (pESG)
        {
            double dacFrequency;
            uint8_t phaseAccumulatorSize;
            if ((pESG->IsDDS(&dacFrequency, &phaseAccumulatorSize)))
            {
                sigGenPrecision = dacFrequency / std::pow(2.0, phaseAccumulatorSize);
            }
            else
            {
                sigGenPrecision = 0.0;
            }
        }
        else
        {
            sigGenPrecision = ps->GetSignalGeneratorPrecision();
        }

        double sampleRate;

        if (!(ps->GetFrequencyFromTimebase(timebase, sampleRate)))
        {
            UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine minimum stimulus frequency.  Unable to determine sampling frequency." );
            throw FraFault();
        }
        // Refresh max samples
        if (RESOLUTION_AUTO == resolution)
        {
            resolution = ps->GetMaxResolutionForTimebase(timebase);
        }
        if (!(ps->GetMaxSamples(&maxScopeSamplesPerChannel, resolution, timebase)))
        {
            UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine minimum stimulus frequency.  Unable to determine maximum samples." );
            throw FraFault();
        }
        else
        {
            double minFrequency = sampleRate / (double)maxScopeSamplesPerChannel;

            // Unlike Noise Reject mode where we can always go down to a single cycle if necessary, the
            // number of cycles in Low Noise mode is fixed and needs to be taken into consideration.
            if (samplingMode == LOW_NOISE)
            {
                minFrequency *= mLowNoiseCyclesCaptured;
            }

            // Round up to the nearest signal generator frequency because the frequency could get rounded down, causing sample calculations to exceed scope capacity
            double nearestFrequency = 0.0;

            if (pESG)
            {
                pESG->GetNearestFrequency( minFrequency, &nearestFrequency );
            }
            else
            {
                nearestFrequency = ps->GetClosestSignalGeneratorFrequency( minFrequency );
            }

            if (nearestFrequency < minFrequency)
            {
                minFrequency = nearestFrequency + sigGenPrecision;
            }
            else
            {
                minFrequency = nearestFrequency;
            }
            return minFrequency;
        }
    }
    else
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Device not initialized." );
        throw FraFault();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GetMaxFrequency
//
// Purpose: Gets the maximum frequency that can be supported by the frequnecy response
//
// Parameters: [out] return - Max Frequency supported
//
// Notes: Used by input parameter validation; for now this is limited strictly by the signal
//        generator, but with external signal generators, it may need to also be limited by the
//        the scope's bandwidth and sampling frequency.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

double PicoScopeFRA::GetMaxFrequency(void)
{
    if (pESG)
    {
        double maxFreq;
        if (pESG->GetMaxFrequency(&maxFreq))
        {
            return maxFreq;
        }
        else
        {
            throw FraFault();
        }
    }
    else if (ps)
    {
        return ps->GetMaxFuncGenFreq();
    }
    else
    {
        throw FraFault();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GetMaxFuncGenVpp
//
// Purpose: Gets the maximum amplitude/range in Vpp that can be supported by the signal generator
//
// Parameters: [in] startFreq - start frequency for requested range
//             [in] stopFreq - stop frequency for requested range
//             [out] return - Max Vpp amplitude/range supported
//
// Notes: Used by input parameter validation
//
///////////////////////////////////////////////////////////////////////////////////////////////////

double PicoScopeFRA::GetMaxFuncGenVpp(double startFreq, double stopFreq)
{
    if (pESG)
    {
        double maxAmplitudeVpp;
        if (pESG->GetMaxAmplitudeVpp(&maxAmplitudeVpp, startFreq, stopFreq))
        {
            return maxAmplitudeVpp;
        }
        else
        {
            throw FraFault();
        }
    }
    else if (ps)
    {
        // PicoScope built-in function generator output range is not frequency dependent
        return ps->GetMaxFuncGenVpp();
    }
    else
    {
        throw FraFault();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GetMinFuncGenVpp
//
// Purpose: Gets the minimum amplitude in Vpp that can be supported by the signal generator
//
// Parameters: [in] startFreq - start frequency for requested range
//             [in] stopFreq - stop frequency for requested range
//             [out] return - Min Vpp amplitude supported
//
// Notes: Used by input parameter validation
//
///////////////////////////////////////////////////////////////////////////////////////////////////

double PicoScopeFRA::GetMinFuncGenVpp(double startFreq, double stopFreq)
{
    if (pESG)
    {
        double minAmplitudeVpp;
        if (pESG->GetMinAmplitudeVpp(&minAmplitudeVpp, startFreq, stopFreq))
        {
            return minAmplitudeVpp;
        }
        else
        {
            throw FraFault();
        }
    }
    else if (ps)
    {
        // PicoScope built-in function generator output range is not frequency dependent
        return ps->GetMinFuncGenVpp();
    }
    else
    {
        throw FraFault();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SigGenSupportsDcOffset
//
// Purpose: Tells whether the signal generator supports a DC offset
//
// Parameters: [out] return - whether the signal generator supports a DC offset
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::SigGenSupportsDcOffset(void)
{
    if (pESG)
    {
        return pESG->SupportsDcOffset();
    }
    else
    {
        return true;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SetupChannels
//
// Purpose: Sets up channel parameters for input channel, output channel, and function generator
//
// Parameters: [in] inputChannel - Channel to use for input signal
//             [in] inputChannelCoupling - AC/DC coupling for input channel
//             [in] inputChannelAttenuation - Attenuation setting for input channel
//             [in] inputDcOffset - DC Offset for input channel
//             [in] outputChannel - Channel to use for output signal
//             [in] outputChannelCoupling - AC/DC coupling for output channel
//             [in] outputChannelAttenuation - Attenuation setting for output channel
//             [in] outputDcOffset - DC Offset for output channel
//             [in] initialStimulusVpp - Volts peak to peak of the stimulus signal; initial for
//                                       adaptive stimulus mode, constant otherwise
//             [in] maxStimulusVpp - Maximum volts peak to peak of the stimulus signal; used
//                                   in adaptive stimulus mode.
//             [in] stimulusDcOffset - d.c. offset of the stimulus signal
//             [out] return - Whether the function was successful.
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::SetupChannels( int inputChannel, int inputChannelCoupling, int inputChannelAttenuation, double inputDcOffset,
                                  int outputChannel, int outputChannelCoupling, int outputChannelAttenuation, double outputDcOffset,
                                  double initialStimulusVpp, double maxStimulusVpp, double stimulusDcOffset )
{
    FRA_STATUS_MESSAGE_T fraStatusMsg;

    if (!ps)
    {
        return false;
    }

    mInputChannelCoupling = (PS_COUPLING)inputChannelCoupling;
    mOutputChannelCoupling = (PS_COUPLING)outputChannelCoupling;

    mInputChannelAttenuation = (ATTEN_T)inputChannelAttenuation;
    mOutputChannelAttenuation = (ATTEN_T)outputChannelAttenuation;

    mInputChannel = (PS_CHANNEL)inputChannel;
    mOutputChannel = (PS_CHANNEL)outputChannel;

    if (!ps->IsAutoAttenuation(mInputChannel))
    {
        ps->SetChannelAttenuation( mInputChannel, mInputChannelAttenuation );
    }
    if (!ps->IsAutoAttenuation(mOutputChannel))
    {
        ps->SetChannelAttenuation( mOutputChannel, mOutputChannelAttenuation );
    }

    inputMinRange = ps->GetMinRange( mInputChannel, mInputChannelCoupling, inputDcOffset );
    inputMaxRange = ps->GetMaxRange( mInputChannel, mInputChannelCoupling, inputDcOffset );

    if (-1 == inputMinRange || -1 == inputMaxRange)
    {
        double minInputDcOffset, maxInputDcOffset;
        std::wstringstream fraStatusText;
        ps->GetMinMaxChannelDcOffsets(minInputDcOffset, maxInputDcOffset);
        fraStatusText << L"Input DC offset must be between " << minInputDcOffset << L" and " << maxInputDcOffset;
        UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText.str().c_str());
    }

    outputMinRange = ps->GetMinRange( mOutputChannel, mOutputChannelCoupling, outputDcOffset );
    outputMaxRange = ps->GetMaxRange( mOutputChannel, mOutputChannelCoupling, outputDcOffset );

    if (-1 == outputMinRange || -1 == outputMaxRange)
    {
        double minOutputDcOffset, maxOutputDcOffset;
        std::wstringstream fraStatusText;
        ps->GetMinMaxChannelDcOffsets(minOutputDcOffset, maxOutputDcOffset);
        fraStatusText << L"Output DC offset must be between " << minOutputDcOffset << L" and " << maxOutputDcOffset;
        UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText.str().c_str());
    }

    if (inputMinRange < 0 || inputMaxRange < 0 || outputMinRange < 0 || outputMaxRange < 0)
    {
        return false;
    }

    inputRangeInfo = ps->GetRangeCaps( mInputChannel );
    outputRangeInfo = ps->GetRangeCaps( mOutputChannel );

    if (-1 == mInputStartRange)
    {
        for (currentInputChannelRange = inputMaxRange; currentInputChannelRange > inputMinRange; currentInputChannelRange--)
        {
            if (initialStimulusVpp > 2.0*inputRangeInfo[currentInputChannelRange].rangeVolts*stimulusBasedInitialRangeEstimateMargin)
            {
                currentInputChannelRange++; // We stepped one too far, so backup
                currentInputChannelRange = min(currentInputChannelRange, inputMaxRange); // Just in case, so we can't get an illegal range
                break;
            }
        }
    }
    else
    {
        currentInputChannelRange = min(inputMaxRange, max(mInputStartRange, inputMinRange));
    }

    if (-1 == mOutputStartRange)
    {
        for (currentOutputChannelRange = outputMaxRange; currentOutputChannelRange > outputMinRange; currentOutputChannelRange--)
        {
            if (initialStimulusVpp > 2.0*outputRangeInfo[currentOutputChannelRange].rangeVolts*stimulusBasedInitialRangeEstimateMargin)
            {
                currentOutputChannelRange++; // We stepped one too far, so backup
                currentOutputChannelRange = min(currentOutputChannelRange, outputMaxRange); // Just in case, so we can't get an illegal range
                break;
            }
        }
    }
    else
    {
        currentOutputChannelRange = min(outputMaxRange, max(mOutputStartRange, outputMinRange));
    }

    mInputDcOffset = inputDcOffset;
    mOutputDcOffset = outputDcOffset;

    mMaxStimulusVpp = maxStimulusVpp;
    currentStimulusVpp = initialStimulusVpp;
    currentStimulusOffset = stimulusDcOffset;

    if (!(ps->Initialized()))
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Device not initialized." );
        return false;
    }

    if( !(ps->SetupChannel((PS_CHANNEL)mInputChannel, (PS_COUPLING)mInputChannelCoupling, currentInputChannelRange, mInputDcOffset)) )
    {
        return false;
    }
    if( !(ps->SetupChannel((PS_CHANNEL)mOutputChannel, (PS_COUPLING)mOutputChannelCoupling, currentOutputChannelRange, mOutputDcOffset)) )
    {
        return false;
    }

    // If either channel is AC coupling, turn on a delay for settling out DC offsets caused by
    // discontinuities from switching the signal generator.
    delayForAcCoupling = (mInputChannelCoupling == PS_DC && mOutputChannelCoupling == PS_DC) ? false : true;

    // Explicitly turn off the other channels if they exist
    if (numAvailableChannels > 2)
    {
        for (int chan = 0; chan < numAvailableChannels; chan++)
        {
            if (chan != mInputChannel && chan != mOutputChannel)
            {
                if( !(ps->DisableChannel((PS_CHANNEL)chan) ))
                {
                    return false;
                }
            }
        }
    }
    ps->DisableAllDigitalChannels(); // Ignore failures, which may occur if the scope is not connected to aux DC power

    // Tell the PicoScope which channels are input and output
    ps->SetChannelDesignations( mInputChannel, mOutputChannel );

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::ResetChannelRangesBasedOnStimulus
//
// Purpose: Changes the channel ranges based on stimulus if they are so configured.
//
// Parameters: [out] return - Whether the function was successful.
//
// Notes: Useful for multi-segment mode to optimize range seeking efficiency when a segment takes
//        a jump in stimulus
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::ResetChannelRangesBasedOnStimulus(void)
{
    if (!ps)
    {
        return false;
    }

    // Channels, attenuations, couplings, ranges, etc, should not change between segments, but do this for future
    // when we do allow it.
    if (!ps->IsAutoAttenuation(mInputChannel))
    {
        ps->SetChannelAttenuation( mInputChannel, mInputChannelAttenuation );
    }
    if (!ps->IsAutoAttenuation(mOutputChannel))
    {
        ps->SetChannelAttenuation( mOutputChannel, mOutputChannelAttenuation );
    }

    inputMinRange = ps->GetMinRange( mInputChannel, mInputChannelCoupling, mInputDcOffset );
    inputMaxRange = ps->GetMaxRange( mInputChannel, mInputChannelCoupling, mInputDcOffset );
    outputMinRange = ps->GetMinRange( mOutputChannel, mOutputChannelCoupling, mOutputDcOffset );
    outputMaxRange = ps->GetMaxRange( mOutputChannel, mOutputChannelCoupling, mOutputDcOffset );

    inputRangeInfo = ps->GetRangeCaps( mInputChannel );
    outputRangeInfo = ps->GetRangeCaps( mOutputChannel );

    if (-1 == mInputStartRange)
    {
        for (currentInputChannelRange = inputMaxRange; currentInputChannelRange > inputMinRange; currentInputChannelRange--)
        {
            if (currentStimulusVpp > 2.0*inputRangeInfo[currentInputChannelRange].rangeVolts*stimulusBasedInitialRangeEstimateMargin)
            {
                currentInputChannelRange++; // We stepped one too far, so backup
                currentInputChannelRange = min(currentInputChannelRange, inputMaxRange); // Just in case, so we can't get an illegal range
                break;
            }
        }
    }

    if (-1 == mOutputStartRange)
    {
        for (currentOutputChannelRange = outputMaxRange; currentOutputChannelRange > outputMinRange; currentOutputChannelRange--)
        {
            if (currentStimulusVpp > 2.0*outputRangeInfo[currentOutputChannelRange].rangeVolts*stimulusBasedInitialRangeEstimateMargin)
            {
                currentOutputChannelRange++; // We stepped one too far, so backup
                currentOutputChannelRange = min(currentOutputChannelRange, outputMaxRange); // Just in case, so we can't get an illegal range
                break;
            }
        }
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::ExecuteFRA
//
// Purpose: Carries out a multi-segment custom plan execution of the FRA
//
// Parameters: [in] fraSegments - vector of segments to execute
//             [out] return - Whether the function was successful.
//
// Notes: SetupChannels must be called before this function
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::ExecuteFRA( vector<FRASegment_T> fraSegments )
{
    bool retVal = true;
    size_t numSegments = fraSegments.size();

    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[128];

    // Results accumulated across all segments
    std::vector<double> accumulatedCompletedFreqsLogHz;
    std::vector<double> accumulatedCompletedGainsDb;
    std::vector<double> accumulatedCompletedPhasesDeg;
    std::vector<double> accumulatedCompletedUnwrappedPhasesDeg;

    try
    {
        // Get total number of steps
        numTotalSegmentSteps = 0;
        skipFirstStep = false;
        for (const FRASegment_T& fraSegment : fraSegments)
        {
            mStartFreqHz = fraSegment.startFreqHz;
            mStopFreqHz = fraSegment.stopFreqHz;
            mStepsPerDecade = fraSegment.steps;
            GenerateFrequencyPoints();
            numTotalSegmentSteps += numSteps;
            skipFirstStep = true;
        }

        if (mSweepDescending)
        {
            std::reverse(fraSegments.begin(), fraSegments.end());
        }

        skipFirstStep = false;
        numCompletedSegmentSteps = 0;
        currentSegmentNumber = 1;
        for (const FRASegment_T& fraSegment : fraSegments)
        {
            wsprintf(fraStatusText, L"Status: Starting segment %d of %d", currentSegmentNumber, numSegments);
            UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, FRA_PROGRESS);

            currentStimulusVpp = fraSegment.stimulusVpp;
            currentStimulusOffset = fraSegment.stimulusOffset;

            if (!ResetChannelRangesBasedOnStimulus())
            {
                retVal = false;
                break;
            }
            if (ExecuteFRA(fraSegment.startFreqHz, fraSegment.stopFreqHz, fraSegment.steps))
            {
                // Tack on the results
                accumulatedCompletedFreqsLogHz.insert( accumulatedCompletedFreqsLogHz.end(), latestCompletedFreqsLogHz.begin(), latestCompletedFreqsLogHz.end() );
                accumulatedCompletedGainsDb.insert( accumulatedCompletedGainsDb.end(), latestCompletedGainsDb.begin(), latestCompletedGainsDb.end() );
                accumulatedCompletedPhasesDeg.insert( accumulatedCompletedPhasesDeg.end(), latestCompletedPhasesDeg.begin(), latestCompletedPhasesDeg.end() );
                accumulatedCompletedUnwrappedPhasesDeg.insert( accumulatedCompletedUnwrappedPhasesDeg.end(), latestCompletedUnwrappedPhasesDeg.begin(), latestCompletedUnwrappedPhasesDeg.end() );

                numCompletedSegmentSteps += numSteps;
                currentSegmentNumber++;
                skipFirstStep = true;
            }
            else
            {
                if (numCompletedSteps && numCompletedSteps != numSteps)
                {
                    // Tack on the partial results
                    accumulatedCompletedFreqsLogHz.insert( accumulatedCompletedFreqsLogHz.end(), latestCompletedFreqsLogHz.begin(), latestCompletedFreqsLogHz.end() );
                    accumulatedCompletedGainsDb.insert( accumulatedCompletedGainsDb.end(), latestCompletedGainsDb.begin(), latestCompletedGainsDb.end() );
                    accumulatedCompletedPhasesDeg.insert( accumulatedCompletedPhasesDeg.end(), latestCompletedPhasesDeg.begin(), latestCompletedPhasesDeg.end() );
                    accumulatedCompletedUnwrappedPhasesDeg.insert( accumulatedCompletedUnwrappedPhasesDeg.end(), latestCompletedUnwrappedPhasesDeg.begin(), latestCompletedUnwrappedPhasesDeg.end() );

                    numCompletedSegmentSteps += numCompletedSteps;
                }
                retVal = false;
                break;
            }
        }
    }
    catch (const std::exception& e)
    {
        UNREFERENCED_PARAMETER(e);
        retVal = false;
    }

    // rewrite the results
    latestCompletedNumSteps = numCompletedSegmentSteps;
    latestCompletedFreqsLogHz = accumulatedCompletedFreqsLogHz;
    latestCompletedGainsDb = accumulatedCompletedGainsDb;
    latestCompletedPhasesDeg = accumulatedCompletedPhasesDeg;
    latestCompletedUnwrappedPhasesDeg = accumulatedCompletedUnwrappedPhasesDeg;

    // Reset these since they are indicators that we're executing segments in a custom plan mode
    skipFirstStep = false;
    numTotalSegmentSteps = 0;
    currentSegmentNumber = 0;
    numCompletedSegmentSteps = 0;

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::ExecuteFRA
//
// Purpose: Carries out the execution of the FRA
//
// Parameters: [in] startFreqHz - Beginning frequency
//             [in] stopFreqHz - End frequency
//             [in] stepsPerDecade - Steps per decade
//             [out] return - Whether the function was successful.
//
// Notes: SetupChannels must be called before this function
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::ExecuteFRA(double startFreqHz, double stopFreqHz, int stepsPerDecade )
{
    bool retVal = true;
    mStartFreqHz = startFreqHz;
    mStopFreqHz = stopFreqHz;
    mStepsPerDecade = stepsPerDecade;
    vector<vector<wstring>> warnings;
    vector<vector<wstring>> errors;

    DWORD dwWaitResult;
    DWORD winError;

    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[128];

    try
    {
        numCompletedSteps = 0;

        if (!ps->Connected())
        {
            ps->Close();
            UpdateStatus(fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Error: Scope not connected.");
            return false;
        }

        // This needs to be here for cases where settings like timebase/resolution/channels/ESG were changed after
        // frequency values were validated in a Custom Plan.
        double minFrequency = GetMinFrequency( mDeviceResolution );
        if (mStartFreqHz < minFrequency)
        {
            swprintf( fraStatusText, 128, L"Fatal error: Start frequency cannot be less than %lg Hz", minFrequency );
            UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, fraStatusText );
            throw FraFault();
        }

        GenerateFrequencyPoints();
        AllocateFraData();

        if (!PlanSampling(warnings, errors))
        {
            bool errorsExist = false;

            // There were problems found.  Send interactive event
            UpdateStatus( fraStatusMsg, FRA_STATUS_PRE_RUN_ALERT, warnings, errors );

            for (auto errorCategory : errors)
            {
                for (auto error : errorCategory)
                {
                    errorsExist = true;
                }
            }

            if (errorsExist || false == fraStatusMsg.responseData.proceed)
            {
                // Notify of cancellation
                UpdateStatus( fraStatusMsg, FRA_STATUS_CANCELED );
                throw FraFault();
            }
        }

        cancel = false;
        if (TRUE != (ResetEvent( hCaptureEvent )))
        {
            winError = GetLastError();
            wsprintf( fraStatusText, L"Fatal error: Failed to reset capture event: %u", winError );
            UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, fraStatusText );
            throw FraFault();
        }

        if (!(ps->ChannelDesignationsAreOptimal()))
        {
            UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE,
                L"WARNING: Input and output channels are from the same sampling block.\r\n"
                L"This may cause suboptimal sampling rate and/or vertical resolution.",
                FRA_WARNING );
        }

        // Update the status to indicate the FRA has started
        UpdateStatus( fraStatusMsg, FRA_STATUS_IN_PROGRESS );
        freqStepCounter = 1;

        freqStepIndex = mSweepDescending ? numSteps-1 : 0;
        while ((mSweepDescending && freqStepIndex >= 0) || (!mSweepDescending && freqStepIndex < numSteps))
        {
            currentFreqHz = freqsHz[freqStepIndex];

            swprintf(fraStatusText, 128, L"Status: Starting frequency step %d (%0.3lf Hz)", freqStepCounter + numCompletedSegmentSteps, currentFreqHz);
            UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, FRA_PROGRESS);

            for (autorangeRetryCounter = 0, adaptiveStimulusRetryCounter = 0;
                 autorangeRetryCounter < maxAutorangeRetries && adaptiveStimulusRetryCounter < maxAdaptiveStimulusRetries;)
            {
                try
                {
                    int totalFreqStepCounter = numTotalSegmentSteps ? numCompletedSegmentSteps + freqStepCounter : freqStepCounter;
                    if (mAdaptiveStimulus)
                    {
                        wsprintf(fraStatusText, L"Status: Starting frequency step %d, range try %d, adaptive stimulus try %d", totalFreqStepCounter, autorangeRetryCounter + 1, adaptiveStimulusRetryCounter + 1);
                    }
                    else
                    {
                        wsprintf(fraStatusText, L"Status: Starting frequency step %d, range try %d", totalFreqStepCounter, autorangeRetryCounter + 1);
                    }
                    UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, STEP_TRIAL_PROGRESS);
                    if (true != StartCapture(currentFreqHz))
                    {
                        throw FraFault();
                    }
                    // Adjust the delay time for a safety factor of 3x+10
                    timeIndisposedMs = timeIndisposedMs * 3.0 + 10000.0;
                    dwWaitResult = WaitForSingleObject(hCaptureEvent, (DWORD)timeIndisposedMs);

                    if (cancel)
                    {
                        // Notify of cancellation
                        UpdateStatus(fraStatusMsg, FRA_STATUS_CANCELED);
                        ps->CancelCapture();
                        throw FraFault();
                    }

                    if (dwWaitResult == WAIT_OBJECT_0)
                    {
                        if (PICO_OK == PicoScopeFRA::captureStatus)
                        {
                            if (false == ProcessData())
                            {
                                if (mDiagnosticsOn)
                                {
                                    if (!mDiagnosticsFailed)
                                    {
                                        pTimeDomainDiagnosticDataProcessor->PostMsg(&timeDomainDiagnosticData);
                                    }
                                    else
                                    {
                                        UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, L"WARNING: Failed to generate time domain diagnostic data plots", FRA_WARNING);
                                    }
                                }

                                // At least one of the channels needs adjustment
                                continue; // Try again on a different range
                            }
                            else // Data is good, calculate and move on to next frequency
                            {
                                // Currently no error is possible so just cast to void
                                (void)CalculateGainAndPhase(&gainsDb[freqStepIndex], &phasesDeg[freqStepIndex]);

                                // Notify progress
                                numCompletedSteps++;
                                UpdateStatus(fraStatusMsg, FRA_STATUS_IN_PROGRESS);

                                // Send new data point
                                UpdateStatus(fraStatusMsg, FRA_STATUS_DATA, freqsLogHz[freqStepIndex], gainsDb[freqStepIndex], phasesDeg[freqStepIndex]);

                                // Send the time domain data send here so that it executes after updating Bode Plot.  This should be more efficient as we can then
                                // be plotting time domain data while waiting on the next block capture.
                                if (mDiagnosticsOn)
                                {
                                    if (!mDiagnosticsFailed)
                                    {
                                        pTimeDomainDiagnosticDataProcessor->PostMsg(&timeDomainDiagnosticData);
                                    }
                                    else
                                    {
                                        UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, L"WARNING: Failed to generate time domain diagnostic data plots", FRA_WARNING);
                                    }
                                }

                                break;
                            }
                        }
                        else if (PICO_POWER_SUPPLY_CONNECTED == PicoScopeFRA::captureStatus ||
                                 PICO_POWER_SUPPLY_NOT_CONNECTED == PicoScopeFRA::captureStatus)
                        {
                            throw PicoScope::PicoPowerChange(PicoScopeFRA::captureStatus);
                        }
                        else
                        {
                            wstringstream wssError;
                            wssError << L"Fatal Error: Data capture error: " << PicoScopeFRA::captureStatus;
                            UpdateStatus(fraStatusMsg, FRA_STATUS_FATAL_ERROR, wssError.str().c_str());
                            ps->CancelCapture();
                            throw FraFault();
                        }
                    }
                    else
                    {
                        UpdateStatus(fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal Error: Data capture wait timed out");
                        ps->CancelCapture();
                        throw FraFault();
                    }
                }
                catch (const PicoScope::PicoPowerChange& ex)
                {
                    UpdateStatus( fraStatusMsg, FRA_STATUS_POWER_CHANGED, ex.GetState() == PICO_POWER_SUPPLY_CONNECTED );
                    // Change the power state regardless of whether the user wants to continue FRA execution.
                    ps->ChangePower(ex.GetState());
                    ps->CancelCapture();
                    if (true == fraStatusMsg.responseData.proceed)
                    {
                        // Start the step over again
                        autorangeRetryCounter = 0;
                        adaptiveStimulusRetryCounter = 0;
                        continue;
                    }
                    else
                    {
                        throw FraFault();
                    }
                }
            }

            if (autorangeRetryCounter == maxAutorangeRetries ||
                adaptiveStimulusRetryCounter == maxAdaptiveStimulusRetries)
            {
                // This is a temporary solution until we implement a fully interactive one.
                UpdateStatus( fraStatusMsg, FRA_STATUS_RETRY_LIMIT, inputChannelAutorangeStatus, outputChannelAutorangeStatus );
                if (true == fraStatusMsg.responseData.proceed)
                {
                    if (fraStatusMsg.responseData.retry)
                    {
                        // Start the step over again
                        continue; // bypasses step index and counter updates
                    }
                    else // continue to next step
                    {
                        // Mark as invalid
                        gainsDb[freqStepIndex] = NAN;
                        phasesDeg[freqStepIndex] = NAN;
                        numCompletedSteps++;
                        // Notify progress
                        UpdateStatus( fraStatusMsg, FRA_STATUS_IN_PROGRESS );
                        // Send new data point
                        UpdateStatus(fraStatusMsg, FRA_STATUS_DATA, freqsLogHz[freqStepIndex], gainsDb[freqStepIndex], phasesDeg[freqStepIndex]);
                    }
                }
                else
                {
                    // Notify of cancellation
                    UpdateStatus( fraStatusMsg, FRA_STATUS_CANCELED );
                    throw FraFault();
                }
            }

            // Step index and counter updates
            if (mSweepDescending)
            {
                freqStepIndex--;
            }
            else
            {
                freqStepIndex++;
            }
            freqStepCounter++;
        }

        freqStepCounter--; // For accurate reporting in case of failure after looping
    }
    catch (const FraFault& e)
    {
        UNREFERENCED_PARAMETER(e);
        if (!ps->Connected())
        {
            ps->Close();
            UpdateStatus(fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Error: Scope not connected.");
        }
        retVal = false;
    }
    catch (const runtime_error& e)
    {
        wstringstream wssError;
        wssError << L"FRA execution error: " << e.what();
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, wssError.str().c_str() );
        retVal = false;
    }
    catch (const std::bad_alloc& e)
    {
        UNREFERENCED_PARAMETER(e);
        wstringstream wssError;
        wssError << L"FRA execution error: Failed to allocate memory.";
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, wssError.str().c_str() );
        retVal = false;
    }

    if (numCompletedSteps)
    {
        if (numCompletedSteps != numSteps)
        {
            freqsLogHz.resize( numCompletedSteps );
            gainsDb.resize( numCompletedSteps );
            phasesDeg.resize( numCompletedSteps );
        }

        // Generate any alternate forms
        UnwrapPhases();

        TransferLatestResults();
    }

    UpdateStatus( fraStatusMsg, FRA_STATUS_COMPLETE );

    // Finally, disable the signal generator, but don't let failure be fatal
    try
    {
        if (pESG)
        {
            if (pESG->Ready() && !(pESG->DisableSignalGenerator()))
            {
                throw FraFault();
            }
        }
        else if (ps->Connected() && !(ps->DisableSignalGenerator()))
        {
            throw FraFault();
        }
    }
    catch (const exception& e)
    {
        UNREFERENCED_PARAMETER(e);
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::CancelFRA
//
// Purpose: Cancels the FRA execution
//
// Parameters: [out] return - Whether the function was successful.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::CancelFRA()
{
    cancel = true;
    SetEvent( hCaptureEvent ); // To break it out of waiting to capture data in case
    // there are several seconds of data to capture
    return true; // bool return reserved for possible future event based signalling that may fail
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::SetCaptureStatus
//
// Purpose: Communicate the status value from the capture callback
//
// Parameters: [in] status - the status value from the capture callback
//
// Notes:
//
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::SetCaptureStatus(PICO_STATUS status)
{
    PicoScopeFRA::captureStatus = status;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::CheckSignalOverflows
//
// Purpose: Makes auto-ranging decisions based on channel over-voltage info.
//
// Parameters: [out] return: whether to proceed with further computation, based on whether both
//                           channels are over-voltage
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::CheckSignalOverflows(void)
{
    bool retVal = true;
    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[128];

    // Reset to default
    inputChannelAutorangeStatus = OK;
    outputChannelAutorangeStatus = OK;

    // Make records for diagnostics
    timeDomainDiagnosticData.inputOV = ovIn;
    timeDomainDiagnosticData.outputOV = ovOut;
    timeDomainDiagnosticData.inputRange = currentInputChannelRange;
    timeDomainDiagnosticData.outputRange = currentOutputChannelRange;

    if (ovIn)
    {
        if (currentInputChannelRange < inputMaxRange)
        {
            inputChannelAutorangeStatus = CHANNEL_OVERFLOW;
            currentInputChannelRange = (PS_RANGE)((int)currentInputChannelRange + 1);
        }
        else
        {
            inputChannelAutorangeStatus = HIGHEST_RANGE_LIMIT_REACHED;
        }
        wsprintf( fraStatusText, L"Status: Measured input signal over-range" );
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, AUTORANGE_DIAGNOSTICS );
    }
    if (ovOut)
    {
        if (currentOutputChannelRange < outputMaxRange)
        {
            outputChannelAutorangeStatus = CHANNEL_OVERFLOW;
            currentOutputChannelRange = (PS_RANGE)((int)currentOutputChannelRange + 1);
        }
        else
        {
            outputChannelAutorangeStatus = HIGHEST_RANGE_LIMIT_REACHED;
        }
        wsprintf( fraStatusText, L"Status: Measured output signal over-range" );
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, AUTORANGE_DIAGNOSTICS );
    }

    if (ovIn && ovOut) // If both are over-range, signal to skip further processing.
    {
        retVal = false;
        // Initialize the diagnostic record since CheckSignalRanges will not run
        timeDomainDiagnosticData.inputAmplitude = 0.0;
        timeDomainDiagnosticData.outputAmplitude = 0.0;
    }

    // Clear overflow
    ovIn = ovOut = false;

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::CheckSignalRanges
//
// Purpose: Makes auto-ranging decisions based on signal max amplitude info.
//
// Parameters: [out] return: whether to proceed with further computation, based on whether both
//                           in-range
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::CheckSignalRanges(void)
{

    bool retVal = true;
    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[128];

    // Want amplitude to be above a lower threshold to avoid excessive quantization noise.
    // Want the signal to be below an upper threshold to avoid being close to overflow.

    // Check Input
    if (inputChannelAutorangeStatus == OK)
    {
        if (((double)inputAbsMax/rangeCounts) > maxAmplitudeRatio)
        {
            if (currentInputChannelRange < inputMaxRange)
            {
                currentInputChannelRange = (PS_RANGE)((int)currentInputChannelRange + 1);
                inputChannelAutorangeStatus = AMPLITUDE_TOO_HIGH;
            }
            else
            {
                inputChannelAutorangeStatus = OK; // Can't go any higher, but OK to stay here
            }
            retVal = false;
        }
        else if (((double)inputAbsMax/rangeCounts) <
                 (maxAmplitudeRatio/inputRangeInfo[currentInputChannelRange].ratioDown - minAmplitudeRatioTolerance))
        {
            if (currentInputChannelRange > inputMinRange)
            {
                currentInputChannelRange = (PS_RANGE)((int)currentInputChannelRange - 1);
                inputChannelAutorangeStatus = AMPLITUDE_TOO_LOW;
                retVal = false;
            }
            else
            {
                if (((double)inputAbsMax/rangeCounts) < minAllowedAmplitudeRatio)
                {
                    inputChannelAutorangeStatus = LOWEST_RANGE_LIMIT_REACHED;
                    retVal = false;
                }
                else
                {
                    // Do nothing
                }
            }
        }
        else
        {
            // Do nothing
        }
        swprintf( fraStatusText, 128, L"Status: Measured input absolute peak: %hu counts", inputAbsMax );
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, AUTORANGE_DIAGNOSTICS );
    }
    else
    {
        retVal = false; // Still need to adjust for the overflow on this channel
    }

    // Check Output
    if (outputChannelAutorangeStatus == OK)
    {
        if (((double)outputAbsMax/rangeCounts) > maxAmplitudeRatio)
        {
            if (currentOutputChannelRange < outputMaxRange)
            {
                currentOutputChannelRange = (PS_RANGE)((int)currentOutputChannelRange + 1);
                outputChannelAutorangeStatus = AMPLITUDE_TOO_HIGH;
            }
            else
            {
                outputChannelAutorangeStatus = OK; // Can't go any higher, but OK to stay here
            }
            retVal = false;
        }
        else if (((double)outputAbsMax/rangeCounts) <
                 (maxAmplitudeRatio/outputRangeInfo[currentOutputChannelRange].ratioDown - minAmplitudeRatioTolerance))
        {
            if (currentOutputChannelRange > outputMinRange)
            {
                currentOutputChannelRange = (PS_RANGE)((int)currentOutputChannelRange - 1);
                outputChannelAutorangeStatus = AMPLITUDE_TOO_LOW;
                retVal = false;
            }
            else
            {
                if (((double)outputAbsMax/rangeCounts) < minAllowedAmplitudeRatio)
                {
                    outputChannelAutorangeStatus = LOWEST_RANGE_LIMIT_REACHED;
                    retVal = false;
                }
                else
                {
                    // Do nothing
                }
            }
        }
        else
        {
            // Do nothing
        }
        swprintf( fraStatusText, 128, L"Status: Measured output absolute peak: %hu counts", outputAbsMax );
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, AUTORANGE_DIAGNOSTICS );
    }
    else
    {
        retVal = false; // Still need to adjust for the overflow on this channel
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GetResults
//
// Purpose: To get the results from the the most recently executed Frequency Response Analysis
//
// Parameters:
//    [out] numSteps - the number of frequency steps taken (also the size of the other arrays}
//    [out] freqsLogHz - array of frequency points taken, expressed in log base 10 Hz
//    [out] gainsDb - array of gains at each frequency point of freqsLogHz, expressed in dB
//    [out] phasesDeg - array of phase shifts at each frequency point of freqsLogHz, expressed in degrees
//    [out] unwrappedPhasesDeg - array of unwrapped phase shifts at each frequency point of freqsLogHz, 
//          expressed in degrees
//
// Notes: The memory returned in the pointers is only valid until the next FRA execution or
//        destruction of the PicoScope FRA object.  If there is no valid data, numSteps
//        is set to 0.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::GetResults( int* numSteps, double** freqsLogHz, double** gainsDb, double** phasesDeg, double** unwrappedPhasesDeg )
{
    if (numSteps && freqsLogHz && gainsDb && phasesDeg && unwrappedPhasesDeg )
    {
        *numSteps = latestCompletedNumSteps;
        *freqsLogHz = latestCompletedFreqsLogHz.data();
        *gainsDb = latestCompletedGainsDb.data();
        *phasesDeg = latestCompletedPhasesDeg.data();
        *unwrappedPhasesDeg = latestCompletedUnwrappedPhasesDeg.data();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::TransferLatestResults
//
// Purpose: Transfers the latest results into storage.  Used at the completion of the FRA to
//          facilitate not writing over the old results until we have to.
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::TransferLatestResults(void)
{
    latestCompletedNumSteps = numCompletedSteps;
    latestCompletedFreqsLogHz = freqsLogHz;
    latestCompletedGainsDb = gainsDb;
    latestCompletedPhasesDeg = phasesDeg;
    latestCompletedUnwrappedPhasesDeg = unwrappedPhasesDeg;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GenerateFrequencyPoints
//
// Purpose: Given the user's selection of start frequency, stop frequency, and steps per decade,
//          compute the frequency step points.
//
// Parameters: N/A
//
// Notes: Because the scopes have output frequency precision limits, this function also 
//        "patches up" the results to align to frequencies the scope is capable of
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::GenerateFrequencyPoints(void)
{
    double logStartFreqHz, logStopFreqHz;

    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[128];

    logStartFreqHz = log10(mStartFreqHz);
    logStopFreqHz = log10(mStopFreqHz);
    double logStepSize;

    // ceil to account for the fractional leftover and thus allow reaching the stop frequency
    // +1 to account for the start frequency
    numSteps = (int)(ceil((logStopFreqHz - logStartFreqHz)*mStepsPerDecade)) + 1;
    logStepSize = 1.0 / mStepsPerDecade;

    freqsHz.resize(numSteps);
    freqsLogHz.resize(numSteps);

    // Loop up to the second-to-last frequency point and
    // fill in the last one as the end frequency
    for (int i = 0; i < numSteps-1; i++)
    {
        freqsLogHz[i] = logStartFreqHz + i*logStepSize;
        freqsHz[i] = pow( 10.0, freqsLogHz[i] );
    }
    freqsLogHz[numSteps-1] = logStopFreqHz;
    freqsHz[numSteps-1] = mStopFreqHz;

    // Now "patch-up" the frequencies to account for the precision
    // limitations of the signal generator
    for (int i = 0; i < numSteps; i++)
    {
        if (pESG)
        {
            double patchedFrequency;
            if (!pESG->GetNearestFrequency(freqsHz[i], &patchedFrequency))
            {
                throw FraFault();
            }
            else
            {
                freqsHz[i] = patchedFrequency;
            }
        }
        else
        {
            freqsHz[i] = ps->GetClosestSignalGeneratorFrequency( freqsHz[i] );
        }
        freqsLogHz[i] = log10(freqsHz[i]);
    }

    // Remove all the duplicates
    freqsHz.erase(std::unique(freqsHz.begin(), freqsHz.end()), freqsHz.end());
    freqsLogHz.erase(std::unique(freqsLogHz.begin(), freqsLogHz.end()), freqsLogHz.end());
    if (numSteps != freqsHz.size())
    {
        swprintf( fraStatusText, 128, L"WARNING: Duplicate frequency points removed.  Sampling %zu points instead of %d.", freqsHz.size(), numSteps );
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, FRA_WARNING );
        numSteps = (int)freqsHz.size();
    }

    // Skip the first step for sequential segments in a custom plan
    // For descending mode, it's actually the last vector element that needs to be skipped
    if (skipFirstStep)
    {
        if (mSweepDescending)
        {
            freqsHz.pop_back();
            freqsLogHz.pop_back();
        }
        else
        {
            freqsHz.erase(freqsHz.begin());
            freqsLogHz.erase(freqsLogHz.begin());
        }
        numSteps--;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::AllocateFraData
//
// Purpose: Allocates data that the FRA will use to store input data, intermediate results, and 
//          diagnotic data.
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::AllocateFraData(void)
{
    idealStimulusVpp.resize(numSteps);

    // Clear and reset the sample plan
    samplePlan.resize(0);
    samplePlan.resize(numSteps);

    gainsDb.resize(numSteps);
    phasesDeg.resize(numSteps);
    unwrappedPhasesDeg.resize(numSteps);
}

std::function<void( PicoScopeFRA::TimeDomainDiagnosticData_T& )> PicoScopeFRA::functionGenerateDiagnosticOutput;

void PicoScopeFRA::StaticGenerateDiagnosticOutput( TimeDomainDiagnosticData_T& timeDomainData )
{
    functionGenerateDiagnosticOutput( timeDomainData );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::GenerateDiagnosticOutput
//
// Purpose: Creates time-domain plots of data gathered to support diagnosis of any problems 
//          encountered during the FRA - e.g. outputs that seem incorrect.
//
// Parameters: N/A
//
// Notes: Plots are stored in the user's Documents/FRA4PicoScope/diag directory
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::GenerateDiagnosticOutput( TimeDomainDiagnosticData_T& timeDomainData )
{
    stringstream fileName;
    stringstream overallTitleLine1;
    stringstream overallTitleLine2;
    stringstream inputTitle;
    stringstream outputTitle;
    wstring diagDataPath;
    vector<double> times;
    vector<double> inputMinVoltages;
    vector<double> outputMinVoltages;
    vector<double> inputMaxVoltages;
    vector<double> outputMaxVoltages;
    double inputAmplitude, outputAmplitude;
    FRA_STATUS_MESSAGE_T fraStatusMsg;
    DWORD dwWaitResult;

    try
    {
        times.resize(timeDomainDiagnosticDataLengthLimit);
        inputMinVoltages.resize(timeDomainDiagnosticDataLengthLimit);
        outputMinVoltages.resize(timeDomainDiagnosticDataLengthLimit);
        inputMaxVoltages.resize(timeDomainDiagnosticDataLengthLimit);
        outputMaxVoltages.resize(timeDomainDiagnosticDataLengthLimit);

        // Setup and test the diag directory for use as a current directory
        diagDataPath = mBaseDataPath + L"diag";
        if (0 == SetCurrentDirectory(diagDataPath.c_str()))
        {
            // Try to create it if it doesn't exist
            CreateDirectory(diagDataPath.c_str(), NULL);
            if (0 == SetCurrentDirectory(diagDataPath.c_str()))
            {
                throw runtime_error("Error generating diagnostic plots.  Could not find diagnostic data directory.");
            }
        }

        UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, L"Status: Generating diagnostic time domain plot.", FRA_PROGRESS);

        fileName.clear();
        fileName.str("");
        overallTitleLine1.clear();
        overallTitleLine1.str("");
        overallTitleLine2.clear();
        overallTitleLine2.str("");
        inputTitle.clear();
        inputTitle.str("");
        outputTitle.clear();
        outputTitle.str("");

        if (WAIT_OBJECT_0 != (dwWaitResult = WaitForSingleObject(hPLplotMutex, INFINITE)))
        {
            stringstream ss;
            ss << L"Error in time domain diagnostic plotting.  WaitForSingleObject returned " << dwWaitResult;
            throw runtime_error(ss.str());
        }

        // Use a different stream than the FRA plot since its stream is is still open, being used.
        plsstrm(1);

        plsdev( "svg" );
        plsexit(HandlePLplotError);

        if (timeDomainData.segmentNumber)
        {
            fileName << "segment " << timeDomainData.segmentNumber << " ";
        }

        if (timeDomainData.adaptiveStimulusEnabled)
        {
            fileName << "step " << timeDomainData.stepNumber << " auto range try " << timeDomainData.autoRangeTry
                        << " adaptive stimulus try " << timeDomainData.adaptiveStimulusTry << ".svg";
        }
        else
        {
            fileName << "step " << timeDomainData.stepNumber << " try " << timeDomainData.autoRangeTry << ".svg";
        }

        plsfnam( fileName.str().c_str() );

        plstar( 1, 2 ); // Setup to stack the input and output plots

        // Compute time and voltage data for plotting.
        for (uint32_t kl = 0; kl < timeDomainData.numSamplesToPlot; kl++)
        {
            times[kl] = ((double)kl) * timeDomainData.sampleInterval;
        }
        for (uint32_t kl = 0; kl < timeDomainData.numSamplesToPlot; kl++)
        {
            inputMinVoltages[kl] = ((double)timeDomainData.inputMinData[kl] / timeDomainData.rangeCounts) * inputRangeInfo[timeDomainData.inputRange].rangeVolts;
        }
        for (uint32_t kl = 0; kl < timeDomainData.numSamplesToPlot; kl++)
        {
            outputMinVoltages[kl] = ((double)timeDomainData.outputMinData[kl] / timeDomainData.rangeCounts) * outputRangeInfo[timeDomainData.outputRange].rangeVolts;
        }
        if (timeDomainData.numSamplesCaptured != timeDomainData.numSamplesToPlot) // Data was downsampled (aggregated)
        {
            for (uint32_t kl = 0; kl < timeDomainData.numSamplesToPlot; kl++)
            {
                inputMaxVoltages[kl] = ((double)timeDomainData.inputMaxData[kl] / timeDomainData.rangeCounts) * inputRangeInfo[timeDomainData.inputRange].rangeVolts;
            }
            for (uint32_t kl = 0; kl < timeDomainData.numSamplesToPlot; kl++)
            {
                outputMaxVoltages[kl] = ((double)timeDomainData.outputMaxData[kl] / timeDomainData.rangeCounts) * outputRangeInfo[timeDomainData.outputRange].rangeVolts;
            }
        }

        // Plot input
        plenv(0.0, timeDomainData.numSamplesToPlot * timeDomainData.sampleInterval,
                -inputRangeInfo[timeDomainData.inputRange].rangeVolts, inputRangeInfo[timeDomainData.inputRange].rangeVolts,
                0, 0);
        plcol0(1);

        if (timeDomainData.numSamplesCaptured == timeDomainData.numSamplesToPlot)
        {
            plline(timeDomainData.numSamplesToPlot, times.data(), inputMinVoltages.data());
        }
        else  // Data was downsampled (aggregated)
        {
            plwidth(0.5);
            for (uint32_t kl = 1; kl < timeDomainData.numSamplesToPlot; kl++)
            {
                PLFLT x[4], y[4];
                x[0] = x[1] = times[kl-1];
                x[2] = x[3] = times[kl];
                y[0] = inputMinVoltages[kl-1];
                y[1] = inputMaxVoltages[kl-1];
                y[2] = inputMinVoltages[kl];
                y[3] = inputMaxVoltages[kl];
                plfill(4, x, y);
            }
            plwidth(1.0);
        }

        // Draw input amplitude lines if no overflow
        plcol0(2);
        if (!timeDomainData.inputOV)
        {
            inputAmplitude = (timeDomainData.inputAmplitude / timeDomainData.rangeCounts) * inputRangeInfo[timeDomainData.inputRange].rangeVolts;
            pljoin(0.0, inputAmplitude, timeDomainData.numSamplesToPlot * timeDomainData.sampleInterval, inputAmplitude);
            pljoin(0.0, -inputAmplitude, timeDomainData.numSamplesToPlot * timeDomainData.sampleInterval, -inputAmplitude);
            inputTitle << "Input signal - Amplitude: " << setprecision(6) << inputAmplitude << " V, " << "Purity: " << setprecision(6) << timeDomainData.inputPurity;
        }
        else
        {
            inputTitle << "Input signal - Overrange";
        }

        plcol0(1);
        pllab( "Time (s)", "Volts", "" ); // Reserve the plot title for more precise placement with plmtex

        // Add an overall title, making its position relative to the top plot
        overallTitleLine1 << fixed;
        overallTitleLine2 << fixed;
        if (timeDomainData.segmentNumber)
        {
            overallTitleLine1 << "Segment " << timeDomainData.segmentNumber << ", ";
        }

        if (timeDomainData.adaptiveStimulusEnabled)
        {
            overallTitleLine1 << "Step " << timeDomainData.stepNumber << ", Auto Range Try " << timeDomainData.autoRangeTry << ", Adaptive Stimulus Try " << timeDomainData.adaptiveStimulusTry;
            plmtex("t", 4.3, 0.5, 0.5, overallTitleLine1.str().c_str());
            overallTitleLine2 << "\nFrequency: " << setprecision(3) << timeDomainData.freqHz
                                << " Hz, Stimulus Vpp: " << setprecision(6) << timeDomainData.stimVpp
                                << " V, Stimulus Cycles: " << timeDomainData.numStimulusCyclesCaptured
                                << ", Samples Captured: " << timeDomainData.numSamplesCaptured;
            plmtex("t", 3.0, 0.5, 0.5, overallTitleLine2.str().c_str());
            plmtex("t", 1.1, 0.5, 0.5, inputTitle.str().c_str());
        }
        else
        {
            overallTitleLine1 << "Step " << timeDomainData.stepNumber << ", Try " << timeDomainData.autoRangeTry << "; Frequency: " << setprecision(3) << timeDomainData.freqHz
                                << " Hz, Stimulus Vpp: " << setprecision(6) << timeDomainData.stimVpp
                                << " V, Stimulus Cycles: " << timeDomainData.numStimulusCyclesCaptured
                                << ", Samples Captured: " << timeDomainData.numSamplesCaptured;
            plmtex("t", 4.0, 0.5, 0.5, overallTitleLine1.str().c_str());
            plmtex("t", 2.0, 0.5, 0.5, inputTitle.str().c_str());
        }

        // Plot output
        plenv(0.0, timeDomainData.numSamplesToPlot * timeDomainData.sampleInterval,
                -outputRangeInfo[timeDomainData.outputRange].rangeVolts, outputRangeInfo[timeDomainData.outputRange].rangeVolts,
                0, 0);
        plcol0(1);

        if (timeDomainData.numSamplesCaptured == timeDomainData.numSamplesToPlot)
        {
            plline(timeDomainData.numSamplesToPlot, times.data(), outputMinVoltages.data());
        }
        else  // Data was downsampled (aggregated)
        {
            plwidth(0.5);
            for (uint32_t kl = 1; kl < timeDomainData.numSamplesToPlot; kl++)
            {
                PLFLT x[4], y[4];
                x[0] = x[1] = times[kl-1];
                x[2] = x[3] = times[kl];
                y[0] = outputMinVoltages[kl-1];
                y[1] = outputMaxVoltages[kl-1];
                y[2] = outputMinVoltages[kl];
                y[3] = outputMaxVoltages[kl];
                plfill(4, x, y);
            }
            plwidth(1.0);
        }

        // Draw output amplitude lines if no overflow
        plcol0(2);
        if (!timeDomainData.outputOV)
        {
            outputAmplitude = (timeDomainData.outputAmplitude / timeDomainData.rangeCounts) * outputRangeInfo[timeDomainData.outputRange].rangeVolts;
            pljoin(0.0, outputAmplitude, timeDomainData.numSamplesToPlot * timeDomainData.sampleInterval, outputAmplitude);
            pljoin(0.0, -outputAmplitude, timeDomainData.numSamplesToPlot * timeDomainData.sampleInterval, -outputAmplitude);
            outputTitle << "Output signal - Amplitude: " << setprecision(6) << outputAmplitude << " V, " << "Purity: " << setprecision(6) << timeDomainData.outputPurity;
        }
        else
        {
            outputTitle << "Output signal - Overrange";
        }

        plcol0(1);
        pllab( "Time (s)", "Volts", "" );

        if (timeDomainData.adaptiveStimulusEnabled)
        {
            plmtex("t", 1.1, 0.5, 0.5, outputTitle.str().c_str());
        }
        else
        {
            plmtex("t", 2.0, 0.5, 0.5, outputTitle.str().c_str());
        }

        // End this stream
        plend1();

        if (!ReleaseMutex(hPLplotMutex))
        {
            throw runtime_error("Unable to release PLplot mutex in time domain diagnostic plotting thread.");
        }
    }
    catch (std::exception ex)
    {
        mDiagnosticsFailed = true;
        wstringstream wssError;
        wssError << L"Error: Failure in time domain diagnostic data output processing: " << ex.what();
        UpdateStatus(fraStatusMsg, FRA_STATUS_MESSAGE, wssError.str().c_str());
        ExitThread(-1);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::HandlePLplotError
//
// Purpose: Function that gets installed to handle PLplot errors more gracefully.  Normally
//          PLplot just calls abort(), silently terminating the whole application.  We'll 
//          report the error via exceptions and handle them at a higher level.
//
// Parameters: [in] error - string containing error message from PLplot
//
// Notes: This function must not return and must throw an exception
//
///////////////////////////////////////////////////////////////////////////////////////////////////

int PicoScopeFRA::HandlePLplotError(const char* error)
{
    string plplotErrorString = "PLplot error generating diagnostic plots: ";
    plplotErrorString += error;
    plend();
    HANDLE hPLplotMutex;
    if ((HANDLE)NULL != (hPLplotMutex = OpenMutex(SYNCHRONIZE, FALSE, L"PLplot Mutex")))
    {
        ReleaseMutex(hPLplotMutex);
    }
    throw runtime_error( plplotErrorString.c_str() );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::PlanSampling
//
// Purpose: Creates the sample plan.  Signals when the planned FRA may not function
//          ideally (warnings), or at all (errors)
//
// Parameters: [out] warnings - warning strings categorized by type
//             [out] error - error strings categorized by type
//             [out] return - whether there are warning or error conditions.
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::PlanSampling( vector<vector<wstring>>& warnings, vector<vector<wstring>>& errors )
{
    bool retVal = true;
    uint32_t timebase;
    uint32_t numCycles;
    RESOLUTION_T actualResolution;

    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[256];
    wchar_t fraStatusLocation[128];

    // Setting minimum resolution to ensure timebase functions don't fail
    if (!ps->SetResolution( RESOLUTION_MIN, actualResolution ))
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to set device resolution while generating sampling plan." );
        throw FraFault();
    }

    warnings.resize( NUMBER_OF_PRE_RUN_WARNING_CATEGORIES );
    errors.resize( NUMBER_OF_PRE_RUN_ERROR_CATEGORIES );

    for (size_t idx = 0; idx < freqsHz.size(); idx++)
    {
        double measFreqHz = freqsHz[idx];

        swprintf( fraStatusLocation, sizeof(fraStatusLocation) / sizeof(wchar_t), L" [Step %zu, Stimulus frequency %.3lg Hz]", idx+1, measFreqHz );

        // Determine resolution for the step
        if (RESOLUTION_AUTO == mDeviceResolution)
        {
            if (LOW_NOISE == mSamplingMode && !ps->GetMaxResolutionForFrequency( measFreqHz * mLowNoiseOversampling, actualResolution ))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to get device resolution." );
                throw FraFault();
            }
            // Order of calls to GetNoiseRejectModeTimebase and GetMaxResolutionForTimebase is important
            else if (HIGH_NOISE == mSamplingMode && (!ps->GetNoiseRejectModeTimebase( timebase ) || !ps->GetMaxResolutionForTimebase( timebase, actualResolution )))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to get device resolution." );
                throw FraFault();
            }
        }
        else
        {
            if (!ps->SetResolution( mDeviceResolution, actualResolution ))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to set device resolution." );
                throw FraFault();
            }
            else if (mDeviceResolution != actualResolution)
            {
                wstring requestedResolutionStr, actualResolutionStr;
                requestedResolutionStr = ps->GetResolutionString( mDeviceResolution );
                actualResolutionStr = ps->GetResolutionString( actualResolution );
                wsprintf( fraStatusText, L"WARNING: Requested resolution %s unable to be met.  Setting resolution to %s.", requestedResolutionStr.c_str(), actualResolutionStr.c_str() );
                samplePlan[idx].warnings.push_back( fraStatusText );
                warnings[UNABLE_TO_MEET_REQUESTED_RESOLUTION].push_back( wstring(fraStatusText) + wstring(fraStatusLocation) );
            }
        }

        samplePlan[idx].resolution = actualResolution;

        ////////////////////////////////////////////////
        // Calculate timebase and number of samples
        ////////////////////////////////////////////////

        if (mSamplingMode == LOW_NOISE)
        {
            // Setup the sampling frequency and number of samples.  Criteria:
            // - In order for the amplitude calculation to be accurate, and minimize
            //   spectral leakage, we need to try to capture an integer number of
            //   periods of the signal of interest.
            double desiredSamplingFrequency = measFreqHz * (double)mLowNoiseOversampling;
            if (!(ps->GetTimebase( desiredSamplingFrequency, actualResolution, &actualSampFreqHz, &timebase )))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine low noise mode timebase." );
                throw FraFault();
            }
            else
            {
                if (actualSampFreqHz < desiredSamplingFrequency)
                {
                    swprintf( fraStatusText, sizeof(fraStatusText) / sizeof(wchar_t), L"WARNING: Actual sampling rate (%.3lg Hz) less than requested (%.3lg Hz).", actualSampFreqHz, desiredSamplingFrequency );
                    samplePlan[idx].warnings.push_back( fraStatusText );
                    warnings[SAMPLING_RATE_LESS_THAN_REQUESTED].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
                }

                // Refresh max samples since resolution may have changed above
                if (!(ps->GetMaxSamples( &maxScopeSamplesPerChannel, actualResolution, timebase )))
                {
                    UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine maximum samples." );
                    throw FraFault();
                }
            }
            // Calculate actual sample frequency, and number of samples, collecting enough
            // samples for the configured cycles of the measured frequency.
            numCycles = mLowNoiseCyclesCaptured;
            double samplesPerCycle = actualSampFreqHz / measFreqHz;
            // Deferring the integer conversion till this point ensures
            // the least inaccuracy in hitting the integer periods criteria
            numSamples = saturation_cast<uint32_t, long long>(llround( samplesPerCycle * (double)numCycles ));

            // Check whether the request will exceed the scope's buffer size.
            if (numSamples > maxScopeSamplesPerChannel)
            {
                swprintf( fraStatusText, sizeof(fraStatusText) / sizeof(wchar_t), L"ERROR: Request of %u samples exceeds scope buffer capacity of %u samples.", numSamples, maxScopeSamplesPerChannel );
                errors[REQUEST_EXCEEDS_SAMPLE_BUFFER].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
            }
        }
        else // NOISE_REJECT
        {
            uint32_t minBwSamples;
            double actualDftBw;
            bool adjustedByChannelSettings;

            if (!(ps->GetNoiseRejectModeTimebase( timebase, &adjustedByChannelSettings )))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine noise reject mode timebase." );
                throw FraFault();
            }
            if (adjustedByChannelSettings)
            {
                swprintf( fraStatusText, sizeof(fraStatusText) / sizeof(wchar_t), L"WARNING: Sampling rate limited by channel utilization." );
                samplePlan[idx].warnings.push_back( fraStatusText );
                warnings[SAMPLING_RATE_LIMITED_BY_CHANNEL_UTILIZATION].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
            }
            // Refresh max samples since resolution may have changed above
            if (!(ps->GetMaxSamples( &maxScopeSamplesPerChannel, actualResolution, timebase )))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine maximum samples." );
                throw FraFault();
            }

            if (!ps->GetNoiseRejectModeSampleRate( actualSampFreqHz ))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to determine noise reject mode sample rate." );
                throw FraFault();
            }

            // Calculate minimum number of samples required to stay <= maximum bandwidth
            minBwSamples = min( (saturation_cast<uint32_t, double>(ceil( actualSampFreqHz / mMaxDftBw ))), maxScopeSamplesPerChannel );
            // Calculate number of whole stimulus cycles required to have at least minBwSamples
            uint32_t initialNumCycles = numCycles = saturation_cast<uint32_t, double>(ceil( (double)minBwSamples * measFreqHz / actualSampFreqHz ));

            do
            {
                // Calculate actual number of samples to be taken
                numSamples = saturation_cast<uint32_t, long long>(llround( ((double)numCycles * actualSampFreqHz) / measFreqHz ));
                if (numSamples > maxScopeSamplesPerChannel)
                {
                    numCycles--;
                    if (numCycles == 0) // Should never happen in UI version due to checks on SR / BW
                    {
                        errors[REQUEST_EXCEEDS_SAMPLE_BUFFER].push_back( wstring(L"ERROR: Cannot fit a whole cycle into the buffer") + wstring( fraStatusLocation ) );
                    }
                }
            } while (numSamples > maxScopeSamplesPerChannel);

            if (numCycles != initialNumCycles)
            {
                swprintf( fraStatusText, sizeof(fraStatusText) / sizeof(wchar_t), L"WARNING: Cycles adjusted from %u to %u to remain within scope sample buffer", initialNumCycles, numCycles );
                samplePlan[idx].warnings.push_back( fraStatusText );
                warnings[CYCLES_ADJUSTED_TO_REMAIN_WITHIN_BUFFER].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
            }

            // Calculate actual DFT bandwidth
            actualDftBw = actualSampFreqHz / (double)numSamples;

            if (actualDftBw > mMaxDftBw)
            {
                swprintf( fraStatusText, sizeof(fraStatusText) / sizeof(wchar_t), L"WARNING: Actual DFT bandwidth (%.3lg Hz) greater than requested (%.3lg Hz)", actualDftBw, mMaxDftBw );
                samplePlan[idx].warnings.push_back( fraStatusText );
                warnings[DFT_BANDWIDTH_GREATER_THAN_REQUESTED].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
            }
        }

        if (actualSampFreqHz < 2.0 * measFreqHz)
        {
            swprintf( fraStatusText, sizeof( fraStatusText ) / sizeof( wchar_t ), L"WARNING: Actual sampling rate (%.3lg Hz) less than Nyquist frequency for stimulus.", actualSampFreqHz );
            samplePlan[idx].warnings.push_back( fraStatusText );
            warnings[SAMPLING_RATE_LESS_THAN_NYQUIST].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
        }

        // For this warning (AC_COUPLING_USED_WITH_LOW_FREQUENCY), only warn when both are in AC coupling.  If only one channel is in AC coupling, the concerns with mixed coupling and
        // low frequencies are more significant and we should favor that warning (MISMATCHED_COUPLING_WITH_LOW_FREQUENCY) instead.  There is not much value in displaying both warnings.
        if (mInputChannelCoupling == PS_AC && mOutputChannelCoupling == PS_AC && measFreqHz < acCouplingSignalDegradationThreshold)
        {
            swprintf( fraStatusText, sizeof( fraStatusText ) / sizeof( wchar_t ), L"WARNING: Use of low stimulus frequency (less than %.3lg Hz) with AC coupling.", acCouplingSignalDegradationThreshold );
            samplePlan[idx].warnings.push_back( fraStatusText );
            warnings[AC_COUPLING_USED_WITH_LOW_FREQUENCY].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
        }

        if (((mInputChannelCoupling == PS_AC && (mOutputChannelCoupling == PS_DC_1M || mOutputChannelCoupling == PS_DC_50R)) ||
             (mOutputChannelCoupling == PS_AC && (mInputChannelCoupling == PS_DC_1M || mInputChannelCoupling == PS_DC_50R))) &&
            measFreqHz < mixedCouplingSignalMismatchThreshold)
        {
            swprintf( fraStatusText, sizeof( fraStatusText ) / sizeof( wchar_t ), L"WARNING: Use of low stimulus frequency (less than %.3lg Hz) with mixed AC/DC coupling.", mixedCouplingSignalMismatchThreshold );
            samplePlan[idx].warnings.push_back( fraStatusText );
            warnings[MISMATCHED_COUPLING_WITH_LOW_FREQUENCY].push_back( wstring( fraStatusText ) + wstring( fraStatusLocation ) );
        }

        samplePlan[idx].timebase = timebase;
        samplePlan[idx].actualSampFreqHz = actualSampFreqHz;
        samplePlan[idx].numSamples = numSamples;
        samplePlan[idx].numCycles = numCycles;
    }

    for (auto warningCategory : warnings)
    {
        if (warningCategory.size())
        {
            retVal = false;
        }
    }

    for (auto errorCategory : errors)
    {
        if (errorCategory.size())
        {
            retVal = false;
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::StartCapture
//
// Purpose: Sets up the signal generator, and starts a data capture.
//
// Parameters: [in] measFreqHz - The frequency which we are currently measuring.
//             [out] return - Whether the function was successful.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::StartCapture( double measFreqHz )
{
    uint32_t timebase;
    uint32_t numCycles;

    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[256];

    timebase = samplePlan[freqStepIndex].timebase;
    actualSampFreqHz = samplePlan[freqStepIndex].actualSampFreqHz;
    numSamples = samplePlan[freqStepIndex].numSamples;
    numCycles = samplePlan[freqStepIndex].numCycles;

    if (autorangeRetryCounter == 0)
    {
        swprintf( fraStatusText, 128, L"Status: Setting signal generator to %0.6lf Vpp, %0.3lf Hz", currentStimulusVpp, measFreqHz );
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, SIGNAL_GENERATOR_DIAGNOSTICS );
    }

    if (mAdaptiveStimulus)
    {
        if (0 == adaptiveStimulusRetryCounter)
        {
            // Compute the initial stimulus Vpp since this is the first attempt at this frequency
            CalculateStepInitialStimulusVpp();
            stimulusChanged = true;
        }
        swprintf( fraStatusText, 128, L"Status: Setting signal generator amplitude to %0.6lf Vpp", currentStimulusVpp );
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, ADAPTIVE_STIMULUS_DIAGNOSTICS );
    }

    if (autorangeRetryCounter == 0 || (mAdaptiveStimulus && stimulusChanged))
    {
        if (pESG)
        {
            if (!(pESG->SetSignalGenerator(currentStimulusVpp, mAdaptiveStimulus ? 0.0 : currentStimulusOffset, measFreqHz)))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to set signal generator." );
                throw FraFault();
            }
        }
        else if (!(ps->SetSignalGenerator(currentStimulusVpp, mAdaptiveStimulus ? 0.0 : currentStimulusOffset, measFreqHz)))
        {
            UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to set signal generator." );
            throw FraFault();
        }

        if (mExtraSettlingTimeMs > 0)
        {
            Sleep( mExtraSettlingTimeMs );
        }
    }

    timeDomainDiagnosticData.stimVpp = currentStimulusVpp;

    // Setting resolution here because FlexRes scopes require calling SetChannel after calling
    // SetDeviceResolution and before calling RunBlock
    RESOLUTION_T actualResolution;
    if (!ps->SetResolution( samplePlan[freqStepIndex].resolution, actualResolution ))
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to set device resolution." );
        throw FraFault();
    }

    // Refresh range counts since resolution may have changed
    int16_t iRangeCounts;
    if (!ps->GetMaxValue(iRangeCounts))
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to get max value." );
        throw FraFault();
    }
    else
    {
        rangeCounts = iRangeCounts;
    }

    // Diagnostics and warnings for resolution settings
    wstring currentResolution;
    currentResolution = ps->GetResolutionString(RESOLUTION_CURRENT);
    wsprintf( fraStatusText, L"Status: Setting resolution to %s", currentResolution.c_str() );
    UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, VERTICAL_RESOLUTION_DIAGNOSTICS );
    if (freqStepCounter > 1)
    {
        if (currentResolution != previousResolution)
        {
            wsprintf( fraStatusText, L"WARNING: Resolution changed to %s", currentResolution.c_str() );
            UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, FRA_WARNING );
        }
    }
    previousResolution = currentResolution;

    // Setup channels
    wsprintf( fraStatusText, L"Status: Setting input channel range to %s", inputRangeInfo[currentInputChannelRange].name );
    UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, AUTORANGE_DIAGNOSTICS );
    if( !(ps->SetupChannel((PS_CHANNEL)mInputChannel, (PS_COUPLING)mInputChannelCoupling, currentInputChannelRange, mInputDcOffset, measFreqHz)) )
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to setup input channel." );
        throw FraFault();
    }

    wsprintf( fraStatusText, L"Status: Setting output channel range to %s", outputRangeInfo[currentOutputChannelRange].name );
    UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, AUTORANGE_DIAGNOSTICS );
    if( !(ps->SetupChannel((PS_CHANNEL)mOutputChannel, (PS_COUPLING)mOutputChannelCoupling, currentOutputChannelRange, mOutputDcOffset, measFreqHz)) )
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to setup output channel." );
        throw FraFault();
    }

    // Record these pre-adjustment versions here.  There is code later in step execution that
    // cannot use post-adjusted values.
    adaptiveStimulusInputChannelRange = currentInputChannelRange;
    adaptiveStimulusOutputChannelRange = currentOutputChannelRange;
    stepStimulusVpp = currentStimulusVpp;

    // Set no triggers
    if (!(ps->DisableChannelTriggers()))
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to disable channel triggers." );
        throw FraFault();
    }

    for (auto warning : samplePlan[freqStepIndex].warnings)
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, warning.c_str(), FRA_WARNING );
    }

    if (mDiagnosticsOn)
    {
        timeDomainDiagnosticData.numStimulusCyclesCaptured = numCycles;
        timeDomainDiagnosticData.rangeCounts = rangeCounts;
    }

    // Insert a progressive delay to settle out DC offsets caused by
    // discontinuities from switching the signal generator.
    if (delayForAcCoupling)
    {
        Sleep( 200*autorangeRetryCounter );
    }

    // Setup block mode
    if (!(ps->RunBlock(numSamples, timebase, &timeIndisposedMs, DataReady, &hCaptureEvent)))
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to start data capture." );
        throw FraFault();
    }

#if defined(WORKAROUND_PS_TIMEINDISPOSED_BUG)
    timeIndisposedMs = ((double)numSamples / actualSampFreqHz)*1000.0;
#endif

    swprintf( fraStatusText, 128, L"Status: Capturing %u samples (%u cycles) at %.3lg Hz takes %0.1lf sec.", numSamples, numCycles, actualSampFreqHz, timeIndisposedMs/1000.0 );
    UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, SAMPLE_PROCESSING_DIAGNOSTICS );

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::ProcessData
//
// Purpose: Retrieve the data the scope has captured and run signal processing
//
// Parameters: [out] return - Whether the function was successful.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::ProcessData(void)
{
    bool retVal = true;
    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[128];
    double inputAmplitude, outputAmplitude;

    wsprintf( fraStatusText, L"Status: Transferring and processing %u samples", numSamples );
    UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, SAMPLE_PROCESSING_DIAGNOSTICS );

    if (mDiagnosticsOn)
    {
        timeDomainDiagnosticData.autoRangeTry = autorangeRetryCounter + 1;
        timeDomainDiagnosticData.adaptiveStimulusEnabled = mAdaptiveStimulus;
        timeDomainDiagnosticData.adaptiveStimulusTry = adaptiveStimulusRetryCounter + 1;
    }

    if (!(ps->GetPeakValues( inputAbsMax, outputAbsMax, ovIn, ovOut )))
    {
        UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to get data peak values." );
        throw FraFault();
    }
    else if (false == CheckSignalOverflows())
    {
        // Both channels are over-range, don't bother with further analysis.
        retVal = false; // Signal to try again on a different range
        autorangeRetryCounter++;
    }
    else if (false == CheckSignalRanges())
    {
        // At least one of the channels needs adjustment
        retVal = false; // Signal to try again on a different range
        autorangeRetryCounter++;
    }

    // Run signal processing
    // 1) If both signal's ranges are acceptable
    //    OR
    // 2) If diagnostics are on and at least one of the signal's range is acceptable
    //    OR
    // 3) If adaptive stimulus is on and at least one of the signal's range is acceptable
    if (retVal == true || ((mDiagnosticsOn || mAdaptiveStimulus) &&
                           (CHANNEL_OVERFLOW != inputChannelAutorangeStatus ||
                            CHANNEL_OVERFLOW != outputChannelAutorangeStatus)))
    {
        uint32_t currentSampleIndex = 0;
        uint32_t maxDataRequestSize = ps->GetMaxDataRequestSize();
        uint32_t numSamplesToFeed;

        InitGoertzel( numSamples, actualSampFreqHz, currentFreqHz );

        for (currentSampleIndex = 0; currentSampleIndex < numSamples; currentSampleIndex+=numSamplesToFeed)
        {
            numSamplesToFeed = min(maxDataRequestSize, numSamples-currentSampleIndex);

            if (false == ps->GetData( numSamplesToFeed, currentSampleIndex, &pInputBuffer, &pOutputBuffer ))
            {
                UpdateStatus( fraStatusMsg, FRA_STATUS_FATAL_ERROR, L"Fatal error: Unable to get captured data from scope." );
                throw FraFault();
            }
            else
            {
                FeedGoertzel(pInputBuffer->data(), pOutputBuffer->data(), numSamplesToFeed);
            }
        }

        GetGoertzelResults( currentInputMagnitude, currentInputPhase, inputAmplitude, currentInputPurity, 
                            currentOutputMagnitude, currentOutputPhase, outputAmplitude, currentOutputPurity );

        if (mAdaptiveStimulus)
        {
            // Using the range indices recorded before auto-ranging, because auto-ranging could have changed the current index
            currentInputAmplitudeVolts = (inputAmplitude / rangeCounts) *
                                         inputRangeInfo[adaptiveStimulusInputChannelRange].rangeVolts;
            currentOutputAmplitudeVolts = (outputAmplitude / rangeCounts) *
                                          outputRangeInfo[adaptiveStimulusOutputChannelRange].rangeVolts;

            swprintf( fraStatusText, 128, L"Status: Measured input amplitude: %0.6lf V", currentInputAmplitudeVolts );
            UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, ADAPTIVE_STIMULUS_DIAGNOSTICS );
            swprintf( fraStatusText, 128, L"Status: Measured output amplitude: %0.6lf V", currentOutputAmplitudeVolts );
            UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, ADAPTIVE_STIMULUS_DIAGNOSTICS );

            if (false == CheckStimulusTarget())
            {
                retVal = false;
            }
            else if (CHANNEL_OVERFLOW != inputChannelAutorangeStatus && CHANNEL_OVERFLOW != outputChannelAutorangeStatus)
            {
                CheckStimulusTarget(true);
                idealStimulusVpp[freqStepIndex] = currentStimulusVpp;
            }
        }
    }
    if (mDiagnosticsOn)
    {
        vector<int16_t> inputMinData, outputMinData, inputMaxData, outputMaxData;

        ps->GetCompressedData( timeDomainDiagnosticDataLengthLimit, inputMinData, outputMinData,
                                                                    inputMaxData, outputMaxData );

        std::copy(inputMinData.begin(), inputMinData.end(), timeDomainDiagnosticData.inputMinData);
        std::copy(outputMinData.begin(), outputMinData.end(), timeDomainDiagnosticData.outputMinData);
        std::copy(inputMaxData.begin(), inputMaxData.end(), timeDomainDiagnosticData.inputMaxData);
        std::copy(outputMaxData.begin(), outputMaxData.end(), timeDomainDiagnosticData.outputMaxData);

        timeDomainDiagnosticData.inputAmplitude = inputAmplitude;
        timeDomainDiagnosticData.outputAmplitude = outputAmplitude;
        timeDomainDiagnosticData.inputPurity = currentInputPurity;
        timeDomainDiagnosticData.outputPurity = currentOutputPurity;
        timeDomainDiagnosticData.segmentNumber = currentSegmentNumber;
        timeDomainDiagnosticData.stepNumber = freqStepIndex + 1;
        timeDomainDiagnosticData.freqHz = currentFreqHz;
        timeDomainDiagnosticData.numSamplesToPlot = (uint32_t)inputMinData.size();
        timeDomainDiagnosticData.numSamplesCaptured = numSamples;

        if (timeDomainDiagnosticData.numSamplesToPlot < timeDomainDiagnosticData.numSamplesCaptured)
        {
            // The data for plotting is downsampled (aggregated)
            timeDomainDiagnosticData.sampleInterval = ((double)(timeDomainDiagnosticData.numSamplesCaptured - 1) / (double)(timeDomainDiagnosticData.numSamplesToPlot - 1)) / actualSampFreqHz;
        }
        else
        {
            timeDomainDiagnosticData.sampleInterval = 1.0 / actualSampFreqHz;
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::CheckStimulusTarget
//
// Purpose: Determine whether the stimulus amplitude needs to change for adaptive stimulus mode
//
// Parameters: [in] forceAdjust - if true, re-calculate regardless of whether the input or
//                                output amplitude are acceptable (within target + tolerance).
//                                useful for one final calculation of ideal stimulus.
//                                Defaults to false.
//             [out] return - false if the stimulus needs to change
//
// Notes: Strategy is to get the smaller signal within margin of target
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool PicoScopeFRA::CheckStimulusTarget(bool forceAdjust)
{
    double newStimulusFromInput = 0.0;
    double newStimulusFromOutput = 0.0;
    int inputRelation = 0; // -1 => under target; 1 => over target; 0 => within target
    int outputRelation = 0; // -1 => under target; 1 => over target; 0 => within target

    // Only bother checking on channels that are not over-range
    if (CHANNEL_OVERFLOW != inputChannelAutorangeStatus)
    {
        if (currentInputAmplitudeVolts < mTargetResponseAmplitude)
        {
            inputRelation = -1;
        }
        else if (currentInputAmplitudeVolts > (1.0 + mTargetResponseAmplitudeTolerance) * mTargetResponseAmplitude)
        {
            inputRelation = 1;
        }
        else
        {
            inputRelation = 0;
        }

        if (0 != inputRelation || forceAdjust)
        {
            // Calculate new value
            newStimulusFromInput = currentStimulusVpp * (((1.0 + mTargetResponseAmplitudeTolerance / 2.0) * mTargetResponseAmplitude) / currentInputAmplitudeVolts);
        }
    }
    // else - just leave inputRelation as "OK" since auto-ranging will cause a retry
    if (CHANNEL_OVERFLOW != outputChannelAutorangeStatus)
    {
        if (currentOutputAmplitudeVolts < mTargetResponseAmplitude)
        {
            outputRelation = -1;
        }
        else if (currentOutputAmplitudeVolts > (1.0 + mTargetResponseAmplitudeTolerance) * mTargetResponseAmplitude)
        {
            outputRelation = 1;
        }
        else
        {
            outputRelation = 0;
        }

        if (0 != outputRelation || forceAdjust)
        {
            // Calculate new value
            newStimulusFromOutput = currentStimulusVpp * (((1.0 + mTargetResponseAmplitudeTolerance / 2.0) * mTargetResponseAmplitude) / currentOutputAmplitudeVolts);
        }
    }
    // else - just leave outputRelation as "OK" since auto-ranging will cause a retry

    if (((inputRelation == 0 && outputRelation != -1) || (outputRelation == 0 && inputRelation != -1)) && !forceAdjust)
    {
        stimulusChanged = false;
        return true;
    }
    else
    {
        adaptiveStimulusRetryCounter += forceAdjust ? 0 : 1;
        // Bound the result.  Can't be higher than function generator maximum.  Need to avoid 0.0 or else future
        // adjustment will be bound to 0.0.  Don't update if it's our last allowed attempt.
        if (adaptiveStimulusRetryCounter < maxAdaptiveStimulusRetries)
        {
            currentStimulusVpp = max(ps->GetMinNonZeroFuncGenVpp(), min(mMaxStimulusVpp, max(newStimulusFromInput, newStimulusFromOutput)));
            stimulusChanged = true;
        }
        return forceAdjust;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::CalculateStepInitialStimulusVpp
//
// Purpose: Compute the initial value of stimulus amplitude for the new frequency for adaptive
//          stimulus mode
//
// Parameters: None
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::CalculateStepInitialStimulusVpp(void)
{
    if (freqStepCounter > 2)
    {
        // Two prior values exist, so estimate new stimulus Vpp based on their slope
        uint32_t idxMinus1, idxMinus2;
        idxMinus1 = mSweepDescending ? freqStepIndex + 1 : freqStepIndex - 1;
        idxMinus2 = mSweepDescending ? freqStepIndex + 2 : freqStepIndex - 2;
        currentStimulusVpp += ((freqsHz[freqStepIndex] - freqsHz[idxMinus1]) *
                               (idealStimulusVpp[idxMinus1] - idealStimulusVpp[idxMinus2])) /
                               (freqsHz[idxMinus1] - freqsHz[idxMinus2]);
        // Bound the result.  Can't be higher than function generator maximum.  Need to avoid 0.0 or else future
        // adjustment will be bound to 0.0.
        currentStimulusVpp = min(mMaxStimulusVpp, max(ps->GetMinNonZeroFuncGenVpp(), currentStimulusVpp));
    }
    // if freqStepCounter is 1, then just start with the initialized value
    // if freqStepCounter is 2, then just keep the prior value.
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::CalculateGainAndPhase
//
// Purpose: Calculate gain and phase shift of the input and output signals.
//
// Parameters: [out] gain - gain of input over output
//             [out] phase - phase shift from input to output
//             [out] return - Whether the function was successful.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////
 
bool PicoScopeFRA::CalculateGainAndPhase( double* gain, double* phase)
{
    double tempPhase;
    double crossover, crossoverUpper, crossoverLower;

    // Compute gain as dB
    // Compute channel range gain factor
    double channelGainFactor = outputRangeInfo[currentOutputChannelRange].rangeVolts /
                               inputRangeInfo[currentInputChannelRange].rangeVolts;
    *gain = 20.0 * log10( channelGainFactor * currentOutputMagnitude / currentInputMagnitude );

    // Compute phase in degrees, crossing over at the designated crossover
    crossover = M_PI * (mPhaseWrappingThreshold / 180.0);

    if (crossover < 0.0)
    {
        crossoverLower = crossover;
        crossoverUpper = crossoverLower + 2.0*M_PI; 
    }
    else
    {
        crossoverUpper = crossover;
        crossoverLower = crossoverUpper - 2.0*M_PI; 
    }

    tempPhase = currentOutputPhase - currentInputPhase;
    if (tempPhase > crossoverUpper)
    {
        tempPhase -= 2.0*M_PI;
    }
    else if (tempPhase < crossoverLower)
    {
        tempPhase += 2.0*M_PI;
    }

    *phase = tempPhase*180.0/M_PI;

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::UnwrapPhases
//
// Purpose: Take raw phase values and remove jumps in phase.
//
// Parameters: [out] phasesDeg - raw phases to unwrap
//             [out] unwrappedPhasesDeg - unwrapped phases
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void PicoScopeFRA::UnwrapPhases(void)
{
    double mostRecentValidUnwrappedPhase = NAN;

    // Copy over the raw phases
    unwrappedPhasesDeg = phasesDeg;

    std::vector<double>::iterator unwrappedIt = unwrappedPhasesDeg.begin();

    for (unwrappedIt++; unwrappedIt != unwrappedPhasesDeg.end(); unwrappedIt++)
    {
        if (!isnan(*std::prev(unwrappedIt)))
        {
            mostRecentValidUnwrappedPhase = *std::prev(unwrappedIt);
        }
        if (!isnan(*unwrappedIt))
        {
            if(!isnan(mostRecentValidUnwrappedPhase))
            {
                double oldJump = *unwrappedIt - mostRecentValidUnwrappedPhase;
                if(fabs(oldJump) > 180.0)
                {
                    double newJump = remainder(oldJump, 360.0);
                    *unwrappedIt = mostRecentValidUnwrappedPhase + newJump;
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::XXXXGoertzel
//
// Purpose: The Goertzel algorithm is a fast method of computing a single point DFT.  Both magnitude and phase
// are returned.  The advantage of performing the Goertzel algorithm vs an FFT is that for our application we're only
// interested in a single frequency per measurement, and thus Goertzel is faster than FFT.  Also, an FFT requires the
// full sample buffer to be stored.  Without an additional decimation, storing the whole sample buffer would be highly
// impractical for some PicoScopes (up to 8GB of RAM needed).  This implementation is a generalized Goertzel algorithm
// which allows for a non-integer bin (k ∈ R) [1], and thus is really a single point DTFT.  An important advantage of
// the generalized Goertzel is that we don't have to adjust the number of samples or sampling rate to maintain accuracy
// of the frequency selection.  These functions also compute other useful parameters such as amplitude and purity, which
// can be used for data quality decisions.
//
// Parameters: 
//    InitGoertzel
//             [in] totalSamples - Total number of samples in the full signal
//             [in] fSamp - frequency of the sampling
//             [in] fDetect - frequency to detect
//    FeedGoertzel
//             [in] inputSamples - input channel data sample points
//             [in] outputSamples - output channel data sample points
//             [in] n - number of samples in this block of samples
//    GetGoertzelResults
//             [output] [in/out]putMagnitude - The Goertzel magnitude
//             [output] [in/out]putPhase - The Goertzel phase
//             [output] [in/out]putAmplitude - The measured amplitude of the signal
//             [output] [in/out]putPurity - The purity of the signal (signal power over total power)
//
// Notes:
//
// - The standard recurrence used by the original Goertzel is known to have numerical accuracy
//   issues [2] - i.e. O(N^2) error growth for theta near 0 or PI.  This can start to be a real problem for
//   scopes with very large sample buffers (e.g. currently up to 2GS on the 6000E series).  The Reinsch
//   modifications are a well known technique to deal with error at these extremes [2][3].  As theta approaches
//   PI/2, the Reinsch recurrence numerical accuracy becomes worse than Goertzel [4].  To achieve the least
//   error, the technique of Oliver [4] will be used to divide the domain into three regions, applying the
//   best recurrence to each:
//  (0.0 - 0.305*PI): REINSCH_0; (0.305*PI - 0.705*PI): GOERTZEL; (0.705*PI - PI): REINSCH_PI
//
// - The d.c. energy calculation can be seen as an execution of the Goertzel with Magic Circle Oscillator.
//   When the tuning frequency is 0Hz, the tuning parameter is 0.  Thus the energy calculation is simplified.
//    - The idea to use alternate oscillators comes from the work of Clay Turner on Oscillator theory and
//      application to the Goertzel:
//      Ref. http://www.claysturner.com/dsp/digital_resonators.pdf
//      Ref. http://www.claysturner.com/dsp/ResonatorTable.pdf
//
// - In experimenting with different oscillators, it was found that the (k ∈ R) phase correction factor can
//   differ from e^j*2pi*k, but it still remains dependent only on k.  However, since this application is only
//   interested in phase shift between input and output signals, and these have the same frequency (k), we need
//   not apply the (k ∈ R) phase correction factor.  So, this implementation does not produce a "mathematically
//   correct" phase, but that's OK for our application.
//
// - The benefit of processing the input and output signals together is that we can take advantage of SIMD
//   parallelism (SSE, AVX, etc);  However, since the MSVC auto-vectorizer seems unable to vectorize the core
//   loop, we're going to code it with intrinsics.
//
//   [1] Goertzel algorithm generalized to non-integer multiples of fundamental frequency
//       Petr Sysel and Pavel Rajmic
//       EURASIP Journal on Advances in Signal Processing 2012 2012:56.
//       doi:10.1186/1687-6180-2012-56
//
//   [2] An error analysis of Goertzel's (Watt's) method for computing Fourier coefficients
//       W.M. Gentleman
//       Comput. J., Vol. 12, 1969, pp. 160-165.
//       doi:10.1093/comjnl/12.2.160
//
//   [3] Stoer, J.; Bulirsch, R. (2002), "Introduction to Numerical Analysis", Springer
//
//   [4] On the sensitivity to rounding errors of Chebyshev series approximations
//       J. Oliver
//       Journal of Computational and Applied Mathematics
//       Volume 3, Issue 2, June 1977, Pages 89-98
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Goertzel coefficient and state data
// Making these non class members to help simplify alignment
typedef enum
{
    REINSCH_0,
    GOERTZEL,
    REINSCH_PI
} DftRecurrence_T;

const double recurrenceThreshold1 = 0.305 * M_PI; // Oliver 77
const double recurrenceThreshold2 = 0.705 * M_PI; // Oliver 77
DftRecurrence_T dftRecurrence = REINSCH_0;
double theta;
alignas(16) static double Kappa;
alignas(16) static double a[2], b[2], τ[2];
alignas(16) static double totalEnergy[2];
alignas(16) static double dcEnergy[2];
static uint32_t samplesProcessed;
static uint32_t N;
// Goertzel outputs
static array<double,2> magnitude, phase, amplitude, purity;

void PicoScopeFRA::InitGoertzel( uint32_t totalSamples, double fSamp, double fDetect )
{
    double halfTheta;
    τ[0] = τ[1] = a[0] = a[1] = b[0] = b[1] = 0.0;
    totalEnergy[0] = totalEnergy[1] = 0.0;
    dcEnergy[0] = dcEnergy[1] = 0.0;
    samplesProcessed = 0;
    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[128];

    N = totalSamples;

    theta = 2.0 * M_PI * (fDetect / fSamp);

    if (theta < recurrenceThreshold1)
    {
        dftRecurrence = REINSCH_0;
        halfTheta = M_PI * (fDetect / fSamp);
        Kappa = -4.0 * pow( sin( halfTheta ), 2.0 );
        swprintf( fraStatusText, 128, L"Status: Computing DFT using Reinsch(0); actual BW: %.3lg Hz", fSamp / N );
    }
    else if (theta < recurrenceThreshold2)
    {
        dftRecurrence = GOERTZEL;
        Kappa = 2.0 * cos( theta );
        swprintf( fraStatusText, 128, L"Status: Computing DFT using Goertzel; actual BW: %.3lg Hz", fSamp / N );
    }
    else
    {
        dftRecurrence = REINSCH_PI;
        halfTheta = M_PI * (fDetect / fSamp);
        Kappa = 4.0 * pow( cos( halfTheta ), 2.0 );
        swprintf( fraStatusText, 128, L"Status: Computing DFT using Reinsch(PI); actual BW: %.3lg Hz", fSamp / N );
    }

    UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, DFT_DIAGNOSTICS );
}

void PicoScopeFRA::FeedGoertzel( int16_t* inputSamples, int16_t* outputSamples, uint32_t n )
{
    bool lastBlock = false;

    // Vectors
    __m128d KappaVec, τVec, aVec, bVec, totalEnergyVec, dcEnergyVec, sampleVec;

    FRA_STATUS_MESSAGE_T fraStatusMsg;
    wchar_t fraStatusText[1024];

    // Determine if this is the last block.  If it is, there is special processing.
    lastBlock = ((samplesProcessed + n) == N);

    // Load vectors
    KappaVec = _mm_load1_pd(&Kappa);
    τVec = _mm_load_pd(τ);
    aVec = _mm_load_pd(a);
    bVec = _mm_load_pd(b);
    totalEnergyVec = _mm_load_pd(totalEnergy);
    dcEnergyVec = _mm_load_pd(dcEnergy);

    // Execute the filter, d.c. Energy and Parseval energy time domain calculation
    if (REINSCH_0 == dftRecurrence)
    {
        for (uint32_t i = 0; i < n; i++)
        {
            // Load and convert samples to packed doubles
            sampleVec = _mm_cvtepi32_pd( _mm_set_epi32( 0, 0, outputSamples[i], inputSamples[i] ) );

            //totalEnergy += samples[i]*samples[i];
            totalEnergyVec = _mm_add_pd( totalEnergyVec, _mm_mul_pd( sampleVec, sampleVec ) );

            //dcEnergy += samples[i];
            dcEnergyVec = _mm_add_pd( dcEnergyVec, sampleVec );

            //b = b + K*a + samples[i]
            //a = a + b
            bVec = _mm_add_pd( _mm_add_pd(bVec, _mm_mul_pd(KappaVec, aVec)), sampleVec);
            aVec = _mm_add_pd(aVec, bVec);
        }
    }
    else if (GOERTZEL == dftRecurrence)
    {
        for (uint32_t i = 0; i < n; i++)
        {
            // Load and convert samples to packed doubles
            sampleVec = _mm_cvtepi32_pd( _mm_set_epi32( 0, 0, outputSamples[i], inputSamples[i] ) );

            //totalEnergy += samples[i]*samples[i];
            totalEnergyVec = _mm_add_pd( totalEnergyVec, _mm_mul_pd( sampleVec, sampleVec ) );

            //dcEnergy += samples[i];
            dcEnergyVec = _mm_add_pd( dcEnergyVec, sampleVec );

            //τ = K*a - b + samples[i];
            //b = a;
            //a = τ;
            τVec = _mm_add_pd( _mm_sub_pd( _mm_mul_pd( KappaVec, aVec ), bVec ), sampleVec );
            bVec = aVec;
            aVec = τVec;
        }
    }
    else // REINSCH_PI
    {
        for (uint32_t i = 0; i < n; i++)
        {
            // Load and convert samples to packed doubles
            sampleVec = _mm_cvtepi32_pd( _mm_set_epi32( 0, 0, outputSamples[i], inputSamples[i] ) );

            //totalEnergy += samples[i]*samples[i];
            totalEnergyVec = _mm_add_pd( totalEnergyVec, _mm_mul_pd( sampleVec, sampleVec ) );

            //dcEnergy += samples[i];
            dcEnergyVec = _mm_add_pd( dcEnergyVec, sampleVec );

            //b = K*a - b + samples[i]
            //a = b - a
            bVec = _mm_add_pd(_mm_sub_pd(_mm_mul_pd(KappaVec, aVec), bVec), sampleVec);
            aVec = _mm_sub_pd(bVec, aVec);
        }
    }

    samplesProcessed += n;

    if (lastBlock)
    {
        array<complex<double>, 2> y;
        array<double, 2> signalEnergy;

        // Iterate Goertzel once more with 0 input to get correct phase
        if (REINSCH_0 == dftRecurrence)
        {
            bVec = _mm_add_pd(bVec, _mm_mul_pd(KappaVec, aVec));
            aVec = _mm_add_pd(aVec, bVec);
        }
        else if (GOERTZEL == dftRecurrence)
        {
            τVec = _mm_sub_pd( _mm_mul_pd( KappaVec, aVec ), bVec );
            bVec = aVec;
            aVec = τVec;
        }
        else // REINSCH_PI
        {
            bVec = _mm_sub_pd(_mm_mul_pd(KappaVec, aVec), bVec);
            aVec = _mm_sub_pd(bVec, aVec);
        }

        // Unvectorize
        _mm_store_pd(a, aVec);
        _mm_store_pd(b, bVec);
        _mm_store_pd(totalEnergy, totalEnergyVec);
        _mm_store_pd(dcEnergy, dcEnergyVec);

        // Compute the complex output
        if (REINSCH_0 == dftRecurrence)
        {
            y[0] = std::complex<double>(b[0] + a[0] * Kappa/2.0, a[0] * sin(theta));
            y[1] = std::complex<double>(b[1] + a[1] * Kappa/2.0, a[1] * sin(theta));
        }
        else if (GOERTZEL == dftRecurrence)
        {
            y[0] = std::complex<double>(a[0] - b[0] * cos(theta), b[0] * sin(theta));
            y[1] = std::complex<double>(a[1] - b[1] * cos(theta), b[1] * sin(theta));
        }
        else // REINSCH_PI
        {
            y[0] = std::complex<double>(b[0] - a[0] * Kappa/2.0, -a[0] * sin(theta));
            y[1] = std::complex<double>(b[1] - a[1] * Kappa/2.0, -a[1] * sin(theta));
        }

        magnitude[0] = abs(y[0]);
        magnitude[1] = abs(y[1]);
        phase[0] = arg(y[0]);
        phase[1] = arg(y[1]);

        // Using N+1 because this form of the Goertzel iterates N+1 times, with x[N] = 0, thus effectively using N+1 samples.
        // The x[N]=0 sample has no effect on the time domain Parseval's energy calculation.
        amplitude[0] = 2.0 * magnitude[0] / (N+1);
        amplitude[1] = 2.0 * magnitude[1] / (N+1);
        signalEnergy[0] = 2.0 * (magnitude[0] * magnitude[0]) / (N+1);
        signalEnergy[1] = 2.0 * (magnitude[1] * magnitude[1]) / (N+1);
        dcEnergy[0] = (dcEnergy[0] * dcEnergy[0]) / (N+1);
        dcEnergy[1] = (dcEnergy[1] * dcEnergy[1]) / (N+1);

        purity[0] = signalEnergy[0] / (totalEnergy[0] - dcEnergy[0]);
        purity[1] = signalEnergy[1] / (totalEnergy[1] - dcEnergy[1]);

        // Output diagnostics
        swprintf( fraStatusText, 1024, L"Status: DFT results:\r\n"
                                       L"   Input magnitude: %lg; Input amplitude: %lg; Input phase: %lg; Input purity: %lg; Input DC energy: %lg; Input signal energy: %lg, Input total energy: %lg\r\n"
                                       L"   Output magnitude: %lg; Output amplitude: %lg; Output phase: %lg; Output purity: %lg; Output DC energy: %lg; Output signal energy: %lg, Output total energy: %lg",
                                       magnitude[0], amplitude[0], phase[0], purity[0], dcEnergy[0], signalEnergy[0], totalEnergy[0],
                                       magnitude[1], amplitude[1], phase[1], purity[1], dcEnergy[1], signalEnergy[1], totalEnergy[1] );

        UpdateStatus( fraStatusMsg, FRA_STATUS_MESSAGE, fraStatusText, DFT_DIAGNOSTICS );
    }
    else
    {
        // Unvectorize
        _mm_store_pd(τ, τVec);
        _mm_store_pd(a, aVec);
        _mm_store_pd(b, bVec);
        _mm_store_pd(totalEnergy, totalEnergyVec);
        _mm_store_pd(dcEnergy, dcEnergyVec);
    }
}

void PicoScopeFRA::GetGoertzelResults( double& inputMagnitude, double& inputPhase, double& inputAmplitude, double& inputPurity,
                                       double& outputMagnitude, double& outputPhase, double& outputAmplitude, double& outputPurity )
{
    inputMagnitude = magnitude[0];
    inputPhase = phase[0];
    inputAmplitude = amplitude[0];
    inputPurity = purity[0];

    outputMagnitude = magnitude[1];
    outputPhase = phase[1];
    outputAmplitude = amplitude[1];
    outputPurity = purity[1];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PicoScopeFRA::~PicoScopeFRA
//
// Purpose: Destructor
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

PicoScopeFRA::~PicoScopeFRA(void)
{
    if (NULL != hCaptureEvent)
    {
        (void)CloseHandle(hCaptureEvent);
    }
    if (NULL != pTimeDomainDiagnosticDataProcessor)
    {
        delete pTimeDomainDiagnosticDataProcessor;
    }

    // End use of PLplot
    plend();
}
