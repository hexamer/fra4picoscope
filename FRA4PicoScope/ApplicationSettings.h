//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ApplicationSettings.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

// Suppress an unnecessary C4996 warning about use of checked
// iterators invoked from a ptree compare operation
#if !defined(_SCL_SECURE_NO_WARNINGS)
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <string>
#include <codecvt>
#include <tuple>

#include <boost/property_tree/ptree.hpp>
#include <boost/range/adaptor/indexed.hpp>
using namespace boost::property_tree;
using namespace boost::adaptors;

#include "PicoScopeInterface.h"
#include "ExtSigGenPlugin.h"
#include "PicoScopeFRA.h"

typedef struct
{
    std::wstring startFreqHz;
    std::wstring stopFreqHz;
    std::wstring steps;
    std::wstring stimulusVpp;
    std::wstring stimulusOffset;
} FRASegmentStrings_T;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: class ApplicationSettings
//
// Purpose: This class supports an inteface to application and scope setting data.
//
// Parameters: N/A
//
// Notes: This class uses Boost Property tree do the bulk of the work
//
///////////////////////////////////////////////////////////////////////////////////////////////////

class ApplicationSettings
{
    public:
        ApplicationSettings( wstring _appDataFolder );
        ~ApplicationSettings(void);
        bool ReadApplicationSettings( void );
        bool WriteApplicationSettings( void );
        bool ReadScopeSettings( PicoScope* pScope );
        bool WriteScopeSettings( void );
        bool ReadEsgSettings(bool bInitialize = true);
        bool WriteEsgSettings(void);
        bool InitializeEsgParameters( ExtSigGen* pESG );
        void SetNoScopeSettings( void );

        // Accessors/Mutators for Application Settings

        inline string GetApplicationSettingsVersion(void)
        {
            string versionString;
            wstring versionStringW;
            try
            {
                versionStringW = AppSettingsPropTree.get<wstring>( L"appVersion" );
                versionString = wstring_convert<codecvt_utf8<wchar_t>>().to_bytes(versionStringW);
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                versionString = "unknown";
            }

            return versionString;
        }

        inline string GetScopeSettingsVersion(void)
        {
            string versionString;
            wstring versionStringW;
            try
            {
                versionStringW = ScopeSettingsPropTree.get<wstring>( L"appVersion" );
                versionString = wstring_convert<codecvt_utf8<wchar_t>>().to_bytes(versionStringW);
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                versionString = "unknown";
            }

            return versionString;
        }

        inline string GetEsgSettingsVersion(void)
        {
            string versionString;
            wstring versionStringW;
            try
            {
                versionStringW = EsgSettingsPropTree.get<wstring>( L"appVersion" );
                versionString = wstring_convert<codecvt_utf8<wchar_t>>().to_bytes(versionStringW);
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                versionString = "unknown";
            }

            return versionString;
        }

        inline void GetMostRecentScope( ScopeModel_T& model, ScopeDriverFamily_T& family, string& sn )
        {
            wstring snW = AppSettingsPropTree.get<wstring>( L"mostRecentScope.SN" );
            sn = wstring_convert<codecvt_utf8<wchar_t>>().to_bytes(snW);
            family = (ScopeDriverFamily_T)AppSettingsPropTree.get<int>( L"mostRecentScope.family" );
            model = (ScopeModel_T)AppSettingsPropTree.get<int>( L"mostRecentScope.model" );
        }

        inline void SetMostRecentScope( ScopeModel_T model, ScopeDriverFamily_T family, string sn )
        {
            wstring snW = wstring_convert<codecvt_utf8<wchar_t>>().from_bytes(sn);
            AppSettingsPropTree.put( L"mostRecentScope.SN", snW );
            AppSettingsPropTree.put( L"mostRecentScope.family", family );
            AppSettingsPropTree.put( L"mostRecentScope.model", model );
        }

        inline UseExtSigGen_T GetSignalGeneratorSource( void )
        {
            return (UseExtSigGen_T)AppSettingsPropTree.get<int>( L"signalGeneratorSource" );
        }
        inline void SetSignalGeneratorSource( UseExtSigGen_T source )
        {
            AppSettingsPropTree.put( L"signalGeneratorSource", source );
        }

        inline const wstring GetMostRecentSignalGeneratorPlugin( void )
        {
            return AppSettingsPropTree.get<wstring>( L"mostRecentSignalGenerator.plugin" );
        }
        inline void SetMostRecentSignalGeneratorPlugin( const wchar_t* mostRecentSignalGeneratorPlugin )
        {
            AppSettingsPropTree.put( L"mostRecentSignalGenerator.plugin", mostRecentSignalGeneratorPlugin );
        }

        inline const wstring GetMostRecentSignalGeneratorID( void )
        {
            return AppSettingsPropTree.get<wstring>( L"mostRecentSignalGenerator.ID" );
        }
        inline void SetMostRecentSignalGeneratorID( const wchar_t* mostRecentSignalGeneratorID )
        {
            AppSettingsPropTree.put( L"mostRecentSignalGenerator.ID", mostRecentSignalGeneratorID );
        }

        // Accessors/Mutators for Application Settings
        inline SamplingMode_T GetSamplingMode( void )
        {
            return (SamplingMode_T)ScopeSettingsPropTree.get<int>( L"picoScope.samplingMode" );
        }
        inline void SetSamplingMode( SamplingMode_T mode )
        {
            ScopeSettingsPropTree.put( L"picoScope.samplingMode", mode );
        }

        inline bool GetAdaptiveStimulusMode( void )
        {
            return (bool)AppSettingsPropTree.get<bool>( L"adaptiveStimulusMode" );
        }
        inline void SetAdaptiveStimulusMode( bool mode )
        {
            AppSettingsPropTree.put( L"adaptiveStimulusMode", mode );
        }

        inline const wstring GetTargetResponseAmplitudeAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"targetResponseAmplitude" );
        }
        inline double GetTargetResponseAmplitudeAsDouble( void )
        {
            double targetResponseAmplitude = 0.0;
            try // Because some callers may call with a blank field
            {
                targetResponseAmplitude = (double)AppSettingsPropTree.get<double>( L"targetResponseAmplitude" );
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                targetResponseAmplitude = 0.0;
            }
            return targetResponseAmplitude;
        }
        inline void SetTargetResponseAmplitude( wchar_t* targetResponseAmplitude )
        {
            AppSettingsPropTree.put( L"targetResponseAmplitude", targetResponseAmplitude );
        }

        inline bool GetSweepDescending( void )
        {
            return AppSettingsPropTree.get<bool>( L"sweepDescending" );
        }
        inline void SetSweepDescending( bool sweepDescending )
        {
            AppSettingsPropTree.put( L"sweepDescending", sweepDescending );
        }

        // Plot settings

        inline tuple<bool, double, double> GetFreqScale(void)
        {
            auto retVal = make_tuple( AppSettingsPropTree.get<bool>( L"plot.freqAxis.autoscale" ),
                                      AppSettingsPropTree.get<double>( L"plot.freqAxis.min" ),
                                      AppSettingsPropTree.get<double>( L"plot.freqAxis.max" ) );
            return retVal;
        }
        inline void SetFreqScale(tuple<bool, double, double> freqScale)
        {
            AppSettingsPropTree.put( L"plot.freqAxis.autoscale", get<0>(freqScale) );
            AppSettingsPropTree.put( L"plot.freqAxis.min", get<1>(freqScale) );
            AppSettingsPropTree.put( L"plot.freqAxis.max", get<2>(freqScale) );
        }

        inline tuple<bool, double, double> GetGainScale(void)
        {
            auto retVal = make_tuple( AppSettingsPropTree.get<bool>( L"plot.gainAxis.autoscale" ),
                                      AppSettingsPropTree.get<double>( L"plot.gainAxis.min" ),
                                      AppSettingsPropTree.get<double>( L"plot.gainAxis.max" ) );
            return retVal;
        }
        inline void SetGainScale(tuple<bool, double, double> gainScale)
        {
            AppSettingsPropTree.put( L"plot.gainAxis.autoscale", get<0>(gainScale) );
            AppSettingsPropTree.put( L"plot.gainAxis.min", get<1>(gainScale) );
            AppSettingsPropTree.put( L"plot.gainAxis.max", get<2>(gainScale) );
        }

        inline tuple<bool, double, double> GetPhaseScale(void)
        {
            auto retVal = make_tuple( AppSettingsPropTree.get<bool>( L"plot.phaseAxis.autoscale" ),
                                      AppSettingsPropTree.get<double>( L"plot.phaseAxis.min" ),
                                      AppSettingsPropTree.get<double>( L"plot.phaseAxis.max" ) );
            return retVal;
        }
        inline void SetPhaseScale(tuple<bool, double, double> phaseScale)
        {
            AppSettingsPropTree.put( L"plot.phaseAxis.autoscale", get<0>(phaseScale) );
            AppSettingsPropTree.put( L"plot.phaseAxis.min", get<1>(phaseScale) );
            AppSettingsPropTree.put( L"plot.phaseAxis.max", get<2>(phaseScale) );
        }

        inline tuple<double, uint8_t, bool, bool> GetFreqIntervals(void)
        {
            auto retVal = make_tuple( AppSettingsPropTree.get<double>( L"plot.freqAxis.majorTickInterval" ),
                                      AppSettingsPropTree.get<uint8_t>( L"plot.freqAxis.minorTicksPerMajorInterval" ),
                                      AppSettingsPropTree.get<bool>( L"plot.freqAxis.majorGrids" ),
                                      AppSettingsPropTree.get<bool>( L"plot.freqAxis.minorGrids" ));
            return retVal;
        }
        inline void SetFreqIntervals(tuple<double, uint8_t, bool, bool> freqIntervals)
        {
            AppSettingsPropTree.put( L"plot.freqAxis.majorTickInterval", get<0>(freqIntervals) );
            AppSettingsPropTree.put( L"plot.freqAxis.minorTicksPerMajorInterval", get<1>(freqIntervals) );
            AppSettingsPropTree.put( L"plot.freqAxis.majorGrids", get<2>(freqIntervals) );
            AppSettingsPropTree.put( L"plot.freqAxis.minorGrids", get<3>(freqIntervals) );
        }

        inline tuple<double, uint8_t, bool, bool> GetGainIntervals(void)
        {
            auto retVal = make_tuple( AppSettingsPropTree.get<double>( L"plot.gainAxis.majorTickInterval" ),
                                      AppSettingsPropTree.get<uint8_t>( L"plot.gainAxis.minorTicksPerMajorInterval" ),
                                      AppSettingsPropTree.get<bool>( L"plot.gainAxis.majorGrids" ),
                                      AppSettingsPropTree.get<bool>( L"plot.gainAxis.minorGrids" ));
            return retVal;
        }
        inline void SetGainIntervals(tuple<double, uint8_t, bool, bool> gainIntervals)
        {
            AppSettingsPropTree.put( L"plot.gainAxis.majorTickInterval", get<0>(gainIntervals) );
            AppSettingsPropTree.put( L"plot.gainAxis.minorTicksPerMajorInterval", get<1>(gainIntervals) );
            AppSettingsPropTree.put( L"plot.gainAxis.majorGrids", get<2>(gainIntervals) );
            AppSettingsPropTree.put( L"plot.gainAxis.minorGrids", get<3>(gainIntervals) );
        }

        inline bool GetGainMasterIntervals(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.gainAxis.masterGrids" );
        }
        inline void SetGainMasterIntervals( bool masterIntervals )
        {
            AppSettingsPropTree.put( L"plot.gainAxis.masterGrids", masterIntervals );
        }

        inline tuple<double, uint8_t, bool, bool> GetPhaseIntervals(void)
        {
            auto retVal = make_tuple( AppSettingsPropTree.get<double>( L"plot.phaseAxis.majorTickInterval" ),
                                      AppSettingsPropTree.get<uint8_t>( L"plot.phaseAxis.minorTicksPerMajorInterval" ),
                                      AppSettingsPropTree.get<bool>( L"plot.phaseAxis.majorGrids" ),
                                      AppSettingsPropTree.get<bool>( L"plot.phaseAxis.minorGrids" ));
            return retVal;
        }
        inline void SetPhaseIntervals(tuple<double, uint8_t, bool, bool> phaseIntervals)
        {
            AppSettingsPropTree.put( L"plot.phaseAxis.majorTickInterval", get<0>(phaseIntervals) );
            AppSettingsPropTree.put( L"plot.phaseAxis.minorTicksPerMajorInterval", get<1>(phaseIntervals) );
            AppSettingsPropTree.put( L"plot.phaseAxis.majorGrids", get<2>(phaseIntervals) );
            AppSettingsPropTree.put( L"plot.phaseAxis.minorGrids", get<3>(phaseIntervals) );
        }

        inline bool GetPhaseMasterIntervals(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.phaseAxis.masterGrids" );
        }
        inline void SetPhaseMasterIntervals( bool masterIntervals )
        {
            AppSettingsPropTree.put( L"plot.phaseAxis.masterGrids", masterIntervals );
        }

        inline bool GetPlotRealTime(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.realTimeUpdate" );
        }
        inline void SetPlotRealTime( bool realTime )
        {
            AppSettingsPropTree.put( L"plot.realTimeUpdate", realTime );
        }

        inline bool GetAutoAxes(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.autoAxes" );
        }
        inline void SetAutoAxes( bool autoAxes )
        {
            AppSettingsPropTree.put( L"plot.autoAxes", autoAxes );
        }

        inline bool GetPlotGain(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.plotGain" );
        }
        inline void SetPlotGain( bool plotGain )
        {
            AppSettingsPropTree.put( L"plot.plotGain", plotGain );
        }

        inline bool GetPlotPhase(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.plotPhase" );
        }
        inline void SetPlotPhase( bool plotPhase )
        {
            AppSettingsPropTree.put( L"plot.plotPhase", plotPhase );
        }

        inline bool GetPlotGainMargin(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.plotGainMargin" );
        }
        inline void SetPlotGainMargin( bool plotGainMargin )
        {
            AppSettingsPropTree.put( L"plot.plotGainMargin", plotGainMargin );
        }

        inline bool GetPlotPhaseMargin(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.plotPhaseMargin" );
        }
        inline void SetPlotPhaseMargin( bool plotPhaseMargin )
        {
            AppSettingsPropTree.put( L"plot.plotPhaseMargin", plotPhaseMargin );
        }

        inline bool GetPlotNeg3dB(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.plotNeg3dB" );
        }
        inline void SetPlotNeg3dB( bool plotNeg3dB )
        {
            AppSettingsPropTree.put( L"plot.plotNeg3dB", plotNeg3dB );
        }

        inline bool GetPlotUnwrappedPhase(void)
        {
            return AppSettingsPropTree.get<bool>( L"plot.plotUnwrappedPhase" );
        }
        inline void SetPlotUnwrappedPhase( bool plotUnwrappedPhase )
        {
            AppSettingsPropTree.put( L"plot.plotUnwrappedPhase", plotUnwrappedPhase );
        }

        inline double GetPhaseWrappingThresholdAsDouble(void)
        {
            return AppSettingsPropTree.get<double>( L"plot.phaseWrappingThreshold" );
        }
        inline const wstring GetPhaseWrappingThresholdAsString(void)
        {
            return AppSettingsPropTree.get<wstring>( L"plot.phaseWrappingThreshold" );
        }
        inline void SetPhaseWrappingThreshold( const wstring phaseWrappingThreshold )
        {
            AppSettingsPropTree.put( L"plot.phaseWrappingThreshold", phaseWrappingThreshold );
        }

        inline double GetGainMarginPhaseCrossoverAsDouble(void)
        {
            return AppSettingsPropTree.get<double>( L"plot.gainMarginPhaseCrossover" );
        }
        inline const wstring GetGainMarginPhaseCrossoverAsString(void)
        {
            return AppSettingsPropTree.get<wstring>( L"plot.gainMarginPhaseCrossover" );
        }
        inline void SetGainMarginPhaseCrossover( wstring gainMarginPhaseCrossover )
        {
            AppSettingsPropTree.put( L"plot.gainMarginPhaseCrossover", gainMarginPhaseCrossover );
        }

        inline const wstring GetPlotTitle( void )
        {
            return AppSettingsPropTree.get<wstring>( L"plot.plotTitle" );
        }
        inline void SetPlotTitle( wstring title )
        {
            AppSettingsPropTree.put( L"plot.plotTitle", title );
        }

        inline bool GetPlotTitleAutoIncrement( void )
        {
            return AppSettingsPropTree.get<bool>( L"plot.plotAutoIncrement" );
        }
        inline void SetPlotTitleAutoIncrement( bool autoIncrement )
        {
            AppSettingsPropTree.put( L"plot.plotAutoIncrement", autoIncrement );
        }

        inline uint16_t GetPlotTitleNextNumberAsUint16( void )
        {
            return AppSettingsPropTree.get<uint16_t>( L"plot.plotNextNumber" );
        }
        inline const wstring GetPlotTitleNextNumberAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"plot.plotNextNumber" );
        }
        inline void SetPlotTitleNextNumber( uint16_t nextNumber )
        {
            AppSettingsPropTree.put( L"plot.plotNextNumber", nextNumber );
        }

        inline double GetPlotTitleFontScaleAsDouble( void )
        {
            return AppSettingsPropTree.get<double>( L"plot.plotTitleFontScale" );
        }
        inline const wstring GetPlotTitleFontScaleAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"plot.plotTitleFontScale" );
        }
        inline void SetPlotTitleFontScale( wstring scale )
        {
            AppSettingsPropTree.put( L"plot.plotTitleFontScale", scale );
        }

        inline uint16_t GetExtraSettlingTimeMsAsUint16( void )
        {
            return AppSettingsPropTree.get<uint16_t>( L"extraSettlingTimeMs" );
        }
        inline const wstring GetExtraSettlingTimeMsAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"extraSettlingTimeMs" );
        }
        inline void SetExtraSettlingTimeMs( uint16_t extraSettlingTimeMs )
        {
            AppSettingsPropTree.put( L"extraSettlingTimeMs", extraSettlingTimeMs );
        }

        inline uint8_t GetAutorangeTriesPerStepAsUint8( void )
        {
            return AppSettingsPropTree.get<uint8_t>( L"autorangeTuning.autorangeTriesPerStep" );
        }
        inline const wstring GetAutorangeTriesPerStepAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"autorangeTuning.autorangeTriesPerStep" );
        }
        inline void SetAutorangeTriesPerStep( uint8_t autorangeTriesPerStep )
        {
            AppSettingsPropTree.put( L"autorangeTuning.autorangeTriesPerStep", autorangeTriesPerStep );
        }

        inline double GetAutorangeToleranceAsFraction( void )
        {
            return (AppSettingsPropTree.get<double>( L"autorangeTuning.autorangeTolerance" )) / 100.0;
        }
        inline const wstring GetAutorangeToleranceAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"autorangeTuning.autorangeTolerance" );
        }
        inline void SetAutorangeTolerance( wstring autorangeTolerance )
        {
            AppSettingsPropTree.put( L"autorangeTuning.autorangeTolerance", autorangeTolerance );
        }

        inline double GetMaxAutorangeAmplitude( void )
        {
            return AppSettingsPropTree.get<double>( L"autorangeTuning.maxAutorangeAmplitude" );
        }
        inline void SetMaxAutorangeAmplitude( double maxAutorangeAmplitude )
        {
            AppSettingsPropTree.put( L"autorangeTuning.maxAutorangeAmplitude", maxAutorangeAmplitude );
        }

        inline uint8_t GetAdaptiveStimulusTriesPerStepAsUint8( void )
        {
            return AppSettingsPropTree.get<uint8_t>( L"adaptiveStimulusTuning.adaptiveStimulusTriesPerStep" );
        }
        inline const wstring GetAdaptiveStimulusTriesPerStepAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"adaptiveStimulusTuning.adaptiveStimulusTriesPerStep" );
        }
        inline void SetAdaptiveStimulusTriesPerStep( uint8_t adaptiveStimulusTriesPerStep )
        {
            AppSettingsPropTree.put( L"adaptiveStimulusTuning.adaptiveStimulusTriesPerStep", adaptiveStimulusTriesPerStep );
        }

        inline double GetTargetResponseAmplitudeToleranceAsFraction( void )
        {
            return (AppSettingsPropTree.get<double>( L"adaptiveStimulusTuning.targetResponseAmplitudeTolerance" )) / 100.0;
        }
        inline const wstring GetTargetResponseAmplitudeToleranceAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"adaptiveStimulusTuning.targetResponseAmplitudeTolerance" );
        }
        inline void SetTargetResponseAmplitudeTolerance( wstring targetResponseAmplitudeTolerance )
        {
            AppSettingsPropTree.put( L"adaptiveStimulusTuning.targetResponseAmplitudeTolerance", targetResponseAmplitudeTolerance );
        }

        inline uint16_t GetLowNoiseCyclesCapturedAsUint16( void )
        {
            return ScopeSettingsPropTree.get<uint16_t>( L"picoScope.sampleParam.lowNoiseCyclesCaptured" );
        }
        inline const wstring GetLowNoiseCyclesCapturedAsString( void )
        {
            return ScopeSettingsPropTree.get<wstring>( L"picoScope.sampleParam.lowNoiseCyclesCaptured" );
        }
        inline void SetLowNoiseCyclesCaptured( uint16_t lowNoiseCyclesCaptured )
        {
            ScopeSettingsPropTree.put( L"picoScope.sampleParam.lowNoiseCyclesCaptured", lowNoiseCyclesCaptured );
        }

        inline uint16_t GetLowNoiseOversamplingAsUint16( void )
        {
            return ScopeSettingsPropTree.get<uint16_t>( L"picoScope.sampleParam.lowNoiseOversampling" );
        }
        inline const wstring GetLowNoiseOversamplingAsString( void )
        {
            return ScopeSettingsPropTree.get<wstring>( L"picoScope.sampleParam.lowNoiseOversampling" );
        }
        inline void SetLowNoiseOversampling( uint16_t lowNoiseOversample )
        {
            ScopeSettingsPropTree.put( L"picoScope.sampleParam.lowNoiseOversampling", lowNoiseOversample );
        }

        inline double GetNoiseRejectBandwidthAsDouble(void)
        {
            return ScopeSettingsPropTree.get<double>( L"picoScope.sampleParam.noiseRejectBandwidth" );
        }
        inline const wstring GetNoiseRejectBandwidthAsString(void)
        {
            return ScopeSettingsPropTree.get<wstring>( L"picoScope.sampleParam.noiseRejectBandwidth" );
        }
        inline void SetNoiseRejectBandwidth( wstring noiseRejectBandwidth )
        {
            ScopeSettingsPropTree.put( L"picoScope.sampleParam.noiseRejectBandwidth", noiseRejectBandwidth );
        }

        ////
        inline bool GetQualityLimitsState(void)
        {
            return AppSettingsPropTree.get<bool>( L"qualityLimits.enable" );
        }
        inline void SetQualityLimitsState( bool enable )
        {
            AppSettingsPropTree.put( L"qualityLimits.enable", enable );
        }

        inline double GetAmplitudeLowerLimitAsFraction( void )
        {
            return (AppSettingsPropTree.get<double>( L"qualityLimits.amplitudeLowerLimit" )) / 100.0;
        }
        inline const wstring GetAmplitudeLowerLimitAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"qualityLimits.amplitudeLowerLimit" );
        }
        inline void SetAmplitudeLowerLimit( wstring amplitudeLowerLimit )
        {
            AppSettingsPropTree.put( L"qualityLimits.amplitudeLowerLimit", amplitudeLowerLimit );
        }

        inline double GetPurityLowerLimitAsFraction( void )
        {
            return (AppSettingsPropTree.get<double>( L"qualityLimits.purityLowerLimit" )) / 100.0;
        }
        inline const wstring GetPurityLowerLimitAsString( void )
        {
            return AppSettingsPropTree.get<wstring>( L"qualityLimits.purityLowerLimit" );
        }
        inline void SetPurityLowerLimit( wstring purityLowerLimit )
        {
            AppSettingsPropTree.put( L"qualityLimits.purityLowerLimit", purityLowerLimit );
        }

        inline bool GetDcExcludedFromNoiseState(void)
        {
            return AppSettingsPropTree.get<bool>( L"qualityLimits.excludeDcFromNoise" );
        }
        inline void SetDcExcludedFromNoiseState( bool enable )
        {
            AppSettingsPropTree.put( L"qualityLimits.excludeDcFromNoise", enable );
        }

        inline bool GetLogVerbosityFlag(LOG_MESSAGE_FLAGS_T flag)
        {
            return (((LOG_MESSAGE_FLAGS_T)AppSettingsPropTree.get<int>( L"diagnostics.logVerbosityFlags" ) & flag) == flag);
        }
        inline void SetLogVerbosityFlag(LOG_MESSAGE_FLAGS_T flag, bool set)
        {
            LOG_MESSAGE_FLAGS_T currentFlags = (LOG_MESSAGE_FLAGS_T)AppSettingsPropTree.get<int>( L"diagnostics.logVerbosityFlags" );
            if (set)
            {
                currentFlags  = (LOG_MESSAGE_FLAGS_T)((int)currentFlags | (int)flag);
            }
            else
            {
                currentFlags  = (LOG_MESSAGE_FLAGS_T)((int)currentFlags & ~(int)flag);
            }
            AppSettingsPropTree.put( L"diagnostics.logVerbosityFlags", (int)currentFlags );
        }
        inline void SetLogVerbosityFlags(uint16_t flags)
        {
            AppSettingsPropTree.put( L"diagnostics.logVerbosityFlags", (int)flags );
        }

        inline bool GetTimeDomainPlotsEnabled( void )
        {
            return AppSettingsPropTree.get<bool>( L"diagnostics.timeDomainPlots" );
        }
        inline void SetTimeDomainPlotsEnabled( bool timeDomainPlotsEnabled )
        {
            AppSettingsPropTree.put( L"diagnostics.timeDomainPlots", timeDomainPlotsEnabled );
        }

        // Accessors/Mutators for Scope Settings
        inline uint8_t GetNumChannels(void)
        {
            return numAvailableChannels;
        }
        inline void SetNumChannels( uint8_t numChannels )
        {
            numAvailableChannels = numChannels;
        }
        inline int GetInputChannel()
        {
            return ScopeSettingsPropTree.get<wstring>(L"picoScope.inputChannel.name").c_str()[0] - wchar_t('A');
        }
        inline void SetInputChannel(int channel)
        {
            wchar_t chanLetter[2] = L"A";
            chanLetter[0] += channel;
            ScopeSettingsPropTree.put(L"picoScope.inputChannel.name", wstring(chanLetter));
        }

        inline int GetInputCoupling()
        {
            return ScopeSettingsPropTree.get<int>(L"picoScope.inputChannel.coupling");
        }
        inline void SetInputCoupling(int coupling)
        {
            ScopeSettingsPropTree.put(L"picoScope.inputChannel.coupling", coupling);
        }

        inline int GetInputAttenuation()
        {
            return ScopeSettingsPropTree.get<int>(L"picoScope.inputChannel.attenuation");
        }
        inline void SetInputAttenuation(int attenuation)
        {
            ScopeSettingsPropTree.put(L"picoScope.inputChannel.attenuation", attenuation);
        }

        inline const wstring GetInputDcOffsetAsString()
        {
            return ScopeSettingsPropTree.get<wstring>(L"picoScope.inputChannel.dcOffset");
        }
        inline double GetInputDcOffsetAsDouble()
        {
            return ScopeSettingsPropTree.get<double>(L"picoScope.inputChannel.dcOffset");
        }
        inline void SetInputDcOffset(const wchar_t* dcOffset)
        {
            ScopeSettingsPropTree.put(L"picoScope.inputChannel.dcOffset", wstring(dcOffset));
        }

        inline int GetInputStartingRange()
        {
            return ScopeSettingsPropTree.get<int>(L"picoScope.inputChannel.startingRange");
        }
        inline void SetInputStartingRange(int range)
        {
            ScopeSettingsPropTree.put(L"picoScope.inputChannel.startingRange", range);
        }

        inline int GetOutputChannel()
        {
            return ScopeSettingsPropTree.get<wstring>(L"picoScope.outputChannel.name").c_str()[0] - wchar_t('A');
        }
        inline void SetOutputChannel(int channel)
        {
            wchar_t chanLetter[2] = L"A";
            chanLetter[0] += channel;
            ScopeSettingsPropTree.put(L"picoScope.outputChannel.name", wstring(chanLetter));
        }

        inline int GetOutputCoupling()
        {
            return ScopeSettingsPropTree.get<int>(L"picoScope.outputChannel.coupling");
        }
        inline void SetOutputCoupling(int coupling)
        {
            ScopeSettingsPropTree.put(L"picoScope.outputChannel.coupling", coupling);
        }

        inline int GetOutputAttenuation()
        {
            return ScopeSettingsPropTree.get<int>(L"picoScope.outputChannel.attenuation");
        }
        inline void SetOutputAttenuation(int attenuation)
        {
            ScopeSettingsPropTree.put(L"picoScope.outputChannel.attenuation", attenuation);
        }

        inline const wstring GetOutputDcOffsetAsString()
        {
            return ScopeSettingsPropTree.get<wstring>(L"picoScope.outputChannel.dcOffset");
        }
        inline double GetOutputDcOffsetAsDouble()
        {
            return ScopeSettingsPropTree.get<double>(L"picoScope.outputChannel.dcOffset");
        }
        inline void SetOutputDcOffset(const wchar_t* dcOffset)
        {
            ScopeSettingsPropTree.put(L"picoScope.outputChannel.dcOffset", wstring(dcOffset));
        }

        inline int GetOutputStartingRange()
        {
            return ScopeSettingsPropTree.get<int>(L"picoScope.outputChannel.startingRange");
        }
        inline void SetOutputStartingRange(int range)
        {
            ScopeSettingsPropTree.put(L"picoScope.outputChannel.startingRange", range);
        }

        inline const wstring GetStimulusVppAsString()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<wstring>(L"picoScope.fraParam.stimulusVpp");
            }
            else
            {
                return EsgSettingsPropTree.get<wstring>(L"esg.fraParam.stimulusVpp");
            }
        }
        inline double GetStimulusVppAsDouble()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<double>(L"picoScope.fraParam.stimulusVpp");
            }
            else
            {
                return EsgSettingsPropTree.get<double>(L"esg.fraParam.stimulusVpp");
            }
        }
        inline void SetStimulusVpp(const wchar_t* stimulusVpp)
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                ScopeSettingsPropTree.put(L"picoScope.fraParam.stimulusVpp", wstring(stimulusVpp));
            }
            else
            {
                EsgSettingsPropTree.put(L"esg.fraParam.stimulusVpp", wstring(stimulusVpp));
            }
        }

        inline const wstring GetStimulusOffsetAsString()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<wstring>(L"picoScope.fraParam.stimulusOffset");
            }
            else
            {
                return EsgSettingsPropTree.get<wstring>(L"esg.fraParam.stimulusOffset");
            }
        }
        inline double GetStimulusOffsetAsDouble()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<double>(L"picoScope.fraParam.stimulusOffset");
            }
            else
            {
                return EsgSettingsPropTree.get<double>(L"esg.fraParam.stimulusOffset");
            }
        }
        inline void SetStimulusOffset(const wchar_t* stimulusOffset)
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                ScopeSettingsPropTree.put(L"picoScope.fraParam.stimulusOffset", wstring(stimulusOffset));
            }
            else
            {
                EsgSettingsPropTree.put(L"esg.fraParam.stimulusOffset", wstring(stimulusOffset));
            }
        }

        inline const wstring GetMaxStimulusVppAsString()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<wstring>(L"picoScope.fraParam.maxStimulusVpp");
            }
            else
            {
                return EsgSettingsPropTree.get<wstring>(L"esg.fraParam.maxStimulusVpp");
            }
        }
        inline double GetMaxStimulusVppAsDouble()
        {
            double maxStimulusVpp = 0.0;
            try // Because some callers may call with a blank field
            {
                if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
                {
                    maxStimulusVpp = ScopeSettingsPropTree.get<double>(L"picoScope.fraParam.maxStimulusVpp");
                }
                else
                {
                    maxStimulusVpp = EsgSettingsPropTree.get<double>(L"esg.fraParam.maxStimulusVpp");
                }
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                maxStimulusVpp = 0.0;
            }
            return maxStimulusVpp;
        }
        inline void SetMaxStimulusVpp(const wchar_t* maxStimulusVpp)
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                ScopeSettingsPropTree.put(L"picoScope.fraParam.maxStimulusVpp", wstring(maxStimulusVpp));
            }
            else
            {
                EsgSettingsPropTree.put(L"esg.fraParam.maxStimulusVpp", wstring(maxStimulusVpp));
            }
        }

        inline const wstring GetStartFreqAsString()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<wstring>(L"picoScope.fraParam.startFrequency");
            }
            else
            {
                return EsgSettingsPropTree.get<wstring>(L"esg.fraParam.startFrequency");
            }
        }
        inline double GetStartFreqAsDouble()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<double>(L"picoScope.fraParam.startFrequency");
            }
            else
            {
                return EsgSettingsPropTree.get<double>(L"esg.fraParam.startFrequency");
            }
        }
        inline void SetStartFrequency(const wchar_t* startFreq)
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                ScopeSettingsPropTree.put(L"picoScope.fraParam.startFrequency", wstring(startFreq));
            }
            else
            {
                EsgSettingsPropTree.put(L"esg.fraParam.startFrequency", wstring(startFreq));
            }
        }

        inline const wstring GetStopFreqAsString()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<wstring>(L"picoScope.fraParam.stopFrequency");
            }
            else
            {
                return EsgSettingsPropTree.get<wstring>(L"esg.fraParam.stopFrequency");
            }
        }
        inline double GetStopFreqAsDouble()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<double>(L"picoScope.fraParam.stopFrequency");
            }
            else
            {
                return EsgSettingsPropTree.get<double>(L"esg.fraParam.stopFrequency");
            }
        }
        inline void SetStopFrequency(const wchar_t* stopFreq)
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                ScopeSettingsPropTree.put(L"picoScope.fraParam.stopFrequency", wstring(stopFreq));
            }
            else
            {
                EsgSettingsPropTree.put(L"esg.fraParam.stopFrequency", wstring(stopFreq));
            }
        }

        inline const wstring GetStepsPerDecadeAsString()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<wstring>(L"picoScope.fraParam.stepsPerDecade");
            }
            else
            {
                return EsgSettingsPropTree.get<wstring>(L"esg.fraParam.stepsPerDecade");
            }
        }
        inline int GetStepsPerDecadeAsInt()
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                return ScopeSettingsPropTree.get<int>(L"picoScope.fraParam.stepsPerDecade");
            }
            else
            {
                return EsgSettingsPropTree.get<int>(L"esg.fraParam.stepsPerDecade");
            }
        }
        inline void SetStepsPerDecade(const wchar_t* stepsPerDecade)
        {
            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                ScopeSettingsPropTree.put(L"picoScope.fraParam.stepsPerDecade", wstring(stepsPerDecade));
            }
            else
            {
                EsgSettingsPropTree.put(L"esg.fraParam.stepsPerDecade", wstring(stepsPerDecade));
            }
        }

        inline void SetCustomPlanEnabled(bool enabled)
        {
            wptree* pPtree = NULL;
            std::wstring customPlanEnabledNode;

            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                pPtree = &ScopeSettingsPropTree;
                customPlanEnabledNode = L"picoScope.customPlan";
            }
            else
            {
                pPtree = &EsgSettingsPropTree;
                customPlanEnabledNode = L"esg.customPlan";
            }

            pPtree -> put(customPlanEnabledNode, enabled);
        }

        inline bool GetCustomPlanEnabled(void)
        {
            bool retVal = true;
            wptree* pPtree = NULL;
            std::wstring customPlanEnabledNode;

            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                pPtree = &ScopeSettingsPropTree;
                customPlanEnabledNode = L"picoScope.customPlan";
            }
            else
            {
                pPtree = &EsgSettingsPropTree;
                customPlanEnabledNode = L"esg.customPlan";
            }

            try
            {
                retVal = (bool)pPtree -> get<bool>(customPlanEnabledNode);
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                retVal = false;
            }

            return retVal;
        }

        inline bool GetCustomPlan( std::vector<FRASegment_T>& fraSegments )
        {
            bool retVal = true;
            wptree* pPtree = NULL;
            std::wstring formatStringParamNode;
            std::wstring customPlanEnabledNode;
            std::wstring customPlanCountNode;
            std::wstring customPlanStepTypeNode;

            wchar_t paramNode[128];
            int segmentCount = 0;
            StepType_T stepType;

            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                pPtree = &ScopeSettingsPropTree;
                customPlanEnabledNode = L"picoScope.customPlan";
                customPlanCountNode = L"picoScope.customPlan.count";
                customPlanStepTypeNode = L"picoScope.customPlan.stepType";
                formatStringParamNode = L"picoScope.customPlan.fraParam%d.%s";
            }
            else
            {
                pPtree = &EsgSettingsPropTree;
                customPlanEnabledNode = L"esg.customPlan";
                customPlanCountNode = L"esg.customPlan.count";
                customPlanStepTypeNode = L"esg.customPlan.stepType";
                formatStringParamNode = L"esg.customPlan.fraParam%d.%s";
            }

            try
            {
                retVal = (bool)pPtree -> get<bool>(customPlanEnabledNode);
                segmentCount = (int)pPtree -> get<int>(customPlanCountNode);
                stepType = (StepType_T)pPtree -> get<int>(customPlanStepTypeNode);

                for (int i = 0; i < segmentCount; i++)
                {
                    FRASegment_T fraSegment;

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"stimulusVpp");
                    fraSegment.stimulusVpp = (double)(pPtree -> get<double>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"stimulusOffset");
                    fraSegment.stimulusOffset = (double)(pPtree -> get<double>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"startFrequency");
                    fraSegment.startFreqHz = (double)(pPtree -> get<double>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"stopFrequency");
                    fraSegment.stopFreqHz = (double)(pPtree -> get<double>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"steps");
                    fraSegment.steps = (int)(pPtree -> get<int>( paramNode ));

                    fraSegment.stepType = stepType;

                    fraSegments.push_back(fraSegment);
                }
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                retVal = false;
            }

            return retVal;
        }

        inline bool GetCustomPlanAsString( std::vector<FRASegmentStrings_T>& fraSegments, StepType_T& stepType )
        {
            bool retVal = true;
            wptree* pPtree = NULL;
            std::wstring formatStringParamNode;
            std::wstring customPlanEnabledNode;
            std::wstring customPlanCountNode;
            std::wstring customPlanStepTypeNode;

            wchar_t paramNode[128];
            int segmentCount = 0;

            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                pPtree = &ScopeSettingsPropTree;
                customPlanEnabledNode = L"picoScope.customPlan";
                customPlanCountNode = L"picoScope.customPlan.count";
                customPlanStepTypeNode = L"picoScope.customPlan.stepType";
                formatStringParamNode = L"picoScope.customPlan.fraParam%d.%s";
            }
            else
            {
                pPtree = &EsgSettingsPropTree;
                customPlanEnabledNode = L"esg.customPlan";
                customPlanCountNode = L"esg.customPlan.count";
                customPlanStepTypeNode = L"esg.customPlan.stepType";
                formatStringParamNode = L"esg.customPlan.fraParam%d.%s";
            }

            try
            {
                retVal = (bool)pPtree -> get<bool>(customPlanEnabledNode);
                segmentCount = (int)pPtree -> get<int>(customPlanCountNode);
                stepType = (StepType_T)pPtree -> get<int>(customPlanStepTypeNode);

                for (int i = 0; i < segmentCount; i++)
                {
                    FRASegmentStrings_T fraSegment;

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"stimulusVpp");
                    fraSegment.stimulusVpp = (std::wstring)(pPtree -> get<std::wstring>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"stimulusOffset");
                    fraSegment.stimulusOffset = (std::wstring)(pPtree -> get<std::wstring>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"startFrequency");
                    fraSegment.startFreqHz = (std::wstring)(pPtree -> get<std::wstring>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"stopFrequency");
                    fraSegment.stopFreqHz = (std::wstring)(pPtree -> get<std::wstring>( paramNode ));

                    wsprintf( paramNode, formatStringParamNode.c_str(), i, L"steps");
                    fraSegment.steps = (std::wstring)(pPtree -> get<std::wstring>( paramNode ));

                    fraSegments.push_back(fraSegment);
                }
            }
            catch ( const ptree_error& pte )
            {
                UNREFERENCED_PARAMETER(pte);
                retVal = false;
            }

            return retVal;
        }

        inline void SetCustomPlan(std::vector<FRASegmentStrings_T>& fraSegments, StepType_T stepType)
        {
            wptree* pPtree = NULL;
            std::wstring formatStringParamNode;
            std::wstring customPlanCountNode;
            std::wstring customPlanStepTypeNode;
            wchar_t paramNode[128];

            if (SIG_GEN_BUILT_IN == GetSignalGeneratorSource())
            {
                pPtree = &ScopeSettingsPropTree;
                customPlanCountNode = L"picoScope.customPlan.count";
                customPlanStepTypeNode = L"picoScope.customPlan.stepType";
                formatStringParamNode = L"picoScope.customPlan.fraParam%d.%s";
            }
            else
            {
                pPtree = &EsgSettingsPropTree;
                customPlanCountNode = L"esg.customPlan.count";
                customPlanStepTypeNode = L"esg.customPlan.stepType";
                formatStringParamNode = L"esg.customPlan.fraParam%d.%s";
            }

            pPtree -> put(customPlanCountNode, fraSegments.size());
            pPtree -> put(customPlanStepTypeNode, stepType);

            for (auto const& fraSegment : fraSegments | indexed(0))
            {
                wsprintf( paramNode, formatStringParamNode.c_str(), fraSegment.index(), L"stimulusVpp");
                pPtree -> put( paramNode, fraSegment.value().stimulusVpp );

                wsprintf( paramNode, formatStringParamNode.c_str(), fraSegment.index(), L"stimulusOffset");
                pPtree -> put( paramNode, fraSegment.value().stimulusOffset );

                wsprintf( paramNode, formatStringParamNode.c_str(), fraSegment.index(), L"startFrequency");
                pPtree -> put( paramNode, fraSegment.value().startFreqHz );

                wsprintf( paramNode, formatStringParamNode.c_str(), fraSegment.index(), L"stopFrequency");
                pPtree -> put( paramNode, fraSegment.value().stopFreqHz );

                wsprintf( paramNode, formatStringParamNode.c_str(), fraSegment.index(), L"steps");
                pPtree -> put( paramNode, fraSegment.value().steps );
            }
        }

        inline int GetNoiseRejectModeTimebaseAsInt()
        {
            return ScopeSettingsPropTree.get<int>(L"picoScope.sampleParam.noiseRejectModeTimebase");
        }
        inline const wstring GetNoiseRejectModeTimebaseAsString()
        {
            return ScopeSettingsPropTree.get<wstring>(L"picoScope.sampleParam.noiseRejectModeTimebase");
        }
        inline void SetNoiseRejectModeTimebase(int timebase)
        {
            ScopeSettingsPropTree.put(L"picoScope.sampleParam.noiseRejectModeTimebase", timebase);
        }

        inline RESOLUTION_T GetDeviceResolution()
        {
            return (RESOLUTION_T)ScopeSettingsPropTree.get<int>(L"picoScope.sampleParam.deviceResolution");
        }
        inline void SetDeviceResolution(RESOLUTION_T res)
        {
            ScopeSettingsPropTree.put(L"picoScope.sampleParam.deviceResolution", res );
        }

        inline double GetSimulatedLpfCornerFrequency()
        {
            return (double)ScopeSettingsPropTree.get<double>( L"picoScope.fraParam.simulatedLpfCornerFrequency" );
        }

        // Accessors/Mutators for External Signal Generator Settings
        inline const wstring GetEsgSettings()
        {
            return EsgSettingsPropTree.get<wstring>(L"esg.settings");
        }
        inline void SetEsgSettings(const wchar_t* settings)
        {
            EsgSettingsPropTree.put(L"esg.settings", wstring(settings));
        }

        inline bool GetLogAutoClear( void )
        {
            return (bool)AppSettingsPropTree.get<bool>( L"logAutoClear" );
        }
        inline void SetLogAutoClear( bool enable )
        {
            AppSettingsPropTree.put( L"logAutoClear", enable );
        }

        inline bool GetShowPreRunWarnings( void )
        {
            return (bool)AppSettingsPropTree.get<bool>( L"preRunAlerts.showWarnings" );
        }
        inline void SetShowPreRunWarnings( bool enable )
        {
            AppSettingsPropTree.put( L"preRunAlerts.showWarnings", enable );
        }

    private:

        typedef enum
        {
            APPLICATION_SETTINGS,
            SCOPE_SETTINGS,
            ESG_SETTINGS
        } Settings_T;

        bool InitializeEsgSettingsFile( void );
        bool InitializeScopeSettingsFile( PicoScope* pScope );
        bool InitializeApplicationSettingsFile( void );
        void CheckSettingsVersionAndUpgrade( Settings_T type, PicoScope* pScope = NULL );

        wstring appDataFolder;
        wstring appDataFilename;
        wstring scopeDataFilename;
        wstring esgDataFilename;

        wptree AppSettingsPropTree;
        wptree AppSettingsPropTreeClean;
        wptree ScopeSettingsPropTree;
        wptree ScopeSettingsPropTreeClean;
        wptree EsgSettingsPropTree;
        wptree EsgSettingsPropTreeClean;

        wstring scopeSN;
        wstring esgID;

        uint8_t numAvailableChannels;

};

