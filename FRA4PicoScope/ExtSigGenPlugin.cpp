//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2020 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ExtSigGenPlugin.cpp: Contains functions to manage DLL plugins for signal generators not
//                              built into a PicoScope.  This is intended to be a singleton class
//                              that will act as a factory to create ExtSigGen objects.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <Windows.h>
#include <iostream>
#include <sstream>
#include "ExtSigGenPlugin.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::ExtSigGenPlugin
//
// Purpose: Simple constructor
//
// Parameters: N/A
//
// Notes: Initializes plugin name, module handle and module processes
//
////////////////////////////////////////////////////////////////////////////////////////////////////

ExtSigGenPlugin::ExtSigGenPlugin()
{
    pCurrentEsg = NULL;
    loadedPluginName = L"";
    hLoadedModule = NULL;
    ProcOpenExtSigGen = NULL;
    ProcCloseExtSigGen = NULL;
    ProcEnumerateExtSigGen = NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::~ExtSigGenPlugin
//
// Purpose: Simple destructor
//
// Parameters: N/A
//
// Notes: Unloads the currently loaded module in case the client forgot to
//
////////////////////////////////////////////////////////////////////////////////////////////////////

ExtSigGenPlugin::~ExtSigGenPlugin()
{
    UnloadExtSigGenPlugin();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::LoadExtSigGenPlugin
//
// Purpose: Loads the DLL module for the external signal generator plugin
//
// Parameters: pluginName: The filename of the DLL file to load.  Must be in the DLL load path
//             return: success
//
// Notes: Will only support one module loaded at a time.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ExtSigGenPlugin::LoadExtSigGenPlugin( const std::wstring& pluginName )
{
    bool retval = false;

    if ( loadedPluginName != pluginName )
    {
        // Unload the currently loaded module since we only support one at at time
        if (loadedPluginName != L"")
        {
            UnloadExtSigGenPlugin();
        }
        // Try to load the plugin library
        HMODULE hModule = LoadLibraryW( pluginName.c_str() );
        if ( hModule != NULL )
        {
            ProcOpenExtSigGen = (fnOpenExtSigGen)GetProcAddress( hModule, "OpenExtSigGen" );
            ProcCloseExtSigGen = (fnCloseExtSigGen)GetProcAddress( hModule, "CloseExtSigGen" );
            ProcEnumerateExtSigGen = (fnEnumerateExtSigGen)GetProcAddress( hModule, "EnumerateExtSigGen" );
            if ( ProcOpenExtSigGen != NULL && ProcCloseExtSigGen != NULL &&  ProcEnumerateExtSigGen != NULL)
            {
                loadedPluginName = pluginName;
                hLoadedModule = hModule;
                retval = true;
            }
            else
            {
                FreeLibrary(hModule);
            }
        }
    }
    else
    {
        retval = true;
    }

    return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::EnumerateExtSigGen
//
// Purpose: Enumerates the signal generator instruments available through the plugin
//
// Parameters: ids: An out parameter containing a comma separated list of instrument ids
//             return: whether the plugin supports enumeration
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ExtSigGenPlugin::EnumerateExtSigGen( std::vector<std::wstring>& ids )
{
    bool bSupports = false;
    wchar_t _ids[1024];
    uint16_t count;
    uint16_t lth = sizeof(_ids);

    ids.clear();

    if (ProcEnumerateExtSigGen)
    {
        bSupports = ProcEnumerateExtSigGen(&count, _ids, &lth);

        if (bSupports)
        {
            std::wstringstream ssIds(_ids);
            while (ssIds.good())
            {
                std::wstring substr;
                std::getline(ssIds, substr, L','); //get first string delimited by comma
                ids.push_back(substr);
            }
        }
    }

    return bSupports;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::OpenExtSigGen
//
// Purpose: This is the factory method to create an ExtSigGen object
//
// Parameters: id: The id of the instrument to open.  Use blank if enumeration or ids are not
//                 supported.
//             return: Pointer to the plugin object; NULL if failed.
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

ExtSigGen* ExtSigGenPlugin::OpenExtSigGen( const std::wstring id )
{
    ExtSigGen* pEsg = NULL;

    if ( loadedPluginName != L"" )
    {
        if ( hLoadedModule != NULL )
        {
            if ( ProcOpenExtSigGen != NULL )
            {
                if (pCurrentEsg)
                {
                    CloseExtSigGen();
                }
                // Invoke the function to get the plugin from the DLL.
                pEsg = ProcOpenExtSigGen(id.c_str());
            }
        }
    }

    pCurrentEsg = pEsg;

    return pCurrentEsg;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::CloseExtSigGen
//
// Purpose: Close (return) the ExtSigGen object
//
// Parameters: N/A
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void ExtSigGenPlugin::CloseExtSigGen( void )
{
    if (ProcCloseExtSigGen != NULL)
    {
        ProcCloseExtSigGen(pCurrentEsg);
    }
    pCurrentEsg = NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::GetCurrentExtSigGen
//
// Purpose: Get the current ExtSigGen object
//
// Parameters: return - current ExtSigGen object
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

ExtSigGen* ExtSigGenPlugin::GetCurrentExtSigGen(void)
{
    return pCurrentEsg;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExtSigGenPlugin::UnloadExtSigGenPlugin
//
// Purpose: Unload the currently loaded plugin module
//
// Parameters: N/A
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void ExtSigGenPlugin::UnloadExtSigGenPlugin( void )
{
    if ( hLoadedModule != NULL)
    {
        CloseExtSigGen();
        FreeLibrary(hLoadedModule);
        loadedPluginName = L"";
        hLoadedModule = NULL;
        ProcOpenExtSigGen = NULL;
        ProcCloseExtSigGen = NULL;
        ProcEnumerateExtSigGen = NULL;
    }
}